// Author Laurent Brunel

package com.photonlyx.toolbox.util;

import java.util.*;
import java.awt.*;
import javax.swing.*;


public class Messager
{
	static ResourceBundle messages;

	// Vecteur de ResourceBundles 
	//
	static Vector<ResourceBundle> customResourceBundles = new Vector<ResourceBundle>();

	static Component PARENT;
	static boolean verbose=true;

	static Locale currentLocale;

	public Messager(Component PARENT,String language,String country)
	{
		currentLocale = new Locale(language, country);
		messages = ResourceBundle.getBundle("MessageBundle",currentLocale);

		System.out.println(getString("Langue")+" "+currentLocale.getDisplayLanguage());
		System.out.println(getString("Pays")+" "+currentLocale.getDisplayCountry());

		//System.out.println(getString("new_message_bundle"));

		//logFrame=new TextEditor("Log Frame\n");
	}

	public static void init(String language,String country)
	{
		init(language,country,true);
	}
	
	public static void init(String language,String country,boolean verbose)
	{
		currentLocale = new Locale(language, country);
		try 
		{
			messages = ResourceBundle.getBundle("MessageBundle",currentLocale);
		}
		catch(Exception e)
		{
			Messager.messErr("Can't find ResourceBundle for "+language+" "+country);	
		}
		if (verbose)
		{
			System.out.println(getString("Langue")+" "+currentLocale.getDisplayLanguage());
			System.out.println(getString("Pays")+" "+currentLocale.getDisplayCountry());
		}
	}



	public static void changeLocale(String language,String country)
	{
		currentLocale = new Locale(language, country);
		messages = ResourceBundle.getBundle("MessageBundle",currentLocale);
		System.out.println(getString("Langue")+" "+currentLocale.getDisplayLanguage());
		System.out.println(getString("Pays")+" "+currentLocale.getDisplayCountry());
	}

	/**
log true for a log window
	 */
	public Messager(Component PARENT,String language,String country,boolean log)
	{
		currentLocale = new Locale(language, country);
		messages = ResourceBundle.getBundle("MessageBundle",currentLocale);
		System.out.println(getString("Langue")+" "+currentLocale.getDisplayLanguage()); 
		System.out.println(getString("Pays")+" "+currentLocale.getDisplayCountry());  
		//System.out.println(getString("new_message_bundle"));  
	}

	/** 
	 * Ajoute un bundle la liste des bundles disponibles
	 * 
	 * @param bundleName nom du bundle à ajouter 
	 */
	public static void addCustomBundle(String bundleName)
	{
		System.out.println ("Ajout du bundle :" + bundleName + ".");

		ResourceBundle newBundle = ResourceBundle.getBundle(bundleName,currentLocale);

		customResourceBundles.addElement(newBundle);
	}

	public static String getString(String s)
	{
		String mes = null;
		try 
		{
			// On cherche d'abord dans les "custom" bundles
			//
			Iterator i = customResourceBundles.iterator();

			while (i.hasNext())
			{
				ResourceBundle currentRB = (ResourceBundle)i.next();

				try
				{
					mes = currentRB.getString(s);
				}
				catch (Exception e) 
				{
					mes = null;
				}

				if (mes != null)
				{
					return mes;
				}
			}

			// Si on n'a pas trouve on cherche dans le bundle par defaut de jolo
			//
			mes=messages.getString(s);
			return mes;
		}
		catch (Exception e) 
		{
			return s;
		}
	}

	/**
	 * If PARENT is not set to null then returns PARENT.
	 * If PARENT is null, returns the focused window.
	 *
	 * This function is usefull for the message boxes, because
	 * their parent component must be the focused window especialy
	 * when the focused window is modal
	 *
	 * */
	public static Component getMessageParent()
	{
		Component owner = PARENT;

		if (PARENT == null)
		{
			KeyboardFocusManager focusManager =
					KeyboardFocusManager.getCurrentKeyboardFocusManager();

			owner = focusManager.getFocusedWindow();
		}

		return owner;
	}

	/** utility for addressing a message to the user (error like). 
The standard output is used and a window is poped up. 
The program is stopped until the user kill the window*/
	public static void messErr(String s)
	{
		System.err.println(s);
		//Ferreur f=new Ferreur(PARENT,s);

		JOptionPane.showMessageDialog(getMessageParent(),s,getString("Messager_error_dialog_title"),JOptionPane.WARNING_MESSAGE );
		//f.dispose();
	}

	/**
	 * utility for addressing a message to the user (error like). 
The standard output is used and a window is poped up. 
The program is stopped until the user kill the window
  @param title title of the window poped up
 @param s the message
	 */
	public static void messErr(String title,String s)
	{
		System.err.println(s);
		//Ferreur f=new Ferreur(PARENT,s);

		JOptionPane.showMessageDialog(getMessageParent(),s,title,JOptionPane.WARNING_MESSAGE );
		//f.dispose();
	}

	/** utility for addressing a message to the user (friendly like message). 
The standard output is used and a window is popped up.
The program is stopped until the user kill the window*/
	public static void mess(String s)
	{
		System.out.println(s);
		JOptionPane.showMessageDialog(getMessageParent(),s,getString("info"),JOptionPane.INFORMATION_MESSAGE  );
		//Fmessage f=new Fmessage(PARENT,s);
		//f.dispose();
	}

	/** utility for addressing a message to the user (friendly like message). 
The standard output is used and a window is popped up.
The program is stopped until the user kill the window*/
	public static void mess(String s,String title)
	{
		System.out.println(s);
		JOptionPane.showMessageDialog(getMessageParent(),s,title,JOptionPane.INFORMATION_MESSAGE  );
		//Fmessage f=new Fmessage(PARENT,s);
		//f.dispose();
	}

	/** 
	 * Displays a yes/no/cancel dialog box 
	 * 
	 * @param question Question asked to the user
	 * @param title Title of the dialog
	 * @return One of the strings "yes","no" or "cancel"
	 */
	public static String yesNoCancelQuestion(String question, String title)
	{
		int answer = JOptionPane.showConfirmDialog(
				getMessageParent(),
				question,
				title,
				JOptionPane.YES_NO_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE  );

		switch (answer)
		{
		case JOptionPane.YES_OPTION:
			return "yes";
		case JOptionPane.NO_OPTION:
			return "no";
		default:
			return "cancel";
		}
	}

	/** 
	 * Displays a yes/no dialog box 
	 * 
	 * @param question Question asked to the user
	 * @param title Title of the dialog
	 * @return One of the strings "yes" or "no"
	 */
	public static String okCancelQuestion(Object question, String title)
	{
		int answer = JOptionPane.showConfirmDialog(
				getMessageParent(),
				question,
				title,
				JOptionPane.OK_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE  );

		if (answer == JOptionPane.OK_OPTION) return "ok";
		else if (answer == JOptionPane.CLOSED_OPTION) return "closed";
		else return "cancel";
	}

	/** 
	 * Displays a yes/no dialog box 
	 * 
	 * @param question Question asked to the user
	 * @param title Title of the dialog
	 * @return One of the strings "yes" or "no"
	 */
	public static String yesNoQuestion(Object question, String title)
	{
		int answer = JOptionPane.showConfirmDialog(
				getMessageParent(),
				question,
				title,
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE  );

		if (answer == JOptionPane.YES_OPTION) return "yes";
		else if (answer == JOptionPane.CLOSED_OPTION) return "closed";
		else return "no";
	}

	/** 
	 * Displays a yes/no dialog with a warning symbol 
	 * 
	 * @param question Question asked to the user
	 * @param title Title of the dialog
	 * @return One of the strings "yes" or "no" or "closed"
	 */
	public static String yesNoWarningQuestion(Object question, String title)
	{
		int answer = JOptionPane.showConfirmDialog(
				getMessageParent(),
				question,
				title,
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE  );

		if (answer == JOptionPane.YES_OPTION) return "yes";
		else if (answer == JOptionPane.CLOSED_OPTION) return "closed";
		else return "no";
	}


	public static Component getParentComponent ()
	{
		return PARENT;
	}

	public static void setParentComponent (Component parent)
	{
		PARENT = parent;
	}


}
