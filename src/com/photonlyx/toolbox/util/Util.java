package com.photonlyx.toolbox.util;

import java.lang.reflect.*;
import java.awt.*;

import javax.swing.*;

import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSorter;

import java.net.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.io.*;
import java.util.*;
import java.awt.image.*;


import java.util.jar.*;
import java.util.zip.*;


/**

tools

 */
public class Util
{
	public static Vector<Character> spacers= new Vector<Character>(); 

	//	if ((ch1=='\t')||(ch1==' ')||(ch1==',')||(ch1=='\n')||(ch1=='\r')) 
	static
	{
		spacers.add('\t');
		spacers.add(' ');
		spacers.add(',');
		spacers.add('\n');
		spacers.add('\r');
	}


	/**get an object with constructor defined by a Definition , the class name must be the word
of index 1 (second word) of the definition which name is defname*/
	public static Object getObject(Definition def)
	{
		Object object;
		Class classe;
		if (def==null) return null;
		String className=def.word(1);
		if (className==null) return null;
		try
		{
			classe=Class.forName(className);
		}
		catch (Exception e)
		{
			Messager.messErr(Messager.getString("Can_t_find_class")+" :"
					+className+"\n"+e+"\n");
			e.printStackTrace();
			return null;
		}
		try
		{
			Class[] parameterTypes=new Class[1];
			parameterTypes[0]=Class.forName("lexique.Definition");
			Constructor constructeur=classe.getConstructor(parameterTypes);
			Object[] initargs=new Object[1];
			initargs[0]=def;
			System.out.println("Je construis: "+def.nom_definition());
			object= constructeur.newInstance(initargs);
		}
		catch (Exception e)
		{
			Messager.messErr(Messager.getString("ERREUR")+" (getObject) "+" :"
					+className+"\n"+e+"\n");
			return null;
		}
		return object;
	}


	public static Object getObject(String className)
	{
		return 	getObject( className, true);
	}
	public static Object getObject(String className,boolean alert)
	{
		Class classe;
		Object object;
		try {classe=Class.forName(className);}
		catch (Exception e1)
		{
			if (alert) 
			{
				Messager.messErr(Messager.getString("Can_t_find_class")+" :"+className+"\n"+e1+"\n");
				e1.printStackTrace();
			}
			return null;
		}

		try
		{
			object=classe.newInstance();
		}
		catch (Exception e2)
		{
			if (alert)
			{
				Messager.messErr(Messager.getString("ERREUR")+" (Util.getObject) "+" :"+className+"\n"+e2+"\n");
				e2.printStackTrace();
			}
			return null;
		}
		return object;
	}

	/** return one sentence information about a  class */
	public static String getInfo(String className)
	{
		return Messager.getString(className+"_info");
	}


	/**
get the icon of this class
	 */
	public static Icon getJoloIcon(String className)
	{
		//System.out.println(getClass()+" try to find  "+"icons/"+def.word(1)+".jpg.......");
		ClassLoader cl=new Util().getClass().getClassLoader();
		URL url=cl.getResource("icons/"+className+".png");
		if (url==null) url=cl.getResource("icons/"+className+".jpg");
		//System.out.println(getClass()+" "+url);
		if (url==null) url=cl.getResource("icons/"+"default"+".png");
		if (url==null) return null; else return new ImageIcon(url);
	}


	/**
get the icon of this class
	 */
	public static Icon getIcon(String imagefilename)
	{
		//System.out.println(getClass()+" try to find  "+"icons/"+def.word(1)+".jpg.......");
		ClassLoader cl=new Util().getClass().getClassLoader();
		//URL url=cl.getResource("icons/"+imagefilename);
		URL url=cl.getResource(imagefilename);
		if (url==null) 
		{
			url=cl.getResource(imagefilename);
			if (url==null) 
			{
				url=cl.getResource("icons/"+"default.png");
			}
		}
		return new ImageIcon(url);
	}

	/**
return an image stored in a file (jpg or gif) in Global.chemin
	 */
	public static Image getImage(String imagefilename)
	{
		return getImage(imagefilename, true);
	}

	/**
return an image stored in a file (jpg or gif) in resources or Global.chemin
@param check with error msg if image not found.
	 */
	public static Image getImage(String imagefilename, boolean check)
	{
		Image img=null;

		ClassLoader cl=new Util().getClass().getClassLoader();
		URL url=cl.getResource(imagefilename);
		//System.err.println("getImage------------------------------------------"+imagefilename);
		if (url!=null) 
		{
			try {img =  Toolkit.getDefaultToolkit().createImage((ImageProducer)url.getContent());}
			catch(Exception e) 
			{
				e.printStackTrace();
			}
			// Wait for the image to be loaded
			MediaTracker mt = new MediaTracker (new Canvas());

			mt.addImage (img, 0);
			try {
				mt.waitForID (0);
			}
			catch (InterruptedException e) {}
		} 
		else 
		{
			System.err.println("No resource for "+imagefilename+" check in the path...");
			img=getImageAtAnyPath(imagefilename, check);
		}

		if (check) 
			if (img==null) 
				Messager.messErr("Util.getImage: "+"pas d'url pour "+imagefilename);

		return img;
	}


	/**
return an image stored in a file (jpg or gif)
@param check with error msg if image not found.
	 */
	public static Image getImageAtAnyPath(String fullimagefilename, boolean check)
	{
		Image img = null;
		try
		{
			img = Toolkit.getDefaultToolkit().getImage(fullimagefilename);

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		// Wait for the image to be loaded
		MediaTracker mt = new MediaTracker (new Canvas());

		mt.addImage (img, 0);
		try {
			mt.waitForID (0);
		}
		catch (InterruptedException e) {}

		if (img.getWidth(new JPanel()) == -1 ) 
		{
			if (check) Messager.messErr("Util.getImage: "+"pas d'url pour "+fullimagefilename);
			return null;
		}

		return img;
	}

	/**return an image stored in a file (jpg or gif) in the same package the object*/
	public static Image getImage(String imagefilename,Object object)
	{
		ClassLoader cl=object.getClass().getClassLoader();
		URL url=cl.getResource(imagefilename);
		if (url==null)
		{
			Messager.messErr("Util.getImage: "+"pas d'url pour "+imagefilename);
			return null;
		}
		//System.out.println("load resource :"+url);
		Image img = null;
		try {img =  Toolkit.getDefaultToolkit().createImage((ImageProducer)url.getContent());}
		catch(Exception e) {}
		return img;
	}



	/**
return the list of  directory names (sorted in alphabetic order) at the specified absolute path
	 */
	public static String[] getDirectories(String path)
	{
		//System.out.println(getClass()+" "+getNode().getName()+" path="+path);
		File f=new File(path);
		String[] listprov=f.list();
		if (listprov==null) return null;
		//System.out.println(getClass()+" "+getNode().getName()+" found "+listprov.length+" sub dirs");

		Vector v=new Vector();
		for (int i=0;i<listprov.length;i++)
		{
			File ff=new File(path+listprov[i]);
			if ((ff.isDirectory()) &&!(listprov[i].startsWith(".")) )
			{
				//System.out.println(getClass()+" "+getNode().getName()+" found subdir="+listprov[i]);
				v.addElement(listprov[i]);
			}
		}
		v.addElement(".");
		//nouvelle liste avec les directoires
		String[] listdir=new String[v.size()];
		v.copyInto(listdir);
		//for (int i=0;i<listdir.length;i++) System.out.println(getClass()+" "+getNode().getName()+" subdir number "+i+": "+listdir[i]);
		StringSorter.sort(listdir);
		return listdir;
	}


	public static double log10(double r){return Math.log(r)/Math.log(10);}
	public static double p10(double r){return Math.pow(10,r);}



	/**
remove c++ comments
	 */
	public static StringBuffer eliminateCommentsInString(String s)
	{
		StringBuffer buffer = new StringBuffer();

		try
		{
			StringReader sr = new StringReader(s);
			Reader in = new BufferedReader(sr);
			int ch1=0,ch2=0;

			ch1=ch2;ch2= in.read();
			while (ch2 > -1)
			{
				ch1=ch2;
				ch2= in.read();
				//System.out.print((char)ch2);
				if ((ch1=='/')&&(ch2=='*'))
				{
					//System.out.println("on voit un /*");
					while (!( (ch1=='*')&&(ch2=='/') ))
					{
						ch1=ch2;ch2= in.read();//System.out.print((char)ch2);
					}
					ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
					ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
				}
				if ((ch1=='/')&&(ch2=='/'))
				{
					//System.out.println("on voit un //");
					while ( !(ch2=='\n') )
					{
						ch1=ch2;ch2= in.read();//System.out.print((char)ch2);
					}
					ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
					//ch1=ch2;ch2= in.read();	//System.out.print((char)ch2);
				}
				buffer.append((char)ch1);
			}
			in.close();
			sr.close();
			//buffer.deleteCharAt(0);
			//System.out.println("C++ Comments removed");
			return buffer;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}

	}




	/**read sentences separated by semicolon ; in a StringBuffer*/
	public static String[] readStrings(StringBuffer sb)
	{
		Vector list=new Vector();
		StringBuffer buffer = new StringBuffer();
		char ch;
		int index=0;
		ch = sb.charAt(index++);
		while (index != sb.length())
		{
			while ( (ch!=';')&&(index != sb.length()) )
			{
				//System.out.print(ch);
				buffer.append(ch);
				ch = sb.charAt(index++);
			}
			//System.out.println(buffer.toString());
			if(index != sb.length()) list.addElement(buffer.toString());
			buffer= new StringBuffer();
			if(index != sb.length()) ch =sb.charAt(index++);
			//remove end of lines
			while ( (ch=='\n')&&(index != sb.length()) )  ch = sb.charAt(index++);
		}
		String[] strings=new String[list.size()];
		//for (int i=0;i<strings.length;i++) strings[i]=(String)list.elementAt(i);
		list.copyInto(strings);
		//System.out.println(strings.length+" sentences read");
		return strings;
	}




	public static Vector<String> readWordsInString(String s)
	{
		return readWordsInString(s,Util.spacers);
	}

	/**
read the words separated by space, comma  or tab
	 */
	public static Vector<String> readWordsInString(String s,Vector<Character> spacers)
	{
		StringBuffer buffer = new StringBuffer();
		Vector<String> v=new Vector<String>();
		try
		{
			StringReader sr = new StringReader(s);

			EscapedStringReader esr = new EscapedStringReader(sr);

			int ch1=0;
			StringBuffer word=new StringBuffer();



			for(;;)
			{
				// 	System.out.println((char)ch1);
				for (;;)//read the blanks
				{
					ch1= esr.getNextChar();
					if (!((ch1=='\t')||(ch1==' ')||(ch1==',')||(ch1=='\n')||(ch1=='\r')))  //no blank appears
					{
						if (!(esr.wordDelimiterFound())) word.append((char)ch1);
						break;
					}
				}
				if ((ch1 == -1)||(ch1 == 65535)) //end of string, store the last word and exit the loop
				{
					break;
				}
				if ((esr.wordDelimiterFound())) //start of a string with authorised blanks
				{
					//System.out.println((char)ch1);
					for (;;)
					{
						ch1= esr.getNextChar();
						if ((ch1 == -1)||(ch1 == 65535)) //end of string, store the last word and exit the loop
						{
							v.add(word.toString());//store the current word
							// 				System.out.println("nouveau mot="+word);
							break;
						}
						if ((esr.wordDelimiterFound())) //end of authorised blanks
						{
							ch1= esr.getNextChar();
							//System.out.println((char)ch1);
							v.add(word.toString());//store the current word
							// 				System.out.println("nouveau mot="+word);
							word=new StringBuffer();//create a new empty word
							break;
						}
						else word.append((char)ch1);
					}
				}
				else for (;;)//read the non blanks in ormal way
				{
					ch1= esr.getNextChar();
					if ((ch1 == -1)||(ch1 == 65535)) //end of string, store the last word and exit the loop
					{
						v.add(word.toString());//store the current word
						// 			System.out.println("nouveau mot="+word);
						break;
					}
					//if ((ch1=='\t')||(ch1==' ')||(ch1==',')||(ch1=='\n')||(ch1=='\r')) //a blank appears
					boolean b=false;
					for (char cSpacer:spacers) b=b||(ch1==cSpacer);
					if (b)
					{
						v.add(word.toString());//store the current word
						// 			System.out.println("nouveau mot="+word);
						word=new StringBuffer();//create a new empty word
						break;
					}
					else word.append((char)ch1);
				}
				if ((ch1 == -1)||(ch1 == 65535)) //end of string, store the last word and exit the loop
				{
					break;
				}
			}
			//String[] stringArray=new String[v.size()];
			//v.copyInto(stringArray);
			sr.close();
			//buffer.deleteCharAt(0);
			//System.out.println("C++ Comments removed");
			return v;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}

	}




	/** 
	 * Tells if a class name  is inherited or implements interface of the given name
 @param className the name of the class to check
 @param superName the name of the class or interface that might apply to the check class
 @return true if className is or is inherited or implements interface of the name superName
	 **/
	public static boolean isA (String className, String superClassName)
	{
		Object o=Util.getObject(className,false);
		if (o==null) return false;
		Class superClass;
		try
		{
			superClass=Class.forName(superClassName);
		}
		catch(Exception e) 
		{
			return false;
		}
		return  superClass.isInstance(o);
	}


	/** 
	 * Tells if an object  is inherited or implements interface of the given name
 @param o the object to check
 @param superClassName the name of the class or interface that might apply to the check class
 @return true if className is or is inherited or implements interface of the name superName
	 **/
	public static boolean isA (Object o, String superClassName)
	{
		Class superClass;
		try
		{
			superClass=Class.forName(superClassName);
		}
		catch(Exception e) 
		{
			return false;
		}
		return  superClass.isInstance(o);
	}

	/** 
	 * Tells if the argument argName is present in the array args
	 * 
	 * @param args array of Strings that contains the arguments
	 * @param argName Name of the argument to find
	 * @return true if the argName is found in args
	 */
	public static boolean isArgPresent(String[] args, String argName)
	{
		for (int i = 0; i < args.length; i++)
		{
			if (args[i].equals(argName))
			{
				return true;
			}
		}

		return false;
	}

	/** 
	 * Returns the value of a given arguement: if the arguments are: <br/>
	 * -arg1 val1 -arg2 val2 <br/>
	 * the value of the argument "arg1" is "val1"
	 * 
	 * @param args array of Strings that contains the arguments
	 * @param argName Name of the argument to find
	 * @return The value of the given argument or null if the argument is not 
	 *	found.
	 */
	public static String getArgValue(String[] args, String argName)
	{
		for (int i = 0; i < args.length; i++)
		{
			if (args[i].equals(argName))
			{
				if (i+1 == args.length)
				{
					return null;
				}
				else
				{
					return args[i+1];
				}
			}
		}

		return null;
	}



	/**
	 * get the returned type of a param
	 * @param paramOwner
	 * @param paramName
	 * @return
	 */
	public static Class getType(Object paramOwner,String paramName)
	{
		Method[] methods=paramOwner.getClass().getMethods();
		Class c=null;
		String firstLetter=paramName.substring(0, 1).toUpperCase();
		String s2=firstLetter+paramName.substring(1, paramName.length());
		//paramName=s2;
		//System.out.println(s2);
		for (Method m:methods) 
		{
			String name=m.getName();
			//System.out.println(name);
			if (  
					(name.compareTo("get"+s2)==0)||(name.compareTo("is"+s2)==0) 
					||(name.compareTo("get"+paramName)==0)||(name.compareTo("is"+paramName)==0) 
					) 
			{
				c=m.getReturnType();
				break;
			}
		}

		if (c==null)
		{
			Field field=null;

			try
			{
				field=paramOwner.getClass().getField(paramName);
				c=field.get(paramOwner).getClass();
			}
			catch (Exception e){e.printStackTrace();};
			//System.out.println(paramOwner.getClass()+" **************************field: "+field);
		}

		if (c==null) 
			Messager.messErr(paramOwner.getClass().getSimpleName()+" has no get"+s2+ " or is"+s2+" method");
		return c;
	}


	/**
	 * This utility class is looking for all the classes implementing or 
	 * inheriting from a given interface or class.
	 *
	 * @author <a href="mailto:daniel@satlive.org">Daniel Le Berre</a>
	 * @version 1.0
	 */
	public static class SubClassFinder {


		/**
		 * Returns all the classes inheriting or implementing a given
		 * class in the currently loaded packages.
		 * @param tosubclassname the name of the class to inherit from
		 * @return a Vector with the results (may contain 0 elements)
		 */
		public static Vector<Class> find(String tosubclassname) {
			Vector<Class> results = new Vector<Class>();
			try {
				Class tosubclass = Class.forName(tosubclassname);
				Package [] pcks = Package.getPackages();
				for (int i=0;i<pcks.length;i++) {
					results.addAll(find(pcks[i].getName(),tosubclass));
				}
			} catch (ClassNotFoundException ex) {
				System.err.println("Class "+tosubclassname+" not found!");
			}
			return results;
		}

		/**
		 * Returns all the classes inheriting or implementing a given
		 * class in a given package.
		 * @param pckname 
		 * @param tosubclassname 
		 * @return a Vector with the results (may contain 0 elements)
		 */
		public static Vector<Class> find(String pckname, String tosubclassname) {
			try {
				Class tosubclass = Class.forName(tosubclassname);
				return find(pckname,tosubclass);
			} catch (ClassNotFoundException ex) {
				System.err.println("Class "+tosubclassname+" not found!");
			}
			Vector<Class> results = new Vector<Class>();
			return results;
		}

		/**
		 * Returns all the classes inheriting or implementing a given
		 * class in a given package.
		 * @param pckgname the fully qualified name of the package
		 * @param tosubclass the Class object to inherit from
		 * @return a Vector with the results (may contain 0 elements)
		 */
		public static Vector<Class> find(String pckgname, Class tosubclass) {
			Vector<Class> results = new Vector<Class>();

			// Code from JWhich
			// ======
			// Translate the package name into an absolute path
			String name = new String(pckgname);
			if (!name.startsWith("/")) {
				name = "/" + name;
			}	
			name = name.replace('.','/');

			// Get a File object for the package
			URL url = SubClassFinder.class.getResource(name);
			// URL url = tosubclass.getResource(name);
			// URL url = ClassLoader.getSystemClassLoader().getResource(name);
			//System.out.println(name+"->"+url);

			// Happens only if the jar file is not well constructed, i.e.
			// if the directories do not appear alone in the jar file like here:
			// 
			//          meta-inf/
			//          meta-inf/manifest.mf
			//          commands/                  <== IMPORTANT
			//          commands/Command.class
			//          commands/DoorClose.class
			//          commands/DoorLock.class
			//          commands/DoorOpen.class
			//          commands/LightOff.class
			//          commands/LightOn.class
			//          RTSI.class
			//
			if (url==null) return results;

			File directory = null;
			try {
				directory = new File(URLDecoder.decode(url.getFile(), System.getProperty( "file.encoding" )));
			} catch (UnsupportedEncodingException e1) {
				e1.printStackTrace();
			}

			// New code
			// ======
			if (directory != null && directory.exists()) {
				// Get the list of the files contained in the package
				String [] files = directory.list();
				for (int i=0;i<files.length;i++) {

					// we are only interested in .class files
					if (files[i].endsWith(".class")) {
						// removes the .class extension
						String classname = files[i].substring(0,files[i].length()-6);
						try {
							// Try to create an instance of the object
							Object o = Class.forName(pckgname+"."+classname).newInstance();
							if (tosubclass.isInstance(o)) {
								results.addElement(Class.forName(pckgname+"."+classname));
							}
						} catch (ClassNotFoundException cnfex) {
							System.err.println(cnfex);
						} catch (InstantiationException iex) {
							// We try to instanciate an interface
							// or an object that does not have a 
							// default constructor
						} catch (IllegalAccessException iaex) {
							// The class is not public
						}
					}
				}
			} else {
				try {
					// It does not work with the filesystem: we must
					// be in the case of a package contained in a jar file.
					JarURLConnection conn = (JarURLConnection)url.openConnection();
					String starts = conn.getEntryName();
					JarFile jfile = conn.getJarFile();
					Enumeration e = jfile.entries();
					while (e.hasMoreElements()) {
						ZipEntry entry = (ZipEntry)e.nextElement();
						String entryname = entry.getName();
						if (entryname.startsWith(starts)
								&&(entryname.lastIndexOf('/')<=starts.length())
								&&entryname.endsWith(".class")) {
							String classname = entryname.substring(0,entryname.length()-6);
							if (classname.startsWith("/")) 
								classname = classname.substring(1);
							classname = classname.replace('/','.');
							try {
								// Try to create an instance of the object
								Object o = Class.forName(classname).newInstance();
								if (tosubclass.isInstance(o)) {
									results.addElement(Class.forName(classname));
								}
							} catch (ClassNotFoundException cnfex) {
								System.err.println(cnfex);
							} catch (InstantiationException iex) {
								// We try to instanciate an interface
								// or an object that does not have a 
								// default constructor
							} catch (IllegalAccessException iaex) {
								// The class is not public
							}
						}
					}
				} catch (IOException ioex) {
					System.err.println(ioex);
				}	
			}

			return results;
		}

		public static void main(String []args) {
			if (args.length==2) {
				find(args[0],args[1]);
			} else {
				if (args.length==1) {
					find(args[0]);
				} else {
					System.out.println("Usage: java RTSI [<package>] <subclass>");
				}
			}
		}
	}// RTSI


	public static String  getOrCreateApplicationUserFolder (String appliName)
	{
		return getOrCreateApplicationUserFolder (appliName,false);
	}


	/**
	 * create the application folder having the name of the application
	 */
	public static String  getOrCreateApplicationUserFolder (String appliName,boolean verbose)
	{
		// get the standard user directory for the current user
		String mainUserSpace = System.getProperty("user.home");

		// get the name of the OS currently being used
		// will return "Windows Vista" if we are under a flavour of Vista
		String osName = System.getProperty("os.name");

		//System.out.println("Operating system: "+osName);

		// get the file separator used on the OS
		char sep = System.getProperty("file.separator").charAt(0);        // Get the file separator - almost certainly \ in any Windows OS

		// define the filepaths to be used for toolbox dir  and tmp
		java.io.File targetFolderToolBoxDir = new java.io.File(mainUserSpace + sep + Global.photonlyxToolBoxDir);
		java.io.File targetFolderAppli = new java.io.File(mainUserSpace + sep + Global.photonlyxToolBoxDir + sep + appliName);

		// check if the photonlyxToolBoxDir folder already exists
		if (targetFolderToolBoxDir.exists() )
		{
			if (verbose) System.out.println("Found photonlyx.toolbox Directory @ : " + targetFolderToolBoxDir.toString());
		}
		else 
			// if not, create the photonlyxToolBoxDir folder
		{
			String dirCioninName = mainUserSpace + sep + Global.photonlyxToolBoxDir;

			boolean success = (new File(dirCioninName)).mkdir();
			if (success) 
			{
				if (verbose) System.out.println("Successfully created the folder " + mainUserSpace + sep + Global.photonlyxToolBoxDir);
			}
			else 
			{
				if (verbose) System.out.println("Error creating the folder " + mainUserSpace + sep + Global.photonlyxToolBoxDir);
			}
		}

		// check if the application  folder already exists
		if (targetFolderAppli.exists() )
		{
			if (verbose) System.out.println("Found "+appliName+" Directory @ : " + targetFolderAppli.toString());
		}
		else 
			// if not, create the appli folder
		{
			String dirName = mainUserSpace + sep + Global.photonlyxToolBoxDir + sep +appliName;

			Boolean successTmp = (new File(dirName)).mkdir();
			if (successTmp) 
			{
				if (verbose) System.out.println("Successfully created the folder " + mainUserSpace + sep + appliName);
			}
			else 
			{
				if (verbose) System.out.println("Error creating the folder " + mainUserSpace + sep + appliName);
			}
		}

		String   directory=mainUserSpace + sep + Global.photonlyxToolBoxDir +sep+appliName; 
		return directory;



	}



	public static Process runCommand(String com) 
	{
		String s = null;
		System.out.println("Run the command: "+com+" \n");
		Process p=null; 
		try 
		{
			p = Runtime.getRuntime().exec(com);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			// read the output from the command  
			//System.out.println("Standard output:");
			while ((s = stdInput.readLine()) != null) 
			{
				System.out.println(s);
				//Global.setMessage(s);
			}
			// read any errors from the attempted command
			//System.out.println("Standard error:");
			while ((s = stdError.readLine()) != null) 
			{
				System.out.println(s);
			}
			while(p.isAlive()) 
			{
				//System.out.println("Wait 200ms");
				try {Thread.sleep(200);} catch(Exception e){;}
			}
		}
		catch (IOException e) 
		{
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			
		}
		return p;


	}



	public static boolean copyFile(String fullPathSource,String fullPathDest,boolean verbose)
	{
		File fsource=new File(fullPathSource);
		File fdest=new File(fullPathDest);
		try
		{
			Files.copy(fsource.toPath(),fdest.toPath(),StandardCopyOption.REPLACE_EXISTING);
			if (verbose) System.out.println("Copy "+fsource.toPath()+" to "+fdest.toPath());
			return true;
		}
		catch (Exception e)
		{
			String s="Problem copying the file "+fsource+" to "+fdest;
			if (verbose)
			{
				//Messager.messErr(s);
				System.err.println(s);
			}
			return false;
		}
	}

	public static Object getMethodGetVal(Object o,String paramName)
	{
		//look for a method getter for the attribute of name paramName:
		Method m=getMethodGet(o,paramName);
		if (m==null) return null;
		Object dd=null;
		try 
		{
			dd=m.invoke(o, (Object[])null);
			return dd;
		}
		catch(Exception e2)
		{
			System.err.println("Object: "+o.getClass().getName()+"Can't find getter for the attribute "+paramName);
			//e2.printStackTrace();
			return null;
		}	
	}


	public static Object getPublicAttributeOrMethodGetVal(Object o,String paramName)
	{
		//look for a public attribute of name paramName:
		try
		{
			Field field=o.getClass().getField(paramName);
			if (field!=null) 
			{
				return field.get(paramName);
			}
		}
		catch (Exception e)
		{
			System.err.println("Object: "+o.getClass().getName()+"Can't find public attribute of name "+paramName);
			//e.printStackTrace();

			//look for a method getter for the attribute of name paramName:
			Method m=getMethodGet(o,paramName);
			if (m==null) return 0;
			Object dd=null;
			try 
			{
				dd=m.invoke(o, (Object[])null);
				return dd;
			}
			catch(Exception e2)
			{
				System.err.println("Object: "+o.getClass().getName()+"Can't find getter for the attribute "+paramName);
				//e2.printStackTrace();
				return null;
			}
		}
		return null;
	}


/**
 * invoke the setter of the param of name paramName to apply the value dd
 * @param o
 * @param paramName
 * @param dd
 * @return
 */
	public static boolean setVal(Object o,String paramName,Object dd) 
	{
		Method m=getMethodSet(o,paramName);
		if (m==null) return false;
		try {m.invoke(o, dd);}
		catch(Exception e){e.printStackTrace();}
		return true;
	}


	//	
	//
	//	public static int getIntVal(Object o,String paramName)
	//	{
	//		Field field=null;
	//		try
	//		{
	//			field=o.getClass().getField(paramName);
	//			if (field!=null) 
	//			{
	//				return (Integer)field.get(paramName);
	//			}
	//		}
	//		catch (Exception e){};
	//
	//		Method m=getMethodGet(o,paramName);
	//		if (m==null) return 0;
	//		int dd=0;
	//		try {dd=(int)m.invoke(o, (Object[])null);}
	//		catch(Exception e){e.printStackTrace();}
	//		return dd;
	//	}
	//
	//
	//	public static String getStringVal(Object o,String paramName)
	//	{
	//		Field field=null;
	//		try
	//		{
	//			field=o.getClass().getField(paramName);
	//			if (field!=null) 
	//			{
	//				return (String)field.get(paramName);
	//			}
	//		}
	//		catch (Exception e){};
	//
	//		Method m=getMethodGet(o,paramName);
	//		if (m==null) return null;
	//		String dd=null;
	//		try {dd=(String)m.invoke(o, (Object[])null);}
	//		catch(Exception e){e.printStackTrace();}
	//		return dd;
	//	}
	//
	//	



	public static Method getMethodGet(Object o,String paramName)
	{
		String firstLetter=paramName.substring(0, 1).toUpperCase();
		String s2=firstLetter+paramName.substring(1, paramName.length());
		Method[] methods=o.getClass().getMethods();
		for (Method m:methods) 
		{
			String name=m.getName();
			//System.out.println(name);
			if (  (name.compareTo("get"+paramName)==0)||(name.compareTo("get"+s2)==0) ) return m;
			else if (  (name.compareTo("is"+paramName)==0)||(name.compareTo("is"+s2)==0) ) return m;
		}
		//System.out.println(" Util.getMethodGet() Warning: could not find "+"get"+paramName+" or "+"get"+s2+" in "+o.getClass());
		System.out.println(" Util.getMethodGet() Warning: could not find "+"get"+paramName+" or "+"get"+s2+" or "+"is"+paramName+" or "+"is"+s2+" in "+o.getClass());
		return null;
	}

	public static Method getMethodSet(Object o,String paramName)
	{
		String firstLetter=paramName.substring(0, 1).toUpperCase();
		String s2=firstLetter+paramName.substring(1, paramName.length());
		Method[] methods=o.getClass().getMethods();
		for (Method m:methods) 
		{
			String name=m.getName();
			//System.out.println(name+ "is "+"set"+paramName+"   OR  "+"set"+s2+"  ?");
			if (  (name.compareTo("set"+paramName)==0)||(name.compareTo("set"+s2)==0) ) return m;
		}
		System.out.println("com.photonlyx.toolbox.Util"+" getMethodSet() Warning: could not find "+"set"+paramName+" or "+"set"+s2+" in "+o.getClass());
		return null;
	}

	//	public static Method getMethod(Object o,String paramName)
	//	{
	//		String firstLetter=paramName.substring(0, 1).toUpperCase();
	//		String s2=firstLetter+paramName.substring(1, paramName.length());
	//		Method[] methods=o.getClass().getMethods();
	//		for (Method m:methods) 
	//		{
	//			String name=m.getName();
	//			//System.out.println(name+ "is "+"set"+paramName+"   OR  "+"set"+s2+"  ?");
	//			if (  (name.compareTo("set"+paramName)==0)||(name.compareTo("set"+s2)==0) ) return m;
	//			else if (  (name.compareTo("is"+paramName)==0)||(name.compareTo("is"+s2)==0) ) return m;
	//		}
	//		System.out.println(" Util.getMethodSet() Warning: could not find "+"set"+paramName+" or "+"set"+s2+" or "+"is"+paramName+" or "+"is"+s2+" in "+o.getClass());
	//		return null;
	//	}

	//	public static Boolean getBoolVal(Object o,String paramName)
	//	{
	//		Field field=null;
	//		try
	//		{
	//			field=o.getClass().getField(paramName);
	//			if (field!=null) 
	//			{
	//				return (Boolean)field.get(paramName);
	//			}
	//		}
	//		catch (Exception e){};
	//
	//		Method m=getMethodIs(o,paramName);
	//		if (m==null) return null;
	//		Boolean dd=null;
	//		try {dd=(Boolean)m.invoke(o, (Object[])null);}
	//		catch(Exception e){e.printStackTrace();}
	//		return dd;
	//	}
	//
	//	
	//	/**
	//	 * affect a String parameter to an object
	//	 * @param o
	//	 * @param paramName
	//	 * @param dd
	//	 * @return
	//	 */
	//	public static boolean setStringVal(Object o,String paramName,String dd) 
	//	{
	//		//System.out.println(getClass()+" setting val "+this.getParamName()+" to :"+dd);
	//		Field field=null;
	//		try
	//		{
	//			field=o.getClass().getField(paramName);
	//			if (field!=null) 
	//			{
	//				field.set(o, dd);
	//				return true;
	//			}
	//		}
	//		catch (Exception e){};
	//
	//		if (dd==null) dd="null";
	//
	//		Method m=getMethod(o,paramName);
	//		if (m==null) return false;
	//		try {m.invoke(o, dd);}
	//		catch(Exception e){e.printStackTrace();}
	//		return true;
	//	}



	//	public static Method getMethodIs(Object o,String paramName)
	//	{
	//	String firstLetter=paramName.substring(0, 1).toUpperCase();
	//	String s2=firstLetter+paramName.substring(1, paramName.length());
	//	Method[] methods=o.getClass().getMethods();
	//	for (Method m:methods) 
	//		{
	//		String name=m.getName();
	//		//System.out.println(name);
	//		if (  (name.compareTo("is"+paramName)==0)||(name.compareTo("is"+s2)==0) ) return m;
	//		}
	//	System.out.println("Util getMethodGet() Warning: could not find "+"is"+paramName+" or "+"is"+s2+" in "+o.getClass());
	//	return null;
	//	}


}











class EscapedStringReader
{
	protected char escapeChar = '\\';
	protected boolean escapeCharFound = false;
	protected int escapedChar;

	protected boolean wordDelimiterFound = false;

	StringReader sr;

	EscapedStringReader(StringReader sr)
	{
		this.sr = sr;
	}

	public int getNextChar() throws IOException
	{
		int result;

		if (escapeCharFound)
		{
			escapeCharFound = false;
			wordDelimiterFound = false;
			return escapedChar;
		}
		else
		{
			result = sr.read();

			if (result == escapeChar)
			{
				wordDelimiterFound = false;
				escapedChar = sr.read();

				if (escapedChar == '"')
				{
					result = '"';
				}
				else if (escapedChar == '\\')
				{
					result = '\\';
				}
				else
				{
					escapeCharFound = true;
				}

			}
			else
			{
				if ((char)result == '"')
				{
					wordDelimiterFound = true;
				}
				else
				{
					wordDelimiterFound = false;
				}
			}
		}

		return result;
	}

	public boolean wordDelimiterFound()
	{
		return wordDelimiterFound;
	}
}
