
// Author Laurent Brunel



package com.photonlyx.toolbox.util;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CMessageHolder;



public class Global
{
private  static Vector<CMessageHolder> cmessageHolderList=new Vector<CMessageHolder>();	

//name of the file where is stored the last working directory:
public static String workingDirectoryFileName="workingDir.txt";

//directory of the application where user params are stored:
public static String appHome;

//current path of the session, the working directory
public static String path;


//directory from where the application is executed
//public static String execDir;

public static Font font=new Font("DejaVu Sans",Font.PLAIN,10);
//public static Font font=new Font("Arial",Font.PLAIN,10);
//public static Font font=new Font("Berlin Sans FB",Font.PLAIN,10);

public static Color background=Color.white;
public static Color foreground=Color.black;
public static Color chartGridLinesColor=new Color(200,200,200);
public static boolean log=false;//make a log file
public static boolean messagesInConsole=false;//if true print the messages in console
public static String logFileName="log.txt";
public static String photonlyxToolBoxDir="photonlyx.toolbox";


public static void addMessageHolder(CMessageHolder cmh)
{
cmessageHolderList.add(cmh);
}

public static void removeMessageHolder(CMessageHolder cmh)
{
cmessageHolderList.remove(cmh);
}

public  static void setMessage(String s)
{
for (CMessageHolder cmessageHolder:cmessageHolderList) cmessageHolder.setMessage(s);	
log(s);
}


public  static void log(String s)
{
if (messagesInConsole) System.out.println(s);
if (log)
	{
	try
		{
		File file =new File(logFileName);
		if(!file.exists()){
			file.createNewFile();
		}
		FileWriter fileWritter = new FileWriter(file.getName(),true);
        BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
        bufferWritter.write(s+"\n");
        bufferWritter.close();
		}
	catch (Exception e) 
		{
		Messager.messErr(Messager.getString("Probleme_ecriture_fichier")+" "+logFileName);;
		}
	}
}

public static void resetDefaultMessage()
{
setMessage("PhotonLyX")	;

}

}

