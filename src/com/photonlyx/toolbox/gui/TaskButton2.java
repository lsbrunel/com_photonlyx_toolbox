// Author Laurent Brunel

package com.photonlyx.toolbox.gui;

import javax.swing.*;

import com.photonlyx.toolbox.thread.CTask;
import com.photonlyx.toolbox.thread.CTask2;
import com.photonlyx.toolbox.thread.TaskEndListener;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.Util;

import java.awt.event.*;
import java.awt.*;
import java.net.*;


/** button that launch the run method (execution in a thread of the son) */
public class TaskButton2 implements ActionListener,TaskEndListener
{
//links
private CTask2 ctask;
private TriggerList triggerListBefore ;//to trig before starting
private TriggerList triggerListAfter ;//to trig before starting
private TriggerList triggerListDuring;//to trig during the process
//params
private Color stoppedColor=ColorUtil.getPanelGray();//color when stopped
private Color runningColor=Color.RED;//color when running
private String stoppedLabel="start";//label when stopped
private String runningLabel="stop";//label when running
private boolean borderPainted=true;//tells if the border of the button is painted
private String L_toolTip;//the tool tip to explain what the button does
private boolean isContentAreaFilled=true; // if false, the default look and feel won't paint anything, and the button

//internal use
private JButton b=new JButton();
private Thread thread;
private Icon iconReady;
private Icon iconRunning;
private Icon iconDisabled;



/**
 * 
 * @param ctask task that will be launched by this button
 * @param triggerListBefore trig this just before starting the task (can be null)
 */
public TaskButton2(CTask2 ctask, TriggerList triggerListBefore) 
{
super();
this.ctask = ctask;
this.triggerListBefore = triggerListBefore;
ctask.setTaskEndListener(this);

actualiseGraphics();

//always init with the stopped state
stop();
}

/**
 * 
 * @param ctask task that will be launched by this button
 * @param triggerListBefore trig this just before starting the task (can be null)
 */
public TaskButton2(CTask2 ctask, TriggerList triggerListBefore, TriggerList triggerListAfter) 
{
super();
this.ctask = ctask;
this.triggerListBefore = triggerListBefore;
this.triggerListAfter = triggerListAfter;
ctask.setTaskEndListener(this);

actualiseGraphics();

//always init with the stopped state
//stop();
ctask.setStopped(true);
setStoppedLook();
}
/**
 * 
 * @param ctask task that will be launched by this button
 * @param triggerListBefore trig this just before starting the task (can be null)
 */
public TaskButton2(CTask2 ctask, TriggerList triggerListBefore,TriggerList triggerListDuring, TriggerList triggerListAfter) 
{
super();
this.ctask = ctask;
this.triggerListBefore = triggerListBefore;
this.triggerListDuring = triggerListDuring;
ctask.setTriggerListDuring(triggerListDuring);
this.triggerListAfter = triggerListAfter;
ctask.setTaskEndListener(this);

actualiseGraphics();

//always init with the stopped state
//stop();
ctask.setStopped(true);
setStoppedLook();
}


/**
 * 
 * @param ctask task that will be launched by this button
 * @param triggerListBefore trig this just before starting the task (can be null)
 */
public TaskButton2(CTask2 ctask) 
{
super();
this.ctask = ctask;
ctask.setTaskEndListener(this);
actualiseGraphics();
//always init with the stopped state
//stop();
}


public void setTask(CTask2 t)
{
this.ctask=t;
}

private void actualiseGraphics()
{
b.addActionListener(this);
b.setBackground(Global.background);

b.setSize(140,30);
b.setPreferredSize(new Dimension(140,30));

b.setMnemonic(KeyEvent.VK_R);
//b.setForeground(ColorUtil.getColor(foreground));
//b.setBackground(ColorUtil.getColor(background));
Font font=null;
font=Global.font;//new Font(Global.fontName,java.awt.Font.PLAIN,fontsize);
if (font!=null) b.setFont(font);
if (L_toolTip!=null) b.setToolTipText(L_toolTip);


b.setBorderPainted(borderPainted);

b.setContentAreaFilled(isContentAreaFilled);
	
}




public void start()
{

//launch sons subnetworks:
if (triggerListBefore != null)
	{
	triggerListBefore.trig();
	}

if (thread!=null)
	{
	do
		{
		//System.out.println("Wait!");
		}
	while (thread.isAlive());
	}
ctask.setStopped(false);
//stopped=false;

setRunningLook();

thread=new Thread(ctask);
//System.out.println("Button stopped="+stopped);
thread.start();
}

public void stop()
{
//System.out.println("Button stop");
ctask.setStopped(true);
setStoppedLook();
if (triggerListAfter != null)
	{
	triggerListAfter.trig();
	}
}

public void setEnabled(boolean bool)
{
b.setEnabled(bool);
if (bool)
	{
	if (ctask.isStopped()) setStoppedLook();else setRunningLook();
	}
else setDisabledLook();
}


public void setRunningLook()
{
b.setText(runningLabel);
b.setBackground(runningColor);
if (iconRunning!=null) b.setIcon(iconRunning);
}

public void setStoppedLook()
{
b.setBackground(stoppedColor);
b.setText(stoppedLabel);
//setIconReady();
if (iconReady!=null) b.setIcon(iconReady);
}


private void setDisabledLook()
{
b.setBackground(stoppedColor);
b.setText(stoppedLabel);
//if (iconReady!=null) b.setIcon(iconDisabled);
}



public void actionPerformed(ActionEvent e)
{
Object source = e.getSource();
if (b==source)
	{
	if (ctask.isStopped())start();else stop();
	}
try
	{
	Thread.sleep(0);
	}
catch(Exception ex) {System.out.println(ex);}

}

public JButton getJButton()
{
return b;
}



public String getLabel() {

	return stoppedLabel;
}


public String getToolTip() {
if (L_toolTip!=null)  return L_toolTip; else return "";
}


public void setRunningLabel(String runningLabel)
{
	this.runningLabel = runningLabel;
}

public void setStoppedLabel(String stoppedLabel) 
{
this.stoppedLabel = stoppedLabel;
b.setText(stoppedLabel);
}

public void setStoppedColor(Color c)
{
this.stoppedColor = c;
this.getJButton().setBackground(c);
}


public void setRunningColor(Color c)
{
this.runningColor = c;
}

public void setText(String string)
{
setStoppedLabel(string ) ;	
}


//private void setIconReady()
//{
//if (iconFileName==null) return;
//	ClassLoader cl=new Util().getClass().getClassLoader();
//	URL url=cl.getResource(iconFileName);
//	Icon icon=new ImageIcon(url);
//	if (icon==null) Messager.messErr("Problem to find resource "+iconFileName);
//	b.setIcon(icon)	;
//}

public void setIconReadyFileName(String iconFileName)
{
//	this.iconFileName=iconFileName;
ClassLoader cl=new Util().getClass().getClassLoader();
URL url=cl.getResource(iconFileName);
iconReady=new ImageIcon(url);
if (iconReady==null) Messager.messErr("Problem to find resource "+iconFileName);
b.setIcon(iconReady);
}


public void setRunningIconFileName(String runningIconFileName)
{
ClassLoader cl=new Util().getClass().getClassLoader();
URL url=cl.getResource(runningIconFileName);
iconRunning=new ImageIcon(url);
if (iconRunning==null) Messager.messErr("Problem to find resource "+runningIconFileName);
b.setPressedIcon(iconRunning);
}


public void setDisabledIconFileName(String disabledIconFileName)
{
ClassLoader cl=new Util().getClass().getClassLoader();
URL url=cl.getResource(disabledIconFileName);
iconDisabled=new ImageIcon(url);
if (iconDisabled==null) Messager.messErr("Problem to find resource "+disabledIconFileName);
b.setDisabledIcon(iconDisabled);
}



@Override
public void whenTaskFinished() 
{
	stop();	
}




}
