package com.photonlyx.toolbox.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import com.photonlyx.toolbox.io.*;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;

public class FileIconsPanel
{
private JPanel jp=new JPanel();
private ClassLoader cl=getClass().getClassLoader();
private TriggerList tlAfterAction;

/**
 * 
 * @param jfm file manager
 * @param jff file name filter
 * @param t trigger to trig after the action on file
 */
public FileIconsPanel(CFileManager jfm,CFileFilter jff,TriggerList tl,CJFrame fatherWindow)
{
this.tlAfterAction=tl;
jp.setPreferredSize(new Dimension(40*4, 40));
//jp.setSize(new Dimension(250, 30));
jp.setBackground(Global.background);
jp.setLayout(new FlowLayout(FlowLayout.LEFT));//create the legend of this chart:
SaveAsFileAction safa;
addIcon("com/photonlyx/toolbox/gui/NewFileAction.png",new Trigger(new NewFileAction(jfm,jff,fatherWindow),"work"));
addIcon("com/photonlyx/toolbox/gui/OpenFileAction.png",new Trigger(new OpenFileAction(jfm,jff,fatherWindow),"work"));
addIcon("com/photonlyx/toolbox/gui/SaveAsFileAction.png",new Trigger(safa=new SaveAsFileAction(jfm,jff,fatherWindow),"work"));
addIcon("com/photonlyx/toolbox/gui/SaveFileAction.png",new Trigger(new SaveFileAction(jfm,jff,safa),"work"));
}
	

private void addIcon(String f,Trigger tAction)
{
TriggerList tl1=new TriggerList();
tl1.add(tlAfterAction);
tl1.add(tAction);
CButton cb=new CButton(tl1);
cb.setPreferredSize(new Dimension(30, 30));
URL url=cl.getResource(f);
cb.setIcon(new ImageIcon(url));
cb.setContentAreaFilled(false);
cb.setBorderPainted(false);
jp.add(cb);	
}


public Component getComponent()
{
	return jp;
}



}
