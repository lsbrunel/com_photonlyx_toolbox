package com.photonlyx.toolbox.gui;

import java.awt.*;

/**
Wrapper to the GridBagConstraints class
*/
public class Grider extends GridBagConstraints
{

/**
 * Sets the gridx and gridy to the GridBagConstraints
 *
 * @param gridx the gridx parameter
 * @param gridy the gridy parameter
*/
public Grider(int gridx, int gridy)
	{
	this.gridx = gridx;
	this.gridy = gridy;
	}

/**
 * Sets the gridx and gridy to the GridBagConstraints
 *
 * @param gridx the gridx parameter
 * @param gridy the gridy parameter
 * @param gridwidth the cell width (in cells)
 * @param gridheight the cell height (in cells)
*/
public Grider(int gridx, int gridy, int gridwidth, int gridheight)
	{
	this.gridx = gridx;
	this.gridy = gridy;
	this.gridwidth = gridwidth;
	this.gridheight = gridheight;
	}

/** 
 * Sets the gridheight parameter 
 * 
 * @param gridheight 
 * @param gridheight the cell height (in cells)
 * @return 
 */
public Grider setGridHeight(int gridheight)
	{
	this.gridheight = gridheight;
	return this;
	}

/** 
 * Sets the gridwidth parameter 
 * 
 * @param gridwidth the cell width (in cells)
 * @return this
 */
public Grider setGridWidth(int gridwidth)
	{
	this.gridwidth = gridwidth;
	return this;
	}

/**
 * Sets the anchor parameter.
 *
 * @param anchor the anchor value
 * @return this 
*/
public Grider setAnchor(int anchor)
	{
	this.anchor = anchor;
	return this;
	}

/**
 * Sets the fill parameter.
 *
 * @param fill the fill parameter
 * @return this 
*/
public Grider setFill(int fill)
	{
	this.fill = fill;
	return this;
	}

/**
 * Sets the cell weightx and weighty.
 *
 * @param weightx the cell weight in x-direction
 * @param weighty the cell weight in y-direction
 * @return this
*/
public Grider setWeight(double weightx, double weighty)
	{
	this.weightx = weightx;
	this.weighty = weighty;
	return this;
	}

/**
 * Define an Inset of equal distances surrounding the component
 *
 * @param distance the spacing to use in all directions
 * @return this
*/
public Grider setInsets(int distance)
	{
	this.insets = new Insets(distance, distance, distance, distance);
	return this;
	}

/**
 * Sets the insets parameter
 *
 * @param top the spacing to use on top
 * @param left the spacing to use to the left
 * @param bottom the spacing to use on the bottom
 * @param right the spacing to use to the right
 * @return this
*/
public Grider setInsets(int top, int left, int bottom, int right)
	{
	this.insets = new Insets(top, left, bottom, right);
	return this;
	}

/**
 *  Sets the ipadx and ipady parameters
 *
 *  @param ipadx the ipadx param
 *  @param ipady the ipady param
 *  @return this
 */
public Grider setIpad(int ipadx, int ipady)
	{
	this.ipadx = ipadx;
	this.ipady = ipady;
	return this;
	}
}


