package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import com.photonlyx.toolbox.param.ParInt;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;




/**
permits to show and edit a param (name,value,unit) and trig objects when value changed

 */
public class ParIntBox  extends ParamBox implements DocumentListener,KeyListener,MouseWheelListener
{
	private Color valbackground=Color.lightGray;
	private boolean editable=true;
	private int fieldLength=5;

	private TriggerList triggerList;//list of object to update when this parameter is changed
	private ParInt p;
	private JTextField val;
	private JLabel name,labelUnit;
	private boolean actualiseOnMouseWheel=false;
	private int wheelIncrement=1;
	//private NumberFormat nf;
	//private DecimalFormatSymbols dfs=new DecimalFormatSymbols(Locale.ENGLISH);
	private double maxLimit=Integer.MAX_VALUE,minLimit=Integer.MIN_VALUE;


	private JPanel jp=new JPanel();

	/**
	 * 
	 * @param tl can be null
	 * @param p
	 */
	public ParIntBox(TriggerList tl,ParInt p,int w,int h,String label,String unit)
	{
		triggerList=tl;	
		this.p=p;
		this.setW(w);
		this.setH(h);

		jp.removeAll();

		//int w=intVal("w",150);
		//int h=intVal("h",30);
		jp.setSize(getW(),getH());
		jp.setPreferredSize(new Dimension(getW(),getH()));
		jp.setBackground(this.getBackground());

		name=new JLabel();
		name.setFont(this.getFont());
		name.setBackground(Global.background);
		if (label!=null) name.setText(label);else name.setText(p.getParamName());
		jp.add(name);

		val=new JTextField(fieldLength);

		jp.add(val);
		val.getDocument().addDocumentListener(this);
		val.addKeyListener(this);
		val.addMouseWheelListener(this);
		val.setOpaque(true);
		val.setEditable(editable);
		val.setFont(this.getFont());
		//val.setBackground(this.getBackground());
		if (editable) val.setBackground(this.getBackground());
		else val.setBackground(new Color(200,200,200));

		if (unit!=null) 
		{
			labelUnit=new JLabel("");
			labelUnit.setFont(this.getFont());
			labelUnit.setBackground(this.getBackground());
			labelUnit.setText(unit);
			jp.add(labelUnit);
		}

		val.doLayout();
		update();
	}


	public void setSize(int w, int h)
	{
		jp.setPreferredSize(new Dimension(getW(),getH()));
		super.setW(w);
		super.setH(h);
		//init();
		update();
	}



	public void setForeground(String color) 
	{
		Color foreground=ColorUtil.getColor(color);
		jp.setForeground(foreground);
		name.setForeground(foreground);
		val.setForeground(foreground);
		labelUnit.setForeground(foreground);
	}

	public void setBackground(String color) 
	{
		Color background=ColorUtil.getColor(color);
		jp.setBackground(background);
		name.setBackground(background);
		val.setBackground(background);
		labelUnit.setBackground(background);
	}

	public void setValBackground(String color) 
	{
		Color background=ColorUtil.getColor(color);
		val.setBackground(background);
	}

	public TriggerList getTriggerList() {
		return triggerList;
	}


	public void setTriggerList(TriggerList triggerList) {
		this.triggerList = triggerList;
	}


	public JTextField getVal() {
		return val;
	}


	public void setVal(JTextField val) {
		this.val = val;
	}


	public boolean isEditable() {
		return editable;
	}


	public void setEditable(boolean editable) {
		this.editable = editable;
		if (editable) val.setBackground(this.getBackground());
		else val.setBackground(new Color(200,200,200));
		val.setEditable(editable);
	}


	public void setFieldLength(int fieldLength) {
		this.fieldLength = fieldLength;
		val.setColumns(fieldLength);
	}


	public void setActualiseOnMouseWheel(boolean actualiseOnMouseWheel) {
		this.actualiseOnMouseWheel = actualiseOnMouseWheel;
	}




	public void update()
	{
		val.getDocument().removeDocumentListener(this);
		//System.out.println(getClass()+" val="+p.stringVal());
		//labelUnit.setText(unit);
		//name.setText(label);
		val.setText(p.stringVal());
		//val.doLayout();
		//jp.doLayout();
		val.getDocument().addDocumentListener(this);
	}




	//***********interface DocumentListener ***************

	//Gives notification that an attribute or set of attributes changed.
	public  void changedUpdate(DocumentEvent e)
	{
		//System.out.println(getClass()+" changedUpdate ");
		actualiseParamFromBox();
		//System.out.println(getClass()+" has changed:"+p.hasChanged());
	}

	//Gives notification that there was an insert into the document.
	public   void insertUpdate(DocumentEvent e)
	{
		//System.out.println(getClass()+" insertUpdate ");
		actualiseParamFromBox();
	}
	public   void removeUpdate(DocumentEvent e)
	{
		//System.out.println(getClass()+" removeUpdate ");
		actualiseParamFromBox();
	}
	//***********end interface DocumentListener ***************


	//**********interface keyListener**********************
	public void keyPressed(KeyEvent e)
	{
		switch (e.getKeyCode())
		{
		case KeyEvent.VK_ENTER :
			// 	System.out.println(getClass()+" VK_ENTER ");
			//if (triggerList!=null) triggerList.trig();
			//launchTheNetwork();
			jp.repaint();
			break;
		}


	}

	public void keyTyped(KeyEvent e){}
	public void keyReleased(KeyEvent e){}

	//**********end interface keyListener**********************



	public void mouseWheelMoved(MouseWheelEvent e)
	{
		if (!editable) return;
		int nbClicks=-e.getWheelRotation();
		val.getDocument().removeDocumentListener(this);
		//p.setVal(""+(p.val()+nbClicks*wheelIncrement));
		String sval=""+(p.val()+nbClicks*wheelIncrement);
		val.setText(sval);
		actualiseParamFromBox();

		val.getDocument().addDocumentListener(this);
	}

	private void actualiseParamFromBox()
	{
		if (p==null) return;
		if (!ParamBox.autoUpdate)return;
		//System.out.println(getClass()+" actualiseParamFromBox : "+val.getText());


		//		if (p instanceof ParString) 
		//		{
		//			String s1=val.getText();
		//			((ParString)p).setVal(s1);
		//			//	if (s1!=null) val.setColumns(s1.length());
		//			//	else val.setColumns(6);
		//		}
		//		else 	
		{
			//System.out.println(getClass()+" actualiseParamFromBox : not ParString ");
			boolean ok;
			int value=0;
			//ok=p.setVal(val.getText());

			try
			{
				value=new Integer(val.getText().replace(',','.')).intValue();
				ok=true;
			}
			catch (NumberFormatException e)
			{
				ok=false;
			}	

			if (ok) 
			{
				if ((value>maxLimit)||(value<minLimit)) ok=false;
			}


			if (!ok) val.setBackground(Color.red);
			else 
			{
				val.setBackground(valbackground);
				((ParInt)p).setVal(value);
				//				//if the object that have this parameter is a MysqlLine, update the database:
				//				if (p.getParamOwner() instanceof MysqlLine) 
				//				{
				//					MysqlLine l=(MysqlLine)p.getParamOwner();
				//					l.getTable().updateSQL(l,p.getParamName());
				//				}

			}
		}
		if (triggerList!=null) if (triggerEnabled) triggerList.trig();
		//af("val "+p.val());
	}

	/**
set or not the field having the value editable
	 */
	//public void setEditable(boolean b)
	//{
	//if (val!=null) 
	//	{
	//	val.setEditable(b);
	//	if (b) valbackground=Color.white;else valbackground=background; 
	//	val.setBackground(valbackground);
	//	}
	//}

	public Component getComponent(){return jp;}

	public String getParamName()
	{
		return p.getParamName();
	}

	public void setEnabled(boolean b)
	{
		this.setEditable(b);
		val.setEnabled(b);
		name.setEnabled(b);
		labelUnit.setEnabled(b);
	}

	public Param getParam(){return p;}


	public int getWheelIncrement()
	{
		return wheelIncrement;
	}


	public void setWheelIncrement(int wheelIncrement)
	{
		this.wheelIncrement = wheelIncrement;
	}


	public double getMaxLimit() {
		return maxLimit;
	}


	public void setMaxLimit(double maxLimit) {
		this.maxLimit = maxLimit;
	}


	public double getMinLimit() {
		return minLimit;
	}


	public void setMinLimit(double minLimit) {
		this.minLimit = minLimit;
	}



}