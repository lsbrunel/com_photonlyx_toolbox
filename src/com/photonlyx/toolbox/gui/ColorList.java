// Author Laurent Brunel

package com.photonlyx.toolbox.gui;


import java.awt.*;
import java.util.Vector;

import com.photonlyx.toolbox.txt.Definition;



/**
provide a configurable  list of colours
*/
public class ColorList  implements  ColorListInterface
{
//private String colorList="black  red  green blue yellow lightGray darkGray pink pastelBlue pastelRed";
private int index;
private Vector<String> list=new Vector<String>();

public ColorList()
{
	list.add("black");
	list.add("red");
	list.add("green");
	list.add("blue");
	list.add("gray");
	list.add("pastelBlue");
	list.add("pastelRed");
	list.add("cyan");
	list.add("orange");
	list.add("brown");
}


public ColorList(String s)
{
list.removeAllElements();
Definition def=new Definition(s);
for (int i=0;i<def.dim();i++)
	{
	int ii=i%def.dim();
	list.add(def.word(ii));
	}
}

/**
return the color number i modulo the nb of colors
@return i the color index
*/
public  String colour(int i)
{
i=i%list.size();
return list.elementAt(i);
}

public  Color colour_(int i)
{
return ColorUtil.getColor(colour(i));
}


/**
get a new color (different than last one)
*/
public String getNewColour()
{
// System.out.println(getClass()+" getNewColour: "+colour(index));
String s=list.elementAt(index++);
index=index%list.size();
return s;
}

/**
get a new color (different than last one)
*/
public String getNextColour()
{
// System.out.println(getClass()+" getNewColour: "+colour(index));
//index++;
String  s=colour(index);
index++;
return s;
}

/**
get a new color (different than last one)
*/
public String getPreviousColour()
{
index--;
if (index<0) index=list.size()-1;
// System.out.println(getClass()+" getNewColour: "+colour(index));
return colour(index);
}



/**
return the color number i modulo the nb of colors
@param i the color index
*/
public Color javaColor(int i)
{
return ColorUtil.getColor(colour(i));
}


/**
get a new color (different than last one)
*/
public Color getNewJavaColor()
{
return ColorUtil.getColor(colour(index++));
}

/**
set the number of colors that will be used
*/
public void setColorNumber(int n)
{
}


public int nbColors(){return list.size();}

}
