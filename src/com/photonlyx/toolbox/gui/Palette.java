package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.JPanel;

import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;

/**
 * @deprecated
 * @author laurent
 *
 */
public class Palette    implements PaletteInterface
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double vmin=0,vmax=1;//min and max values 
	private int offsetHue=20;//hue value corresponding to the 0 value (0 to 100%)
	private int ampHue=80;//hue amplitude used by the palette (0 to 100%)
	//private String format;//format of numbers written in the palette
	private float hue;//for COLOR_N_WHITE
	private int type=0;
	public final static int GRAY_SCALE=0;
	public final static int COLOR_N_WHITE=1;
	public final static int COLOR=2;
	private Vector<PaletteChangedListener> listeners=new Vector(); 





	//test app
	public static void main(String[] args) 
	{
		//create frame 
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(200, 400);
		cjf.setVisible(true);	
		cjf.setTitle("Palette");
		cjf.setFont(Global.font);
		cjf.add(new Palette().getVisualisation());
		cjf.setVisible(true);
	}


	/**
	 * construct a  palette
	 */
	public Palette()
	{
	}


	public void setVmin(double d){vmin=(float)d;}
	public void setVmax(double d){vmax=(float)d;}


	/**return the RGB with colours  for a  value*/
	private  int getRGBcoul(double val)
	{
		int coul;
		float r,g,b;
		if (val<vmin) val=vmin;
		if (val>vmax) val=vmax;
		// float hue=(float)( 0.12 +val*(0.8)/(vmax.val-vmin.val));
		double offset,amp;
		offset=offsetHue/100.;
		amp=ampHue/100.;
		float hue=(float)( offset +(val-vmin)/(vmax-vmin)*amp);
		if (hue>1) hue-=1;
		coul=Color.HSBtoRGB(hue,1,1);
		if (val==0) coul=Color.black.getRGB();
		return coul;
	}

	/**return the RGB code grey for a value*/
	private int getRGBnb(double val)
	{
		int coul;
		if (val<vmin) val=vmin;
		if (val>vmax) val=vmax;
		double offset,amp;
		offset=offsetHue/100.;
		amp=ampHue/100.;
		float coef=(float)( offset +(val-vmin)/(vmax-vmin)*amp);
		coul=new Color(coef,coef,coef).getRGB();
		return coul;
	}

	/**return the RGB code grey for a value*/
	private int getRGBcolorNwhite(double val)
	{
		int coul;
		if (val<vmin) val=vmin;
		if (val>vmax) val=vmax;
		double offset,amp;
		offset=offsetHue/100.;
		amp=ampHue/100.;
		float coef=(float)( offset +(val-vmin)/(vmax-vmin)*amp);
		float h=hue;
		float s=coef;
		float b=1;
		coul=Color.HSBtoRGB(h,s,b);
		return coul;
	}

	/**return the RGB code for a  value*/
	public int getRGB(double val)
	{
		int c=Palette.GRAY_SCALE;
		switch (type)
		{
		case Palette.GRAY_SCALE  :
			c=getRGBnb(val);
			break;
		case Palette.COLOR_N_WHITE  :
			c=getRGBcolorNwhite(val);
			break;
		case Palette.COLOR  :
			c=getRGBcoul(val);
			break;
		}
		return c;
	}

	public void update()
	{
		for (PaletteChangedListener l:listeners) l.onPaletteChanged();
	}


	public void addListener(PaletteChangedListener l)
	{
		listeners.add(l);
	}



	public int getType()
	{
		return type;
	}

	/**
	 * Palette.GRAY_SCALE    Palette.COLOR_N_WHITE=0    Palette.COLOR=0
	 * @param type
	 */
	public void setType(int t)
	{
		if (t>=3) return;
		if (t<0) return;
		this.type = t;

		if (t==2)
		{
			offsetHue=20;
			ampHue=80;
		}
		else 
		{
			offsetHue=0;
			ampHue=100;
		}
		update();
	}




	public float getHue()
	{
		return hue;
	}

	/**
	 * hue used for the case Palette.COLOR_N_WHITE
	 * @param hue
	 */
	public void setHue(float hue)
	{
		this.hue = hue;
	}

	/**
	 * hue used for the case Palette.COLOR_N_WHITE
	 * @param hue
	 */
	public int getOffsetHue()
	{
		return offsetHue;
	}


	public void setOffsetHue(int offsetHue)
	{
		this.offsetHue = offsetHue;
	}


	public int getAmpHue()
	{
		return ampHue;
	}


	public void setAmpHue(int ampHue)
	{
		this.ampHue = ampHue;
	}


	public double getVmin()
	{
		return vmin;
	}


	public double getVmax()
	{
		return vmax;
	}


	public Component getVisualisation()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
