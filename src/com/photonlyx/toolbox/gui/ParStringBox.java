package com.photonlyx.toolbox.gui;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import com.photonlyx.toolbox.mysql.MysqlRow;
import com.photonlyx.toolbox.param.ParDouble;
import com.photonlyx.toolbox.param.ParInt;
import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;




/**
permits to show and edit a param (name,value,unit) and trig objects when value changed

*/
public class ParStringBox  extends ParamBox implements DocumentListener,KeyListener,MouseWheelListener
{
//private String unit="";
//private String label="";
private boolean editable=true;
private int fieldLength=5;
	
private TriggerList triggerList;//list of object to update when this parameter is changed
private ParString p;
private JTextField val;
private JLabel name,labelUnit;



private JPanel jp=new JPanel();

/**
 * 
 * @param tl can be null
 * @param p
 */
public ParStringBox(TriggerList tl,ParString  p,int w,int h,String label,String unit)
{
triggerList=tl;	
this.p=p;
this.setW(w);
this.setH(h);


jp.removeAll();
jp.setPreferredSize(new Dimension(getW(),getH()));
jp.setBackground(this.getBackground());

name=new JLabel();
name.setFont(this.getFont());
name.setBackground(this.getBackground());
if (label!=null) name.setText(label);else name.setText(p.getParamName());
jp.add(name);

val=new JTextField(fieldLength);

jp.add(val);
val.getDocument().addDocumentListener(this);
val.addKeyListener(this);
val.addMouseWheelListener(this);
val.setOpaque(true);
val.setEditable(editable);
val.setFont(this.getFont());
val.setBackground(this.getBackground());

labelUnit=new JLabel("");
labelUnit.setFont(this.getFont());
labelUnit.setBackground(this.getBackground());
if (unit!=null) labelUnit.setText(unit);
jp.add(labelUnit);

val.doLayout();
update();
}


public void setSize(int w, int h)
{
super.setW(w);
super.setH(h);
//jp.setPreferredSize(new Dimension(w,h));
//init();
update();
}

//
//
//public void setUnit(String unit) {
//	this.unit = unit;
//}
//
//public void setLabel(String label) {
//	this.label = label;
//}


public void setForeground(String color) 
{
Color foreground=ColorUtil.getColor(color);
jp.setForeground(foreground);
name.setForeground(foreground);
val.setForeground(foreground);
labelUnit.setForeground(foreground);
}

public void setBackground(String color) 
{
Color background=ColorUtil.getColor(color);
jp.setBackground(background);
name.setBackground(background);
val.setBackground(background);
labelUnit.setBackground(background);
}

public void setValBackground(String color) 
{
Color background=ColorUtil.getColor(color);
val.setBackground(background);
}

public TriggerList getTriggerList() {
	return triggerList;
}


public void setTriggerList(TriggerList triggerList) {
	this.triggerList = triggerList;
}


public JTextField getVal() {
	return val;
}


public void setVal(JTextField val) {
	this.val = val;
}


public boolean isEditable() {
	return editable;
}


public void setEditable(boolean editable) {
	this.editable = editable;
	if (editable) val.setBackground(this.getBackground());
	else val.setBackground(new Color(200,200,200));
	val.setEditable(editable);
}


public void setFieldLength(int fieldLength) {
	this.fieldLength = fieldLength;
	val.setColumns(fieldLength);
}





public void update()
{
//labelUnit.setText(unit);
//name.setText(label);
val.setText(p.stringVal());
//val.doLayout();
//jp.doLayout();
}




//***********interface DocumentListener ***************

//Gives notification that an attribute or set of attributes changed.
public  void changedUpdate(DocumentEvent e)
{
//System.out.println(getClass()+" val box changed ");
actualiseParamFromBox();
//System.out.println(getClass()+" has changed:"+p.hasChanged());
}

//Gives notification that there was an insert into the document.
public   void insertUpdate(DocumentEvent e)
{
//System.out.println(getClass()+" inserted ");
actualiseParamFromBox();
}
public   void removeUpdate(DocumentEvent e)
{
//System.out.println(getClass()+" remove ");
actualiseParamFromBox();
}
//***********end interface DocumentListener ***************


//**********interface keyListener**********************
public void keyPressed(KeyEvent e)
{
switch (e.getKeyCode())
	{
	case KeyEvent.VK_ENTER :
// 	System.out.println(getClass()+" VK_ENTER ");
	//if (triggerList!=null) triggerList.trig();
	//launchTheNetwork();
	jp.repaint();
	break;
	}

	
}

public void keyTyped(KeyEvent e){}
public void keyReleased(KeyEvent e){}

//**********end interface keyListener**********************



public void mouseWheelMoved(MouseWheelEvent e)
{


}

private void actualiseParamFromBox()
{
//System.out.println(getClass()+" actualiseParamFromBox : "+val.getText()+" (autoUpdate="+autoUpdate+")");
if (p==null) return;
if (!ParamBox.autoUpdate)return;
p.setVal(val.getText());
////if the object that have this parameter is a MysqlLine, update the database:
//if (p.getParamOwner() instanceof MysqlLine) 
//	{
//	MysqlLine l=(MysqlLine)p.getParamOwner();
//	l.getTable().updateSQL(l,p.getParamName());
//	}
if (triggerList!=null) if (triggerEnabled)triggerList.trig();
}



public Component getComponent(){return jp;}

public String getParamName()
{
return p.getParamName();
}

public void setEnabled(boolean b)
{
val.setEnabled(b);
name.setEnabled(b);
labelUnit.setEnabled(b);
}

public Param getParam(){return p;}

}