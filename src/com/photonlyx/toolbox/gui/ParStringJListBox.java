package com.photonlyx.toolbox.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;

/**
 * 
 * @author laurent
 *
 * List of selectable strings
 *
 */
public class ParStringJListBox extends ParamBox implements  MouseListener
{
	private JPanel jp;
	private JLabel name;
	private TriggerList triggerList;//list of object to update when this parameter is changed
	private ParString p;//list of the checked boxes names
	private DefaultListModel<String> listModel = new DefaultListModel<String>();
	private JList<String> jlist=new JList<String>(listModel);
	private String rightClickselected=null;
	private boolean multipleSelections=true;

	public ParStringJListBox(TriggerList tl,ParString p)
	{
		jp=new JPanel(new BorderLayout());
		this.triggerList=tl;	
		this.p=p;
		init();
	}


	public ParStringJListBox(TriggerList tl,ParString p,String label)
	{
		jp=new JPanel(new BorderLayout());
		this.triggerList=tl;	
		this.p=p;
		init();
		name.setText(label);
	}


	public void init()
	{
		jp.removeAll();
		jp.setSize(getW(),getH());
		jp.setPreferredSize(new Dimension(getW(),getH()));
		jp.setBackground(this.getBackground());

		name=new JLabel();
		name.setFont(this.getFont());
		name.setBackground(this.getBackground());

		jlist.setFont(Global.font);
		jlist.setBackground(Global.background);
		jlist.addMouseListener(this);
		

		jp.add(name);
		jp.add(jlist);
	}

	public void setRightClickSelection()
	{
		jlist.addMouseListener( new MouseAdapter()
		{
		    public void mousePressed(MouseEvent e)
		    {
		        if ( SwingUtilities.isRightMouseButton(e) )
		        {
		            JList list = (JList)e.getSource();
		            int row = list.locationToIndex(e.getPoint());
		            list.setSelectedIndex(row);
		        }
		    }

		});	
	}


	public void addItem(String name)
	{
		listModel.addElement(name);
	}


	//select the item corresponding to the param having the selected value
	public void synchronizeToParam()
	{
		System.out.println(getClass()+" synchronizeToParam:"+p.val());
		setSelected(p.val());
	}

	public void setSelected(String s)
	{
		//for (JCheckBox jrb:cbs) if (jrb.getActionCommand().compareTo(s)==0) jrb.setSelected(true);
		jlist.setSelectedValue(s, true);
	}


	public void actionPerformed(ActionEvent e)
	{
		//if (e.getSource()==jlist)
		{
		}
	}




	@Override
	public Component getComponent()
	{
		return jp;
	}

	@Override
	public void update()
	{
		synchronizeToParam();
	}

	@Override
	public String getParamName()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setEnabled(boolean b)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public Param getParam()
	{
		// TODO Auto-generated method stub
		return null;
	}




	public void setBackGround(Color c)
	{
		jp.setBackground(c);
		jlist.setBackground(c);
	}


	public boolean isMultipleSelections() {
		return multipleSelections;
	}


	public void setMultipleSelections(boolean multipleSelections) 
	{
		this.multipleSelections = multipleSelections;
		if (multipleSelections) jlist.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		else jlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}


	@Override
	public void setEditable(boolean b) {
		// TODO Auto-generated method stub

	}


	@Override
	public void setFieldLength(int l) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseClicked(MouseEvent e) 
	{
		//System.out.println(e.getClass());
		if (e.getButton()==MouseEvent.BUTTON3)
		{
			//System.out.println(getClass()+" mouse clicked");
			List<String> l=jlist.getSelectedValuesList();
			String list="";
			for (String s:l) list+=s+" ";
			list=list.substring(0, list.length()-1);
			p.setVal(list);
			if (triggerList!=null) triggerList.trig();
			System.out.println(getClass()+" liste: "+list);
		}
	}


	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}


}
