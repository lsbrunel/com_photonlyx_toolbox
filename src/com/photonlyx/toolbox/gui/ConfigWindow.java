package com.photonlyx.toolbox.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.JPanel;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.util.Global;

/**
 * pop up window to show configuration parameters
 * @author laurent
 *
 */
public class ConfigWindow 
{
private CJFrame cjf;	
private JPanel mainPanel;
private int w=200,h=450;
private String title="Config";
private Vector<Component> list=new Vector<Component>();

public ConfigWindow()
{

	create();
}
public ConfigWindow(int w,int h)
{
	this.w=w;
	this.h=h;

	create();
}



private void create()
{

cjf=new CJFrame();
cjf.setSize(w, h);
cjf.setResizable(false);
cjf.setLocation(100, 100);
cjf.setBackground(Global.background);
cjf.setTitle(title);
cjf.setFont(Global.font);
cjf.getContentPane().setBackground(Global.background);
cjf.getContentPane().setLayout(new BorderLayout());
cjf.setExitAppOnExit(false);
cjf.setVisible(true);	

mainPanel=new JPanel();
//mainPanel.setLayout(new GridLayout(0, 1));
mainPanel.setLayout(new FlowLayout());
mainPanel.setBackground(Global.background);

cjf.getContentPane().add(mainPanel,BorderLayout.CENTER);
mainPanel.validate();
cjf.validate();
}


public void update()
{
mainPanel.removeAll();	
for (Component c:list) mainPanel.add(c);
mainPanel.validate();
cjf.validate();

}

public JPanel getMainPanel()
{
return mainPanel;
}

public void add(Component c)
{
	list.add(c);
}

public void removeAll()
{
	list.removeAllElements();
}

public void showHide()
{
if (cjf==null) create();
//change the visibility state :
cjf.setVisible(!cjf.isVisible());
if (cjf.isVisible()) update();
}


public CJFrame getCJFrame()
{
return cjf;
}


public void setSize(int i, int j) 
{
w=i;
h=j;
if (cjf!=null) cjf.setLocation(w, h);
}


public void setTitle(String string) 
{
title=string;	
if (cjf!=null) cjf.setTitle(title);
}



}
