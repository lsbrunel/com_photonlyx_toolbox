package com.photonlyx.toolbox.gui;

import java.awt.Component;


public interface PaletteInterface
{
/**
return the RGB code for a value
*/
public int getRGB(double val);

/**set the min possible value*/
public void setVmin(double d);

/**set the max possible value*/
public void setVmax(double d);
public void setType(int t);

public Component getVisualisation();

public void addListener(PaletteChangedListener l);

}
