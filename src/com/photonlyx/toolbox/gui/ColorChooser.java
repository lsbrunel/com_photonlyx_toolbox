package com.photonlyx.toolbox.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JComponent;
import javax.swing.JPanel;


public class ColorChooser  implements MouseListener,MouseWheelListener
{
private ColorApplier ca;
//params
private int colorChoosed;
//internal
private float br=1;//brightness
private int bandSize=20;
private JPanel panel=new JPanel()
{
public void paintComponent(Graphics g)
	{
	for (int i=bandSize;i<getSize().width;i++)
		for (int j=0;j<getSize().height;j++)
			{
			float sat=(1-(float)(i-bandSize)/(float)(getSize().width-bandSize));
			float hue=(1-(float)j/(float)getSize().height);
			Color coul=new Color(Color.HSBtoRGB(hue,sat,br));
			g.setColor(coul);
			g.drawRect(i,j,1,1);
			}
	g.setColor(new Color(colorChoosed));
	g.fillRect(0,0,bandSize,getSize().height);
	
	g.setColor(Color.black);
	Color c=new Color(colorChoosed);
	int r=c.getRed();
	int gr=c.getGreen();
	int b=c.getBlue();
	g.drawString(r+" "+gr+" "+b, 20, 20);
	}
	
	}
	;

public ColorChooser()
{
panel.addMouseListener(this);
panel.addMouseWheelListener(this);
}

public JComponent getComponent() {return panel;}
public Point getInitLocation(){return null;}
public Dimension getInitSize(){return null;}
public void setEnabled(boolean enabled){}


public void setColorApplier(ColorApplier ca)
{
this.ca=ca;
}

public void mouseClicked(MouseEvent e) 
{
float sat=1-(float)(e.getPoint().x-bandSize)/(float)(panel.getWidth()-bandSize);
float hue=1-(float)e.getPoint().y/(float)panel.getHeight();
int c=Color.HSBtoRGB(hue,sat,br);
colorChoosed=c;
panel.repaint();
ca.applyColor(ColorUtil.color2String(new Color(colorChoosed)));
}




public void mouseEntered(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}

public void mouseExited(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}

public void mousePressed(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}

public void mouseReleased(MouseEvent arg0) {
	// TODO Auto-generated method stub
	
}




public void mouseWheelMoved(MouseWheelEvent e) 
{
//af(e.getWheelRotation());
br+=e.getWheelRotation()/30.;
if (br>1) br=0;
if (br<0) br=1;
//af(br);
panel.repaint();
}


}
