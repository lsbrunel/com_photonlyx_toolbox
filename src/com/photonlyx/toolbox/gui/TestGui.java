package com.photonlyx.toolbox.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JPanel;

public class TestGui extends WindowApp
{
	private JPanel jp=new JPanel();

	private double double1=1000;
	private double double2=2000;
	private String string1="toto";
	private String string2="toto";
	public String[] string2Options={"toto1","toto2"};
	private boolean bool1=true;
	private int int1=2;
	private int int2=4;
	
	private int dimx=100,dimy=100;//size of scanning matrix
	private double w=2.2,h=1.6;//size of area scanned
	private int nbScans=10;//nb of scans acquired
	
	private  int ROI=1024;//size of the region of interest (a square),unbinned
	private  int rawBinning=4;
	private double pause_ms=80;//exposure time to take an image

	public TestGui()
	{
		jp.setPreferredSize(new Dimension(500,500));
		this.add(jp,"test");
		jp.setLayout(new FlowLayout());
		{
			int w1=300;
			int h=25;
			String[] names={"w","h","dimx","dimy","nbScans"};
			String[] units={"mm","mm","pts","pts","scans"};
			ParamsBox paramBoxMatrix=new ParamsBox(this,null,"scanner",names,null,units,ParamsBox.VERTICAL,w1-20,h);
			paramBoxMatrix.setEditable(false);
			jp.add(paramBoxMatrix.getComponent());
		}
		{
			String[] names={"ROI","rawBinning","pause_ms"};
			String[] labels={"ROI","binning","exp"};
			String[] units={"pix","","ms"};
			ParamsBox paramsBoxPausems=new ParamsBox(this,null,null,names,labels,units,ParamsBox.HORIZONTAL,120,30);
			jp.add(paramsBoxPausems.getComponent());
		}
		{
			String[] names1={"double1","string1","string2","bool1","int1","int2"};
			String[] units1={"mm",     "unit1",   "unit2",  "ceci","caféeeecccce","jus"};
			ParamsBox pb=new ParamsBox(this,null,"Test",names1,null,units1,ParamsBox.VERTICAL,200,30);
			pb.setBackground(Color.GREEN);
			jp.add(pb);
		}
		{
			String[] names1={"double1","double2","string1"};
			ParamsBox pb=new ParamsBox(this,null,"Test",names1,null,null,ParamsBox.HORIZONTAL,150,30);
			pb.setBackground(Color.RED);
			jp.add(pb);
		}
		{
			String[] names1={"double1","int1","int2"};
			ParamsBox pb=new ParamsBox(this,null,"Test",names1,null,null,ParamsBox.HORIZONTAL,120,30);
			pb.setBackground(Color.BLUE);
			jp.add(pb);
		}

		jp.validate();
	}




	public double getDouble1() {
		return double1;
	}

	public void setDouble1(double double1) 
	{
		System.out.println("setDouble1 to "+double1);
		this.double1 = double1;
	}

	public double getDouble2() {
		return double2;
	}

	public void setDouble2(double double2) {
		this.double2 = double2;
	}

	public String getString1() {
		return string1;
	}

	public void setString1(String string1) {
		System.out.println("setString1 to "+string1);
		this.string1 = string1;
	}

	public String getString2() {
		return string2;
	}




	public void setString2(String string2) 
	{
		System.out.println("setString2 to "+string2);
		this.string2 = string2;
	}




	public boolean isBool1() {
		return bool1;
	}

	public void setBool1(boolean bool1) {
		System.out.println("setBool1  to "+bool1);
		this.bool1 = bool1;
	}

	public int getInt2() {
		return int2;
	}

	public void setInt2(int int2) {
		this.int2 = int2;
	}

	public int getInt1() {
		return int1;
	}

	public void setInt1(int int1) {
		System.out.println("setInt1 to "+int1);
		this.int1 = int1;
	}

	
	
	
	
	
	
	
	
	
	
	public int getDimx() {
		return dimx;
	}




	public void setDimx(int dimx) {
		this.dimx = dimx;
	}




	public int getDimy() {
		return dimy;
	}




	public void setDimy(int dimy) {
		this.dimy = dimy;
	}




	public double getW() {
		return w;
	}




	public void setW(double w) {
		this.w = w;
	}




	public double getH() {
		return h;
	}




	public void setH(double h) {
		this.h = h;
	}




	public int getNbScans() {
		return nbScans;
	}




	public void setNbScans(int nbScans) {
		this.nbScans = nbScans;
	}




	public int getROI() {
		return ROI;
	}




	public void setROI(int rOI) {
		ROI = rOI;
	}




	public int getRawBinning() {
		return rawBinning;
	}




	public void setRawBinning(int rawBinning) {
		this.rawBinning = rawBinning;
	}




	public double getPause_ms() {
		return pause_ms;
	}




	public void setPause_ms(double pause_ms) {
		this.pause_ms = pause_ms;
	}




	public static void main(String args[])
	{
		WindowApp app=new TestGui();
	}

}
