
// Author Laurent Brunel


package com.photonlyx.toolbox.gui;

import java.awt.Dimension;
import java.awt.event.*;
import java.net.URL;

import javax.swing.*;

import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;




/**
A button that allow to launch the work methods of all the subNetworks sons
*/

public  class CButton extends JButton implements ActionListener
{
private TriggerList triggerList;//list of object to update when this parameter is changed

public  CButton(TriggerList triggerList)
{
this.triggerList=triggerList;
addActionListener(this);
this.setSize(new Dimension(140, 30));
this.setPreferredSize(new Dimension(140, 30));
this.setFont(Global.font);
}

public TriggerList getTriggerList() {
	return triggerList;
}

public void setTriggerList(TriggerList triggerList) {
	this.triggerList = triggerList;
}

public void actionPerformed(ActionEvent e)
{
Object source = e.getSource();
if (this==source)
	{
	if (triggerList!=null) triggerList.trig();
	}
}

public void setIconFileName(String iconFileName)
{
//	this.iconFileName=iconFileName;
ClassLoader cl=getClass().getClassLoader();
URL url=cl.getResource(iconFileName);
Icon icon=new ImageIcon(url);
if (icon==null) Messager.messErr("Problem to find resource "+iconFileName);
this.setIcon(icon);
}


}
