package com.photonlyx.toolbox.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.photonlyx.toolbox.param.ParString;
import com.photonlyx.toolbox.param.Param;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;

/**
 * 
 * @author laurent
 *
 * List of checkable boxes. Right click can select one (only one) item: background color is changed
 *
 */
public class ParStringCheckBoxesBox extends ParamBox implements  ActionListener,MouseListener
{
	private JPanel jp;
	//private JLabel name;
	private String label;
	private TriggerList triggerList;//list of object to update when this parameter is changed
	private ParString param;//list of the checked boxes names
	private Vector<JCheckBox> cbs=new Vector<JCheckBox>();
	private int nbColumns=1;
	private String rightClickselected=null;
	private boolean rightButtonSelectable=false;
	private Color background=Global.background;
	private Color selectedBackground=new Color(200,200,200);
	private char spacer=' ';

	public ParStringCheckBoxesBox(TriggerList tl,ParString p)
	{
		nbColumns=1;
		jp=new JPanel(new GridLayout(0, nbColumns));
		this.triggerList=tl;	
		this.param=p;
		init();
	}

	public ParStringCheckBoxesBox(TriggerList tl,ParString p,int nbColumns)
	{
		this.nbColumns=nbColumns;
		jp=new JPanel(new GridLayout(0, nbColumns));
		this.triggerList=tl;	
		this.param=p;
		init();
	}

	public ParStringCheckBoxesBox(TriggerList tl,ParString p,String label)
	{
		nbColumns=1;
		jp=new JPanel(new GridLayout(0, 1));
		this.triggerList=tl;	
		this.param=p;
		this.label=label;
		init();
	}


	public void init()
	{
		jp.removeAll();
		jp.setSize(getW(),getH());
		jp.setPreferredSize(new Dimension(getW(),getH()));
		jp.setBackground(this.getBackground());

		if (label!=null)
		{
			JLabel name=new JLabel(label);
			name.setFont(this.getFont());
			name.setBackground(this.getBackground());
			jp.add(name);
		}
	}

	/**
	 * 
	 * @param name name of the check box
	 * @param tip tip to show
	 */
	public void addItem(String name,String tip)
	{

		JCheckBox jcb=new JCheckBox(name);
		jcb.setFont(this.getFont());
		jcb.setBackground(background);
		jcb.addActionListener(this);
		jcb.addMouseListener(this);
		//jcb.addItemListener(this);
		//group.add(jcb);
		cbs.add(jcb);
		//jp.setPreferredSize(new Dimension(getW(),getH()*(int)(cbs.size()*1.2/nbColumns)));
		jp.setPreferredSize(new Dimension(getW(),cbs.size()*20));
		jp.add(jcb);
		jp.validate();
		update();
		jcb.setToolTipText(tip);
	}

	public void addItem(String name)
	{
		addItem(name,null);
	}

	public void removeAllItems()
	{
		cbs.removeAllElements();
		jp.removeAll();
		{
			JLabel name=new JLabel(label);
			name.setFont(this.getFont());
			name.setBackground(this.getBackground());
			jp.add(name);
		}
	}

	//select the item corresponding to the param having the selected value
	public void synchronizeToParam()
	{
		for (JCheckBox jcb:cbs) 
		{
			if (jcb.getActionCommand().compareTo(param.stringVal())==0) 
			{
				//System.out.println(s+" match with param:"+p.stringVal());
				jcb.setSelected(true);
			}
		}
	}

	public void setSelected(String s)
	{
		for (JCheckBox jrb:cbs) if (jrb.getActionCommand().compareTo(s)==0) jrb.setSelected(true);
	}


	public void actionPerformed(ActionEvent e)
	{
		//System.out.println(e.getActionCommand());
		//selected=e.getActionCommand();
		String list="";
		for (JCheckBox cb:cbs)
		{
			if (cb.isSelected()) 
			{
				list+=cb.getText()+spacer;
			}
		}
		param.setVal(list);
		System.out.println(getClass()+" "+list);
		//System.out.println(selected);
		if (triggerList!=null) triggerList.trig();
	}




	@Override
	public Component getComponent()
	{
		return jp;
	}

	@Override
	public void update()
	{
		synchronizeToParam();
	}

	@Override
	public String getParamName()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setEnabled(boolean b)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public Param getParam()
	{
		// TODO Auto-generated method stub
		return null;
	}



	public void setEditable(boolean editable) {

	}

	public void setFieldLength(int fieldLength) {

	}

	public String getRightClickSelected()
	{
		return rightClickselected;
	}

	/**set "right click selection" to one item*/
	public void rightClickSelect(String s)
	{
		for (JCheckBox jcb:cbs) 
		{
			if (jcb.getText().compareTo(s)==0) 
			{
				for (JCheckBox jcb1:cbs) jcb1.setBackground(Global.background);
				jcb.setBackground(new Color(235,235,235));
				rightClickselected=s;
			}
		}
	}

	public char getSpacer() {
		return spacer;
	}

	public void setSpacer(char c) {
		this.spacer = c;
	}

	public Vector<JCheckBox> getJCheckBoxesList()
	{
		return cbs;
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		if (rightButtonSelectable) if ((e.getButton()==MouseEvent.BUTTON3)) 
		{
			JCheckBox jcb=(JCheckBox)e.getSource();
			rightClickSelect(jcb.getText());
			//System.out.println(getClass()+" selected="+selected);
			if (triggerList!=null) triggerList.trig();
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	public boolean isRightButtonSelectable() {
		return rightButtonSelectable;
	}

	public void setRightButtonSelectable(boolean rightButtonSelectable) {
		this.rightButtonSelectable = rightButtonSelectable;
	}

	public void setBackGround(Color c)
	{
		background=c;
		jp.setBackground(c);
		for (JCheckBox jcb:cbs) jcb.setBackground(c);
	}


}
