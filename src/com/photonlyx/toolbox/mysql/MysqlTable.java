package com.photonlyx.toolbox.mysql;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;
import java.util.Vector;

import com.photonlyx.toolbox.util.Util;

public abstract class MysqlTable implements Iterable<MysqlRow>
{
	private MysqlDb db;
	private Vector<MysqlFieldTypes> fieldsList=new Vector<MysqlFieldTypes>();
	private String tableName="";
	private Vector<MysqlRow> lines=new Vector<MysqlRow>();

	public MysqlTable(MysqlDb db,String tableName)
	{
		this.db=db;
		this.tableName=tableName;
	}

	public MysqlDb getDb() {
		return db;
	}

	public abstract MysqlRow getRowInstance();


	public Vector<MysqlRow> getList() {
		return lines;
	}

	public Iterator<MysqlRow> iterator() {
		return lines.iterator();
	}

	public Vector<MysqlFieldTypes> getFieldsList() 
	{
		return fieldsList;
	}

	public String getTableName() 
	{
		return tableName;
	}

	public int size()
	{
		return lines.size();
	}

	public void addRow(MysqlRow l)
	{
		this.addRow(l, false);
	}

	public void addRow(MysqlRow l,boolean updateSQL)
	{
		lines.add(l);
		l.setTable(this);
		//l.setDBnumber(getMaxDBnumber()+1);//the first member has number 0
		if (updateSQL) insertSQL(l);
	}

	public void addRowAndCreateNewDBnumber(MysqlRow l)
	{
		l.setDBnumber(getMaxDBnumber()+1);//the first member has number 0
		this.addRow(l, true);
	}

	public void removeRow(MysqlRow l)
	{
		lines.remove(l);
		removeFromTableSQLcommand(l);
	}

	public void removeAllElements()
	{
		lines.removeAllElements();
	}

	public MysqlRow getRow(int index)
	{
		if ((index<0)||(index>lines.size()-1)) return null;
		return lines.elementAt(index);
	}

	public void createTableSQL() 
	{
		String sql = "CREATE TABLE  "+tableName ;
		sql+=" ( ";
		for (MysqlFieldTypes f:fieldsList)
			if (fieldsList.lastElement()==f) sql+=f.getName()+" "+f.getType();
			else sql+=f.getName()+" "+f.getType()+" ,";
		sql+=" ) ";
		executeSQL(sql);		
	}

	public void dropTableSQL() 
	{
		String sql = "DROP TABLE  "+tableName ;
		executeSQL(sql);		
	}

	public void addColumn(String columnName,String type)
	{
		// ALTER TABLE vendors
		// ADD COLUMN phone VARCHAR(15) AFTER name;	
		String sql = "ALTER TABLE  "+tableName ;
		sql += " ADD "+columnName+" "+type+";";
		executeSQL(sql);
	}


	public void insertSQL(MysqlRow l)
	{
		if (db.getConnection()==null) 
		{
			//db.addToWaitingList(l);
			return;
		}
		if (!db.isEnable()) return;
		String sql = "INSERT INTO "+tableName+" VALUES (";
		for (MysqlFieldTypes f:fieldsList)
		{	
			String val=getVal(l,f.getName());
			//System.out.println("field "+f.getName() +" val ="+val);
			if ( val.contains("'") ) 
			{
				val=val.replaceAll("'", "''");
				//System.out.println(val);
			}
			if (f==fieldsList.lastElement()) sql+= "'"+val+"' ";
			else sql+= "'"+val+"', ";
		}
		sql+= " )";
		executeSQL(sql);		
		System.out.println(sql);
	}



	public void removeFromTableSQLcommand(MysqlRow l) 
	{
		String sql = "DELETE FROM "+tableName+" WHERE  dbNumber='"+getVal(l,"dBnumber")+"'";
		executeSQL(sql);
	}

	public MysqlRow getLineHavingDBNumber(int id)
	{
		for (MysqlRow l:this) if (l.getdBnumber()==id) return l;
		return null;
	}

	/**
	 * get the value of the java attribute via java.lang.Reflect
	 * @param l
	 * @param fieldName
	 * @return
	 */
	public  String getVal(MysqlRow l,String fieldName)
	{
		String val="";
		String type=Util.getType(l,fieldName).getSimpleName();
		if (type.compareTo("String")==0 )
		{
			val=(String)Util.getMethodGetVal(l,fieldName);
		}
		else if ((type.compareTo("int")==0 )  ||(type.compareTo("Integer")==0 ))
		{
			val=""+(Integer)Util.getMethodGetVal(l,fieldName);
		}
		else if ((type.compareTo("long")==0 )  )
		{
			val=""+(long)Util.getMethodGetVal(l,fieldName);
		}
		else if ((type.compareTo("LocalDateTime")==0 )  )
		{
			val=""+Util.getMethodGetVal(l,fieldName);
		}
		else if (type.compareTo("double")==0 )
		{
			val=""+(Double)Util.getMethodGetVal(l,fieldName);
		}
		else if (type.compareTo("boolean")==0 )
		{
			//boolean b=(Boolean)Util.getBoolVal(l,fieldName);
			boolean b=(Boolean)Util.getMethodGetVal(l,fieldName);
			if (b) val="1";else val="0";
		}
		return val;
	}

	//	public  String getUpdateSQLcommand(MysqlRow l,String fieldName) 
	//	{
	//		String sql = "UPDATE "+tableName;
	//		sql+=" SET ";
	//		sql+=" "+fieldName+ "=  '"+getVal(l,fieldName)+"' ";
	//		sql+="WHERE dBnumber= '"+getVal(l,"dBnumber")+"' ";
	//		return sql;	
	//	}

	public void executeSQL(String sqlCommand)
	{
		if (db.getConnection()==null) return;
		if (!db.isEnable()) return;
		try
		{
			Statement stmt = db.getConnection().createStatement();	
			System.out.println(sqlCommand);
			stmt.executeUpdate(sqlCommand);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public  void updateSQL(MysqlRow l,String fieldName)
	{
		if (db.getConnection()==null) 
		{
			//db.addToWaitingList(l);
			return;
		}
		if (!db.isEnable()) return;
		String fieldValue=getVal(l,fieldName);
		String dbNumber=getVal(l,"dBnumber");
		//remove "'" character:
		fieldValue=fieldValue.replaceAll("'","_");
		String sql = "UPDATE "+tableName;
		sql+=" SET ";
		sql+=" "+fieldName+ "=  '"+fieldValue+"' ";
		sql+="WHERE dBnumber= '"+dbNumber+"' ";
		try
		{
			Statement stmt = db.getConnection().createStatement();	
			//String sql=getUpdateSQLcommand(l,fieldName);
			System.out.println(sql);
			stmt.executeUpdate(sql);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}


	public  void updateSQL(MysqlRow l)
	{
		if (db.getConnection()==null) 
		{
			//db.addToWaitingList(l);
			return;
		}
		if (!db.isEnable()) return;
		for (MysqlFieldTypes f:this.getFieldsList()) 
		{
			String fieldName=f.getName();
			String fieldValue=getVal(l,fieldName);
			String dbNumber=getVal(l,"dBnumber");
			//remove "'" character:
			fieldValue=fieldValue.replaceAll("'","_");
			String sql = "UPDATE "+tableName;
			sql+=" SET ";
			sql+=" "+fieldName+ "=  '"+fieldValue+"' ";
			sql+="WHERE dBnumber= '"+dbNumber+"' ";
			try
			{
				Statement stmt = db.getConnection().createStatement();	
				//String sql=getUpdateSQLcommand(l,fieldName);
				System.out.println(sql);
				stmt.executeUpdate(sql);
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
	}

	public void selectSQL1()
	{
		if (db.getConnection()==null) return ;
		try
		{
			Statement stmt =  db.getConnection().createStatement();
			String query = "SELECT "; //"SELECT id, first, last, age FROM Registration";
			for (MysqlFieldTypes f:this.getFieldsList()) 
			{
				if (f==this.getFieldsList().lastElement()) query+=" "+f.getName()+" ";
				else query+=" "+f.getName()+", ";
			}
			query+=" FROM "+this.getTableName();	
			System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			int c=0;
			while(rs.next())
			{
				//if (fieldType)
				for (MysqlFieldTypes f:this.getFieldsList())
				{
					String fieldName=f.getName();
					String fieldType=f.getType();
					System.out.print("("+c+")   "+rs.getObject(fieldName)+"  ");
				}
				System.out.println();
				c++;
			}		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return ;
	}

	/**
	 * get all fields of name fieldName
	 * @param fieldName
	 * @return
	 */
	public Vector<Object> selectFieldOfAllRowsSQL(String fieldName)
	{
		Vector<Object> list=new Vector<Object>();
		if (db.getConnection()==null) return list;
		try
		{
			Statement stmt =  db.getConnection().createStatement();
			String query = "SELECT "; //"SELECT id, first, last, age FROM Registration";
			query+=" "+fieldName+" ";
			query+=" FROM "+this.getTableName();	
			System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			int c=0;
			while(rs.next())
			{
				//System.out.println("["+c+"]   "+rs.getObject(fieldName)+"  ");
				list.add(rs.getObject(fieldName));
				c++;
			}		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return list;
	}


	public int getMaxDBnumber()
	{
		int n=-1;
		for (MysqlRow c:lines) if (c.getdBnumber()>n)n=c.getdBnumber();
		return n;
	}

	public int getIndexOf(MysqlRow p)
	{
		return lines.indexOf(p);
	}



	/**
	 * get all lines of the table
	 * @param fieldName
	 * @return
	 */
	public Vector<Object> fillTableFromSQLdb()
	{
		//System.out.println(getClass()+" fillTableFromSQLdb");
		this.removeAllElements();

		Vector<Object> list=new Vector<Object>();
		if (db.getConnection()==null) return list;
		try
		{
			Statement stmt =  db.getConnection().createStatement();
			String query = "SELECT "; //"SELECT id, first, last, age FROM Registration";
			query+=" * ";
			query+=" FROM "+this.getTableName();	
			//System.out.println(query);
			ResultSet rs = stmt.executeQuery(query);
			this.getDb().setEnableSQL(false);//avoid update of mysql db
			while(rs.next())
			{
				//create raw:
				//System.out.println("Create row  instance of "+this.getTableName());
				MysqlRow l=this.getRowInstance();
				l.setTable(this);
				for (MysqlFieldTypes f:this.getFieldsList()) 
				{
					Object val=rs.getObject(f.getName());
					//System.out.println("field: "+f.getName()+" val in sql="+val+ " class:"+val.getClass().getName());
					try 
					{
						Util.setVal(l,f.getName(),val);
					}
					catch (Exception e)
					{
						System.out.println("field: "+f.getName()+" val in sql="+val+ " class:"+val.getClass().getName());
					}
					//					System.out.println(l.toString());
				}
				//System.out.println(l.toString());
				this.addRow(l,false);
			}
			this.getDb().setEnableSQL(true);//restore mysql db update
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return list;
	}




}