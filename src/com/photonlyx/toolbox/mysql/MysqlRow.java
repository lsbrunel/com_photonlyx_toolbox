package com.photonlyx.toolbox.mysql;

import com.photonlyx.toolbox.util.Util;

public  class MysqlRow 
{
	private MysqlTable table;
	private int dBnumber;
	
	@Deprecated
	private int index;

	public MysqlTable getTable() 
	{
		return table;
	}


	public void setTable(MysqlTable table) 
	{
		this.table = table;
	}
	
	
	public int getdBnumber() 
	{
		return dBnumber;
	}
	
	public void setDBnumber(int i)
	{
		this.dBnumber = i;
	}

	public void setdBnumber(int i)
	{
		this.setDBnumber(i);
	}

	
	
	//
//@Deprecated
//	public void setIndex(int i) 
//	{
//	index=i;	
//	}
//

public String toString()
{
	String s="";
	for (MysqlFieldTypes f:table.getFieldsList()) 
	{
		s+=f.getName()+"="+Util.getMethodGetVal(this,f.getName())+",";
	}
	return s;
}


}
