package com.photonlyx.toolbox.thread;

import com.photonlyx.toolbox.gui.TaskButton;

public interface CTask extends Runnable
{
public void setStopped(boolean b);
public boolean isStopped() ;
public void setButton(TaskButton button);
}


