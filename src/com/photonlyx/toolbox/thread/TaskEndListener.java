package com.photonlyx.toolbox.thread;

public interface TaskEndListener 
{
	/**
	 * action when the task is completed
	 */
	public void whenTaskFinished();


}
