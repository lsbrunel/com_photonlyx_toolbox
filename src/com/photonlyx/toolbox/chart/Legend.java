//Author Laurent Brunel

package com.photonlyx.toolbox.chart;


import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.util.Date;
import java.util.Vector;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.*;

import com.photonlyx.toolbox.gui.ColorUtil;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;



/**
gives the list of the plot's labels with the line color and optionally the comments
 */
public class Legend  
{
	//links
	private Chart chart;//the Chart to show the legend
	//in
	private	int w=100;//width of legend
	private Color background=Global.background;//"white";//background colour
	private boolean seeComments=false;//boolean to see/not see   the comments of the plots
	private boolean seeStyle=true;//boolean to see/not see   the style of the plots
	private boolean seeHeader=false;//boolean to see/not see   the header
	private int nbColumnsLabel=30;//the nb of columns for the label box
	private int nbColumnsComment=50;//the nb of columns for the comment box
	private int labelHeight=10;//the label heigth
	private String labelColumnLabels="Label";//the label to put at the top of the column of the labels (the first)
	private String labelColumnComments="Comment";//the label to put at the top of the column of the comments (the 2nd)
	private String fontName="Arial";//font name
	private int fontsize=12;//font size
	private boolean commentEditable=true;
	private boolean labelEditable=true;
	private boolean seeFilter=false;
	private FilterField filterField=new FilterField(this);

	//internal
	private JPanel panelList=new JPanel();//the panelList with the list of curves
	private JScrollPane sp= new JScrollPane(panelList);
	private Font font=null;
	private boolean shiftPressed=false;
	private boolean controlPressed=false;
	//private int previousSelectedIndex=-1;
	private LegendField previousSelectedLegendField;
	//private LegendField[] plotFields;
	private Vector<LegendField> plotFields=new Vector<LegendField>();
	//listeners
	private Vector<LegendModifiedListener> legendModifiedListener=new Vector<LegendModifiedListener>();

	//	public  Legend()
	//	{
	//			panelList.setLayout(new BoxLayout(panelList,BoxLayout.Y_AXIS));
	//		panelList.setBackground(Global.background);
	//		sp.setBackground(Global.background);
	//		updateGraphics();
	//		font=new Font(fontName,java.awt.Font.PLAIN,fontsize);
	//	}
	//

	public  Legend(Chart chart)
	{
		this.chart=chart;	
		panelList.setLayout(new BoxLayout(panelList,BoxLayout.Y_AXIS));
		panelList.setBackground(Global.background);
		sp.setBackground(Global.background);
		updateGraphics();
		font=new Font(fontName,java.awt.Font.PLAIN,fontsize);
		Border empty = BorderFactory.createEmptyBorder();
		sp.setBorder(empty);

	}


	public Chart getChart() {
		return chart;
	}


	public void setChart(Chart chart)
	{
		this.chart=chart;	
		chart.setLegend(this);
	}

	/**
	 * return the top component
	 * @return
	 */
	public Component getMainPanel()
	{
		return sp;
	}

	/**
	 * return the top component
	 * @return
	 */
	public Component getComponent()
	{
		return sp;
	}


	public void update()
	{
		updateGraphics();	
	}

	public void updateGraphics()
	{
		if (chart==null) return;
		//previousSelectedIndex=-1;
		previousSelectedLegendField=null;
		//System.out.println(getClass()+" updateGraphics");
		panelList.removeAll();
		PlotSet ps=chart.getConcatenatedPlots();

		//add the filter
		if (seeFilter)
		{
			panelList.add(filterField);
		}

		//add the header if needed
		if (seeHeader)
		{
			LegendField titles;
			if ((labelColumnLabels!=null)&(labelColumnComments!=null))
				titles=new LegendField(labelColumnLabels,labelColumnComments,Color.black,background,ps,-1,this);
			else titles=new LegendField(Messager.getString("Name"),Messager.getString("Comment"),Color.black,background,ps,-1,this);
			panelList.add(titles);
			titles.setEditable(false);
		}


		// System.out.println(getClass()+" "+"print legend for "+faisceau.nbPlots()+" plots");
		//plotFields=new LegendField[ps.nbPlots()];
		plotFields.removeAllElements();
		for (int i=0;i<ps.nbPlots();i++)
		{
			String label=ps.getPlot(i).getLabel();
			if (!filterField.accept(ps.getPlot(i))) 
			{
				//ps.getPlot(i).setVisibility(Plot.INVISIBLE);
				ps.getPlot(i).setVisible(false);
				continue;
			}
			else 
			{
				ps.getPlot(i).setVisible(true);
			}
			String c=ps.getPlot(i).getComment();
			Color color=ColorUtil.getColor(ps.getPlot(i).getDotColor());
			if (ps.getPlot(i).isGrayed()) color=ColorUtil.getColor("lightGray");
			LegendField lf=new LegendField(label,c,color,background,ps,i,this);
			panelList.add(lf);
			plotFields.add(lf);
			//plotFields[i]=lf;
			lf.setBold(ps.getPlot(i).isSelected());
			//System.out.println(getClass()+" "+"h of lf="+lf.getHeight());
		}
		//panelList.setPreferredSize(new Dimension(w-15,(ps.nbPlots()+1)*labelHeight));
		panelList.validate();
		sp.validate();
		sp.repaint();
	}




	/**
repaint the component
	 */
	public void repaint()
	{
		sp.repaint();
	}


	public void addLegendModifiedListener(LegendModifiedListener l)
	{
		legendModifiedListener.add(l);	
	}







	public boolean isSeeFilter() {
		return seeFilter;
	}



	public void setSeeFilter(boolean seeFilter) {
		this.seeFilter = seeFilter;
	}



	public void setFontName(String s)
	{
		this.font=new Font(s,font.getStyle(),font.getSize());
	}
	
	public String getFontName()
	{
		return font.getFontName();
	}

	public void setFontSize(int size)
	{
		this.font=font.deriveFont((float)size);
	}

	public int getFontSize()
	{
		return font.getSize();

	}




	/**
special field with in a line the label and the comment
	 */
	class LegendField extends JPanel implements DocumentListener,CaretListener,KeyListener,ActionListener,MouseListener
	{
		private JTextField label;
		private JTextField comment;
		private PlotSet f;//the associated collection of plots
		private int index;//the index of the associated plot in the Plot collection
		private Plot plot; //the plot associated
		private Legend legend;
		private JPopupMenu m;
		private JMenuItem[] popUpMenuItems;
		/**
		 */
		public LegendField(String slabel,String scomment,Color fcolor,Color bcolor,PlotSet f,int index,Legend legend)
		{
			this.f=f;
			this.index=index;
			this.legend=legend;
			this.plot=f.getPlot(index);
			label=new JTextField(slabel,nbColumnsLabel);
			label.setForeground(fcolor);
			label.setBackground(bcolor);
			if (font!=null) label.setFont(font);
			label.getDocument().addDocumentListener(this);
			label.addCaretListener(this);
			label.addKeyListener(this);
			label.setBorder(null);

			label.setPreferredSize(new Dimension(30,20));
			label.setSize(new Dimension(30,20));

			if (f.getPlot(index)!=null) 
			{
				if (f.getPlot(index).isSelected()) label.setFont(label.getFont().deriveFont(Font.BOLD));
				else label.setFont(label.getFont().deriveFont(Font.PLAIN));
			}
			label.setEditable(labelEditable);

			//label.validate();

			// System.out.println(getClass()+" create LegendField for index:"+index);
			// if (index!=-1) System.out.println(getClass()+" create LegendField:"+f.plot(index).label());

			setLayout(new BorderLayout());

			JPanel styleLabelPanel=new JPanel();
			styleLabelPanel.setSize(new Dimension(60,20));
			styleLabelPanel.setPreferredSize(new Dimension(60,20));
			styleLabelPanel.setLayout(new BorderLayout());

			if (seeStyle&(index!=-1))
			{
				styleLabelPanel.add(new StylePanel(f,index,fcolor,bcolor),BorderLayout.WEST);
			}

			styleLabelPanel.add(label,BorderLayout.CENTER);

			if (seeComments) add(styleLabelPanel,BorderLayout.WEST);
			else add(styleLabelPanel,BorderLayout.CENTER);

			if (seeComments)
			{
				comment=new JTextField(scomment,nbColumnsComment);
				comment.setForeground(fcolor);
				comment.setBackground(bcolor);
				if (font!=null) comment.setFont(font);
				comment.getDocument().addDocumentListener(this);
				comment.addCaretListener(this);
				comment.addKeyListener(this);
				comment.setBorder(null);
				//	add(comment);
				add(comment,BorderLayout.CENTER);
				//comment.validate();
				if (f.getPlot(index)!=null)
				{
					if (f.getPlot(index).isSelected()) comment.setFont(comment.getFont().deriveFont(Font.BOLD));
					else comment.setFont(comment.getFont().deriveFont(Font.PLAIN));
				}
				comment.setEditable(commentEditable);
			}
			else 
				{
				DateFormat df=DateFormat.getDateInstance(DateFormat.LONG);
				Plot p=f.getPlot(index);
				Date d=new Date(p.getDateEpochMs());
				label.setToolTipText(d+" "+scomment);
				}
			//setLayout(new BoxLayout(this,BoxLayout.X_AXIS));
			setSize(w,labelHeight);
			//setMaximumSize(new Dimension((int)w.val-5,(int)labelHeight.val));


			m=new JPopupMenu(Messager.getString("Action"));
			m.setFont(Global.font);
			panelList.add(m);
			int nbMenus=4;
			popUpMenuItems=new JMenuItem[nbMenus];

			popUpMenuItems[0]= new JMenuItem(Messager.getString("Move_down"));
			//popUpMenuItems[0].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, ActionEvent.CTRL_MASK));

			popUpMenuItems[1]= new JMenuItem(Messager.getString("Move_up"));
			//popUpMenuItems[1].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_UP, ActionEvent.CTRL_MASK));

			popUpMenuItems[2]= new JMenuItem(Messager.getString("SelectAll"));
			popUpMenuItems[3]= new JMenuItem(Messager.getString("See/Hide_filter"));

			for (JMenuItem mi:popUpMenuItems)
			{
				m.add(mi);
				mi.addActionListener(this);	
				mi.setBackground(background);
				mi.setFont(Global.font);
			}


			label.addMouseListener(this);

		}

		//Gives notification that an attribute or set of attributes changed.
		public  void changedUpdate(DocumentEvent e)
		{
			//System.out.println(getClass()+" change");
			//Object source = e.getDocument();
		}
		//Gives notification that there was an insert into the document.
		public   void insertUpdate(DocumentEvent e)
		{
			//System.out.println(getClass()+" insert"+" plot index="+index);
			updatePlotLabelAndComment();
		}
		public   void removeUpdate(DocumentEvent e)
		{
			//System.out.println(getClass()+" remove"+" plot index="+index);
			updatePlotLabelAndComment();
		}



		public void actionPerformed(ActionEvent e)
		{
			Object source = e.getSource();
			if (source==popUpMenuItems[0]) 
			{
				moveDown();
				legend.updateGraphics();
			}
			if (source==popUpMenuItems[1]) 
			{
				moveUp();
				legend.updateGraphics();
			}
			if (source==popUpMenuItems[2])//select all
			{
				for (int i=0;i<f.nbPlots();i++) 
				{
					if (legend.getFilter().accept(f.getPlot(i))) f.getPlot(i).setSelected(true);
				}
				legend.updateGraphics();
			}
			if (source==popUpMenuItems[3])// see/hide filter
			{
				legend.setSeeFilter(!legend.isSeeFilter());
				legend.updateGraphics();
			}

		}


		protected void  moveUp()
		{
			//af("index= "+index);
			Plot plot=f.getPlot(index);
			PlotSet faisceauOfThisPLot=chart.getPlotSetHavingPlot(plot);
			if (faisceauOfThisPLot!=null) faisceauOfThisPLot.lowerPlot(plot);
		}

		protected void  moveDown()
		{
			//af("index= "+index);
			Plot plot=f.getPlot(index);
			PlotSet faisceauOfThisPLot=chart.getPlotSetHavingPlot(plot);
			if (faisceauOfThisPLot!=null) faisceauOfThisPLot.raisePlot(plot);
		}


		public void mousePressed(MouseEvent e)
		{
		}

		public void mouseClicked(MouseEvent e)
		{
			if (e.getButton()==MouseEvent.BUTTON3) 
			{
				//	m.setVisible(true);
				//	m.setLocation(e.getPoint().x,e.getPoint().y);
				m.show(this,e.getPoint().x,e.getPoint().y);
			}
			legend.getChart().trigPlotModifiedListeners();
		}

		public void mouseEntered(MouseEvent e)
		{
		}

		public void mouseReleased(MouseEvent e)
		{
		}

		public void mouseDragged(MouseEvent e)
		{
		}

		public void mouseMoved(MouseEvent e)
		{
		}
		public void mouseExited(MouseEvent e){}


		/**
update the label and optionally the comment of the plot
		 */
		private void updatePlotLabelAndComment()
		{
			plot.setLabel(label.getText());
			if (seeComments) plot.setComment(comment.getText());
			LegendEvent e=new LegendEvent();
			e.addLegendModifiedPlot(plot);
			for (LegendModifiedListener l:legendModifiedListener) l.legendModified(e);

		}


		private void setBold(boolean b)
		{
			if (b)
			{
				Font font=label.getFont().deriveFont(Font.BOLD);
				label.setFont(font);
				if (comment!=null) comment.setFont(font);
			}
			else
			{
				Font font=label.getFont().deriveFont(Font.PLAIN);
				label.setFont(font);
				if (comment!=null) comment.setFont(font);
			}
		}


		public Plot getAssociatedPlot()
		{
			return plot;
		}

		/**
Called when the caret position is updated.
		 */
		public void caretUpdate(CaretEvent e)
		{
			//if (index==-1) return;
			// System.out.println(getClass()+" caret update1"+" plot index="+index);
			//set this plot as selected
			// f.select(index,!f.plot(index).isSelected());
			if (!controlPressed ) 
			{
				f.unselectAll();
				//	if (plotFields!=null) for (int i=0;i<plotFields.length;i++) 
				//		if (plotFields[i]!=null) plotFields[i].setBold(false);
				for (LegendField plotField:plotFields) plotField.setBold(false);
			}

			//select the plot and legend when clicked:
			plot.setSelected(true);
			setBold(true);

			//if (plot!=null) plot.work();
			// System.out.println(getClass()+" previousSelectedIndex avant="+previousSelectedIndex);
			if (shiftPressed&(previousSelectedLegendField!=null) )
			{
				//	int i1=(int)Math.min(index,previousSelectedIndex);
				//	int i2=(int)Math.max(index,previousSelectedIndex);
				//	//System.out.println(getClass()+" i1="+i1+" i2="+i2);
				//	for (int i=i1;i<=i2;i++) 
				//		{
				//		f.getPlot(i).setSelected(true);
				//		plotFields[i].setBold(true);
				//		}
				legend.selectPlotRange(this,previousSelectedLegendField);

			}
			if (controlPressed&(previousSelectedLegendField!=null) )
			{
				//	f.getPlot(previousSelectedIndex).setSelected(true);
				//	plotFields[previousSelectedIndex].setBold(true);
				legend.selectPlot(previousSelectedLegendField);
			}
			//previousSelectedIndex=index;
			previousSelectedLegendField=this;
			// System.out.println(getClass()+" previousSelectedIndex après ="+previousSelectedIndex);
			if (chart!=null) 
			{
				chart.repaint();
			}
			repaint();
		}

		public void keyPressed(KeyEvent e)
		{
			if (e.getKeyCode()==KeyEvent.VK_SHIFT) shiftPressed=true;
			if (e.getKeyCode()==KeyEvent.VK_CONTROL) controlPressed=true;
			if ((e.getKeyCode()==KeyEvent.VK_A) &(controlPressed==true))
			{
				for (int i=0;i<f.nbPlots();i++) 
				{
					f.getPlot(i).setSelected(true);
				}
				legend.updateGraphics();
			}
			if (e.getKeyCode()==KeyEvent.VK_UP) 
			{
				moveUp();
				legend.updateGraphics();
			}
			if (e.getKeyCode()==KeyEvent.VK_DOWN) 
			{
				moveDown();
				legend.updateGraphics();
			}

			// System.out.println(getClass()+" shift:"+shiftPressed);

		}
		public void keyTyped(KeyEvent e)
		{
		}

		public void keyReleased(KeyEvent e)
		{
			if (e.getKeyCode()==KeyEvent.VK_SHIFT) shiftPressed=false;
			if (e.getKeyCode()==KeyEvent.VK_CONTROL) controlPressed=false;
			// System.out.println(getClass()+" shift:"+shiftPressed);
		}

		public void setEditable(boolean b)
		{
			label.setEditable(b);
			if (seeComments)
			{
				comment.setEditable(b);
			}
		}


	}


	/**
panel that shows the style of the plot
	 */
	class StylePanel extends JPanel
	{
		private PlotSet f;//the associated faisceau (collection of plots)
		private int index;//the index of the associated plot in the faisceau

		public StylePanel(PlotSet f,int index,Color fcolor,Color bcolor)
		{
			this.f=f;
			this.index=index;
			setPreferredSize(new Dimension(20,20));
			setForeground(fcolor);
			setBackground(bcolor);
		}

		public void paintComponent(Graphics g)
		{
			Dimension d=getSize();
			g.setColor(getBackground());
			g.fillRect(0,0,getSize().width,getSize().height);
			if (f!=null) 
			{
				Plot plot=f.getPlot(index);
				if (plot!=null) 
				{
					g.setColor(ColorUtil.getColor(plot.getLineColor()));
					g.drawLine(0,d.height/2,d.width,d.height/2);
					f.getPlot(index).drawSymbol(g,d.width/2,d.height/2,0);
				}
			}

		}

	}



	public class FilterField extends JPanel implements KeyListener
	{
		private JLabel label; //
		private JTextField filterText;
		private Legend legend;


		public FilterField(Legend _legend) 
		{
			super();
			this.setBackground(Global.background);
			this.label = new JLabel("Filter");
			this.legend = _legend;
			filterText=new JTextField("");
			filterText.addKeyListener(this);
			filterText.setBackground(Global.background.brighter());
			this.add(label);
			this.add(filterText);

			label.setFont(Global.font);	
			label.setBackground(Global.background);
			filterText.setFont(Global.font);	
			filterText.setColumns(8);
		}


		//public CharSequence getFilterText() {
		//	// TODO Auto-generated method stub
		//	return filterText.getText();
		//}


		public boolean accept(Plot plot)
		{
			return	(plot.getLabel().contains(filterText.getText())) ;
		}


		public void keyPressed(KeyEvent e) {
			// TODO Auto-generated method stub

		}



		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub

		}



		public void keyTyped(KeyEvent e) {
			legend.update();
			filterText.requestFocus();


		}

		public JLabel getLabel() {return label;}
		public JTextField getFilterText() {return filterText;}



	}//end class FilterField



	public FilterField getFilter() {
		// TODO Auto-generated method stub
		return filterField;
	}



	public void selectPlot(LegendField lf) 
	{
		lf.setBold(true);
		lf.getAssociatedPlot().setSelected(true);
	}



	public void selectPlotRange(LegendField lf1, LegendField lf2) 
	{
		int i1=plotFields.indexOf(lf1);
		int i2=plotFields.indexOf(lf2);
		int j1=(int)Math.min(i1,i2);
		int j2=(int)Math.max(i1,i2);
		for (int j=j1;j<j2;j++) 
		{
			plotFields.elementAt(j).setBold(true);
			plotFields.elementAt(j).getAssociatedPlot().setSelected(true);
		}
	}
















}
