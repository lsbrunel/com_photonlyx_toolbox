package com.photonlyx.toolbox.chart;

public interface LegendModifiedListener
{
	
public void legendModified(LegendEvent e);

}
