package com.photonlyx.toolbox.chart;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.AttributedString;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.chart.process.PlotProcessWindowAddSlope;
import com.photonlyx.toolbox.chart.process.Signal2D1DprocessWindowFourierFilter;
import com.photonlyx.toolbox.chart.process.Signal2D1DprocessWindowSmooth;
import com.photonlyx.toolbox.chartApp.ChartApp;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.gui.ColorApplier;
import com.photonlyx.toolbox.gui.ColorChooserFrame;
import com.photonlyx.toolbox.gui.ColorList;
import com.photonlyx.toolbox.gui.ColorListInterface;
import com.photonlyx.toolbox.gui.ColorUtil;
import com.photonlyx.toolbox.gui.InputDialog;
import com.photonlyx.toolbox.gui.PaletteChangedListener;
import com.photonlyx.toolbox.gui.PaletteInterface;
import com.photonlyx.toolbox.gui.PalettePanel;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.TextEditor;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.imageGui.ImageWindow;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.Util;
import com.photonlyx.toolbox.math.analyse2D.Interpolation2D;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;
import com.photonlyx.toolbox.math.histo.Histogram1D1D;
import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.math.image.ImageFileFilter;
import com.photonlyx.toolbox.math.image.PNGFileFilter;
import com.photonlyx.toolbox.math.image.RawFloatImageFileFilter;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DLogx;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.util.ClipBoard;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.ClipBoard.ImageSelection;




public class Chart extends JPanel implements MouseListener
, MouseMotionListener
, MouseWheelListener
,ActionListener
, KeyListener
, ColorApplier,XMLstorable
, PaletteChangedListener

{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2469402085130680197L;

	private Transformation2DPix t=new Transformation2DPix();
	//private PlotSet myplotset=new PlotSet();//individual plots can be added to this chart
	private Vector<PlotSet> plotSets=new Vector();//serie of  plot sets to plot
	private Legend legend;
	private ChartPanel chartPanel=null;//eventual chartpanel containing this chart
	private Signal2D1D signal2D1D=null; //eventual image to plot
	private PaletteInterface palette=new PalettePanel(); //palette for the colors of eventual image to plot

	private Color background = Global.background;
	private Color graphBackground = Global.background;
	private Color foreground = new Color(20,20,20);//Global.foreground;
	private Color gridLinesColor =Global.chartGridLinesColor;
	private boolean addingApoint=false;//mode : adding a point where the mouse is , after the selected point
	private int selectedPoint;//index of the casual selected point
	private int selectedPointGrip;//index of the selected point when grip start
	private boolean needsRefresh = true;
	private boolean selectOnClick=true;
	private boolean unselectAllOnFarClick=true;
	private boolean takeFocusOnMouseEnter=true;
	private boolean mouseGrip=true;//grip a point function is available
	private boolean grip=false;//gripping a point
	private boolean zooming=false;//user is drawing the zoom rectangle
	private boolean zoomed=false;//the graph is in the zoomed state
	private boolean moving=false;//TODO
	private boolean zoning=false;
	private boolean translateDraggingOn=false;//allow translation of graph by dragging (like google maps)
	private boolean showvalue=false;
	private boolean popUpMenu = true;
	//private boolean mouseZoom = true;
	private boolean keyBoardActions = true;
	private String keyBoardActionsList = null;
	private Point mouseClickPosition;
	private int[] pix1=new int[2];//left up corner of the zoom rectangle
	private int[] pix2=new int[2];//right down corner of the zoom rectangle
	private int[] pix3=new int[2];//for point modif
	private int[] pix=new int[2];//cross  position
	private int crossSize=5;//cross half size of the mouse pointer
	private boolean showCross=true;
	private boolean mouseWheelZoom;
	private String message1="",message2=""; //message on cursor
	private boolean limitZoomInsideBorders;
	private String formatTickx = "0.00" ,formatTicky = "0.00" ;//ticks labels format
	private String formatz = "0.000E0";
	private boolean autoRescale;//TODO remove this ?
	private boolean menu;
	private ColorListInterface colorList=new ColorList("black white red pink pastelRed green blue cyan pastelBlue lightGray darkGray yellow orange ");
	private boolean reticuleH=false;//show big horizontal line on mouse pointer
	private boolean reticuleV=false;//show big vertical line on mouse pointer
	private double[] nearPoint,nearPoint1;//the point near to the mouse and the next one
	private int selectedPlotIndex;

	//parameters for the axis, ticks end gridlines:
	private String title="";
	private boolean xAxisInSeconds = false;
	private boolean xAxisAsDate = false;//suppose that x axis is in ms and put a date format
	private boolean xAxisShows2Digits = true;
	private boolean xAxisShowsDays = false;
	private boolean seeXaxis=false;
	private boolean seeYaxis=false;
	private boolean seeXvalues=true;//show/unshow the numerical values of x axis
	private boolean seeYvalues=true;//show/unshow the numerical values of y axis
	private boolean ticksx=true,ticksy=true;//if true tick on the axis are printed
	private boolean gridLinex=true,gridLiney=true;//if true gridlines are printed
	private boolean adjustBoundariesToTicks=true;//if true boundaries are readjusted to fit the ticks
	private double[] xticks;//to store the ticks positions
	private double[] yticks;//to store the ticks positions
	private int nbTicksdesiredx=4;//nb of ticks wanted in x axis (the actual nb of ticks may be sligtly different since tick are for values 2 5 or 10
	private int nbTicksdesiredy=4;//nb of ticks wanted in x axis (the actual nb of ticks may be sligtly different since tick are for values 2 5 or 10
	//private boolean zoomOnTicks=true;//if true zoom window is readjusted to fit the ticks
	private int tickLength=4;
	private boolean see_xlabel=true;
	private boolean see_ylabel=true;
	private String xunit="";
	private String yunit="";
	private String xlabel="x";
	private String ylabel="y";
	//private int xLabelDistFromAxis=40;
	private boolean xScientific=false;
	private boolean yScientific=false;
	private boolean fixedLimitX=false;//if false limits are adjusted to plots in X 
	private boolean fixedLimitY=false;//if false limits are adjusted to plots in Y
	private double xmin=0,xmax=1,ymin=0,ymax=1;
	private boolean fixedLimitZ=false;//for image print if false limits are adjusted to the Signal2D1D
	private double zmin=0,zmax=1;
	private boolean logz=false;
	private double zoneXmin,zoneYmin,zoneXmax,zoneYmax;
	private Font font = Global.font.deriveFont((float)14);

	private JPopupMenu m;
	//private JMenuItem[] popUpMenuItems;
	private ColorChooserFrame ccf=new ColorChooserFrame();

	private Vector<PlotDeleteListener> plotDeleteListeners=new Vector<PlotDeleteListener>();
	private Vector<PlotModifiedListener> plotModifiedListeners=new Vector<PlotModifiedListener>();

	private static String dayLabel="d";
	private static String hourLabel="h";
	private static String minuteLabel="min";
	private static String secondLabel="s";
	private static String millisecondLabel="ms";

	private Vector<ChartDrawingLayer> chartDrawingLayers=new Vector<ChartDrawingLayer>();
	private ChartDrawingLayerNotes chartDrawingLayerNotes=new ChartDrawingLayerNotes(this);

	private boolean ctrlPressed=false;
	private boolean mouseGripNearX=true;//if true the nearest point is looked only taking into account the x coordinate





	public Chart()
	{
		//plotSetList.add(myplotset);	
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.addMouseWheelListener(this);
		this.addKeyListener(this);

		//configure translation 
		Messager.init("en", "US");

		//Popup Menu	
		m=new JPopupMenu(Messager.getString("Action"));
		Vector<JMenuItem> popUpMenuItems1=new Vector<JMenuItem>();
		JMenuItem jmi;
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("show_selected_plot_data_in_a_text_editor")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/chart/iconExcel.png"));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("show_all_data_in_a_text_editor")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/chart/iconExcel.png"));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("save_as_png_image")));
		jmi.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.CTRL_MASK));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("load_background_image")));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("copy_selected_in_clipboard")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/chart/iconCopy.png"));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("paste_data_in_graph")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/chart/iconPaste.png"));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("paste_data_with_labels_in_graph")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/chart/iconPaste.png"));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("paste_3_columns_with_errors")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/chart/iconPaste.png"));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("copy_data_and_style")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/chart/iconCopy.png"));
		jmi.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("paste_data_and_style")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/chart/iconPaste.png"));
		jmi.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("SelectAll")));
		jmi.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("Put_a_note")));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("Show_hide_color_chooser")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/chart/iconColorChooser.png"));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("Set_rainbow_colors")));
		for (JMenuItem mi:popUpMenuItems1)
		{
			m.add(mi);
			mi.addActionListener(this);	
			mi.setBackground(background);
			mi.setFont(Global.font);
		}


		//process sub menu:
		Vector<JMenuItem> processMenuItems=new Vector<JMenuItem>();
		JMenu processMenu;
		processMenu=new JMenu("Process");
		processMenu.setFont(Global.font);
		processMenu.setBackground(background);
		m.add(processMenu);
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("duplicate")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("multiply_number")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("multiply_x_by_number")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("add_number")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("add_slope")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("mean_plot")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("log_the_Y")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("invert(i)")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("Remove_the_points_out_of_the_zone")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("Delete_the_points_of_the_zone")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("smooth")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("smooth_filtered")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("progressive_averaging")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("filtered_progressive_averaging")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("sort")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("derivate")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("histogram")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("average_values_of_same_X")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("average_values_of_same_X_with_filtering")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("fft")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("calc_usual_things")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("normaliseMax")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("normaliseIntegral")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("integralCumulator")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("integralProfile")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("integralProfileCumulator")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("subtract_two_plots")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("merge_selected_plots")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("solve")));
		processMenuItems.add(jmi=new JMenuItem(Messager.getString("align_vertically_on_zone")));
		for (JMenuItem mi:processMenuItems)
		{
			processMenu.add(mi);
			mi.addActionListener(this);	
			mi.setBackground(background);
			mi.setFont(Global.font);
		}


		//fit sub menu:
		Vector<JMenuItem> fitMenuItems=new Vector<JMenuItem>();
		JMenu fitMenu;
		fitMenu=new JMenu("Fit");
		fitMenu.setFont(Global.font);
		fitMenu.setBackground(background);
		m.add(fitMenu);
		fitMenuItems.add(jmi=new JMenuItem(Messager.getString("fit_a_line")));
		fitMenuItems.add(jmi=new JMenuItem(Messager.getString("fit_a_parabol")));
		fitMenuItems.add(jmi=new JMenuItem(Messager.getString("fit_a_polynome_order_3")));
		fitMenuItems.add(jmi=new JMenuItem(Messager.getString("fit_a_polynome_order_4")));
		fitMenuItems.add(jmi=new JMenuItem(Messager.getString("fit_a_polynome_order_5")));
		fitMenuItems.add(jmi=new JMenuItem(Messager.getString("fit_a_polynome_order_n")));
		fitMenuItems.add(jmi=new JMenuItem(Messager.getString("interpolate_with_a_cubic_spline")));
		fitMenuItems.add(jmi=new JMenuItem(Messager.getString("fit_a_cubic_spline")));


		for (JMenuItem mi:fitMenuItems)
		{
			fitMenu.add(mi);
			mi.addActionListener(this);	
			mi.setBackground(background);
			mi.setFont(Global.font);
		}


		//image sub menu:
		Vector<JMenuItem> imageMenuItems=new Vector<JMenuItem>();
		JMenu imageMenu;
		imageMenu=new JMenu("Image");
		imageMenu.setFont(Global.font);
		imageMenu.setBackground(background);
		m.add(imageMenu);
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("load_image_as_raw")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("save_image_as_raw")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("load_png_image")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("save_png_image")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("copy_image")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("paste_image")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("threshold_relative")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("histogramImage")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("fft2D")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("spotProfile")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("interpolate")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("add_a_constant")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("multiply_by_a_factor")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("rotate90Right")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("rotate")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("binning")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("crop_on_zone")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("fourier_filtering")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("smooth_filtering")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("projectionX")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("projectionY")));
		imageMenuItems.add(jmi=new JMenuItem(Messager.getString("integral")));
		for (JMenuItem mi:imageMenuItems)
		{
			imageMenu.add(mi);
			mi.addActionListener(this);	
			mi.setBackground(background);
			mi.setFont(Global.font);
		}

		add(m);

		plotSets.add(new PlotSet());//add my plot set (where I put the individual plots)

		palette.addListener(this);

		Border empty = BorderFactory.createEmptyBorder();
		this.setBorder(empty);

		this.setLayout(null);
		this.add( chartDrawingLayerNotes);
	}

	private PlotSet plotSet0()
	{
		if (plotSets.size()==0) plotSets.add(new PlotSet());
		return plotSets.elementAt(0); 
	}


	public void add(PlotSet plotSet)
	{
		if (plotSets.contains(plotSet)) return;
		plotSets.addElement(plotSet);
		//myplotset.add(plotSet);
	}

	public void add(Plot plot)
	{
		plotSet0().add(plot);
		//if( legend!=null) legend.update();
	}

	public boolean has(Plot plot)
	{
		boolean b=false;
		for (PlotSet set:plotSets) b=b||set.has(plot);
		b=b||plotSet0().has(plot);
		return b;
	}

	public boolean hasPlotOfDate(long date_ms)
	{
		boolean b=false;
		for (PlotSet set:plotSets) b=b||set.hasPlotOfDate(date_ms);
		b=b||plotSet0().hasPlotOfDate(date_ms);
		return b;
	}

	public void  removePlotOfDate(long date_ms)
	{
		for (PlotSet set:plotSets) set.removePlotOfDate(date_ms);
		plotSet0().removePlotOfDate(date_ms);
	}

	public void removeAllPlots()
	{
		//myplotset().removeAllPlots();
		plotSets.removeAllElements();
	}

	public void remove(Plot p)
	{
		for (PlotSet set:plotSets) set.removePlot(p);
		plotSet0().removePlot(p);
	}

	public void remove(PlotSet plotSet)
	{
		plotSets.remove(plotSet);
	}

	public void setLegend(Legend legend)
	{
		this.legend=legend;
	}

	public Legend getLegend() {
		return legend;
	}


	public ChartPanel getChartPanel() {
		return chartPanel;
	}

	public void setChartPanel(ChartPanel chartPanel) {
		this.chartPanel = chartPanel;
	}

	public void update()
	{
		//System.out.println(getClass()+" update ");
		repaint();
		if (this.getLegend()!=null )this.getLegend().update();
	}


	public void update(boolean updateLegend)
	{
		//System.out.println(getClass()+" update ");
		repaint();
		if (updateLegend )this.getLegend().update();
	}


	public void updateOnlyChart()
	{
		repaint();
	}



	public void paintComponent(Graphics g)
	{
		//System.out.println(getClass()+" paintComponent ");
		super.paintComponent(g);
		Graphics2D g2=(Graphics2D)g;

		if (menu) 
		{
			drawMenu(g);
			return;
		}

		//update panel size
		t.setGraphParams(this.getSize(), t.borderx(), t.bordery());

		//update boundaries if in mode auto:
		//if (!zoomed) 
		//if ( !adjustBoundariesToTicks  )	
		if (!zoomed)
		{
			adjustBoundariesToPlots();
			if (seeXaxis&!fixedLimitY) t.setRealBoudariesy(0, t.ymax());
			if (seeYaxis&!fixedLimitX) t.setRealBoudariesx(0, t.xmax());
			if (fixedLimitX) t.setRealBoudariesx(xmin, xmax);
			if (fixedLimitY) t.setRealBoudariesy(ymin, ymax);
		}

		//if ((ticksx == true)||(gridLinex == true))//calculate the ticks
		{
			if (t.logx()) xticks=t.getDecadesTicksx();
			else
			{
				if (xAxisInSeconds== true)
				{
					xticks=t.getTimeTicksx(nbTicksdesiredx);
				}
				else if (xAxisAsDate== true)
				{
					xticks=t.getTimeTicksx(nbTicksdesiredx);
				}			
				else 
				{
					xticks=t.getTicksx(nbTicksdesiredx);
				}
			}
		}
		//if ((ticksy == true) || (gridLiney == true))//calculate the ticks
		{
			if (t.logy()) 
				yticks=t.getDecadesTicksy();
			else
				yticks=t.getTicksy(nbTicksdesiredy);			
		}
		//if ticks or gridlines are asked, adjust the boundaries to the grid in X
		if ( ( adjustBoundariesToTicks  )/*&(zoomed == false)*/ /*&& (ticksx || gridLinex)*/ )
		{
			if (xticks.length>0)
			{
				double xmin=xticks[0];
				double xmax=xticks[xticks.length-1];
				t.setRealBoudariesx(xmin, xmax);//adjust boundaries to extreme ticks
			}
		}
		//if  ticks or gridlines are asked, adjust the boundaries to the grid in y
		if ( (adjustBoundariesToTicks )/*&(zoomed == false)*/ /*&& (ticksy||gridLiney)*/)
		{
			if (yticks.length>0)
			{
				double ymin=yticks[0];
				double ymax=yticks[yticks.length-1];
				t.setRealBoudariesy(ymin, ymax);//adjust boundaries to extreme ticks
			}
		}

		g.setColor(background);
		g.fillRect(0,0,getWidth(),t.bordery());
		g.fillRect(0,0,t.borderx(),getHeight());
		g.fillRect(0,getHeight()-t.bordery(),getWidth(),t.bordery());
		g.fillRect(getWidth()-t.borderx(),0,t.borderx(),getHeight());
		g.setColor(graphBackground);
		g.fillRect(t.borderx(),t.bordery(),getWidth()-2*t.borderx(),getHeight()-2*t.bordery());
		g.setColor(gridLinesColor);
		g.drawRect(t.borderx(),t.bordery(),getWidth()-2*t.borderx(),getHeight()-2*t.bordery());

		//print the title and labels
		g.setColor(foreground);
		//Font titleFont = font.deriveFont(font.getSize()+4);
		g.setFont(font);
		if (title!=null) g.drawString(title,t.borderx(),t.bordery()-5);
		if (see_xlabel==true)
		{
			String s;
			if (xunit==null) s=xlabel;
			else 
				if (xunit.length()>0) s=xlabel+" ("+xunit+")";
				else s=xlabel;
			java.awt.geom.Rectangle2D rect = font.getStringBounds(s,g2.getFontRenderContext());
			g.drawString(s,getWidth()/2-(int)rect.getWidth()/2,getHeight()-t.bordery()+(int)(rect.getHeight()*2.5));
		}
		if (see_ylabel==true)
		{
			String s;
			if (yunit==null) s=ylabel;
			else 
				if (yunit.length()>0) s=ylabel+" ("+yunit+")";
				else s=ylabel;
			java.awt.geom.Rectangle2D rect = font.getStringBounds(s,g2.getFontRenderContext());
			//calc the max width of y tick values:
			String ss=formatYval(formatTicky,t.ymax());
			java.awt.geom.Rectangle2D rectLabels = font.getStringBounds(ss,g2.getFontRenderContext());
			//System.out.println(this.getFormatTickx()+"    "+rectLabels);
			// clockwise 90 degrees
			AffineTransform old_at = g2.getTransform();
			AffineTransform at = new AffineTransform();
			at.setToRotation(-Math.PI/2.0,0,getHeight()/2 + (rect.getWidth() / 2));
			g2.transform(at);
			g.drawString(s,0,(int)(getHeight()/2+t.borderx()- rect.getHeight()-rectLabels.getWidth()+10   ));
			g2.setTransform(old_at);
		}





		//paint the image if any
		if (signal2D1D!=null) if ((signal2D1D.dimx()*signal2D1D.dimy())!=0)
		{
			g.setClip(t.borderx(),t.bordery(),getWidth()-2*t.borderx(),getHeight()-2*t.bordery());
			Signal2D1D signal2D1DBuffer=signal2D1D;
			Image imageBuffer=signal2D1DBuffer.toImage(palette,fixedLimitZ,zmin,zmax,logz);
			double corner1x=signal2D1DBuffer.xmin();
			double corner1y=signal2D1DBuffer.ymax();
			double corner2x=signal2D1DBuffer.xmax();
			double corner2y=signal2D1DBuffer.ymin();
			int[] pix1=t.realToImage(corner1x,corner1y);
			int[] pix2=t.realToImage(corner2x,corner2y);
			g.drawImage(imageBuffer,pix1[0],pix1[1],pix2[0]-pix1[0],pix2[1]-pix1[1],this);
		}


		//paint the curves
		g.setClip(t.borderx(),t.bordery(),getWidth()-2*t.borderx(),getHeight()-2*t.bordery());
		for (Plot plot:getConcatenatedPlots()) plot.paint(g,t);
		plotSet0().paint(g,t);
		g.setClip(0,0,getWidth(),getHeight());

		//draw the zooming rectangle
		if ((zooming||zoning)/*&&mouseZoom*/&&(!moving))
		{
			g.setColor(foreground);
			int c0=(int)Math.min(pix1[0],pix2[0]);
			int c1=(int)Math.min(pix1[1],pix2[1]);
			int w=(int)Math.abs(pix1[0]-pix2[0]);
			int h=(int)Math.abs(pix1[1]-pix2[1]);
			//graphics.drawRect(pix1[0],pix1[1],pix2[0]-pix1[0],pix2[1]-pix1[1]);
			g.drawRect(c0,c1,w,h);
		}

		//draw the zone
		if (isZoneDefined())
		{
			g.setColor(Color.gray);
			g.setClip(t.borderx(),t.bordery(),getWidth()-2*t.borderx(),getHeight()-2*t.bordery());
			int[] pix1=t.realToImage(zoneXmin,zoneYmin);
			int[] pix2=t.realToImage(zoneXmax,zoneYmax);
			int c0=(int)Math.min(pix1[0],pix2[0]);
			int c1=(int)Math.min(pix1[1],pix2[1]);
			int w=(int)Math.abs(pix1[0]-pix2[0]);
			int h=(int)Math.abs(pix1[1]-pix2[1]);
			g.drawString("zone1", c0, c1-10);
			g.drawRect(c0,c1,w,h);
			g.setClip(0,0,getWidth(),getHeight());
		}



		//trace the  gridlines ...........................................................
		//


		double[] real=new double[2];
		int[] p1im=new int[2];
		int[] p2im=new int[2];
		g.setColor(gridLinesColor);

		g.setClip(t.borderx(),t.bordery(),getWidth()-2*t.borderx(),getHeight()-2*t.bordery());
		//draw the x (vertical) gridlines
		if (gridLinex==true)
			for (int i=0;i<xticks.length;i++)
			{
				real[0]=xticks[i];

				real[1]=t.ymin();
				p1im=t.realToImage(real);
				real[1]=t.ymax();
				p2im=t.realToImage(real);

				if (p1im[0] < t.borderx() || p1im[0] > getWidth() - t.borderx() ) continue;

				g.drawLine(p1im[0],p1im[1],p2im[0],p2im[1]);
			}


		//draw the y (horizontal) gridlines
		if (gridLiney==true)
			for (int i=0;i<yticks.length;i++)
			{
				real[0]=t.xmin();

				real[1]=yticks[i];
				p1im=t.realToImage(real);
				real[0]=t.xmax();
				p2im=t.realToImage(real);

				if (p1im[1] < t.bordery() || p1im[1] > getHeight() - t.bordery() ) continue;

				g.drawLine(p1im[0],p1im[1],p2im[0],p2im[1]);
			}

		//trace the ticks ..........................................................

		g.setClip(0,0,getWidth(),getHeight());
		g.setColor(gridLinesColor);
		//draw the x (vertical) ticks
		if (ticksx)
		{
			for (int i=0;i<xticks.length;i++)
			{
				real[0]=xticks[i];
				real[1]=t.ymin();
				p1im=t.realToImage(real);
				p2im[0]=p1im[0];
				p2im[1]=p1im[1]-tickLength;
				g.drawLine(p1im[0],p1im[1],p2im[0],p2im[1]);
			}
		}

		//draw the y (horizontal) ticks
		if (ticksy)
		{
			for (int i=0;i<yticks.length;i++)
			{
				real[0]=t.xmin();
				real[1]=yticks[i];
				p1im=t.realToImage(real);
				p2im[0]=p1im[0]+tickLength;
				p2im[1]=p1im[1];
				g.drawLine(p1im[0],p1im[1],p2im[0],p2im[1]);
			}
		}

		//draw the axis values 
		if (seeXvalues)
		{
			g.setClip(0,0,getWidth(),getHeight());
			//The x and y values are displayed only if there is at least one
			//plot to display
			//
			if (t.xmax()>t.xmin())
				if (t.ymax()>t.ymin())
					if (t.areGraphLimitsNull() == false)
					{
						g.setColor(foreground);

						//write the value at each tick in x
						//
						if ((ticksx==true)/*||(gridLinex==true)*/)
						{
							Vector<Integer> ticksDisplayed = drawXAxisValues(this.getWidth(),getHeight(), g2, xticks);

							if (ticksDisplayed.size() < 2)
							{
								double[] ticks = new double[2 + ticksDisplayed.size()];

								ticks[0] = t.xmin();
								ticks[1] = t.xmax();

								if (ticksDisplayed.size() == 1)
								{
									ticks[2] = xticks[ticksDisplayed.elementAt(0)];
								}

								drawXAxisValues(getWidth(),getHeight(), g2, ticks);
							}
						}
					}
		}
		if (seeYvalues)
		{
			//write the value at each tick in y
			//
			if ((ticksy==true)/*||(gridLiney==true)*/)
			{
				Vector<Integer> ticksDisplayed = drawYAxisValues(getHeight(), g2, yticks);

				if (ticksDisplayed.size() < 2)
				{
					double[] ticks = new double[2 + ticksDisplayed.size()];

					ticks[0] = t.ymin();
					ticks[1] = t.ymax();

					if (ticksDisplayed.size() == 1)
					{
						ticks[2] = yticks[ticksDisplayed.elementAt(0)];
					}

					drawYAxisValues(getHeight(), g2, ticks);
				}
			}
		}


		//debug show the state
		g.setColor(Color.LIGHT_GRAY);
		g.setFont(font.deriveFont(6));
		if (zooming) g.drawString("zooming",5,10);
		if (zoomed) g.drawString("zoomed",5,10);
		if (zoning) g.drawString("zoning",5,20);


		// eventually show the value pointed
		if 	(showvalue)
		{
			updateMouseCoordsMessage();
			int messagePos = pix[1]+10+font.getSize()+crossSize;
			int xMessagePos = pix[0]-30;
			g.setColor(Color.DARK_GRAY);
			g.drawString(message1,xMessagePos,messagePos);
			messagePos+=font.getSize();
			g.drawString(message2,xMessagePos,messagePos);
		}


		if (cursorIsInTheArea())
		{
			if (showCross == true)
			{
				g.setColor(foreground);
				g.drawLine(pix[0],pix[1]+crossSize,pix[0],pix[1]-crossSize);
				g.drawLine(pix[0]-crossSize,pix[1],pix[0]+crossSize,pix[1]);
				g.drawOval(pix[0]-crossSize,pix[1]-crossSize,2*crossSize,2*crossSize);
				if (zoning) g.drawString("zone1", pix[0], pix[1]-10);
				if (zooming) g.drawString("zoom", pix[0], pix[1]-10);
			}

			//draw the horizontal reticule
			if (reticuleH==true)
			{
				real[0]=t.xmin();
				p1im=t.realToImage(real);
				real[0]=t.xmax();
				p2im=t.realToImage(real);
				g.drawLine(p1im[0],pix[1],p2im[0],pix[1]);
			}

			//draw the vertical reticule
			if (reticuleV==true)
			{
				real[1]=t.ymin();
				p1im=t.realToImage(real);
				real[1]=t.ymax();
				p2im=t.realToImage(real);
				g.drawLine(pix[0],p1im[1],pix[0],p2im[1]);
			}

		} // if (cursorIsInTheArea)

		//int x=this.getWidth()-120,y=this.getHeight()-10,inc=10;
		//g.setColor(Color.LIGHT_GRAY);
		//g.drawString("(menu: type m)",x,y);



		//additional drawing layers:
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) 
		{
			drawingLayer.draw(g,this);
		}

	}


	/**
	 * takes into account the occupancy rectangle of the things to trace
	 */
	public void adjustBoundariesToPlots()
	{
		Vecteur2D Pmin=new Vecteur2D(1e30,1e30);
		Vecteur2D Pmax=new Vecteur2D(-1e30,-1e30);
		//get the  boundaries of the plots:
		for (Plot plot:getConcatenatedPlots()) 
		{
			if (plot.isGrayed()) continue;
			if ((!isLogx())&(!isLogy())) 
			{
				plot.updateBoundariesX(Pmin, Pmax);
				//System.out.println(plot.getLabel()+" "+Pmin.coord(0));
				plot.updateBoundariesY(Pmin, Pmax);
			}
			if ((isLogx())&(!isLogy())) 
			{
				plot.updateBoundariesXStrictPositive(Pmin, Pmax);
				plot.updateBoundariesY(Pmin, Pmax);
			}
			if ((!isLogx())&(isLogy())) 
			{
				plot.updateBoundariesX(Pmin, Pmax);
				plot.updateBoundariesYStrictPositive(Pmin, Pmax);
			}
			if ((isLogx())&(isLogy())) 
			{
				plot.updateBoundariesXStrictPositive(Pmin, Pmax);	
				plot.updateBoundariesYStrictPositive(Pmin, Pmax);
			}
		}
		if (signal2D1D!=null)
		{
			if( signal2D1D==null) return;
			double r=signal2D1D.xmin();
			if (r<Pmin.x()) Pmin.setX(r);
			r=signal2D1D.xmax();
			if (r>Pmax.x()) Pmax.setX(r);
			r=signal2D1D.ymin();
			if (r<Pmin.y()) Pmin.setY(r);
			r=signal2D1D.ymax();
			if (r>Pmax.y()) Pmax.setY(r);
		}
		//update the transformation:
		t.setRealBoundaries(Pmin.x(), Pmin.y(), Pmax.x(), Pmax.y());
		//System.out.println("xmin: "+Pmin.x());
		//System.out.println("xmax: "+Pmax.x());
		//System.out.println("ymin: "+Pmin.y());
		//System.out.println("ymax: "+Pmax.y());
	}

	public Plot selectPlotCloseTo(Transformation2DPix transformation,double[] coords)
	{
		Plot p=null;
		p=selectPlotCloseTo(getConcatenatedPlots(),transformation,coords);
		p=selectPlotCloseTo(plotSet0(),transformation, coords);
		return p;
	}

	public void unselectAll()
	{
		for (Plot plot:getConcatenatedPlots()) plot.setSelected(false);
		//myplotset().unselectAll();
	}




	//MouseListener implementation
	//
	public void mouseClicked(MouseEvent e)
	{
		mouseClickPosition = e.getPoint();
		if ((e.getButton()==MouseEvent.BUTTON1)&&(e.getClickCount()==2)) zoomed=false;

		if ((e.getButton()==MouseEvent.BUTTON3)&& popUpMenu == true ) 
			m.show(this,e.getPoint().x,e.getPoint().y);

		if (addingApoint)
		{
			Plot plot=this.getSelectedPlot();
			if (plot!=null)
			{
				Signal1D1D signal=plot.getSignal();
				if (signal instanceof Signal1D1DXY)
				{
					((Signal1D1DXY)signal).addPoint(t.imageToReal(pix),selectedPoint);
				}
				addingApoint=false;

				needsRefresh=true;
				repaint();
			}
		}
		else if (e.getButton()==MouseEvent.BUTTON1)
		{
			Point p = e.getPoint();

			if (isPointInTheArea(p.x, p.y) == true)
			{
				Plot selectedPlot = null;

				if (selectOnClick == true) 
				{
					double[] coords = new double[2];
					coords[0] = p.x ; 
					coords[1] = p.y ;	
					selectedPlot=selectPlotCloseTo(this.getConcatenatedPlots(),t, coords);
					if (selectedPlot==null) return;
					double currentMinimalDist = getScreenDistanceFromPointToPlot(selectedPlot,t, coords);
					if (currentMinimalDist <= 2) 
					{
						//selectedPlot.setSelected(!selectedPlot.isSelected());
						selectedPlot.setSelected(true);
					}
					else if (unselectAllOnFarClick == true && e.isControlDown() == false)
					{
						unselectAll();
					}
				}
			}

			if (legend!=null) legend.updateGraphics();//update the eventual legend

			needsRefresh=true;
			repaint();
		}
		//pass the event to the drawing layers
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.mouseClicked(e);
		trigPlotModifiedListeners();
	}

	public void mouseEntered(MouseEvent e) 
	{
		if (takeFocusOnMouseEnter == true)
		{
			requestFocus();
		}
		//pass the event to the drawing layers
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.mouseEntered(e);
	}

	public void mouseExited(MouseEvent e) 
	{
		//pass the event to the drawing layers
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.mouseExited(e);
	}

	public void mousePressed(MouseEvent e) 
	{
		requestFocus();
		pix=cross(e.getPoint());
		if (!cursorIsInTheArea()) return;

		if ((mouseGrip==true)&&(grip==false)&&(e.getButton()==MouseEvent.BUTTON2)) 
		{
			grip=true;
			Plot plot=getSelectedPlot();
			if (plot==null) return;

			Transformation2DPix usedTrans = t;

			pix1=pix;
			Signal1D1D signal=plot.getSignal();
			if (mouseGripNearX) selectedPointGrip=signal.getNearestXPoint(usedTrans.imageToReal(pix1));
			else selectedPointGrip=signal.getNearestPoint(usedTrans.imageToReal(pix1));
			//		double[] near=signal.getPoint(selectedPoint);
			//		pix=usedTrans.realToImage(near);
		}
		else
			grip=false;

		if (addingApoint) return;

		if ((!grip)/*&&(mouseZoom)*/&&(e.getButton()==MouseEvent.BUTTON1))
		{
			if (!zoning) zooming=true;
			pix1=pix;
		}

		int onmask = MouseEvent.SHIFT_DOWN_MASK | MouseEvent.BUTTON1_DOWN_MASK;


		if (translateDraggingOn)//TODO
			if ((e.getModifiersEx() & (onmask /*| offmask*/)) == onmask) 
			{
				moving=true;
				System.out.println("moving="+ moving);
			}
		//pass the event to the drawing layers
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.mousePressed(e);

	}




	public void mouseReleased(MouseEvent e)  
	{
		if (moving)
		{
			int[] delta=new int[2];
			delta[0]=cross(e.getPoint())[0]-pix[0];
			delta[1]=cross(e.getPoint())[1]-pix[1];
			t.translate(delta);
			moving=false;
			repaint();
			return;
		}
		if ((zooming||zoning)&&(!grip)/*&&(mouseZoom)*/&&(e.getButton()==MouseEvent.BUTTON1))//zoom after the zone has been selected
		{
			pix=cross(e.getPoint());
			pix2=pix;

			if (limitZoomInsideBorders == true)
			{
				limitInsideBorders(pix2);
			}

			//don't zoom if moving or if rectangle size is less than 20 pixels in x and y
			if ((moving)||((Math.abs(pix2[0]-pix1[0])<20)&&(Math.abs(pix2[1]-pix1[1])<20)))
			{
				zooming=false;
			}
			else
			{
				double[] point1=t.imageToReal(pix1);
				double[] point2=t.imageToReal(pix2);
				if ((point1[0]==point2[0])&&(point1[1]==point2[1])) return;
				if (point1[0]>point2[0]) {double r=point1[0];point1[0]=point2[0];point2[0]=r;}
				if (point1[1]>point2[1]) {double r=point1[1];point1[1]=point2[1];point2[1]=r;}
				if (zooming)
				{
					t.setRealBoundaries(point1[0],point1[1],point2[0],point2[1]);
					zooming=false;
					zoomed=true;
				}
				if (zoning)
				{
					zoneXmin=point1[0];
					zoneXmax=point2[0];
					zoneYmin=point1[1];
					zoneYmax=point2[1];
					zoning=false;
				}
				repaint();
			}
		}
		if (grip)
		{
			//		pix3=pix;
			pix=cross(e.getPoint());
			double[] point=t.imageToReal(pix);
			Signal1D1D signal=getSelectedPlot().getSignal();
			signal.setPoint(selectedPointGrip,point);
			grip=false;
			autoRescale=true;
			needsRefresh = true;
			repaint();
		}
		moving=false;

		//pass the event to the drawing layers
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.mouseReleased(e);
	}

	// MouseMotionListener implementation
	//
	public void mouseMoved(MouseEvent e)
	{
		//TODO ?
		//onMouseMovedAtPoint(e.getPoint());
		pix=cross(e.getPoint());
		updateMouseCoordsMessage();
		repaint();
		//pass the event to the drawing layers
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.mouseMoved(e);
	}

	public void mouseDragged(MouseEvent e)
	{
		//System.out.println("mouseDragged "+e.getButton()+"     grip="+grip);
		if (moving)
		{
			return;
		}
		if ((zooming/*&&(mouseZoom)*/)||zoning)
		{
			pix=cross(e.getPoint());
			pix2=pix;

			if (limitZoomInsideBorders == true)
			{
				limitInsideBorders(pix2);
			}
		}
		//if ((grip)&&(e.getButton()==MouseEvent.BUTTON3))
		if (grip)
		{
			pix=cross(e.getPoint());
			pix3=pix;
			double[] point=t.imageToReal(pix3);
			Signal1D1D signal=getSelectedPlot().getSignal();
			signal.setPoint(selectedPoint,point);
			//System.out.println("point "+point[0]+"  "+point[1]);
			autoRescale=false;
		}
		if ((showvalue)&&grip)
		{
			pix=cross(e.getPoint());
			double[] real=t.imageToReal(pix);
			//	NumberFormat nf = new DecimalFormat("0.000E0");
			message1=formatXval(real[0]);
			message2=formatYval(real[1]);
		}
		repaint();
		//pass the event to the drawing layers
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.mouseDragged(e);

	}

	public void mouseWheelMoved(MouseWheelEvent e)
	{
		if (mouseWheelZoom)
		{
			//System.out.println("mouseWheelMoved");
			int nbClicks=e.getWheelRotation();
			if (nbClicks<0) for (int i=0;i<-nbClicks;i++) t.zoom(1.1);
			if (nbClicks>0) for (int i=0;i<nbClicks;i++) t.zoom(1.0/1.1);
			repaint();
		}

		//pass the event to the drawing layers
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.mouseWheelMoved(e);
	}






	//KeyListener implementation
	//
	public void keyPressed(KeyEvent e)
	{
		if (keyBoardActions==false) return;
		if (e.getKeyCode()==17)//CTRL pressed
		{
			ctrlPressed=true;
			return;
		}
		Plot plot;
		Vector<Plot> plots;
		int key=e.getKeyCode();
		char keychar=e.getKeyChar();
		//Plot selectedPlot;
		//Vector<Plot> selectedPlots;
		//System.out.println(getClass()+" keychar="+keychar+" keycode="+key+" ctrl pressed:"+ctrlPressed);
		//	String s=KeyEvent.getKeyText(key);
		boolean allowed=true;
		if (keyBoardActionsList!=null) if (keyBoardActionsList.indexOf(keychar)==-1) allowed=false;
		// System.out.println(getClass()+" keyBoardActionsList.val="+keyBoardActionsList.val);
		// System.out.println(getClass()+" allowed="+allowed);
		if (allowed)
		{
			switch (keychar)
			{
			case 'r'  :
				zoomed=false;
				//signal2D1DBuffer=null;//TODO the signalBuffer is not valid anymore
				break;
			case 's'  :
				showvalue=!showvalue;
				break;
			case 'n'  ://navigate among plots: set invisible the others
				selectedPlotIndex--;
				if (selectedPlotIndex<0) selectedPlotIndex=getConcatenatedPlots().nbPlots()-1;
				unselectAll();
				getConcatenatedPlots().select(selectedPlotIndex);
				if (legend!=null) legend.updateGraphics();//update the eventual legend
				trigPlotModifiedListeners();
				break;
			case 'N'  :
				selectedPlotIndex++;
				if (selectedPlotIndex>=getConcatenatedPlots().nbPlots()) selectedPlotIndex=0;
				unselectAll();
				getConcatenatedPlots().select(selectedPlotIndex);
				if (legend!=null) legend.updateGraphics();//update the eventual legend
				trigPlotModifiedListeners();
				break;
			case 'm'  :
			case 'M'  :
				menu=!menu;
				break;

			case 'c'  :
				if (!ctrlPressed)
				{
					if (this.getSelectedPlot()==null)
					{			
						Color c=ColorUtil.getColor(colorList.getPreviousColour());
						this.graphBackground=c;
						this.background=c;
						if (c.getRed()==0) this.foreground=this.gridLinesColor;
						else this.foreground=Global.foreground;
					}
					else applyColor(colorList.getPreviousColour());
					if (legend!=null) legend.updateGraphics();//update the eventual legend
					trigPlotModifiedListeners();
				}
				else
				{
					copyClipBoard();
				}
				break;
			case 'C'  :
				if (this.getSelectedPlot()==null)
				{
					Color c=ColorUtil.getColor(colorList.getNextColour());
					this.graphBackground=c;
					this.background=c;
					if (c.getRed()==0) this.foreground=this.gridLinesColor;
					else this.foreground=Global.foreground;
				}
				else applyColor(colorList.getNextColour());
				if (legend!=null) legend.updateGraphics();//update the eventual legend
				trigPlotModifiedListeners();
				break;
			case 'L'  :
				plots=getSelectedPlots();
				if (plots!=null) 
				{
					for (Plot selectedPlot:plots)
					{
						//selectedPlot=plots[i];
						if (selectedPlot!=null) 
						{
							int th=selectedPlot.getLineThickness();
							th++;
							if (th>15) th=0;
							selectedPlot.setLineThickness(th);

							if (th == 0) selectedPlot.showLine(false);
							else selectedPlot.showLine(true);
						}
					}

					if (legend!=null) legend.updateGraphics();//update the eventual legend
				}
				trigPlotModifiedListeners();
				break;
			case 'l'  :
				plots=getSelectedPlots();
				if (plots!=null) 
				{
					for (Plot selectedPlot:plots)
						//for (int i=0;i<plots.length;i++)
					{
						//selectedPlot=plots[i];
						if (selectedPlot!=null) 
						{
							int th=selectedPlot.getLineThickness();
							th--;
							if (th<0) th=15;
							selectedPlot.setLineThickness(th);

							if (th == 0) selectedPlot.showLine(false);
							else selectedPlot.showLine(true);
						}
					}
					trigPlotModifiedListeners();

					if (legend!=null) legend.updateGraphics();//update the eventual legend
				}
				break;
			case 'a'  :
				if (mouseGrip==true) addingApoint=true;
				// 		System.out.println("add a point");
				break;
			case 'e'  :
				if (mouseGrip==true)
				{
					plot=getSelectedPlot();
					if (plot!=null)
					{
						Signal1D1D signal=plot.getSignal();
						if (signal instanceof Signal1D1DXY)
						{
							((Signal1D1DXY)signal).removePoint(selectedPoint);
						}
					}
				}
				trigPlotModifiedListeners();
				break;
			case 'v'  :

				if (!ctrlPressed)
				{
					for (Plot selectedPlot:getSelectedPlots())
					{
						selectedPlot.setGrayed(!selectedPlot.isGrayed());
						//System.out.println(selectedPlot.getLabel()+" visible="+selectedPlot.isVisible());
					}
				}
				else			
				{
					pasteClipBoard();
					repaint();
				}
				if (legend!=null) legend.updateGraphics();//update the eventual legend

				break;


			case 'x'  :
				t.setLogx(!t.logx());
				break;
			case 'y'  :
				t.setLogy(!t.logy());
				break;
			case 'D'  ://change the style of the dot
			{
				//				selectedPlot=faisceau.getSelected();
				plots=getSelectedPlots();
				for (Plot selectedPlot:plots)
				{
					//selectedPlot=plots[i];
					selectedPlot.setNextDotStyle();
					if (legend!=null) legend.updateGraphics();//update the eventual legend
				}
			}
			trigPlotModifiedListeners();
			break;
			case 'b'  ://put/unput bars
			{
				plots=getSelectedPlots();
				for (Plot selectedPlot:plots)
				{
					selectedPlot.changeBarsStyle();
				}	
				if (legend!=null) legend.updateGraphics();//update the eventual legend
			}
			case 'T'  :
			{
				plots=getSelectedPlots();
				if (plots!=null) 
				{
					for (Plot selectedPlot:plots)
					{
						//selectedPlot=plots[i];
						if (selectedPlot!=null) 
						{
							int size=selectedPlot.getDotSize();
							size++;
							if (size>=20) size=0;
							selectedPlot.setDotSize(size);
						}
					}
					if (legend!=null) legend.updateGraphics();//update the eventual legend
				}
			}
			trigPlotModifiedListeners();
			break;
			case 't'  :
			{
				plots=getSelectedPlots();
				if (plots!=null)
				{
					for (Plot selectedPlot:plots)
					{
						//selectedPlot=plots[i];
						if (selectedPlot!=null) 
						{
							int size=selectedPlot.getDotSize();
							size--;
							if (size<0) size=20;
							selectedPlot.setDotSize(size);
						}
					}

					if (legend!=null) legend.updateGraphics();//update the eventual legend
				}
			}
			trigPlotModifiedListeners();
			break;
			case 'E'  :
			{
				for (Plot selectedPlot:getSelectedPlots())
					if (selectedPlot!=null) 
					{
						selectedPlot.setEmpty(!selectedPlot.getEmpty());
					}
				if (legend!=null) legend.updateGraphics();//update the eventual legend
			}
			trigPlotModifiedListeners();
			break;

			case 'R'  :
			{
				if ((!reticuleH)&(!reticuleV))
				{
					reticuleH=false;
					reticuleV=true;
				}
				else if ((!reticuleH)&(reticuleV))
				{
					reticuleH=true;
					reticuleV=false;
				}
				else if ((reticuleH)&(!reticuleV))
				{
					reticuleH=true;
					reticuleV=true;
				}
				else if ((reticuleH)&(reticuleV))
				{
					reticuleH=false;
					reticuleV=false;
				}
			}
			break;
			case 'g'  ://gridlines ticks or noting
				if(gridLinex) 
				{
					gridLinex=false;
					ticksx=true;
				}
				else if(ticksx) 
				{
					gridLinex=false;
					ticksx=false;
				}
				else
				{
					gridLinex=true;
					ticksx=false;
				}
				if(gridLiney) 
				{
					gridLiney=false;
					ticksy=true;
				}
				else if(ticksy) 
				{
					gridLiney=false;
					ticksy=false;
				}
				else
				{
					gridLiney=true;
					ticksy=false;
				}
				break;
			case 'z'  ://set/unset zone definition
				zoning=!zoning;
				pix1[0]=0;
				pix1[1]=0;
				pix2[0]=0;
				pix2[1]=0;
				setZoneUnDefined();
				break;
			case 'A': //analyse window
				CJFrame cjf1=new CJFrame();
				cjf1.setPreferredSize(new Dimension(900,800));
				cjf1.setTitle("Analyse");
				cjf1.getContentPane().setLayout(new BorderLayout());
				cjf1.getContentPane().add(new AnalysePanel(this));
				cjf1.pack();
				cjf1.setExitAppOnExit(false);
				cjf1.setAutoCenter(true);
				cjf1.setVisible(true);
				break;
			case 'p': //save as png
				saveToPNG();
				break;
			case 'o'  ://more options
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(this,"update"));
				String[] names1={"title","Xlabel","Ylabel","Xunit","Yunit","SeeXaxis","SeeYaxis","KeepXYratio","FormatTickx",
						"FormatTicky","formatz","xScientific","yScientific",
						"GridLinex","GridLiney","Ticksx","Ticksy","NbTicksdesiredx","NbTicksdesiredy",
						"xAxisAsDate","fontName","FontSize"};
				//ParamsBox paramsBox1=new ParamsBox(this,tl,names1);
				ParamsBox paramsBox1=new ParamsBox(this,tl,null,names1,null,null,ParamsBox.VERTICAL,300,30);
				String[] names2={"FixedLimitX","Xmin","Xmax","FixedLimitY","Ymin","Ymax","FixedLimitZ","Zmin","Zmax","Logz","Formatz"};
				//ParamsBox paramsBox2=new ParamsBox(this,tl,names2);
				ParamsBox paramsBox2=new ParamsBox(this,tl,null,names2,null,null,ParamsBox.VERTICAL,300,30);
				//				String[] names3={"paletteVisible"};
				//				ParamsBox paramsBox3=new ParamsBox(this,tl,names2);
				CJFrame cjf=new CJFrame();
				cjf.setTitle("Chart");
				cjf.getContentPane().setLayout(new FlowLayout());
				cjf.getContentPane().add(paramsBox1.getComponent());
				cjf.getContentPane().add(paramsBox2.getComponent());
				cjf.getContentPane().setBackground(Global.background);
				cjf.pack();
				cjf.setExitAppOnExit(false);
				cjf.setAutoCenter(true);
				cjf.setVisible(true);
				break;
			}
		}	
		switch(key)
		{
		case KeyEvent.VK_DELETE:
		case KeyEvent.VK_BACK_SPACE:
		{
			ChartEvent chev=new ChartEvent();
			for (Plot selectedPlot:getSelectedPlots()) chev.addDeletedPlot(selectedPlot);

			for (Plot selectedPlot:getSelectedPlots())
				for (PlotSet plotSet: plotSets)
				{
					plotSet.removePlot(selectedPlot);
				}

			for (PlotDeleteListener l:plotDeleteListeners) l.plotDeleted(chev);
			if (legend!=null) legend.update();

		}
		break;

		case KeyEvent.VK_LEFT :
			if (keyBoardActionsList!=null) if (keyBoardActionsList.indexOf('<')==-1) break;
			//System.out.println(getClass()+" "+"left");
			//selectedPlots=getSelectedPlots();
			//if (selectedPlots!=null)
			for (Plot selectedPlot:getSelectedPlots())
			{
				Signal1D1D signal=selectedPlot.getSignal();
				if (t.logx()) 
				{
					double coef=Util.p10(Util.log10((t.xmax()/t.xmin()))/100.0);
					// 			System.out.println(getClass()+" coef="+coef+" p10(log10((t.xmax()/t.xmin())/100.0))="+p10(log10((t.xmax()/t.xmin()))/100.0));
					signal._multiply_x(1/coef);
				}
				else
				{
					double offset=(t.xmax()-t.xmin())/100.0;
					signal._translate(-offset);
				}
			}
			if (signal2D1D!=null) signal2D1D._translate(-(t.xmax()-t.xmin())/100.0, 0, 0);
			break;
		case KeyEvent.VK_RIGHT :
			if (keyBoardActionsList!=null) if (keyBoardActionsList.indexOf('>')==-1) break;
			//selectedPlots=getSelectedPlots();
			for (Plot selectedPlot:getSelectedPlots())
			{
				Signal1D1D signal=selectedPlot.getSignal();
				if (t.logx()) 
				{
					double coef=Util.p10(Util.log10((t.xmax()/t.xmin()))/100.0);
					signal._multiply_x(coef);
				}
				else
				{
					double offset=(t.xmax()-t.xmin())/100.0;
					signal._translate(offset);
				}
			}
			if (signal2D1D!=null) signal2D1D._translate((t.xmax()-t.xmin())/100.0, 0, 0);
			break;
		case KeyEvent.VK_UP :
			if (keyBoardActionsList!=null) if (keyBoardActionsList.indexOf('u')==-1) break;
			//System.out.println(getClass()+" "+"left");
			//selectedPlots=getSelectedPlots();
			for (Plot selectedPlot:getSelectedPlots())
			{
				Signal1D1D signal=selectedPlot.getSignal();
				if (t.logy()) 
				{
					double coef=Util.p10(Util.log10((t.ymax()/t.ymin()))/100.0);
					signal._multiply(coef);
				}
				else
				{
					double offset=(t.ymax()-t.ymin())/100.0;
					signal._add(offset);
				}
			}
			if (signal2D1D!=null) signal2D1D._translate(0,(t.ymax()-t.ymin())/100.0, 0);

			break;
		case KeyEvent.VK_DOWN :
			if (keyBoardActionsList!=null) if (keyBoardActionsList.indexOf('d')==-1) break;
			//System.out.println(getClass()+" "+"left");
			//selectedPlots=getSelectedPlots();
			for (Plot selectedPlot:getSelectedPlots())
			{
				Signal1D1D signal=selectedPlot.getSignal();
				if (t.logy()) 
				{
					double coef=Util.p10(Util.log10((t.ymax()/t.ymin()))/100.0);
					signal._multiply(1/coef);
				}
				else
				{
					double offset=(t.ymax()-t.ymin())/100.0;
					signal._add(-offset);
				}
			}
			if (signal2D1D!=null) signal2D1D._translate(0,-(t.ymax()-t.ymin())/100.0, 0);
			break;

		}
		//	Launcher l=(Launcher)joloGraphXY2.getNode().getSonOfClass("fa.reuse.jolo.Launcher",false);
		//	if (l!=null) l.launch();

		// if (subnet!=null) subnet.launch();//for (int i=0;i<subnet.length;i++) ((SubNetwork)subnet[i]).workAll();

		needsRefresh = true;
		repaint();
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.keyPressed(e);
	}

	public void keyTyped(KeyEvent e)
	{
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.keyPressed(e);
	}

	public void keyReleased(KeyEvent e)
	{
		if (e.getKeyCode()==17)
		{
			ctrlPressed=false;
			return;
		}
		for (ChartDrawingLayer drawingLayer:chartDrawingLayers) drawingLayer.keyReleased(e);
	}



	public void actionPerformed(ActionEvent e)
	{
		Object source = e.getSource();

		if (source instanceof JMenuItem)
		{
			JMenuItem mi=(JMenuItem)source;
			String label=mi.getText();
			if (label.compareTo(Messager.getString("show_selected_plot_data_in_a_text_editor"))==0)//show selected plot(s)
			{
				Vector<Plot> plots=getSelectedPlots();
				//System.out.println(plot.label());
				if (plots==null) Messager.messErr(Messager.getString("no_plot_selected"));
				else
				{
					TextEditor textEditor=new TextEditor();
					textEditor.setFont(getFont());
					textEditor.setText(Plot.getPlotsDataForExcel(plots));
				}
			}
			if (label.compareTo(Messager.getString("show_all_data_in_a_text_editor"))==0)
			{
				PlotSet plots_=this.getConcatenatedPlots();
				Vector<Plot> plots=plots_.getPlots();
				//System.out.println(plot.label());
				if (plots==null) 
				{
					Messager.messErr(Messager.getString("no_visible_plot"));
					return;
				}
				TextEditor textEditor=new TextEditor();
				textEditor.setFont(getFont());
				textEditor.setText(Plot.getPlotsDataForExcel(plots));
			}

			if (label.compareTo(Messager.getString("load_background_image"))==0)//load background image
			{
				JFileChooser df=new JFileChooser(this.getChartPanel().getApp().path);
				ImageFileFilter imageFileFilter=new ImageFileFilter();
				df.addChoosableFileFilter(imageFileFilter);
				df.setFileFilter(imageFileFilter);
				int returnVal = df.showOpenDialog(this);
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					//					Global.path=df.getSelectedFile().getParent()+File.separator;
					//					String path=df.getSelectedFile().getParent()+File.separator;
					//					String fileName=df.getSelectedFile().getName();
					//TODO backgroundImage=com.cionin.util.Util.getImageAtAnyPath(path+fileName,true);
				}		
			}

			if (label.compareTo(Messager.getString("save_as_png_image"))==0)//save an image capture
			{
				saveToPNG();
			}



			if (label.compareTo(Messager.getString("duplicate"))==0)//copy the selected curve data in the clipboard
			{
				Vector<Plot> plots=getSelectedPlots();
				if (plots==null) 
				{
					Messager.messErr(Messager.getString("no_plot_selected"));
					return;
				}
				PlotSet f=new PlotSet();
				for (Plot plot:plots) this.add(plot.getAcopy());
			}


			if (label.compareTo(Messager.getString("copy_selected_in_clipboard"))==0)//copy the selected curve data in the clipboard
			{
				Vector<Plot> plots=getSelectedPlots();
				//System.out.println(plot.label());
				if (plots==null) 
				{
					Messager.messErr(Messager.getString("no_plot_selected"));
					return;
				}
				if (plots.size()!=1) 
				{
					Messager.messErr(Messager.getString("select_only_one_plot"));
					return;
				}
				else
				{
					ClipBoard.putText(plots.elementAt(0).toStringJustData());
				}
			}
			if (label.compareTo(Messager.getString("paste_data_in_graph"))==0)//paste the clipboard data in the graph as plots
			{
				plotSet0().pasteStringInPlots(false);
				repaint();
				if (legend!=null) legend.updateGraphics();//update the eventual legend

			}

			if (label.compareTo(Messager.getString("paste_data_with_labels_in_graph"))==0)//paste the clipboard data having the first line with plots labels in the graph as plots
			{
				plotSet0().pasteStringInPlots(true);
				repaint();
				if (legend!=null) legend.updateGraphics();//update the eventual legend
			}
			if (label.compareTo(Messager.getString("paste_3_columns_with_errors"))==0)//paste the clipboard data in 3 columns with errors at end column
			{
				String s=ClipBoard.getText();	
				s=s.replaceAll(",", ".");
				double[][] data=StringSource.fillMatrix(s,false);
				//System.out.println(getClass()+" pasted data:\n"+s);
				if (data==null) return;
				Signal1D1DXY pasted=new Signal1D1DXY(data.length);
				//pasted.init(data.length);
				for (int k=0;k<pasted.getDim();k++) pasted.setValue(k,data[k][0],data[k][1]);
				Signal1D1DXY error=new Signal1D1DXY(data.length);
				for (int k=0;k<pasted.getDim();k++) error.setValue(k,data[k][0],data[k][2]);
				Plot plotpasted=new Plot(pasted);
				plotpasted.setError(error);
				plotpasted.setLabel("paste");
				add(plotpasted);

				repaint();
				if (legend!=null) legend.updateGraphics();//update the eventual legend
			}


			if (label.compareTo(Messager.getString("copy_data_and_style"))==0)
				//copy the selected curve(s) data in the clipboard with colors and styles
			{
				Vector<Plot> plots=getSelectedPlots();
				if (plots==null) 
				{
					Messager.messErr(Messager.getString("no_plot_selected"));
					return;
				}
				PlotSet f=new PlotSet();
				for (Plot plot:plots)
				{
					f.add(plot);
				}
				XMLElement el=f.toXML();
				String s=el.toString();
				ClipBoard.putText(s);
			}

			if (label.compareTo(Messager.getString("paste_data_and_style"))==0)
				//paste the selected curve(s) data in the clipboard with colors and styles
			{
				String s=ClipBoard.getText();
				//System.out.println(s);
				PlotSet f2=new PlotSet();

				XMLElement xml = new XMLElement();
				StringReader reader = new StringReader(s);
				try {	xml.parseFromReader(reader);}
				catch (Exception ex) {ex.printStackTrace();}
				f2.updateFromXML(xml);

				for (int i=0;i<f2.nbPlots();i++)
				{
					f2.getPlot(i).setLabel(""+f2.getPlot(i).getLabel());
					//System.out.println(f2.plot(i));	
					// add to existing plotset if any
					if (plotSets.size()==0)this.add(f2.getPlot(i));
					else plotSets.elementAt(0).add(f2.getPlot(i)); 
				}
				repaint();
				if (legend!=null) legend.updateGraphics();//update the eventual legend
			}

			if (label.compareTo(Messager.getString("SelectAll"))==0)//select all
			{
				selectAll();
				repaint();
				if (legend!=null) legend.updateGraphics();//update the eventual legend		
			}

			if (label.compareTo(Messager.getString("Put_a_note"))==0)//select all
			{
				double[] pos=this.getTransformation().imageToReal(mouseClickPosition);
				chartDrawingLayerNotes.add(new Note(this,pos,"Note"));
			}

			if (label.compareTo(Messager.getString("Show_hide_color_chooser"))==0)//show hide color chooser
			{
				ccf.setColorApplier(this);
				ccf.setVisible(!ccf.isVisible());
			}
			if (label.compareTo(Messager.getString("Set_rainbow_colors"))==0)//set rainbow colors to the plots
			{
				PlotSet plots=this.getConcatenatedPlots();
				plots.setRainbow(true);
				plots.applyRainbowColors();
				repaint();
			}



			//**************************************************************
			//***********process menu items:*********************************
			//***************************************************************

			if (label.compareTo(Messager.getString("log_the_Y"))==0)//log base10  the Y
			{
				Vector<Plot> selectedPlots=this.getSelectedPlots();
				for (Plot plot:selectedPlots) plot.getSignal()._log10TheY();
				ChartEvent chev=new ChartEvent();
				for (Plot p:selectedPlots) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}
			if (label.compareTo(Messager.getString("invert(i)"))==0)//invert
			{
				Vector<Plot> selectedPlots=this.getSelectedPlots();
				for (Plot plot:selectedPlots)
				{
					Signal1D1D signal=plot.getSignal();
					if (t.logy()) 
					{
						signal._inverse();
					}
					else
					{
						signal._multiply(-1);
					}
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:selectedPlots) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}
			if (label.compareTo(Messager.getString("Remove_the_points_out_of_the_zone"))==0)//remove the points out of the zone
			{
				Vector<Plot> selectedPlots=this.getSelectedPlots();
				for (Plot plot:selectedPlots)
				{
					Signal1D1D signal=plot.getSignal();
					Signal1D1DXY s2=signal.transfertTo1D1DXY();
					s2=s2.truncx(zoneXmin, zoneXmax);
					s2=s2.truncy(zoneYmin, zoneYmax);
					plot.setSignal(s2);

				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:selectedPlots) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}
			if (label.compareTo(Messager.getString("Delete_the_points_of_the_zone"))==0)//delete the points of the zone
			{
				Vector<Plot> selectedPlots=this.getSelectedPlots();
				for (Plot plot:selectedPlots)
				{
					Signal1D1D signal=plot.getSignal();
					Signal1D1DXY s2=new Signal1D1DXY();
					for (int i=0;i<signal.getDim();i++)
					{
						if ((signal.x(i)<zoneXmin)||(signal.x(i)>zoneXmax)||(signal.y(i)<zoneYmin)||(signal.y(i)>zoneYmax))  
							s2.addPoint(signal.x(i), signal.y(i));
					}
					signal.copy(s2);
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:selectedPlots) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}
			if (label.compareTo(Messager.getString("fft"))==0)//fft
			{
				Vector<Plot> selectedPlots=this.getSelectedPlots();
				for (Plot plot:selectedPlots)
				{
					Signal1D1D signal=plot.getSignal();
					SignalDiscret1D1D s2=new SignalDiscret1D1D(signal.xmin(),signal.xmax(),signal.getDim());
					for (int i=0;i<s2.getDim();i++) s2.setValue(i, signal.y(i));
					add(new Plot(s2.fft()));
					//JoloPlotFrame jpf=new JoloPlotFrame(s2.fft());
				}
			}
			if (label.compareTo(Messager.getString("smooth"))==0)//smooth
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Nb of points averaged");
				inputDialog.setInitialFieldValue("10");
				inputDialog.show();
				int dimkern=new Integer(inputDialog.getStringAnswer());		
				for (Plot plot:getSelectedPlots())
				{
					plot.getSignal()._smooth(dimkern);
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}

			if (label.compareTo(Messager.getString("smooth_filtered"))==0)//smooth with filter
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Nb of points averaged and threshold in sigmas");
				inputDialog.setInitialFieldValue("10 2.0");
				inputDialog.show();
				Definition def=new Definition(inputDialog.getStringAnswer());
				int dimkern=new Integer(def.word(0));
				double  thresholdInSigma=new Double(def.word(1));	
				for (Plot plot:getSelectedPlots())
				{
					plot.getSignal()._smoothFiltered(dimkern,thresholdInSigma);
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}

			if (label.compareTo(Messager.getString("sort"))==0)//sort
			{
				for (Plot plot:getSelectedPlots())
				{
					Signal1D1D signal=plot.getSignal();
					if (signal instanceof Signal1D1DXY)
					{
						((Signal1D1DXY)signal)._sort();
					}
					else Messager.messErr(signal.getClass()+" can't be sorted");
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}

			if (label.compareTo(Messager.getString("derivate"))==0)//derivate
			{
				for (Plot plot:getSelectedPlots())
				{
					Signal1D1D signal=plot.getSignal();
					Signal1D1DXY s2=signal.transfertTo1D1DXY();
					Signal1D1DXY s3=s2.derivate();
					String _label="Derivative of "+plot.getLabel();	
					Plot plot1=new Plot();
					plot1.setColor(plot.getDotColor());
					plot.setLabel(_label);
					add(plot1);
				}
			}

			if (label.compareTo(Messager.getString("histogram"))==0)//histogram
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Nb of cells");
				inputDialog.setInitialFieldValue("50");
				inputDialog.show();
				int dim=new Integer(inputDialog.getStringAnswer());

				PlotSet ps=new PlotSet();
				ChartWindow cw=new ChartWindow(ps);
				cw.getChartPanel().hidePanelSide();
				cw.getCJFrame().setTitle("Histogram");
				for (Plot plot:getSelectedPlots())
				{
					Signal1D1D signal=plot.getSignal();
					double delta=signal.ymax()-signal.ymin();
					Histogram1D1D histo=new Histogram1D1D(dim,signal.ymin()-delta/2,signal.ymax()+delta/2,signal);
					Plot ploth=new Plot(histo);
					ploth.setColor(plot.getDotColor());
					ploth.setLabel(plot.getLabel());
					ps.add(ploth);
				}
				cw.update();
			}

			if (label.compareTo(Messager.getString("progressive_averaging"))==0)//progressive mean
			{
				for (Plot plot:getSelectedPlots())
				{
					plot.getSignal()._progressiveAverage();
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}

			if (label.compareTo(Messager.getString("filtered_progressive_averaging"))==0)//filtered progressive mean
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Threshold in sigmas");
				inputDialog.setInitialFieldValue("1");
				inputDialog.show();
				double  thresholdInSigma=new Double(inputDialog.getStringAnswer());
				for (Plot plot:getSelectedPlots())
				{
					plot.getSignal()._progressiveFilteredAverage(thresholdInSigma);
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}


			if (label.compareTo(Messager.getString("average_values_of_same_X_with_filtering"))==0)//average values of same X
			{
				for (Plot plot:getSelectedPlots())
				{
					Signal1D1D signal=plot.getSignal();

					if (signal instanceof Signal1D1DXY)  
					{
						Signal1D1DXY signal2=(Signal1D1DXY)signal;
						signal2.copy( signal2.averageForSameX(isLogy()));
					}
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}

			if (label.compareTo(Messager.getString("average_values_of_same_X"))==0)//average values of same X with filtering
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Threshold in sigmas");
				inputDialog.setInitialFieldValue("1");
				inputDialog.show();
				double  thresholdInSigma=new Double(inputDialog.getStringAnswer());
				for (Plot plot:getSelectedPlots())
				{
					Signal1D1D signal=plot.getSignal();
					if (signal instanceof Signal1D1DXY)  
					{
						Signal1D1DXY signal2=(Signal1D1DXY)signal;
						signal2.copy( signal2.averageForSameXFiltering(isLogy(), thresholdInSigma) );
					}
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}

			if (label.compareTo(Messager.getString("calc_usual_things"))==0)//calc usual things
			{
				StringBuffer sb=new StringBuffer();
				sb.append("Calculated for point inside the zone (all if no zone defined)\n");
				sb.append("label\tmean\trms\tintegrale\twidth\n");
				NumberFormat nf = new DecimalFormat("0.0000E0");
				for (Plot plot:getSelectedPlots())
				{
					Signal1D1DXY signal2=plot.getSignal().transfertTo1D1DXY();
					signal2._sort();//sort for the integral calculation
					double mean=signal2.mean();
					double rms=signal2.rms(mean);
					double integrale=signal2.integral();
					//profile width using RMS:
					double w=signal2.calcProfileWidthUsingRMS();
					sb.append(plot.getLabel()+"\t"+nf.format(mean)+"\t"+nf.format(rms)+"\t"+nf.format(integrale)+"\t"+w+"\n");
				}
				TextEditor te=new TextEditor();
				te.setText(sb.toString());
			}

			if (label.compareTo(Messager.getString("multiply_number"))==0)//multiply
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Value to multiply");
				inputDialog.setInitialFieldValue("1");
				inputDialog.show();
				double  value=new Double(inputDialog.getStringAnswer());
				for (Plot plot:getSelectedPlots())
				{
					plot.getSignal()._multiply(value);
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}
			if (label.compareTo(Messager.getString("multiply_x_by_number"))==0)//multiply
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Value to multiply the x values");
				inputDialog.setInitialFieldValue("1");
				inputDialog.show();
				double  value=new Double(inputDialog.getStringAnswer());
				for (Plot plot:getSelectedPlots())
				{
					plot.getSignal()._multiply_x(value);
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}

			if (label.compareTo(Messager.getString("add_number"))==0)
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Value to add");
				inputDialog.setInitialFieldValue("0");
				inputDialog.show();
				double  value=new Double(inputDialog.getStringAnswer());
				for (Plot plot:getSelectedPlots())
				{
					plot.getSignal()._add(value);
				}
				ChartEvent chev=new ChartEvent();
				for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}

			if (label.compareTo(Messager.getString("add_slope"))==0)//add a linear function
			{
				//	InputDialog inputDialog=new InputDialog();
				//	inputDialog.setFieldLabel("slope to add");
				//	inputDialog.setInitialFieldValue("0");
				//	inputDialog.show();
				//	double  value=new Double(inputDialog.getStringAnswer());
				//	for (Plot plot:getSelectedPlots())
				//		{
				//		plot.getSignal()._addSlope(value);
				//		}

				PlotProcessWindowAddSlope ppw=new PlotProcessWindowAddSlope(this,getSelectedPlots());

				//	ChartEvent chev=new ChartEvent();
				//	for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
				//	for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
			}



			if (label.compareTo(Messager.getString("subtract_two_plots"))==0)
			{
				Vector<Plot> plots=getSelectedPlots();
				if (plots.size()!=2)
				{
					Messager.messErr("Select 2 plots");
					return;
				}
				add(PlotProcessing.subtractTwoPLots(plots.elementAt(0),plots.elementAt(1)));
			}

			if (label.compareTo(Messager.getString("merge_selected_plots"))==0)
			{
				Vector<Plot> plots=getSelectedPlots();
				if (plots.size()<=1)
				{
					Messager.messErr("Select at least 2 plots");
					return;
				}
				add(PlotProcessing.mergeSelectedPlots(plots));
			}

			if (label.compareTo(Messager.getString("mean_plot"))==0)
			{
				Vector<Plot> plots=getSelectedPlots();
				if (plots.size()<=1)
				{
					Messager.messErr("Select more than 1 plot");
					return;
				}
				add(PlotProcessing.meanOfPlots(plots));
			}

			if (label.compareTo(Messager.getString("normaliseMax"))==0) //set integral to 1
			{
				for (Plot plot:getSelectedPlots())
				{
					Signal1D1D s=plot.getSignal();
					s._multiply(1/s.ymax());
				}
			}
			if (label.compareTo(Messager.getString("normaliseIntegral"))==0) //set integral to 1
			{
				for (Plot plot:getSelectedPlots())
				{
					Signal1D1D s=plot.getSignal();
					double integral=s.integral();
					s._multiply(1/integral);
				}
			}

			if (label.compareTo(Messager.getString("integralCumulator"))==0) //integral cumulator
			{
				for (Plot plot:getSelectedPlots())
				{
					Signal1D1D c=plot.getSignal().getIntegralCumulator();
					Plot p=plot.getAcopy();
					p.setSignal(c);
					add(p);
				}
			}

			if (label.compareTo(Messager.getString("integralProfile"))==0) //integralProfile
			{
				String s="Integral 3D considering the curve as a profile of a axis symetric shape:\n";
				for (Plot plot:getSelectedPlots())
				{
					double integ=plot.getSignal().getIntegralProfile();
					s+=plot.getLabel()+" "+integ+"\n";
				}
				TextEditor te=new TextEditor();
				te.setText(s);
			}

			if (label.compareTo(Messager.getString("integralProfileCumulator"))==0) //integralProfile
			{
				for (Plot plot:getSelectedPlots())
				{
					Signal1D1D c=plot.getSignal().getIntegralProfileCumulator();
					Plot p=plot.getAcopy();
					p.setSignal(c);
					add(p);
				}
			}



			if (label.compareTo(Messager.getString("solve"))==0)//solve
			{
				if (getSelectedPlots().size()!=1) return;
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Value Y ");
				inputDialog.setInitialFieldValue("0");
				inputDialog.show();
				double  y=new Double(inputDialog.getStringAnswer());

				Signal1D1DXY sig=getSelectedPlots().elementAt(0).getSignal().transfertTo1D1DXY();
				double x=sig.solve(y);
				Messager.mess("x value="+x);

			}
			if (label.compareTo(Messager.getString("align_vertically_on_zone"))==0)//align_vertically_on_zone
			{
				if ((zoneXmin==zoneXmax)||(zoneYmin==zoneYmax))
				{
					Messager.messErr("Please define a zone");
					return;
				}
				Vector<Plot> selectedPlots=this.getSelectedPlots();
				for (Plot plot:selectedPlots)
				{
					Signal1D1D signal=plot.getSignal();
					Signal1D1DXY s2=signal.transfertTo1D1DXY();
					s2=s2.truncx(zoneXmin, zoneXmax);
					s2=s2.truncy(zoneYmin, zoneYmax);
					double mean=s2.mean();
					plot.getSignal()._add(-mean);			
				}
			}


			// fit menu:

			if (label.compareTo(Messager.getString("fit_a_line"))==0)//fit a line
			{
				for (Plot plot:getSelectedPlots())
				{
					Plot plotFit=PlotProcessing.fitLine(plot, isLogx(), isLogy());
					add(plotFit);
				}
			}
			if (label.compareTo(Messager.getString("fit_a_parabol"))==0)//fit a parabol
			{
				for (Plot plot:getSelectedPlots())
				{
					Plot plotFit=PlotProcessing.fitParabol(plot, isLogx(), isLogy());
					add(plotFit);
				}
			}
			if (label.compareTo(Messager.getString("fit_a_polynome_order_3"))==0)//fit a polynome cubic
			{
				for (Plot plot:getSelectedPlots())
				{
					Plot plotFit=PlotProcessing.fitPolynome(plot, isLogx(), isLogy(),3);
					add(plotFit);
				}
			}
			if (label.compareTo(Messager.getString("fit_a_polynome_order_4"))==0)//fit a polynome order 4
			{
				for (Plot plot:getSelectedPlots())
				{
					Plot plotFit=PlotProcessing.fitPolynome(plot, isLogx(), isLogy(),4);
					add(plotFit);
				}
			}
			if (label.compareTo(Messager.getString("fit_a_polynome_order_n"))==0)//fit a polynome order n
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Polynom order");
				inputDialog.setInitialFieldValue("5");
				inputDialog.show();
				int  order=new Integer(inputDialog.getStringAnswer());
				for (Plot plot:getSelectedPlots())
				{
					Plot plotFit=PlotProcessing.fitPolynome(plot, isLogx(), isLogy(),order);
					add(plotFit);
				}
			}
			if (label.compareTo(Messager.getString("interpolate_with_a_cubic_spline"))==0)//interpolate with cubic spline
			{
				for (Plot plot:getSelectedPlots())
				{
					Plot plotFit=PlotProcessing.interpolateWithCubicSpline(plot,isLogx(),isLogy());
					add(plotFit);
				}
			}
			if (label.compareTo(Messager.getString("fit_a_cubic_spline"))==0)//fit a cubic spline
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Nb ctrl pts");
				inputDialog.setInitialFieldValue("10");
				inputDialog.show();
				int  nbCtrl=new Integer(inputDialog.getStringAnswer());
				for (Plot plot:getSelectedPlots())
				{
					Plot plotFit=PlotProcessing.fitCubicSpline(plot,isLogx(),isLogy(),nbCtrl);
					add(plotFit);
				}
			}

			//************************************************************************************************
			//image menu items:
			//************************************************************************************************

			if (label.compareTo(Messager.getString("save_image_as_raw"))==0) //save_image_as_raw
			{
				if (signal2D1D==null) return;
				JFileChooser df=new JFileChooser(this.getChartPanel().getApp().path);
				RawFloatImageFileFilter ff=new RawFloatImageFileFilter();
				df.addChoosableFileFilter(ff);
				df.setFileFilter(ff);
				int returnVal = df.showSaveDialog(this);
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					String path=df.getSelectedFile().getParent()+File.separator;
					//System.out.println("Global.chemin:"+Global.chemin);
					String fileName=df.getSelectedFile().getName();
					//String extension=PNGFileFilter.getExtension();
					if (!fileName.endsWith("raw")) fileName+="."+"raw";
					signal2D1D.saveAsRawFloatImage(path+fileName);
				}	
			}

			if (label.compareTo(Messager.getString("load_image_as_raw"))==0) //save_image_as_raw
			{
				if (signal2D1D==null) signal2D1D=new Signal2D1D();
				JFileChooser df=new JFileChooser(this.getChartPanel().getApp().path);
				RawFloatImageFileFilter ff=new RawFloatImageFileFilter();
				df.addChoosableFileFilter(ff);
				df.setFileFilter(ff);
				int returnVal = df.showOpenDialog(this);
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					//	InputDialog inputDialog=new InputDialog();
					//	inputDialog.setFieldLabel("size (width height)?");
					//	inputDialog.setInitialFieldValue((int)signal2D1D.dimx()+" "+(int)signal2D1D.dimy());
					//	inputDialog.show();
					//	Definition def=new Definition(inputDialog.getStringAnswer());
					//	int w=new Integer(def.word(0)).intValue();
					//	int h=new Integer(def.word(1)).intValue();
					//	signal2D1D.init(w, h);
					String path=df.getSelectedFile().getParent()+File.separator;
					String fileName=df.getSelectedFile().getName();
					signal2D1D.readRawFloatImage(path+fileName);
				}	
			}

			if (label.compareTo(Messager.getString("load_png_image"))==0) //load the png image desaturated values
			{
				if (signal2D1D==null) signal2D1D=new Signal2D1D();

				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("xmin xmax ymin ymax of image ? ");
				inputDialog.setInitialFieldValue("0 1 0 1");
				inputDialog.show();

				Definition def=new Definition(inputDialog.getStringAnswer());
				double xmin=new Double(def.word(0)).doubleValue();
				double xmax=new Double(def.word(1)).doubleValue();
				double ymin=new Double(def.word(2)).doubleValue();
				double ymax=new Double(def.word(3)).doubleValue();

				String path1=this.getChartPanel().getApp().path;
				if (path1==null) path1=".";
				JFileChooser df=new JFileChooser(path1);
				PNGFileFilter ff=new PNGFileFilter();
				df.addChoosableFileFilter(ff);
				df.setFileFilter(ff);
				int returnVal = df.showOpenDialog(this);
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					String path=df.getSelectedFile().getParent()+File.separator;
					String fileName=df.getSelectedFile().getName();
					CImage cim=new CImage(path,fileName,false);
					signal2D1D.init(xmin, xmax, ymin, ymax, cim.w(), cim.h());
					signal2D1D.loadFromImage(cim);
					signal2D1D._flipX();
				}	
			}

			if (label.compareTo(Messager.getString("save_png_image"))==0)//save as png image
			{
				JFileChooser df=new JFileChooser(this.getChartPanel().getApp().path);
				PNGFileFilter pNGFileFilter=new PNGFileFilter();
				df.addChoosableFileFilter(pNGFileFilter);
				df.setFileFilter(pNGFileFilter);
				int returnVal = df.showSaveDialog(this);
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					String path=df.getSelectedFile().getParent()+File.separator;
					//System.out.println("Global.chemin:"+Global.chemin);
					String fileName=df.getSelectedFile().getName();
					String extension=PNGFileFilter.getExtension();
					if (!fileName.endsWith(extension)) fileName+="."+extension;

					Image im=this.getSignal2D1D().toImage(this.getPalette(), this.isFixedLimitZ(), zmin, zmax, logz);
					CImage cim=new CImage(im);
					cim.saveImage(path, fileName);
					//Utils.saveAsImagePNG(path,fileName,this);
					//System.out.println(getClass()+" "+path+fileName+" saved");
				}
			}


			if (label.compareTo(Messager.getString("copy_image"))==0)//copy the Signal2D1D as an image to the clipboard
			{
				Image im=signal2D1D.toImage(palette,fixedLimitZ,zmin,zmax,logz);
				ImageSelection imgSel = new ImageSelection(im);
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null);
			}


			if (label.compareTo(Messager.getString("paste_image"))==0)//paste  the Signal2D1D as an image to the clipboard
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("xmin xmax ymin ymax of image ? ");
				inputDialog.setInitialFieldValue("0 1 0 1");
				inputDialog.show();
				Definition def=new Definition(inputDialog.getStringAnswer());
				double xmin=new Double(def.word(0)).doubleValue();
				double xmax=new Double(def.word(1)).doubleValue();
				double ymin=new Double(def.word(2)).doubleValue();
				double ymax=new Double(def.word(3)).doubleValue();

				Image im=null;
				Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
				if (transferable != null && transferable.isDataFlavorSupported(DataFlavor.imageFlavor))
				{
					try
					{
						im= (Image) transferable.getTransferData(DataFlavor.imageFlavor);
					}
					catch (UnsupportedFlavorException ex)
					{
						// handle this as desired
						ex.printStackTrace();
						return;
					}
					catch (IOException ex)
					{
						// handle this as desired
						ex.printStackTrace();
						return;
					}
				}
				else
				{
					System.err.println("getImageFromClipboard: That wasn't an image!");
					return;
				}

				Signal2D1D sig2=  Signal2D1D.getSignal2D1DFromImage(im, "R", 1) ;
				sig2.setxmin(xmin);
				sig2.setxmax(xmax);
				sig2.setymin(ymin);
				sig2.setymax(ymax);
				sig2.updateSamplings();
				if ( this.getSignal2D1D()!=null) this.getSignal2D1D().copy(sig2);
				else this.setSignal2D1D(sig2);

			}

			if (label.compareTo(Messager.getString("save_as_stl"))==0)//save the image as a 3D shape
			{
			}

			if (label.compareTo(Messager.getString("threshold_relative"))==0)//make a threshold to the image
			{
				if (signal2D1D==null) return;
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("Relative threshold:");
				inputDialog.setInitialFieldValue("0.5");
				inputDialog.show();
				double  t=new Double(inputDialog.getStringAnswer());
				signal2D1D._thresholdRelative(t);
			}

			if (label.compareTo(Messager.getString("histogramImage"))==0)//show an histogram the image
			{
				if (this.getSignal2D1D()!=null)
				{
					ChartWindowOld histocw=new ChartWindowOld();
					histocw.getCjframe().setTitle("Image histogram");
					Chart histo=histocw.getChart();
					Plot hp=new Plot(new Histogram1D1D());
					hp.setBarsStyle(Plot.EMPTY_BARS);
					hp.setLineThickness(0);
					hp.setDotSize(0);
					histo.add(hp);
					Histogram1D1D h=(Histogram1D1D)histo.getPlot(0).getSignal();
					double xmin=this.getSignal2D1D().zmin();
					double xmax=this.getSignal2D1D().zmax();
					h.init(200,xmin,xmax);
					h.processSignal2D1D(this.getSignal2D1D());
					histo.update();
				}
			}
			if (label.compareTo(Messager.getString("fft2D"))==0)//show the fft 2D of the image
			{
				if (this.getSignal2D1D()!=null)
				{
					ChartWindow cw=new ChartWindow(new Signal1D1DXY());
					cw.getCJFrame().setTitle("FFT 2D");
					Chart chart=cw.getChart();
					ChartPanel cp=cw.getChartPanel();
					cp.showPalette();
					//cp.hidePanelSide();
					Signal2D1D fft=signal2D1D.fft2d();
					chart.setSignal2D1D(fft);
					//cw.set
					chart.update();
					//	this.setSignal2D1D(fft);
					//	this.update();
				}
			}


			if (label.compareTo(Messager.getString("fourier_filtering"))==0)//show the fourier filtering tool
			{
				if (this.getSignal2D1D()!=null)
				{
					Signal2D1DprocessWindowFourierFilter ppw=new Signal2D1DprocessWindowFourierFilter(this);

				}
			}

			if (label.compareTo(Messager.getString("smooth_filtering"))==0)//show the smooth filtering tool
			{
				if (this.getSignal2D1D()!=null)
				{
					Signal2D1DprocessWindowSmooth ppw=new Signal2D1DprocessWindowSmooth(this);

				}
			}



			if (label.compareTo(Messager.getString("spotProfile"))==0)//calc the profile of a spot
			{
				if (this.getSignal2D1D()!=null)
				{
					Signal1D1DXY profile=this.getSignal2D1D().getProfile();
					Plot hp=new Plot(profile);
					hp.setLineThickness(1);
					hp.setDotSize(3);
					ChartWindow cw=new ChartWindow(hp);
					cw.getCJFrame().setTitle("Spot profile");
					cw.getChart().update();
				}
			}

			if (label.compareTo(Messager.getString("interpolate"))==0)//interpolate the image
			{
				if (this.getSignal2D1D()!=null)
				{
					InputDialog inputDialog=new InputDialog();
					inputDialog.setFieldLabel("Interpolation factor");
					inputDialog.setInitialFieldValue("5");
					inputDialog.show();
					String s=inputDialog.getStringAnswer();
					if (s==null) return;
					int  f=new Integer(s);
					Signal2D1D pixels=this.getSignal2D1D();
					int dimInterpolx=f*pixels.dimx();
					int dimInterpoly=f*pixels.dimy();
					Signal2D1D pixelsInterpolated=new Signal2D1D(pixels.xmin(),pixels.xmax(),pixels.ymin(),pixels.ymax(),dimInterpolx,dimInterpoly);
					//System.out.println("fill pixelsInterpolated...");
					Interpolation2D function2D1D=new Interpolation2D(pixels);
					pixelsInterpolated.fillWithFonction(function2D1D);
					pixels.copy(pixelsInterpolated);
				}
			}

			if (label.compareTo(Messager.getString("add_a_constant"))==0)//add a constant
			{
				if (this.getSignal2D1D()!=null)
				{
					InputDialog inputDialog=new InputDialog();
					inputDialog.setFieldLabel("Value");
					inputDialog.setInitialFieldValue("0");
					inputDialog.show();
					double  t=new Double(inputDialog.getStringAnswer());
					this.getSignal2D1D()._add(t);
				}
			}

			if (label.compareTo(Messager.getString("multiply_by_a_factor"))==0)//multiply the image by a constant
			{
				if (this.getSignal2D1D()!=null)
				{
					InputDialog inputDialog=new InputDialog();
					inputDialog.setFieldLabel("Factor");
					inputDialog.setInitialFieldValue("1");
					inputDialog.show();
					double  t=new Double(inputDialog.getStringAnswer());
					this.getSignal2D1D()._multiply(t);
				}
			}

			if (label.compareTo(Messager.getString("rotate"))==0)//rotate the image
			{
				if (this.getSignal2D1D()!=null)
				{
					InputDialog inputDialog=new InputDialog();
					inputDialog.setFieldLabel("Angle (deg)");
					inputDialog.setInitialFieldValue("0");
					inputDialog.show();
					double  r=new Double(inputDialog.getStringAnswer());
					this.getSignal2D1D()._rotateAroundCenter(r*Math.PI/180.0);
				}
			}

			if (label.compareTo(Messager.getString("rotate90Right"))==0)//rotate the image
			{
				if (this.getSignal2D1D()!=null)
				{
					this.getSignal2D1D()._rotate90Right();
				}
			}

			if (label.compareTo(Messager.getString("binning"))==0)//binning the image
			{
				if (this.getSignal2D1D()!=null)
				{
					InputDialog inputDialog=new InputDialog();
					inputDialog.setFieldLabel("Binning");
					inputDialog.setInitialFieldValue("2");
					inputDialog.show();
					int  r=new Integer(inputDialog.getStringAnswer());
					this.getSignal2D1D()._binning(r);
				}
			}
			if (label.compareTo(Messager.getString("crop_on_zone"))==0)//crop the image
			{
				if (this.getSignal2D1D()!=null)
				{
					this.getSignal2D1D()._crop(this.getZoneXmin(), this.getZoneYmin(), 
							this.getZoneXmax(), this.getZoneYmax());
				}
			}




			if (label.compareTo(Messager.getString("projectionX"))==0)//calc the projection in X direction
			{
				if (this.getSignal2D1D()!=null)
				{
					Signal2D1D sig2=this.getSignal2D1D().copy();
					sig2._crop(this.getZoneXmin(), this.getZoneYmin(), 
							this.getZoneXmax(), this.getZoneYmax());
					Signal1D1DXY sig=sig2.calcProjectionX();
					Plot hp=new Plot(sig);
					hp.setLineThickness(1);
					hp.setDotSize(3);
					ChartWindow cw=new ChartWindow(hp);
					cw.getCJFrame().setTitle("Projection along x axis");
					cw.getChartPanel().hidePanelSide();
					cw.getChartPanel().hideLegend();
					cw.getChart().update();
				}
			}


			if (label.compareTo(Messager.getString("projectionY"))==0)//calc the projection in Y direction
			{
				if (this.getSignal2D1D()!=null)
				{
					Signal2D1D sig2=this.getSignal2D1D().copy();
					sig2._crop(this.getZoneXmin(), this.getZoneYmin(), 
							this.getZoneXmax(), this.getZoneYmax());
					Signal1D1DXY sig=sig2.calcProjectionY();
					Plot hp=new Plot(sig);
					hp.setLineThickness(1);
					hp.setDotSize(3);
					ChartWindow cw=new ChartWindow(hp);
					cw.getCJFrame().setTitle("Projection along y axis");
					cw.getChartPanel().hidePanelSide();
					cw.getChartPanel().hideLegend();
					cw.getChart().update();
				}
			}
			if (label.compareTo(Messager.getString("integral"))==0)//calc the integral
			{
				if (this.getSignal2D1D()!=null)
				{
					double inte=this.getSignal2D1D().integrale();
					Messager.mess("Integral="+inte,"Image processing");
				}
			}


			update();




		}
	}


	public Plot getSelectedPlot()
	{
		return getConcatenatedPlots().getSelectedPlot();
	}

	/**
	 * return a new PlotSet with all the plots of the plot sets concatenated
	 * @return
	 */
	public PlotSet getConcatenatedPlots()
	{
		PlotSet ps=new PlotSet();
		for (PlotSet plotset:plotSets) 
		{
			//plotset.applyRainbowColors();
			ps.add(plotset);
		}
		return ps;
	}

	public int getNbPlots()
	{
		return getConcatenatedPlots().nbPlots();
	}

	/**
	 * return a new PlotSet with all the plots of the plot sets concatenated
	 * @return
	 */
	public PlotSet getNotGrayedConcatenatedPlots()
	{
		PlotSet ps=new PlotSet();
		for (PlotSet plotset:plotSets) 
		{
			ps.addNotGrayed(plotset);
		}
		return ps;
	}

	public Vector<Plot> getSelectedPlots()
	{
		Vector<Plot> v= new Vector<Plot>();
		for (Plot plot:getConcatenatedPlots()) 
		{
			if (plot.isSelected()) v.add(plot);
		}
		return v;
	}
	private void selectAll(){for (Plot plot:getConcatenatedPlots()) plot.setSelected(true);}


	public PlotSet getPlotSetHavingPlot(Plot plot)
	{
		for (PlotSet plotset:plotSets) if (plotset.has(plot)) return plotset;
		return null;	
	}

	public Plot getPlotHavingInLabel(String s)
	{
		for (PlotSet plotset:plotSets) 
		{
			Plot p=plotset.getPlotHavingInLabel(s);
			if (p!=null) return p;
		}
		return null;	
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Plot getPlot(int index)
	{
		return getConcatenatedPlots().getPlot(index);
	}

	public boolean isPointInTheArea(int x, int y)
	{
		return !(
				((x<t.borderx())||(x>(getWidth()-t.borderx())))
				||
				((y<t.bordery())||(y>(getHeight()-t.bordery())))
				);
	}

	private boolean cursorIsInTheArea()
	{
		return !(
				((pix[0]<t.borderx())||(pix[0]>(getWidth()-t.borderx())))
				||
				((pix[1]<t.bordery())||(pix[1]>(getHeight()-t.bordery())))
				);
	}

	private int[] cross(Point mouse)
	{
		int[] c=new int[2];
		if (showvalue)
		{
			c[0]=mouse.x-crossSize;
			c[1]=mouse.y-crossSize;
		}
		else
		{
			c[0]=mouse.x;
			c[1]=mouse.y;
		}

		return c;
	}





	public static double getScreenDistanceFromPointToPlot(Plot plot,Transformation2DPix transformation,double[] coords)
	{
		Signal1D1D signal = plot.getSignal();
		if (signal==null) return 0;

		int dim = signal.getDim();

		if (dim == 0)
		{
			return 1e300;
		}
		else if (dim == 1)
		{
			double[] p2 = {signal.x(0), signal.y(0)};
			int[] screenPos2 = transformation.realToImage(p2);
			double[] sp2 = {screenPos2[0], screenPos2[1]};

			return pointdist2D(sp2, coords);
		}

		double minDist = 1e300;

		double[] p2 = {signal.x(0), signal.y(0)};
		int[] screenPos2 = transformation.realToImage(p2);

		double[] sp1;
		double[] sp2 = {screenPos2[0], screenPos2[1]};

		for (int i = 1; i < dim ; i++)
		{
			sp1 = sp2;

			p2[0] = signal.x(i);
			p2[1] = signal.y(i);

			screenPos2 = transformation.realToImage(p2);

			sp2 = new double[2];

			sp2[0] = screenPos2[0];
			sp2[1] = screenPos2[1];

			double dist = segdist2D(sp1,sp2,coords);

			if (dist < minDist) minDist = dist;
		}

		return minDist;
	}




	public static double quadDist2D(double[] point)
	{
		return Math.sqrt(point[0]*point[0]+point[1]*point[1]);
	}

	public static double pointdist2D(double[] point1, double[] point2)
	{
		double[] delta = {point1[0]-point2[0], point1[1]-point2[1]};
		return quadDist2D(delta);
	}

	public static double segdist2D(double[] segcoord0, double[] segcoord1, double[] point, double[] intersection)
	{
		double linemag = pointdist2D(segcoord0, segcoord1);
		double u = 
				(point[0] - segcoord0[0]) * (segcoord1[0] - segcoord0[0])
				+ (point[1] - segcoord0[1]) * (segcoord1[1] - segcoord0[1]);

		u/=linemag*linemag;

		// if intersection is not on segment
		if (u < 0.0 || u > 1.0)
		{
			double pdist1 = pointdist2D(segcoord0, point);
			double pdist2 = pointdist2D(segcoord1, point);

			if (pdist1 < pdist2) 
			{
				intersection[0] = segcoord0[0];
				intersection[1] = segcoord0[1];
				return pdist1;
			}
			else
			{
				intersection[0] = segcoord1[0];
				intersection[1] = segcoord1[1];
				return pdist2;
			}
		}

		// intersection is on segment. find distance to segment
		intersection[0] = segcoord0[0] + u*(segcoord1[0] - segcoord0[0]);
		intersection[1] = segcoord0[1] + u*(segcoord1[1] - segcoord0[1]);

		return pointdist2D(intersection, point);
	}

	public static double segdist2D(double[] segcoord0, double[] segcoord1, double[] point)
	{
		double[] intersection = new double[2];

		return segdist2D(segcoord0,segcoord1,point,intersection);
	}


	public static Plot selectPlotCloseTo(PlotSet ps,Transformation2DPix transformation,double[] coords)
	{
		Plot selectedPlot=null;
		double currentMinimalDist = 1e300;
		for (Plot plot : ps) 
		{
			if (!plot.isVisible() ) continue;
			else
			{
				double currentDist = getScreenDistanceFromPointToPlot(plot,transformation, coords);
				if (currentDist < currentMinimalDist) 
				{
					currentMinimalDist = currentDist;
					selectedPlot = plot;
				}
			}
		}
		return selectedPlot;
	}

	private void limitInsideBorders (int[] coords)
	{
		if (coords[0] < t.borderx())
		{
			coords[0] = t.borderx();
		}

		if (coords[0] > getWidth() - t.borderx())
		{
			coords[0] = getWidth() - t.borderx();
		}

		if (coords[1] < t.bordery())
		{
			coords[1] = t.bordery();
		}

		if (coords[1] > getHeight() - t.bordery())
		{
			coords[1] = getHeight() - t.bordery();
		}
	}

	/**
format the x value
	 */
	public String formatXval(double x)
	{
		String string;
		String format=formatTickx;
		//format=format.substring(1);
		DecimalFormat nf = new DecimalFormat(format);
		if (xAxisInSeconds==true)
		{
			string=formatSeconds(x, xAxisShowsDays, xAxisShows2Digits,10);
		}
		if (xAxisAsDate==true)
		{
			//		Calendar cal=new GregorianCalendar();
			//		cal.setTimeInMillis((long)x);
			Date date=new Date((long)x);
			DateFormat df;
			if ((t.xmax()-t.xmin())<(24*3600*1000)) df= DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT, Locale.FRANCE);
			else df= DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);
			string=df.format(date);
		}
		else string=nf.format(x);
		return string;
	}


	public String xAxisAsDate(double x)
	{
		String string;
		String format=formatTickx;
		//format=format.substring(1);
		DecimalFormat nf = new DecimalFormat(format);
		if (xAxisInSeconds==true)
		{
			string=formatSeconds(x, xAxisShowsDays, xAxisShows2Digits,10);
		}
		else string=nf.format(x);
		return string;
	}



	public String formatYval(double x)
	{
		return formatYval(formatTicky,x);
	}

	/**
format the y value
	 */
	public String formatYval(String tickFormat, double x)
	{
		String string;
		String format=tickFormat;
		//format=format.substring(1);

		DecimalFormat nf = new DecimalFormat(format);
		string=nf.format(x);

		if (string.equals(""))
		{
			return "";
		}

		int Eposition = string.indexOf('E');

		if (Eposition < 0)
		{
			return string;
		}

		String begin = string.substring(0,Eposition);
		String end = string.substring(Eposition + 1);

		String resultString = "";

		if (end.charAt(0) == '-' && end.equals("-1") == false)
		{
			resultString = begin + 'e' + end;
		}
		else
		{
			resultString = (new DecimalFormat("0.00E0")).format(x);
		}

		return resultString;
	}

	public String formatZval(double x)
	{
		return formatYval(formatz,x);
	}






	/**
format the time such that the 2 main fields (or one if only one) are taken. For instance 2d3h or 2"350  or 3h or 3h2m.
	 * @param seconds number of seconds to format
	 * @param hasDays show days or only hours (2d3h become 51h13min)
	 * @param has2charsValues if true, the number of minutes and seconds have 2 digits
	 * @param msPrecision When the ms are dissplayed after a number of seconds, this prameter indicates the
	 * 			precision: if 1 then all ms are shown (ex:3s245), if 10, only 2 digits are shown
	 * 			(ex:3s24), if 100, only 1 digit is shown (ex:3s2)
	 * @return the formated string
	 */
	public static String formatSeconds(double seconds, boolean hasDays, boolean has2charsValues, int msPrecision)
	{
		if (Math.abs(seconds)<0.001) return "0";
		boolean negative=(seconds<0);
		seconds=Math.abs(seconds);
		int ms=(int)Math.round((seconds-(int)seconds)*1000.);
		int s=(int)seconds;
		int min=s/60;
		s=s-min*60;
		int h=min/60;
		min=min-h*60;
		int d = 0;
		if (hasDays == true)
		{
			d=h/24;
			h=h-d*24;
		}
		String string="";
		if (d!=0)
		{
			string+=d+dayLabel;
			if (h!=0) string+=h+hourLabel;
		}
		else if ((h!=0))
		{
			string+=h+hourLabel;
			if (min!=0) 
			{
				if (has2charsValues && min < 10 )
				{
					string+="0";
				}
				string+=min+minuteLabel;
			}
		}
		else if ((min!=0))
		{
			string+=min+minuteLabel;
			if (s!=0) 
			{
				if (has2charsValues && s < 10)
				{
					string+="0";
				}
				string+=s+secondLabel;
			}
		}
		else if ((s!=0))
		{
			string+=s+secondLabel;
			int msNbDigits = 1;
			if (msPrecision == 1){msNbDigits = 3;}
			else if (msPrecision == 10){msNbDigits = 2;}
			else if (msPrecision == 100){msNbDigits = 1;}

			if ((int) ms/msPrecision!=0) string+= String.format("%0" + msNbDigits + "d", (int) ms/msPrecision);
		}
		else if ((ms!=0))
		{
			string+=ms+millisecondLabel;
		}
		if (negative) string="-"+string;
		return string;
	}





	private void drawMenu(Graphics graphics)
	{
		//trace the background
		graphics.setClip(null);
		graphics.setColor(background);
		graphics.fillRect(0,0,getWidth(),getHeight());
		//trace the graph Background
		graphics.setColor(graphBackground);
		int x1=t.borderx();
		int y1=t.bordery();
		int dx=getWidth()-2*t.borderx();
		int dy=getHeight()-2*t.bordery();
		graphics.fillRect(x1,y1,dx,dy);

		//trace the border rectangle
		//graphics.setColor(foreground);
		//graphics.drawRect(t.borderx(),t.bordery(),getWidth()-2*t.borderx(),getHeight()-2*t.bordery());
		int x=50,y=50,inc=12;
		graphics.setColor(foreground);
		String allowed="rsnN<>udcClLaevp-tTfxyDSEzRgo";
		if (keyBoardActionsList!=null) allowed= keyBoardActionsList;
		if (allowed.indexOf('r')!=-1) { graphics.drawString("r : restore autozoom",x,y);y+=inc;}
		if (allowed.indexOf('s')!=-1) { graphics.drawString("s : show/hide value at mouse cursor",x,y);y+=inc;}
		if (allowed.indexOf('n')!=-1) { graphics.drawString("n : select the next curve and navigate",x,y);y+=inc;}
		//if (allowed.indexOf('N')!=-1) { graphics.drawString("N : show only one curve and navigate ",x,y);y+=inc;}
		if (allowed.indexOf('<')!=-1) { graphics.drawString("left arrow : move left the selected curve by 1% ",x,y);y+=inc;}
		if (allowed.indexOf('>')!=-1) { graphics.drawString("right arrow : move right the selected curve by 1% ",x,y);y+=inc;}
		if (allowed.indexOf('u')!=-1) { graphics.drawString("up arrow : move up the selected curve by 1% ",x,y);y+=inc;}
		if (allowed.indexOf('d')!=-1) { graphics.drawString("down arrow : move down the selected curve by 1% ",x,y);y+=inc;}
		if ((allowed.indexOf('c')!=-1)&(colorList!=null)) { graphics.drawString("c : previous color for the selected curve",x,y);y+=inc;}
		if ((allowed.indexOf('C')!=-1)&(colorList!=null)) { graphics.drawString("C : next color for the selected curve",x,y);y+=inc;}
		if (allowed.indexOf('l')!=-1) { graphics.drawString("l : decrease the line thickness of the selected curve(s)",x,y);y+=inc;}
		if (allowed.indexOf('L')!=-1) { graphics.drawString("L : increase the line thickness of the selected curve(s)",x,y);y+=inc;}
		if (mouseGrip == true)
		{
			if (allowed.indexOf('a')!=-1) { graphics.drawString("a : add a point to the selected curve (with index after the selected point and at mouse click",x,y);y+=inc;}
			if (allowed.indexOf('e')!=-1) { graphics.drawString("e : remove the selected point",x,y);y+=inc;}
		}
		if (allowed.indexOf('v')!=-1) { graphics.drawString("v : hide/show the selected curve",x,y);y+=inc;}
		if (allowed.indexOf('-')!=-1) { graphics.drawString("Del or Suppr : remove the selected curve",x,y);y+=inc;}
		if (allowed.indexOf('x')!=-1) { graphics.drawString("x : set/unset log scale x axis ",x,y);y+=inc;}
		if (allowed.indexOf('y')!=-1) { graphics.drawString("y : set/unset log scale y axis ",x,y);y+=inc;}
		//TODO ? if (signal2D1D!=null) if (allowed.indexOf('i')!=-1) { graphics.drawString("i : save as image bmp... ",x,y);y+=inc;}
		if (allowed.indexOf('D')!=-1) { graphics.drawString("D : change symbol style (or remove dots) ",x,y);y+=inc;}
		if (allowed.indexOf('t')!=-1) { graphics.drawString("t : decrease symbol size of the selected curves",x,y);y+=inc;}
		if (allowed.indexOf('T')!=-1) { graphics.drawString("T : increase symbol size of the selected curves",x,y);y+=inc;}
		if (allowed.indexOf('E')!=-1) { graphics.drawString("E : fill/unfill symbol ",x,y);y+=inc;}
		if (allowed.indexOf('R')!=-1) { graphics.drawString("R : choose reticule pointer ",x,y);y+=inc;}
		if (allowed.indexOf('g')!=-1) { graphics.drawString("g : switch gridlines, ticks or nothing ",x,y);y+=inc;}
		if (allowed.indexOf('z')!=-1) { graphics.drawString("z : draw a zone of interest ",x,y);y+=inc;}
		if (allowed.indexOf('o')!=-1) { graphics.drawString("o : more graph options... ",x,y);y+=inc;}

		//help for the mouse
		graphics.drawString("",x,y);y+=inc;
		graphics.drawString("Mouse:",x,y);y+=inc;
		graphics.drawString("click left and drag: zoom in",x,y);y+=inc;
		graphics.drawString("click right: utilities menu",x,y);y+=inc;

		//graphics.drawString("graphics : grip/move a point of the selected plot",x,y);y+=inc;
	}

	private void updateMouseCoordsMessage()
	{
		if 	(( ! cursorIsInTheArea() || noPlotVisible() )&&(signal2D1D==null))
		{
			message1="";
			message2="";
			return;
		}
		Plot p=getSelectedPlot();
		if ((showvalue)&&(p==null) || (p!= null && p.isVisible() == false) )
		{
			double[] real=t.imageToReal(pix);
			if (signal2D1D!=null)
			{
				int[] pixInImage=new int[2];
				double sx=(signal2D1D.xmax()-signal2D1D.xmin())/signal2D1D.dimx();
				double sy=(signal2D1D.ymax()-signal2D1D.ymin())/signal2D1D.dimy();
				pixInImage[0]=(int)Math.floor((real[0]-signal2D1D.xmin())/sx);
				pixInImage[1]=(int)Math.floor((real[1]-signal2D1D.ymin())/sy);
				//		pixInImage[0]=(int)Math.floor(real[0]/(signal2D1D.xmax()-signal2D1D.xmin())*signal2D1D.dimx());
				//		pixInImage[1]=(int)Math.floor(real[1]/(signal2D1D.ymax()-signal2D1D.ymin())*signal2D1D.dimy());
				message1="x="+formatXval(real[0])+" "+"y="+formatYval(real[1])
				+" (pix "+pixInImage[0]+" "+pixInImage[1]+")";
				message2=formatZval(signal2D1D.getValue(real));
			}
			else
			{
				String xVal = formatXval(real[0]);
				String yVal = formatYval(real[1]);
				if (xVal.equals("") || yVal.equals(""))
				{
					message1="";
					message2="";
				}
				else
				{
					message1=xlabel+"=" + xVal + " " +xunit;
					message2=ylabel+"=" + yVal + " " +yunit;
				}
			}
		}
		Plot plot=getSelectedPlot();
		nearPoint1=null;
		if (plot!=null && plot.isVisible() == true)
		{
			Transformation2DPix crossTrans = t;
			Signal1D1D signal=plot.getSignal();
			if (signal==null) return;
			if (signal.getDim()!=0)
			{
				double[] real=crossTrans.imageToReal(pix);

				if (!grip)
				{
					if (mouseGripNearX) selectedPoint=signal.getNearestXPoint(real);
					else  selectedPoint=signal.getNearestPoint(real);
					nearPoint=signal.getPoint(selectedPoint);
					pix=crossTrans.realToImage(nearPoint);
				}
				else nearPoint=real;

				message1=xlabel+"="+formatXval(nearPoint[0])+" "+xunit;
				message2=ylabel+"="+formatYval(nearPoint[1])+" "+yunit;

			}
		}
	}


	protected boolean noPlotVisible()
	{
		//TODO if (signal2D1D != null) return false;

		for (Plot plot:getConcatenatedPlots())
			if  (plot.isVisible()) return false;

		return true;
	}





	public Vector<Integer> drawYAxisValues(int height, Graphics2D graphics, double[] ticks)
	{
		return drawYAxisValues(height,graphics,ticks,t,formatTicky,yScientific,t.borderx()-3,true);
	}

	public Vector<Integer> drawYAxisValues(
			int height, Graphics2D graphics, double[] ticks, 
			Transformation2DPix t, String format, 
			boolean yScientific, int xOffset, boolean textOnTheLeft
			)
	{
		double[] real=new double[2];
		int[] p1im=new int[2];

		Vector<Integer> ticksDisplayed = new Vector<Integer>();
		Font font = Global.font.deriveFont((float)14);
		int size=(int)(font.getSize()*2.0/3.0 );
		for (int i=0;i<ticks.length;i++)
		{
			real[0]=t.xmin();
			real[1]=ticks[i];
			p1im=t.realToImage(real);

			if (p1im[1] < t.bordery() || p1im[1] > height - t.bordery() )
			{
				if ( ticks[i] != t.ymin() && ticks[i] != t.ymax())
				{
					continue;
				}
			}

			ticksDisplayed.add(i);

			if (yScientific == true)
			{
				AttributedString ss=formatYval_Scientific(ticks[i]);

				TextLayout layout  = new TextLayout(ss.getIterator() , graphics.getFontRenderContext());

				if (textOnTheLeft == true)
				{
					graphics.drawString(ss.getIterator(),(int)(xOffset-layout.getBounds().getWidth()) - 5,p1im[1]);
				}
				else
				{
					graphics.drawString(ss.getIterator(),xOffset + 5,p1im[1]);
				}
			}
			else
			{
				String ss=formatYval(format,ticks[i]);

				if (textOnTheLeft == true)
				{
					graphics.drawString(ss,xOffset-ss.length()*size,p1im[1]);
				}
				else
				{
					graphics.drawString(ss,xOffset + 5,p1im[1]);
				}
			}
		}

		return ticksDisplayed;
	}

	public Vector<Integer> drawXAxisValues(int width,int height, Graphics2D graphics, double[] ticks)
	{
		double[] real=new double[2];
		int[] p1im=new int[2];

		Vector<Integer> ticksDisplayed = new Vector<Integer>();

		Font font = Global.font.deriveFont((float)14);
		int size=(int)(font.getSize()*2.0/3.0 );
		for (int i=0;i<ticks.length;i++)
		{
			real[0]=ticks[i];
			real[1]=t.ymin();
			p1im=t.realToImage(real);
			p1im[0]-=10;

			if (p1im[0] < t.borderx() || p1im[0] > width - t.borderx() )
			{
				if ( ticks[i] != t.xmin() && ticks[i] != t.xmax())
				{
					continue;
				}
			}

			ticksDisplayed.add(i);

			if (xScientific == true)
			{
				AttributedString ss=formatYval_Scientific(ticks[i]);

				TextLayout layout  = new TextLayout(ss.getIterator() , graphics.getFontRenderContext());

				graphics.drawString(ss.getIterator(),p1im[0],height-t.bordery()+20);
			}
			else
			{
				String ss=formatXval(ticks[i]);
				graphics.drawString(ss,p1im[0],height-t.bordery()+20);
			}
		}

		return ticksDisplayed;
	}



	public AttributedString formatYval_Scientific(double x)
	{
		NumberFormat nf = new DecimalFormat("0.##E0");
		String formatedString = nf.format(x);

		if (formatedString.equals(""))
		{
			return new AttributedString("");
		}

		int Eposition = formatedString.indexOf('E');

		if (Eposition < 0)
		{
			return new AttributedString(formatedString);
		}

		String begin = formatedString.substring(0,Eposition);
		String end = formatedString.substring(Eposition + 1);

		String resultString = "";

		boolean expose = false;

		if (begin.equals("1")) 
		{
			if (end.equals("0"))
			{
				resultString = "1";
			}
			else if (end.equals("1"))
			{
				resultString = "10";
			}
			else
			{
				resultString = "10" + end;
				expose = true;
			}
		}
		else
		{
			resultString = begin;

			if (end.equals("0") == false)
			{
				resultString += " 10" + end;
				expose = true;
			}
		}


		AttributedString string = new AttributedString(resultString);
		Font font = Global.font.deriveFont((float)14);
		string.addAttribute(TextAttribute.FONT,font);

		if (expose == true)
		{
			Font exponentFont = font.deriveFont(java.util.Collections.singletonMap(TextAttribute.SUPERSCRIPT, new Integer(1)));
			string.addAttribute(TextAttribute.FONT, exponentFont , resultString.length() - end.length(), resultString.length());
		}

		return string;
	}

	//public PlotSet getPlotSet() {
	//	return myplotset;
	//}

	public Color getBackground() {
		return background;
	}

	public void setBackground(Color background) {
		this.background = background;
	}

	public Color getGraphBackground() {
		return graphBackground;
	}

	public void setGraphBackground(Color graphBackground) {
		this.graphBackground = graphBackground;
	}

	public Color getForeground() {
		return foreground;
	}

	public void setForeground(Color foreground) {
		this.foreground = foreground;
	}

	public Color getGridLinesColor() {
		return gridLinesColor;
	}

	public void setGridLinesColor(Color gridLinesColor) {
		this.gridLinesColor = gridLinesColor;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public boolean isSelectOnClick() {
		return selectOnClick;
	}

	public void setSelectOnClick(boolean selectOnClick) {
		this.selectOnClick = selectOnClick;
	}

	public boolean isUnselectAllOnFarClick() {
		return unselectAllOnFarClick;
	}

	public void setUnselectAllOnFarClick(boolean unselectAllOnFarClick) {
		this.unselectAllOnFarClick = unselectAllOnFarClick;
	}

	public boolean isTakeFocusOnMouseEnter() {
		return takeFocusOnMouseEnter;
	}

	public void setTakeFocusOnMouseEnter(boolean takeFocusOnMouseEnter) {
		this.takeFocusOnMouseEnter = takeFocusOnMouseEnter;
	}

	public boolean isTranslateDraggingOn() {
		return translateDraggingOn;
	}

	public void setTranslateDraggingOn(boolean translateDraggingOn) {
		this.translateDraggingOn = translateDraggingOn;
	}

	public int getCrossSize() {
		return crossSize;
	}

	public void setCrossSize(int crossSize) {
		this.crossSize = crossSize;
	}

	public String getFormatTickx() {
		return formatTickx;
	}

	public void setFormatTickx(String formatTickx) {
		this.formatTickx = formatTickx;
	}

	public String getFormatTicky() {
		return formatTicky;
	}

	public void setFormatTicky(String formatTicky) {
		this.formatTicky = formatTicky;
	}

	public String getFormatz() {
		return formatz;
	}

	public void setFormatz(String formatz) {
		this.formatz = formatz;
	}

	public ColorListInterface getColorList() {
		return colorList;
	}

	public void setColorList(ColorListInterface colorList) {
		this.colorList = colorList;
	}

	public boolean isReticuleH() {
		return reticuleH;
	}

	public void setReticuleH(boolean reticuleH) {
		this.reticuleH = reticuleH;
	}

	public boolean isReticuleV() {
		return reticuleV;
	}

	public void setReticuleV(boolean reticuleV) {
		this.reticuleV = reticuleV;
	}

	public boolean isxAxisInSeconds() {
		return xAxisInSeconds;
	}

	public void setxAxisInSeconds(boolean xAxisInSeconds) {
		this.xAxisInSeconds = xAxisInSeconds;
	}

	public boolean isxAxisAsDate() {
		return xAxisAsDate;
	}

	public void setxAxisAsDate(boolean xAxisAsDate) {
		this.xAxisAsDate = xAxisAsDate;
	}

	public boolean isxAxisShows2Digits() {
		return xAxisShows2Digits;
	}

	public void setxAxisShows2Digits(boolean xAxisShows2Digits) {
		this.xAxisShows2Digits = xAxisShows2Digits;
	}

	public boolean isxAxisShowsDays() {
		return xAxisShowsDays;
	}

	public void setxAxisShowsDays(boolean xAxisShowsDays) {
		this.xAxisShowsDays = xAxisShowsDays;
	}

	public boolean isSeeXaxis() {
		return seeXaxis;
	}

	public void setSeeXaxis(boolean seeXaxis) {
		this.seeXaxis = seeXaxis;
	}

	public boolean isSeeYaxis() {
		return seeYaxis;
	}

	public void setSeeYaxis(boolean seeYaxis) {
		this.seeYaxis = seeYaxis;
	}

	public boolean isTicksx() {
		return ticksx;
	}

	public void setTicksx(boolean ticksx) {
		this.ticksx = ticksx;
	}

	public boolean isTicksy() {
		return ticksy;
	}

	public void setTicksy(boolean ticksy) {
		this.ticksy = ticksy;
	}

	public boolean isGridLinex() {
		return gridLinex;
	}

	public void setGridLinex(boolean gridLinex) {
		this.gridLinex = gridLinex;
	}

	public boolean isGridLiney() {
		return gridLiney;
	}

	public void setGridLiney(boolean gridLiney) {
		this.gridLiney = gridLiney;
	}

	public double[] getXticks() {
		return xticks;
	}

	public void setXticks(double[] xticks) {
		this.xticks = xticks;
	}

	public double[] getYticks() {
		return yticks;
	}

	public void setYticks(double[] yticks) {
		this.yticks = yticks;
	}

	public int getNbTicksdesiredx() {
		return nbTicksdesiredx;
	}

	public void setNbTicksdesiredx(int nbTicksdesiredx) {
		this.nbTicksdesiredx = nbTicksdesiredx;
	}

	public int getNbTicksdesiredy() {
		return nbTicksdesiredy;
	}

	public void setNbTicksdesiredy(int nbTicksdesiredy) {
		this.nbTicksdesiredy = nbTicksdesiredy;
	}

	//public boolean isZoomOnTicks() {
	//	return zoomOnTicks;
	//}
	//
	//public void setZoomOnTicks(boolean zoomOnTicks) {
	//	this.zoomOnTicks = zoomOnTicks;
	//}

	public int getTickLength() {
		return tickLength;
	}

	public void setTickLength(int tickLength) {
		this.tickLength = tickLength;
	}

	public boolean isSee_xlabel() {
		return see_xlabel;
	}

	public void setSee_xlabel(boolean see_xlabel) {
		this.see_xlabel = see_xlabel;
	}

	public boolean isSee_ylabel() {
		return see_ylabel;
	}

	public void setSee_ylabel(boolean see_ylabel) {
		this.see_ylabel = see_ylabel;
	}

	public String getXunit() {
		return xunit;
	}

	public void setXunit(String xunit) {
		this.xunit = xunit;
	}

	public String getYunit() {
		return yunit;
	}

	public void setYunit(String yunit) {
		this.yunit = yunit;
	}

	public String getXlabel() {
		return xlabel;
	}

	public void setXlabel(String xlabel) {
		this.xlabel = xlabel;
	}

	public String getYlabel() {
		return ylabel;
	}

	public void setYlabel(String ylabel) {
		this.ylabel = ylabel;
	}


	public boolean isSeeXvalues()
	{
		return seeXvalues;
	}

	public void setSeeXvalues(boolean seeXvalues)
	{
		this.seeXvalues = seeXvalues;
	}

	public boolean isSeeYvalues()
	{
		return seeYvalues;
	}

	public void setSeeYvalues(boolean seeYvalues)
	{
		this.seeYvalues = seeYvalues;
	}
	//
	//	public int getxLabelDistFromAxis() {
	//		return xLabelDistFromAxis;
	//	}
	//
	//	public void setxLabelDistFromAxis(int xLabelDistFromAxis) {
	//		this.xLabelDistFromAxis = xLabelDistFromAxis;
	//	}

	public boolean isxScientific() {
		return xScientific;
	}

	public void setxScientific(boolean xScientific) {
		this.xScientific = xScientific;
	}

	public boolean isyScientific() {
		return yScientific;
	}

	public void setyScientific(boolean yScientific) {
		this.yScientific = yScientific;
	}


	public Signal2D1D getSignal2D1D() {
		return signal2D1D;
	}


	public void setSignal2D1D(Signal2D1D signal2d1d) {
		signal2D1D = signal2d1d;
	}




	public boolean isFixedLimitX() {
		return fixedLimitX;
	}




	public void setFixedLimitX(boolean b) {
		this.fixedLimitX = b;
	}

	public boolean isFixedLimitY() {
		return fixedLimitY;
	}


	public void setFixedLimitY(boolean b) {
		this.fixedLimitY = b;
	}




	public double getXmin() {
		return xmin;
	}




	public void setXmin(double xmin) {
		this.xmin = xmin;
	}




	public double getXmax() {
		return xmax;
	}




	public void setXmax(double xmax) {
		this.xmax = xmax;
	}




	public double getYmin() {
		return ymin;
	}




	public void setYmin(double ymin) {
		this.ymin = ymin;
	}




	public double getYmax() {
		return ymax;
	}




	public void setYmax(double ymax) {
		this.ymax = ymax;
	}




	public boolean isFixedLimitZ() {
		return fixedLimitZ;
	}




	public void setFixedLimitZ(boolean fixedLimitZ) {
		this.fixedLimitZ = fixedLimitZ;
	}




	public double getZmin() {
		return zmin;
	}




	public void setZmin(double zmin) {
		this.zmin = zmin;
	}




	public double getZmax() {
		return zmax;
	}




	public void setZmax(double zmax) {
		this.zmax = zmax;
	}




	public boolean isLogx() {
		return t.isLogx();
	}

	public void setLogx(boolean logx) {
		t.setLogx(logx);
	}

	public boolean isLogy() {
		return t.isLogy();
	}

	public void setLogy(boolean logy) {
		t.setLogy(logy);
	}




	public boolean isLogz() {
		return logz;
	}




	public void setLogz(boolean logz) {
		this.logz = logz;
	}




	public boolean isKeepXYratio() {
		return t.isKeepXYratio();
	}




	public void setKeepXYratio(boolean keepXYratio) {
		t.setkeepXYratio(keepXYratio);
	}


	public void setAdjustBoundariesToTicks(boolean adjustBoundariesToTicks) {
		this.adjustBoundariesToTicks = adjustBoundariesToTicks;
	}




	public double getZoneXmin()
	{
		return zoneXmin;
	}

	public double getZoneYmin()
	{
		return zoneYmin;
	}

	public double getZoneXmax()
	{
		return zoneXmax;
	}

	public double getZoneYmax()
	{
		return zoneYmax;
	}

	public boolean isMouseGripNearX() {
		return mouseGripNearX;
	}

	public void setMouseGripNearX(boolean mouseGripNearX) {
		this.mouseGripNearX = mouseGripNearX;
	}

	/**
	 * apply a color to the selected plots
	 */
	public void applyColor(String c) 
	{
		Vector<Plot> plots=getSelectedPlots();
		if( plots!=null) for (Plot plot:plots) plot.setColor(c);
		repaint();
		if (legend!=null) legend.updateGraphics();//update the eventual legend
	}



	public void setReverseRainbow(boolean b) 
	{
		for (PlotSet ps:plotSets) ps.setReverseRainbow(b);
	}



	public PaletteInterface getPalette() 
	{
		return palette;
	}



	public void setPalette(PaletteInterface palette) 
	{
		this.palette = palette;
	}


	private boolean isZoneDefined() { return (!((zoneXmin==0)&(zoneXmax==0)&(zoneYmin==0)&(zoneYmax==0)));}

	private void setZoneUnDefined()
	{
		zoneXmin=0;
		zoneYmin=0;
		zoneXmax=0;
		zoneYmax=0;
	}




	public XMLElement toXML() 
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		el.setAttribute("title",title );
		el.setAttribute("xAxisInSeconds",xAxisInSeconds );
		el.setAttribute("xAxisAsDate",xAxisAsDate );
		el.setAttribute("seeXaxis",seeXaxis );
		el.setAttribute("seeYaxis",seeYaxis );
		el.setAttribute("ticksx",ticksx );
		el.setAttribute("formatTickx",formatTickx );
		el.setAttribute("formatTicky",formatTicky );
		el.setAttribute("ticksy",ticksy );
		el.setAttribute("gridLinex", gridLinex);
		el.setAttribute("gridLiney", gridLiney);
		el.setAttribute("adjustBoundariesToTicks", adjustBoundariesToTicks);
		el.setAttribute("nbTicksdesiredx",nbTicksdesiredx );
		el.setAttribute("nbTicksdesiredy",nbTicksdesiredy );
		el.setAttribute("tickLength", tickLength);
		el.setAttribute("see_xlabel",see_xlabel );
		el.setAttribute("see_ylabel",see_ylabel );
		el.setAttribute("xunit",xunit );
		el.setAttribute("yunit",yunit );
		el.setAttribute("xlabel",xlabel );
		el.setAttribute("ylabel",ylabel );
		//el.setAttribute("xLabelDistFromAxis",xLabelDistFromAxis );
		el.setAttribute("xScientific",xScientific );
		el.setAttribute("yScientific",yScientific );
		el.setAttribute("fixedLimitX",fixedLimitX );
		el.setAttribute("fixedLimitY",fixedLimitY );
		el.setAttribute("xmin",xmin );
		el.setAttribute("xmax",xmax );
		el.setAttribute("ymin",ymin );
		el.setAttribute("ymax",ymax );
		el.setAttribute("fixedLimitZ",fixedLimitZ );
		el.setAttribute("zmin",zmin );
		el.setAttribute("zmax",zmax );
		el.setAttribute("logx",t.logx() );
		el.setAttribute("logy",t.logy() );
		el.setAttribute("logz",logz );
		el.setAttribute("fontName",this.getFontName() );
		el.setAttribute("fontSize",this.getFontSize() );
		for (PlotSet plotset : plotSets) el.addChild(plotset.toXML());
		el.addChild(this.getNotes().toXML());
		return el;
	}



	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			this.removeAllPlots();
			this.getNotes().updateFromXML(null);
			title="";	
			xAxisInSeconds=false;
			xAxisAsDate=false;
			seeXaxis=false;
			seeYaxis=false;
			ticksx=true;
			ticksy=true;
			formatTickx="0.00";
			formatTicky="0.00";
			gridLinex=true;
			gridLiney=true;
			adjustBoundariesToTicks=true;
			nbTicksdesiredx=4;	
			nbTicksdesiredy=4;	
			tickLength=4;	
			see_xlabel=true;
			see_ylabel=true;
			xunit="";	
			yunit="";	
			xlabel="x";	
			ylabel="y";	
			//	xLabelDistFromAxis=40;	
			xScientific=false;
			yScientific=false;
			fixedLimitX=false;
			fixedLimitY=false;
			xmin=0;	
			xmax=1;	
			ymin=0;	
			ymax=0;	
			fixedLimitZ=false;
			zmin=0;	
			zmax=1;	
			logz=false;
			return;
		}
		title=xml.getStringAttribute("title","");	
		xAxisInSeconds=xml.getBooleanAttribute("xAxisInSeconds","true","false",false);
		xAxisAsDate=xml.getBooleanAttribute("xAxisAsDate","true","false",false);
		seeXaxis=xml.getBooleanAttribute("seeXaxis","true","false",false);
		seeYaxis=xml.getBooleanAttribute("seeYaxis","true","false",false);
		ticksx=xml.getBooleanAttribute("ticksx","true","false",true);
		ticksy=xml.getBooleanAttribute("ticksy","true","false",true);
		formatTickx=xml.getStringAttribute("formatTickx","0.00");	
		formatTicky=xml.getStringAttribute("formatTicky","0.00");	
		gridLinex=xml.getBooleanAttribute("gridLinex","true","false",true);
		gridLiney=xml.getBooleanAttribute("gridLiney","true","false",true);
		adjustBoundariesToTicks=xml.getBooleanAttribute("adjustBoundariesToTicks","true","false",true);
		nbTicksdesiredx=xml.getIntAttribute("nbTicksdesiredx",4);	
		nbTicksdesiredy=xml.getIntAttribute("nbTicksdesiredy",4);	
		tickLength=xml.getIntAttribute("tickLength",4);	
		see_xlabel=xml.getBooleanAttribute("see_xlabel","true","false",true);
		see_ylabel=xml.getBooleanAttribute("see_ylabel","true","false",true);
		xunit=xml.getStringAttribute("xunit","");	
		yunit=xml.getStringAttribute("yunit","");	
		xlabel=xml.getStringAttribute("xlabel","x");	
		ylabel=xml.getStringAttribute("ylabel","y");	
		//xLabelDistFromAxis=xml.getIntAttribute("xLabelDistFromAxis",40);	
		xScientific=xml.getBooleanAttribute("xScientific","true","false",false);
		yScientific=xml.getBooleanAttribute("yScientific","true","false",false);
		fixedLimitX=xml.getBooleanAttribute("fixedLimitX","true","false",false);
		fixedLimitY=xml.getBooleanAttribute("fixedLimitY","true","false",false);
		xmin=xml.getDoubleAttribute("xmin",0);	
		xmax=xml.getDoubleAttribute("xmax",1);	
		ymin=xml.getDoubleAttribute("ymin",0);	
		ymax=xml.getDoubleAttribute("ymax",0);	
		fixedLimitZ=xml.getBooleanAttribute("fixedLimitZ","true","false",false);
		zmin=xml.getDoubleAttribute("zmin",0);	
		zmax=xml.getDoubleAttribute("zmax",1);	
		t.setLogx(xml.getBooleanAttribute("logx","true","false",false));	
		t.setLogy(xml.getBooleanAttribute("logy","true","false",false));	
		logz=xml.getBooleanAttribute("logz","true","false",false);	
		this.setFontName(xml.getStringAttribute("fontName","Arial"));	
		this.setFontSize(xml.getIntAttribute("fontSize",14));	
		this.removeAllPlots();
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement son=enumSons.nextElement();
			if (son.getName().contains("ChartDrawingLayerNotes"))
			{
				this.getNotes().updateFromXML(son);
			}
			else
			{
				//System.out.println();
				PlotSet plotset=new PlotSet();
				plotset.updateFromXML(son);
				add(plotset);
			}
		}
		this.update();
	}

	public void addPlotDeleteListener(PlotDeleteListener l)
	{
		plotDeleteListeners.add(l);
	}

	public void addPlotModifiedListener(PlotModifiedListener l)
	{
		plotModifiedListeners.add(l);
	}

	public void setBorderx(int borderx) 
	{
		t.setBorderx(borderx);
	}

	public void setBordery(int bordery) 
	{
		t.setBordery(bordery);
	}

	public void setFont(Font font)
	{
		this.font=font;	
	}

	public void setFontName(String s)
	{
		this.font=new Font(s,font.getStyle(),font.getSize());
		if (legend!=null) legend.setFontName(s);
	}

	public String getFontName()
	{
		return font.getFontName();
	}

	public String[] fontNameOptions()
	{
		String[] fonts= GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
		//for (int i = 0; i < fonts.length; i++) 
		return fonts;
	}


	public void setFontSize(int size)
	{
		this.font=font.deriveFont((float)size);
		if (legend!=null) legend.setFontSize(size);
	}

	public int getFontSize()
	{
		return font.getSize();
	}

	public void onPaletteChanged() 
	{
		update();
	}

	protected void trigPlotModifiedListeners()
	{
		ChartEvent chev=new ChartEvent();
		for (Plot p:getSelectedPlots()) chev.addModifiedPlot(p);	
		for (PlotModifiedListener l:plotModifiedListeners) l.plotModified(chev);
	}


	public Transformation2DPix getTransformation()
	{
		return t;
	}

	public void add(ChartDrawingLayer dl)
	{
		chartDrawingLayers.add(dl);
	}

	/**
	 * paste plots contained in the clipboard
	 */
	private void pasteClipBoard()
	{
		String s=ClipBoard.getText();
		//System.out.println(s);
		PlotSet f2=new PlotSet();				
		XMLElement xml = new XMLElement();
		StringReader reader = new StringReader(s);
		try {	xml.parseFromReader(reader);}
		catch (Exception ex) {ex.printStackTrace();}
		f2.updateFromXML(xml);

		for (int i=0;i<f2.nbPlots();i++)
		{
			f2.getPlot(i).setLabel(""+f2.getPlot(i).getLabel());
			//System.out.println(f2.plot(i));	
			this.add(f2.getPlot(i));
		}
	}


	/**
	 * copy selected plots  in the clipboard
	 */
	private void copyClipBoard()
	{
		Vector<Plot> plots1=getSelectedPlots();
		if (plots1==null) 
		{
			Messager.messErr(Messager.getString("no_plot_selected"));
			return;
		}
		PlotSet f=new PlotSet();
		for (Plot plot1:plots1)
		{
			f.add(plot1);
		}
		XMLElement el=f.toXML();
		String s=el.toString();
		ClipBoard.putText(s);
	}

	public ChartDrawingLayerNotes getNotes() {
		return chartDrawingLayerNotes;
	}

	public void saveToPNG()
	{
		JFileChooser df;
		WindowApp app=this.getChartPanel().getApp();
		String path1=".";
		if (app!=null) 	path1=this.getChartPanel().getApp().path;
		df=new JFileChooser(path1);
		//df.setSelectedFile("");
		df.setDialogTitle("Save to PNG");
		PNGFileFilter pNGFileFilter=new PNGFileFilter();
		df.addChoosableFileFilter(pNGFileFilter);
		df.setFileFilter(pNGFileFilter);
		int returnVal = df.showSaveDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			String path=df.getSelectedFile().getParent()+File.separator;
			String fileName=df.getSelectedFile().getName();
			String extension=PNGFileFilter.getExtension();
			if (!fileName.endsWith(extension)) fileName+="."+extension;
			saveToPNG(path,fileName,true);
		}
	}


	public void saveToPNG(String path,String filename,boolean verbose)
	{
		BufferedImage bi = new BufferedImage(this.getSize().width, this.getSize().height, BufferedImage.TYPE_INT_ARGB); 
		Graphics g = bi.createGraphics();
		if (this.getChartPanel()!=null) 
		{
			System.out.println("Save chart with legend to png");
			this.getChartPanel().update();
			this.getChartPanel().saveToPNG(path, filename, verbose);
		}
		else
		{
			System.out.println("Save chart to png");
			this.repaint();
			this.paint(g);  
			g.dispose();
			try{ImageIO.write(bi,"png",new File(path+File.separator+filename));}catch (Exception e1) {}
		}
		if (verbose) System.out.println("Saved image capture in "+path+File.separator+filename);
	}




	public static void main(String[] args) 
	{
		//Global.background=Color.YELLOW;
		Global.font=new Font("Nebraska",Font.PLAIN, 12);
		//Global.font=new Font("DejaVu Sans",Font.PLAIN,10);
		ChartWindow cw=new ChartWindow();


		//make a PlotSet
		PlotSet ps=new PlotSet();

		Signal1D1DXY s=new Signal1D1DXY();
		s.addPoint(0.1, 0.1);
		s.addPoint(1.1, 1.1);
		s.addPoint(2.1, 2.1);
		s.addPoint(4.1, 5.1);
		s.addPoint(7.1, 1.1);
		s.addPoint(8.5, 3.1);
		Plot p=new Plot(s);
		p.setLabel("toto1");
		//p.setBarsStyle(Plot.EMPTY_BARS);
		p.setLineThickness(1);
		ps.add(p);

		//SignalDiscret1D1D si2=new SignalDiscret1D1D(0.1,4.1,(int)4);
		Signal1D1DLogx si2=new Signal1D1DLogx(1,10,(int)4);
		si2.setValue(0, 1.1);
		si2.setValue(1, 10.1);
		si2.setValue(2, -5.1);
		si2.setValue(3, 6.1);
		si2.setPointsSignal(true);
		//System.out.println(si2.toString());
		//System.out.println("isPointsSignal "+si2.isPointsSignal());
		//System.out.println("samplingx="+si2.sampling());

		Signal1D1DLogx si2_error=new Signal1D1DLogx(1,10,(int)4);
		double err=1;
		si2_error.setValue(0, 1);
		si2_error.setValue(1, 1);
		si2_error.setValue(2, 1);
		si2_error.setValue(3, 1);

		Plot p2=new Plot(si2);
		p2.setError(si2_error);
		p2.setLabel("toto2");
		p2.setColor("blue");
		//p2.setBarsStyle(Plot.EMPTY_BARS);
		p2.setLineThickness(1);
		ps.add(p2);


		cw.getChartPanel().add(ps);

		//check XML encoding
		//XMLElement el=ps.toXML();
		//String s2=el.toString();
		//System.out.println(s2);
		//PlotSet ps2=new PlotSet();
		//XMLElement el2=new XMLElement();
		//el2.parseString(s2);
		//ps2.updateFromXML(el2);
		//System.out.println(ps2.getPlot(0).getSignal());


		//test image ploting

		Signal2D1D im=new Signal2D1D(-8,8,-8,8,100,100);
		double sx=im.samplingx();
		double sy=im.samplingy();
		for (int i=0;i<100;i++)
			for (int j=0;j<100;j++)
			{
				double x=im.xmin()+sx*i;
				double y=im.ymin()+sy*j;
				double val=Math.sin(Math.sqrt(x*x+y*y));
				im.setValue(i, j, val);
			}

		cw.add(im,"Image of doubles example");

		cw.getChartPanel().hidePanelSide();
		cw.getChartPanel().getSplitPane().setDividerLocation(0.7);
		cw.getChart().setBackground(Color.YELLOW);
		cw.update();


		//		//check save to png
		//		String path="/home/laurent";
		//		cw.getChartPanel().getChart().saveToPNG(path,"t2.png", true);
		//		CImageProcessing cimp=new CImageProcessing();
		//		cimp.loadFile(path, "t2.png", false);
		//		ImageWindow iw=new ImageWindow(cimp);


	}



}//class end
