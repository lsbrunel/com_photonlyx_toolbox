//creation 2001

package com.photonlyx.toolbox.chart;

import java.awt.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
manage the scaling, log scaling , co-ordinate transfert between a real and pixel co-ordinates
*/
public class Transformation2DPix
{
private double[] minreal=new double[2];//upper left corner of the real co-ordinates
private double[] maxreal=new double[2];//lower right corner of the real co-ordinates
private double[] minCor=new double[2];//upper left corner of the corrected co-ordinates
private double[] maxCor=new double[2];//upper left corner of the corrected co-ordinates
private Dimension d;
private int borderx=80,bordery=60;
private boolean logx=false,logy=false,keepXYratio=false;
private double coefh,coefv,offseth,offsetv;
private Calendar cal=GregorianCalendar.getInstance();
private boolean autoZoomChanged = false;

public boolean isValid()
	{
	if (       coefh == Double.POSITIVE_INFINITY 
			|| coefh == Double.NEGATIVE_INFINITY 
			|| coefv == Double.POSITIVE_INFINITY
			|| coefv == Double.NEGATIVE_INFINITY
			|| coefh == Double.NaN 
			|| coefh == -Double.NaN 
			|| coefv == Double.NaN
			|| coefv == -Double.NaN
			) return false;

	return true;

	}

public void setAutoZoomChanged(boolean autoZoomChanged)
	{
	this.autoZoomChanged = autoZoomChanged;
	}

//set the limits of the graph in the real space
public void setRealBoundaries(double xmin,double ymin,double xmax,double ymax)
{
if (xmax==xmin) xmax=xmin+1;
if (ymax==ymin) ymax=ymin+1;
minreal[0]=xmin;
minreal[1]=ymin;
maxreal[0]=xmax;
maxreal[1]=ymax;
update();
}

//set the limits of the graph for x axis in the real space
public void setRealBoudariesx(double xmin, double xmax) {
minreal[0]=xmin;
maxreal[0]=xmax;
update();
}
//set the limits of the graph for y axis in the real space
public void setRealBoudariesy(double ymin, double ymax) {
minreal[1]=ymin;
maxreal[1]=ymax;
update();
}



public void setLogxLogy(boolean boolx,boolean booly)
{
logx=boolx;
logy=booly;
update();
}

public void setkeepXYratio(boolean bool)
{
keepXYratio=bool;
update();
}

/**
establish the parameters defining the graph to plot in
*/
public void setGraphParams(Dimension d,int borderx,int bordery)
{
this.d=d;
this.borderx=borderx;
this.bordery=bordery;
//System.out.println("coefh "+coefh+"  coefv"+coefv);
//System.out.println("offseth "+offseth+"  offsetv"+offsetv);
update();
}

/**return the left boundary*/
public double xminreal(){return minreal[0];}
/**return the low boundary*/
public double yminreal(){return minreal[1];}
/**return the right boundary*/
public double xmaxreal(){return maxreal[0];}
/**return the high boundary*/
public double ymaxreal(){return maxreal[1];}


/**calculates the pixel coordinates from the real coordinates*/
public int[] realToImage(double[] real)
{
return toPixels(correction(real));
}

/**calculates the pixel coordinates from the real coordinates with no arrounding*/
public double[] realToImageNoRounding(double[] real)
{
return toPixelsNoRounding(correction(real));
}


/**calculates the pixel coordinates from the real coordinates*/
public int[] realToImage(double xreal,double yreal)
{
double[] p=new double[2];
p[0]=xreal;
p[1]=yreal;
return realToImage(p);
}

/**calculates the real coordinates from the pixel coordinates*/
public double[] imageToReal(int[] pix)
{
return correctionInvert(pixelsToCorrected(pix));
}

/**calculates the real coordinates from the pixel coordinates*/
public double[] imageToReal(Point pix)
{
double[] p=new double[2];
p[0]=pix.x;
p[1]=pix.y;
return correctionInvert(pixelsToCorrected(p));
}

/**calculates the pixel coordinates from the real coordinates*/
public double[] imageToReal(double[] pix)
{
return correctionInvert(pixelsToCorrected(pix));
}


/**return the left boundary*/
public double xmin() {return minreal[0];}
/**return the low boundary*/
public double ymin() {return minreal[1];}
/**return the right boundary*/
public double xmax() {return maxreal[0];}
/**return the high boundary*/
public double ymax() {return maxreal[1];}
/**return true log scale applied in x*/
public boolean logx() {return logx;}
/**return true log scale applied in y*/
public boolean logy() {return logy;}
/** return the size of left right border space in pixels*/
public int borderx(){return borderx;}
/** return the size of up down border space in pixels*/
public int bordery(){return bordery;}
/**return the x and y total dimensions (including border space) of the canvas in  pixels*/
public Dimension getDimension(){return d;}


public boolean boundariesChanged() {return autoZoomChanged;}

/**
 * update the transformation coefficients*/
public void update()
{
// if the dimension has not been set yet, don't do anything
if (d==null) return;

minCor=correction(minreal);
maxCor=correction(maxreal);
if (!keepXYratio)
	{
	coefh=(d.width-2*borderx)/(maxCor[0]-minCor[0]);
	coefv=-(d.height-2*bordery)/(maxCor[1]-minCor[1]);
	}
else
	{
	coefh=(d.width-2*borderx)/(maxCor[0]-minCor[0]);
	coefv=-(d.height-2*bordery)/(maxCor[1]-minCor[1]);
	double ratioHoverWpix=(double)d.height/(double)d.width;
	double ratioHoverWreal=(maxCor[1]-minCor[1])/(maxCor[0]-minCor[0]);
	if (ratioHoverWpix>ratioHoverWreal)
		{
		double signv=1;
		if (coefv<0) signv=-1;
		coefv=Math.abs(coefh)*signv;
		}
	else
		{
		double signh=1;
		if (coefh<0) signh=-1;
		coefh=Math.abs(coefv)*signh;
		}
	}
offseth=borderx-coefh*minCor[0];
offsetv=d.height-bordery-coefv*minCor[1];
//System.out.println("coefh "+coefh+"  coefv"+coefv);
//System.out.println("offseth "+offseth+"  offsetv"+offsetv);
}


/**apply the correction (like log)to all axes x and y*/
private double[] correction(double axe[])
{
double[] cor=new double[2];
cor[0]=correctionX(axe[0]);
cor[1]=correctionY(axe[1]);
return cor;
}

/**apply the correction inverse (like exp)to all axes x and y*/
private double[] correctionInvert(double axe[])
{
double[] cor=new double[2];
cor[0]=correctionXinverse(axe[0]);
cor[1]=correctionYinverse(axe[1]);
return cor;
}


/**calculate the image coordinates. d is the size of the image.
 corAxe are the corrected coordinates*/
public int[] toPixels(double[] corAxe)
{
int[] im=new int[2];
//System.out.println("minCor[0] "+minCor[0]+"  minCor[1]"+minCor[1]);
//System.out.println("maxCor[0] "+maxCor[0]+"  maxCor[1]"+maxCor[1]);
//System.out.println("coefh "+coefh+"  coefv"+coefv);
im[0]=(int)(offseth+coefh*corAxe[0]);
im[1]=(int)(offsetv+coefv*corAxe[1]);
return im;
}

/**calculate the image coordinates. d is the size of the image.
 corAxe are the corrected coordinates*/
private double[] toPixelsNoRounding(double[] corAxe)
{
double[] im=new double[2];
//System.out.println("minCor[0] "+minCor[0]+"  minCor[1]"+minCor[1]);
//System.out.println("maxCor[0] "+maxCor[0]+"  maxCor[1]"+maxCor[1]);
//System.out.println("coefh "+coefh+"  coefv"+coefv);
im[0]=(offseth+coefh*corAxe[0]);
im[1]=(offsetv+coefv*corAxe[1]);
return im;
}

/**
calculate the corrected coordinates from the image coordinates. 
*/
public double[] pixelsToCorrected(int[] pix)
{
double[] cor=new double[2];
cor[0]=(pix[0]-offseth)/coefh;
cor[1]=(pix[1]-offsetv)/coefv;
return cor;
}

/**
calculate the corrected coordinates from the image coordinates. 
*/
private double[] pixelsToCorrected(double[] pix)
{
double[] cor=new double[2];
cor[0]=(pix[0]-offseth)/coefh;
cor[1]=(pix[1]-offsetv)/coefv;
return cor;
}


private double correctionY(double y)
{
if (logy) if (y>0) return Math.log(y)/Math.log(10);else return 0;
else return y;
}
private double correctionYinverse(double Y)
{
if (logy) return Math.pow(10,Y);
else  return Y;
}
private double correctionX(double x)
{
if (logx) if (x>0) return Math.log(x)/Math.log(10); else return 0;
else return x;
}
private double correctionXinverse(double X)
{
if (logx) return Math.pow(10,X);
else  return X;
}


/**change the limits in order to zoom by coef*/
public void zoom(double coef)
{
double xmin=minreal[0];
double ymin=minreal[1];
double xmax=maxreal[0];
double ymax=maxreal[1];

if (!logx)
	{
	double halfrange=(xmax-xmin)/2;
	double centre=(xmax+xmin)/2;
	xmin=centre-halfrange*coef;
	xmax=centre+halfrange*coef;
	}
else
	{
	xmin/=coef;
	xmax*=coef;
	}
if (!logy)
	{
	double halfrange=(ymax-ymin)/2;
	double centre=(ymax+ymin)/2;
	ymin=centre-halfrange*coef;
	ymax=centre+halfrange*coef;
	}
else
	{
	ymin/=coef;
	ymax*=coef;
	}
//System.out.println(minreal[0]+" "+minreal[1]);
//System.out.println(maxreal[0]+" "+maxreal[1]);
minreal[0]=xmin;
minreal[1]=ymin;
maxreal[0]=xmax;
maxreal[1]=ymax;
//System.out.println(minreal[0]+" "+minreal[1]);
//System.out.println(maxreal[0]+" "+maxreal[1]);

update();
}



/**change the transformation such that the clicked point comes to the center*/
public void putAtTheCenter(int x,int y)
{
double xmin=minreal[0];
double ymin=minreal[1];
double xmax=maxreal[0];
double ymax=maxreal[1];

int[] pix=new int[2];
pix[0]=x;
pix[1]=y;
double[] real=imageToReal(pix);
double[] cor=pixelsToCorrected(pix);

double[] min=new double[2];
min[0]=xmin;
min[1]=ymin;
double[] max=new double[2];
max[0]=xmax;
max[1]=ymax;
double[] mincor=correction(min);
double[] maxcor=correction(max);

double[] centrecor=new double[2];
centrecor[0]=(maxcor[0]+mincor[0])/2;
centrecor[1]=(maxcor[1]+mincor[1])/2;
double[] centre=correctionInvert(centrecor);
mincor[0]-=centrecor[0]-cor[0];
mincor[1]-=centrecor[1]-cor[1];
maxcor[0]-=centrecor[0]-cor[0];
maxcor[1]-=centrecor[1]-cor[1];
min=correctionInvert(mincor);
max=correctionInvert(maxcor);

if (!logx)
	{
	double centrex=(xmax+xmin)/2;
	xmin-=centrex-real[0];
	xmax-=centrex-real[0];
	}
else
	{
	xmin=min[0];
	xmax=max[0];
	}
if (!logy)
	{
	double centrey=(ymax+ymin)/2;
	ymin-=centrey-real[1];
	ymax-=centrey-real[1];
	}
else
	{
	ymin=min[1];
	ymax=max[1];
	}

minreal[0]=xmin;
minreal[1]=ymin;
maxreal[0]=xmax;
maxreal[1]=ymax;
update();
}


private static double log10(double r) { return Math.log(r)/Math.log(10);}
private static double p10(double r) { return Math.pow(10,r);}


/**gives a list of values for each tick (the boudaries are thick) at entire decades*/
public  double[] getDecadesTicks(double min,double max)
{

if (max<=min)//impossible case
	{
	double values[]=new double[0];
	return values;
	}
if (min<=0)//no negative values are admitted !
	{
	double values[]=new double[0];
	return values;
	}
// calculate the number or full decades used
double lmin=Math.floor(log10(min));
double newmin=p10(lmin);
double lmax=Math.round(log10(max));
int nbdecades=(int)Math.floor(lmax-lmin);
double values[]=new double[nbdecades+1];
int nbTicks=nbdecades+1;
for (int i=0;i<nbTicks;i++) values[i]=newmin*p10(i);

////test
// System.out.println("min="+min+"  lmin="+lmin+" newmin="+newmin);
// System.out.println("max="+max+"  lmax="+lmax+" newmax="+newmax);
// System.out.println("nbTicks="+nbTicks);
//for (int i=0;i<nbTicks;i++) System.out.println(values[i]);

return values;
}



/**try to put intervals each 1,2 or 5 (at all decades) . gives a list of values for each inner tick*/
public static double[] getTicks(int nbTicksdesired,double min,double max)
{
double[] r;

if (min==max) 
	{
	double values[]=new double[2];
	values[0]=min;
	values[1]=max;
	return values;
	}

//System.out.println();
//System.out.println("min="+min+" "+"max="+max);
//System.out.println("nbTicksdesired="+nbTicksdesired);

//calculate the best ticks interval
//double intervalAvant=(max-min)/(nbTicksdesired+1);
//System.out.println("intervalAvant="+intervalAvant);
r=closestLower125((max-min)/(nbTicksdesired+1));
//System.out.println("interval apres="+r[0]+"e"+r[1]);
double interval=r[0]*p10(r[1]);


//calculate the new xmin
double newmin=Math.floor(min/interval)*interval;
double newmax=Math.ceil(max/interval)*interval;
//System.out.println("oldmin="+min+" newmin="+newmin);
//System.out.println("oldmax="+max+" newmax="+newmax	);

//int nbTicks=(int)((max-newmin)/interval)+1;
int nbTicks=(int)Math.round(((newmax-newmin)/interval)+1);
//System.out.println("nbTicks en fait ="+((newmax-newmin)/interval)+1);
//System.out.println("nbTicks en fait ="+nbTicks);

//transfert the inside values in a new array
double values[]=new double[nbTicks];
for (int i=0;i<nbTicks;i++)
	{
	values[i]=newmin+interval*i;
	}

//System.out.println("ticks: ");
//for (int i=0;i<values.length;i++) System.out.print(values[i]+" ");
//System.out.println();

return values;
}

/**try to put intervals each 1,2 or 5 (at all decades) . gives a list of values for each inner tick*/
public double[] getTicksx(int nbTicksdesired) {
	return getTicks(nbTicksdesired,minreal[0],maxreal[0]);
}

/**try to put intervals each 1,2 or 5 (at all decades) . gives a list of values for each inner tick*/
public double[] getTicksy(int nbTicksdesired) {
	return getTicks(nbTicksdesired,minreal[1],maxreal[1]);
}

/**
return the closest lower among 1 2 and 5 and the decade in which we are. r positive or negative
first value the left part of the scientific notation. Second value the power of 10.
*/
private static double[] closestLower125(double r)
{
if (r>0) return closestLower125Positive(r);
else
	{
	double[] result1=closestUpperer125Positive(-r);
	double[] result=new double[2];
	result[0]=-result1[0];
	result[1]=result1[1];
	return result;
	}
}

/**return the closest lower among 1 2 and 5 and the decade in which we are . r positive
first value the left part of the scientific notation. Second value the power of 10.
*/
private static double[] closestLower125Positive(double r)
{
//calc the decade in which we are
double decade=(double)Math.floor(log10(r));
//then get the left part of the scientific notation
double lscientific=r/p10(decade);
//then get the closest lower among 1 2 and 5
double closest;
if (lscientific>=5) closest=5; else if (lscientific>=2) closest=2; else  closest=1;
double[] result=new double[2];
result[0]=closest;
result[1]=decade;
return result;
}

/**return the closest upperer among 1 2 and 5 and the decade in which we are. r positive
first value the left part of the scientific notation. Second value the power of 10.
*/
private static double[] closestUpperer125Positive(double r)
{
//calc the decade in which we are
double decade=(double)Math.floor(log10(r));
//then get the left part of the sceintific notation
double lscientific=r/p10(decade);
//then get the closest lower among 1 2 and 5
double closest;
if (lscientific<=1) closest=1; else if (lscientific<=2) closest=2; else  closest=5;
double[] result=new double[2];
result[0]=closest;
result[1]=decade;
return result;
}


/**
in a time range expressed in seconds. try to put special intervals likes days, hours, minutes , seconds 
@return gives a list of values for each inner tick
*/
public  double[] getTimeTicksx(int nbTicksdesired)
{
return getTimeTicks(nbTicksdesired,minreal[0],maxreal[0]);
}

/**
in a time range expressed in seconds. try to put special intervals likes days, hours, minutes , seconds
 
@return gives a list of values for each inner tick
*/
/**
 * 
 * @param nbTicksdesired
 * @param min in s
 * @param max in s
 * @return
 */
public  double[] getTimeTicks(int nbTicksdesired,double min,double max)
{
double interval,newmin,newmax,d;
int nbTicks;
double[] values;
double[] r;

if (min>=max) 
	{
	values=new double[2];
	values[0]=min;
	values[1]=max;
	return values;
	}
	
//format the time in days,hours,minutes,seconds and ms
TimeFormat tf=new TimeFormat((max-min)/(nbTicksdesired+1));
if (tf.d()>=1)
	{
	d=tf.d();
	r=closestLower125(d);
	interval=r[0]*p10(r[1])*24*3600;
	
	}
else if (tf.h()>=1)
	{
	d=tf.h();
	r=closestLower125(d);
	interval=r[0]*p10(r[1])*3600;
	}
else if (tf.m()>=1)
	{
	d=tf.m();
	r=closestLower125(d);
	interval=r[0]*p10(r[1])*60;
	}
else if (tf.s()>=1)
	{
	d=tf.s();
	r=closestLower125(d);
	interval=r[0]*p10(r[1]);
	}
else 
	{
	d=tf.ms();
	r=closestLower125(d);
	interval=r[0]*p10(r[1])/1000;
	}

//set the min and max dates
//if (tf.d()>=1)
//	{
//	Date date=new Date((long)(min));
//	cal.setTime(date);
//	cal.set(cal.get(Calendar.YEAR),cal.get(Calendar.MONTH),cal.get(Calendar.DAY_OF_MONTH));
//	newmin=cal.getTime().getTime();
//	cal.add(Calendar.DAY_OF_YEAR, tf.d());
//	newmax=cal.getTime().getTime();
//	}
//else
	{
	newmin=Math.floor(min/interval)*interval;
	newmax=Math.ceil(max/interval)*interval;
	}

nbTicks=(int)((newmax-newmin)/interval)+1;

if (newmin>=newmax || nbTicks <= 0 ) 
	{
	values=new double[2];
	values[0]=min;
	values[1]=max;
	return values;
	}
//transfert the inside values in a new array
/*System.out.println(getClass()+" "+"newmin="+newmin);
System.out.println(getClass()+" "+"newmax="+newmax);
System.out.println(getClass()+" "+"nbTicks="+nbTicks);*/
values=new double[nbTicks];
for (int i=0;i<nbTicks;i++)
	{
	values[i]=newmin+interval*i;
	}
return values;
}



/**
time formater
*/
public class TimeFormat
{
private int d,h,m,s,ms;
//format the time in days,hours,minutes,seconds and ms
public TimeFormat(double seconds)
{
seconds=Math.abs(seconds);
ms=(int)Math.round((seconds-(int)seconds)*1000.);
s=(int)seconds;
m=s/60;
s=s-m*60;
h=m/60;
m=m-h*60;
d=h/24;
h=h-d*24;
// System.out.println(getClass()+" "+d+"d"+h+"h"+m+"m"+s+"s"+ms+"ms");
}
public int d(){return d;}
public int h(){return h;}
public int m(){return m;}
public int s(){return s;}
public int ms(){return ms;}
}

public boolean isInTheArea (double x, double y)
{
if (x < minreal[0] 
		|| x > maxreal[0] 
		|| y < minreal[1] 
		|| y > maxreal[1]
	)
	{
	return false;
	}
else
	{
	return true;
	}
}

public boolean isInTheArea (double[] p)
{
return isInTheArea(p[0],p[1]);
}

public void translate( int[] delta)
{
int[] p1=new int[2];
int[] p2=new int[2];
//System.out.println(delta[0]+" "+delta[1]);

p1[0]=borderx-delta[0];
p1[1]=bordery-delta[1];
double[] r1=imageToReal(p1);

p2[0]=d.width-borderx-delta[0];
p2[1]=d.height-bordery-delta[1];
double[] r2=imageToReal(p2);

//System.out.println(minreal[0]+" "+minreal[1]);
//System.out.println(maxreal[0]+" "+maxreal[1]);

minreal[0]=r1[0];
minreal[1]=r2[1];
maxreal[0]=r2[0];
maxreal[1]=r1[1];

update();

//System.out.println(minreal[0]+" "+minreal[1]);
//System.out.println(maxreal[0]+" "+maxreal[1]);

}



public boolean isLogx() {
	return logx;
}

public void setLogx(boolean logx) {
	this.logx = logx;
}

public boolean isLogy() {
	return logy;
}

public void setLogy(boolean logy) {
	this.logy = logy;
}

public boolean isKeepXYratio() {
	return keepXYratio;
}

public void setKeepXYratio(boolean keepXYratio) {
	this.keepXYratio = keepXYratio;
}

public double[] getDecadesTicksx() {
	return getDecadesTicks(minreal[0],maxreal[0]);
}
public double[] getDecadesTicksy() {
	return getDecadesTicks(minreal[1],maxreal[1]);
}

public boolean areGraphLimitsNull() {
	//check the case where the surface to plot is zero
	double surface=(maxreal[0]-minreal[0])*(maxreal[1]-minreal[1]);
	return (surface==0);
}

public int getBorderx() {
	return borderx;
}

public void setBorderx(int borderx) {
	this.borderx = borderx;
}

public int getBordery() {
	return bordery;
}

public void setBordery(int bordery) {
	this.bordery = bordery;
}


}
