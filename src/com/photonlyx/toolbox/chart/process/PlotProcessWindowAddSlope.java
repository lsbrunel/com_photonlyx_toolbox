package com.photonlyx.toolbox.chart.process;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.WindowEvent;
import java.util.Vector;

import com.photonlyx.toolbox.chart.Chart;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.gui.ParDoubleBox;
import com.photonlyx.toolbox.gui.ParamBox;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;

public class PlotProcessWindowAddSlope extends CJFrame
{
/**
	 * 
	 */
public double slope;
private Vector<Plot> plots,plots2;
private Chart chart;

public PlotProcessWindowAddSlope(Chart chart,Vector<Plot> plots)
{
this.plots=plots;
this.chart=chart;

TriggerList tl=new TriggerList();
tl.add(new Trigger(this,"update"));
String[] names1={"slope"};
ParamsBox paramsBox1=new ParamsBox(this,tl,names1);
ParDoubleBox pb=(ParDoubleBox) paramsBox1.getParamBoxOfParam("slope");
pb.setWheelAdd(true);
pb.setWheelCoef(0.001);

CJFrame cjf=this;
cjf.setTitle("Plot processing");
cjf.getContentPane().setLayout(new FlowLayout());
cjf.getContentPane().add(paramsBox1.getComponent());
cjf.pack();
cjf.setExitAppOnExit(false);
cjf.setAutoCenter(true);
cjf.setVisible(true);

plots2=new Vector<Plot>();
for (Plot p:plots)
	{
	Plot p2=new Plot(p.getSignal().transfertTo1D1DXY(),p.getLabel(),p.getLineColor());
	plots2.add(p2);
	chart.add(p2);
	}
chart.update();
////button to validate and exit
//{
//TriggerList tl_button=new TriggerList();
//tl_button.add(new Trigger(this,"exit"));
//CButton b=new CButton(tl_button);
//b.setPreferredSize(new Dimension(50,20));
//b.setText("<-");
//b.setBorderPainted(false);
//cjf.getContentPane().add(b);
//}


}


public void update()
{
for (int i=0;i<plots.size();i++)
	{
	Plot p=plots.elementAt(i);
	Plot p2=plots2.elementAt(i);
	p2.setSignal(p.getSignal().transfertTo1D1DXY());
	p2.getSignal()._addSlope(slope);
	}
chart.update();
}

//
//public void exit()
//{
//for (int i=0;i<plots.size();i++)
//	{
//	Plot p=plots.elementAt(i);
//	Plot p2=plots2.elementAt(i);
//	p.getSignal()._addSlope(slope);
//	chart.remove(p2);
//	}
//chart.update();
//this.get
//
//}


public void windowClosing(WindowEvent event)
{
for (int i=0;i<plots.size();i++)
	{
	Plot p=plots.elementAt(i);
	Plot p2=plots2.elementAt(i);
	p.getSignal()._addSlope(slope);
	chart.remove(p2);
	}
chart.update();
}


}
