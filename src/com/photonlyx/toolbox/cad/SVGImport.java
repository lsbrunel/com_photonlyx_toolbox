package com.photonlyx.toolbox.cad;

import java.io.File;
import java.io.StringReader;
import java.util.Enumeration;
import javax.swing.JFileChooser;

import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Messager;

import nanoxml.XMLElement;

public class SVGImport 
{



	public  static    PlotSet importsvg(String path)
	{
		PlotSet  plots=new PlotSet();	
		CFileFilter jff=new CFileFilter("svg","svg file");
		String ch=path;
		if (ch==null) ch=".";
		JFileChooser df=new JFileChooser(ch);
		df.addChoosableFileFilter(jff);
		df.setFileFilter(jff);
		int returnVal = df.showOpenDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			path=df.getSelectedFile().getParent()+File.separator;
			//String path=df.getSelectedFile().getParent()+File.separator;
			String fileName=df.getSelectedFile().getName();
			read(path+fileName,plots);
		}
		return plots;
	}



	public static  void  read(String filename,PlotSet  plots) 
	{
		boolean absolute=true;
		String s=TextFiles.readFile(filename,false).toString();

		XMLElement xml = new XMLElement();
		StringReader reader = new StringReader(s);
		try {	xml.parseFromReader(reader);}
		catch (Exception ex) {ex.printStackTrace();}

		String width=(String)xml.getAttribute("width");
		if (!width.contains("mm"))
		{
			Messager.messErr("Use mm units");
			return;
		}
		double wid=new Double(width.replaceAll("mm", ""));
		String vb=(String)xml.getAttribute("viewBox");
		Definition vbd=new Definition(vb);
		double dx=new Double(vbd.word(2))-new Double(vbd.word(0));
		System.out.println("width="+wid+" mm");
		double scale=wid/dx;//mm/pix
		System.out.println("scale="+scale+" mm/pix");

		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements()) 
		{
			XMLElement son = enumSons.nextElement();
			System.out.println("son:   "+son.getName());
			if (son.getName().compareTo("g")==0)
			{
				Enumeration<XMLElement> enumSons2 = son.enumerateChildren();
				while (enumSons2.hasMoreElements()) 
				{
					XMLElement son2 = enumSons2.nextElement();
					System.out.println("        "+son2.getName());
					if (son2.getName().compareTo("path")==0) processPath(son2,plots,scale);
				}

			}
			if (son.getName().compareTo("path")==0) processPath(son,plots,scale);
		}


	}






	public static   void processPath(XMLElement xml,PlotSet  plots,double scale)	
	{
		Signal1D1DXY sig=new Signal1D1DXY(0);
		String s1=(String)xml.getAttribute("d");
		//System.out.println("                 "+s1);
		Definition def=new Definition(s1);
		if ((def.hasWord("c"))||(def.hasWord("C")))
		{
			Messager.messErr("Use only polyline SVG");
			return;
		}
		int i=0;
		double x=0,y=0;
		String mode="";
		for (;;) 
		{
			String w=def.word(i);
			if ((w.compareTo("M")==0) 
					||
					(w.compareTo("L")==0) 
					||
					(w.compareTo("H")==0) 
					||
					(w.compareTo("V")==0) 
					||
					(w.compareTo("m")==0) 
					||
					(w.compareTo("l")==0) 
					||
					(w.compareTo("h")==0) 
					||
					(w.compareTo("v")==0) 
					||
					(w.compareTo("z")==0)
					||
					(w.compareTo("Z")==0)
					)
			{
				mode=w;
				System.out.println(i+" mode "+mode);
				i++;
			}


			if (mode.compareTo("M")==0) 
			{
				x=new Double(def.word(i++));
				y=new Double(def.word(i++));
			}
			if (mode.compareTo("L")==0) 
			{
				x=new Double(def.word(i++));
				y=new Double(def.word(i++));
			}
			if (mode.compareTo("H")==0) 
			{
				x=new Double(def.word(i++));
			}
			if (mode.compareTo("V")==0) 
			{
				y=new Double(def.word(i++));
			}
			if (mode.compareTo("m")==0) 
			{
				x+=new Double(def.word(i++));
				y+=new Double(def.word(i++));
			}
			if (mode.compareTo("l")==0) 
			{
				x+=new Double(def.word(i++));
				y+=new Double(def.word(i++));
			}
			if (mode.compareTo("h")==0) 
			{
				x+=new Double(def.word(i++));
			}
			if (mode.compareTo("v")==0) 
			{
				y+=new Double(def.word(i++));
			}
			if ((mode.compareTo("z")==0)|| (w.compareTo("Z")==0))
			{
				sig.addPoint(sig.x(0), sig.y(0));
				break;
			}
			sig.addPoint(x, y);
			//System.out.println(i+" x="+x+" y="+y);
			if (i>=def.dim()) break;
		}

		//apply scale:
		for (int j=0;j<sig.getDim();j++)
		{
			sig.setValue(j, sig.x(j)*scale,sig.y(j)*scale*(-1));
		}

		Plot plot=new Plot();
		plot.setSignal(sig);
		plot.setLabel("z -1 engraving");
		plots.add(plot);

	}

















	public static void main(String[] args)
	{


		String filename="/home/laurent/temp/drawing.svg";
		String s=TextFiles.readFile(filename,false).toString();

		XMLElement xml = new XMLElement();
		StringReader reader = new StringReader(s);
		try {	xml.parseFromReader(reader);}
		catch (Exception ex) {ex.printStackTrace();}

		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		int i=0;
		while (enumSons.hasMoreElements()) 
		{
			XMLElement son = enumSons.nextElement();
			System.out.println(son.getName());
			if (son.getName().compareTo("g")==0)
			{
				Enumeration<XMLElement> enumSons2 = son.enumerateChildren();
				while (enumSons2.hasMoreElements()) 
				{
					XMLElement son2 = enumSons2.nextElement();
					System.out.println("        "+son2.getName());
					if (son2.getName().compareTo("path")==0)
					{
						System.out.println("                 "+son2.getAttribute("d"));
					}
				}

			}
		}
	}


}
