package com.photonlyx.toolbox.cad;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFileChooser;

import org.kabeja.dxf.Bounds;
import org.kabeja.dxf.DXFCircle;
import org.kabeja.dxf.DXFConstants;
import org.kabeja.dxf.DXFDocument;
import org.kabeja.dxf.DXFLWPolyline;
import org.kabeja.dxf.DXFLayer;
import org.kabeja.dxf.DXFLine;
import org.kabeja.dxf.DXFPolyline;
import org.kabeja.dxf.DXFVertex;
import org.kabeja.dxf.helpers.Point;
import org.kabeja.parser.Parser;
import org.kabeja.parser.DXFParser;
import org.kabeja.parser.ParserBuilder;

import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;

import com.photonlyx.toolbox.util.Messager;


public class DXFImport 
{


public static  PlotSet importdxf(String path)
{
PlotSet  plots=new PlotSet();	
//if (filename.val==null) return;
CFileFilter jff=new CFileFilter("dxf","dxf CAD file");

String ch=path;
if (ch==null) ch=".";
JFileChooser df=new JFileChooser(ch);
df.addChoosableFileFilter(jff);
df.setFileFilter(jff);
int returnVal = df.showOpenDialog(null);
if(returnVal == JFileChooser.APPROVE_OPTION)
	{
	path=df.getSelectedFile().getParent()+File.separator;
	//String path=df.getSelectedFile().getParent()+File.separator;
	String fileName=df.getSelectedFile().getName();
	FileInputStream fis;
	try 	
		{
		 fis = new FileInputStream(path+fileName);
		}
	catch (Exception e) 
		{
		Messager.messErr(Messager.getString("Probleme_ouverture_fichier")+" "+(path+fileName)+"\n"+e);
		return plots;
		}
	//plots.removePlots();
	read(fis ,  plots);
	}
return plots;
}
	
	
public static void  read(InputStream in,PlotSet  plots) 
  {

   Parser parser = ParserBuilder.createDefaultParser();
   try {
		 	
       //parse
       parser.parse(in, DXFParser.DEFAULT_ENCODING);
			
       //get the documnet and the layer
       DXFDocument doc = parser.getDocument();
       
       Iterator itr=doc.getDXFLayerIterator();
       
       while(itr.hasNext()) 
       		{
    	   DXFLayer  layer =(DXFLayer) itr.next(); 
    	   String layerName=layer.getName();
    	   System.out.println("layer "+layer.getName()); 	   
    	   
    	   List lines = layer.getDXFEntities(DXFConstants.ENTITY_TYPE_LINE);
     	   if (lines!=null) 
			   {
     		  System.out.println(lines.size()+"  lines");
			   Iterator itr2=lines.iterator();
	    	   while(itr2.hasNext())
	    	   		{
	    		    Signal1D1DXY signal=new Signal1D1DXY();
	    		    DXFLine l=(DXFLine)itr2.next();
	    			   Point p1=l.getStartPoint();
	    			   Point p2=l.getEndPoint();
	    			   signal.addPoint(p2.getX(),p2.getY());
	    			   signal.addPoint(p1.getX(),p1.getY());
	    			   
	    		    Plot plot=plots.add(signal);
	    		    plot.setLabel("line"+" layer: "+layerName);
	    	   		}
			   }
   	   
    	   List plines = layer.getDXFEntities(DXFConstants.ENTITY_TYPE_LWPOLYLINE);
    	   if (plines!=null) 
    		   {
    		   System.out.println(plines.size()+"  LWpolylines");
    		   Iterator itr2=plines.iterator();
	    	   while(itr2.hasNext())
	    	   		{
	    		   Signal1D1DXY signal=new Signal1D1DXY();
	    		   DXFLWPolyline pl=(DXFLWPolyline)itr2.next();
	    		   System.out.println("LWpolyline    rows: "+pl.getRows()+"  cols: "+pl.getColumns()+"   length: "+pl.getLength()+"    vertex count: "+pl.getVertexCount());
	    		   for (int i=0;i<pl.getVertexCount();i++) 
	    			   {
	    			   DXFVertex v=pl.getVertex(i);
	    			   double x=v.getX();
	    			   double y=v.getY();
	    			   double z=v.getZ();
	    			   //af("             "+i+" "+x+" "+y+" "+z);
	    			   signal.addPoint(x,y);
	    			   }
	    		   Plot plot=plots.add(signal);
	    		   plot.setLabel("z -1 polyline"+" layer: "+layerName);
	    	   		}
    		   }
    	   
    	   
    	   List circles = layer.getDXFEntities(DXFConstants.ENTITY_TYPE_CIRCLE);
    	   if (circles!=null)  
    		   {
    		   System.out.println(circles.size()+"  circles");
    		   Iterator itr2=circles.iterator();
	    	   while(itr2.hasNext())
	    	   		{
	    		   	DXFCircle cir=(DXFCircle)itr2.next();
	    		   	double cx=cir.getCenterPoint().getX();
	    		   	double cy=cir.getCenterPoint().getY();
	    		   	double r=cir.getRadius();
	    		   	System.out.println("Circle center "+cx+" "+cy+" radius="+r);
//	    		   	CircleCurve joloCircle=new CircleCurve(cx,cy,r);
//	    		   	//double segmentLength=0.1;//mm
//	    		   	Signal1D1D seg=WolfstrapUtil.curve2Segments(joloCircle, segmentLengthForCurve.val) ;
	    		   	Signal1D1DXY sig=new Signal1D1DXY(2);
	    		   	sig.addPoint(cx, cy);//centre
	    		   	sig.addPoint(cx+r, cy);//point of the circonference
	    		   	Plot plot=new Plot();
	    		   	plot.setSignal(sig);
	    		   	plot.setLabel("z -1 fullCircle");
	    		    plots.add(plot);
	    	   		}
    		   }
    	   
    	   
      		} 
       
//       DXFLayer layer = doc.getDXFLayer(layerid);
//
//       //get all polylines from the layer
//       List plines = layer.getDXFEntities(DXFConstants.ENTITY_TYPE_POLYLINE);
//			
//       //work with the first polyline
//       doSomething((DXFPolyline) plines.get(0));

      } 
   
   catch (Exception e) {
	e.printStackTrace();
      }	
  }

  
  
  
  public void doSomething(DXFPolyline pline) {

    //iterate over all vertex of the polyline
    for (int i = 0; i < pline.getVertexCount(); i++) {
		    
     DXFVertex vertex = pline.getVertex(i);
                    
     //do something like collect the data and
     //build a mesh for a FEM system
    }
  }
}