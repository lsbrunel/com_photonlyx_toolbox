package com.photonlyx.toolbox.math.analyse2D;

import com.photonlyx.toolbox.math.function.Function2D1D;
import com.photonlyx.toolbox.math.matrix.FullMatrix;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.txt.StringSource;


/**
 * test of the interpolation between pixels with bicubic surface
 * http://en.wikipedia.org/wiki/Bicubic_interpolation </p>
 * vector of the bicubic function coefs</p>
 * alpha=[ a00 a10 a20 a30 a01 a11 a21 a31 a02 a12 a22 a32 a03 a13 a23 a33 ]T
 * alpha in one column
 * f(x,y)=a00*x°*y°+a10*x¹*y°+....+a33*x³*y³

//vector of the values and derivatives values of the function:
// f00 means f(0,0)
// fx00 means fx(0,0), derivative % x
// x=[ f00 f10 f01 f11 fx00 fx10 fx01 fx11 fy00 fy10 fy01 fy11 fxy00 fxy10 fxy01 fxy11 ]T
//      0   1   2   3    4    5   6    7     8    9   10   11    12    13   14    15		
// x in one column
//
//  the coefs are calculated by the matrix product:
//  alpha= Ainv x 
 * 
 * 
 * 
 * 
 * @author laurent
 *
 */


	
public class Interpolation2D extends Function2D1D
{
private FullMatrix Ainv=getAinv();//matrix used to calculate the coefs
private FullMatrix a;
private Signal2D1D m=new Signal2D1D(0,1,0,1,4,4);//the control points 
private FullMatrix x=new FullMatrix(16,1);
private double[] pInPix=new double[2];
private static double[] temp=new double[2];

public  Interpolation2D()
{
}


public  Interpolation2D(Signal2D1D pixels)
{
this.m=pixels;
//m.copy(pixels);
}


/**
 * @param args
 */
public static void main(String[] args)
{
//
//
////ctrl points 
//FullMatrix ctrls=new FullMatrix(4,4);
//ctrls.affect(1, 1, 1);
//ctrls.affect(1, 2, 1);
//ctrls.affect(2, 1, 1);
//ctrls.affect(2, 2, 1);
//
//
////
//// test with 1 values at the middle and 0 at the border:
//
//FullMatrix x=new FullMatrix(16,1);
//
////x.affect(3, 0, 1);//f(1,1)
////x.affect(7, 0, 1);//fx(1,1)
////x.affect(11, 0, 1);//fy(1,1)
////x.affect(15, 0, 1);//fxy(1,1)
//
//x.affect(0, 0, 1);//f(0,0)
//x.affect(4, 0, 1);//fx(0,0)
//x.affect(8, 0, 1);//fy(0,0)
//x.affect(12, 0, 1);//fxy(0,0)
//
////x.affect(1, 0, 1);//f(1,1)
////x.affect(5, 0, 1);//fx(1,1)
////x.affect(9, 0, 1);//fy(1,1)
////x.affect(13, 0, 1);//fxy(1,1)
//



}



public double f(double[] p)
{
if ((m.dimx()<=2)||(m.dimy()<=2)) return 0;
if (p[0]>m.xmax()) return 0;
if (p[1]>m.ymax()) return 0;
if (p[0]<m.xmin()) return 0;
if (p[1]<m.ymin()) return 0;

double sx=m.samplingx();
double sy=m.samplingy();

//coords in pixels unit
double cx=(p[0]-m.xmin())/sx;
double cy=(p[1]-m.ymin())/sy;
//locate the point wrt pixels:
int i=(int)Math.floor(cx);
int j=(int)Math.floor(cy);
//int i=(int)Math.floor(p[0]/sx);
//int j=(int)Math.floor(p[1]/sy);

if ((i>m.dimx()-1)||(j>m.dimy()-1)) return 0;

//coordinates relative to this pixel:
pInPix[0]=cx-i;
pInPix[1]=cy-j;

//System.out.println(i+" "+j+" "+p[0]+" "+p[1]);

FullMatrix a=getBicubicCoefs(i,j);

return bicubic(a,pInPix);
}


public double f(double x, double y)
{
temp[0]=x;
temp[1]=y;
return f(temp);
}

public double[] derivate(double x, double y)
{
temp[0]=x;
temp[1]=y;
return derivate(temp);
}


public double[] derivate(double[] p)
{
	
if ((m.dimx()<=2)||(m.dimy()<=2)) return new double[2];
if (p[0]>=m.xmax()) return new double[2];
if (p[1]>=m.ymax()) return new double[2];
if (p[0]<=m.xmin()) return new double[2];
if (p[1]<=m.ymin()) return new double[2];

double sx=m.samplingx();
double sy=m.samplingy();

//coords in pixels unit
double cx=(p[0]-m.xmin())/sx;
double cy=(p[1]-m.ymin())/sy;

//locate the point wrt pixels:
//int i=(int)Math.floor(p[0]/sx);
//int j=(int)Math.floor(p[1]/sy);
int i=(int)Math.floor(cx);
int j=(int)Math.floor(cy);

//coordinates relative to this pixel:
pInPix[0]=cx-i;
pInPix[1]=cy-j;

//System.out.println(i+" "+j+" "+p[0]+" "+p[1]);

FullMatrix a=getBicubicCoefs(i,j);
double[] der=bicubicDerivative(a,pInPix);
der[0]/=sx;
der[1]/=sy;
return der;
}



private FullMatrix getBicubicCoefs(int i,int j)
{
//fill the x vector
//x=[ f00 f10 f01 f11 fx00 fx10 fx01 fx11 fy00 fy10 fy01 fy11 fxy00 fxy10 fxy01 fxy11 ]T
//   0   1   2   3    4    5   6    7     8    9   10   11    12    13   14    15		
x.affect(0,0, m.z(i  ,j  ));//f(0,0)
x.affect(1,0, m.z(i+1,j  ));//f(1,0)
x.affect(2,0, m.z(i  ,j+1));//f(0,1)
x.affect(3,0, m.z(i+1,j+1));//f(1,1)

//System.out.println("dim:"+m.dimx()+"x"+m.dimy()+"  "+i+" "+j);
double fx00=0,fx10=0,fx01=0,fx11=0,fy00=0,fy10=0,fy01=0,fy11=0;
if (i==0)
	{
	fx00=m.z(i+1,j)-m.z(i,j);
	fx10=(m.z(i+2,j)-m.z(i,j))/2;
	fx01=m.z(i+1,j+1)-m.z(i,j+1);
	fx11=(m.z(i+2,j+1)-m.z(i,j+1))/2;
	}
else if (i==m.dimx()-2)
	{
	fx00=(m.z(i+1,j)-m.z(i-1,j))/2;
	fx10=m.z(i+1,j)-m.z(i,j);
	fx01=(m.z(i+1,j+1)-m.z(i-1,j+1))/2;
	fx11=m.z(i+1,j+1)-m.z(i,j+1);
	}
else 
	{
	fx00=(m.z(i+1,j)-m.z(i-1,j))/2;
	fx10=(m.z(i+2,j)-m.z(i,j))/2;
	fx01=(m.z(i+1,j+1)-m.z(i-1,j+1))/2;
	fx11=(m.z(i+2,j+1)-m.z(i,j+1))/2;
	}
if (j==0)
	{
	fy00=m.z(i,j+1)-m.z(i,j);
	fy10=m.z(i+1,j+1)-m.z(i+1,j);
	fy01=(m.z(i,j+2)-m.z(i,j))/2;
	fy11=(m.z(i+1,j+2)-m.z(i+1,j))/2;
	}
else if (j==m.dimy()-2)
	{
	fy00=(m.z(i,j+1)-m.z(i,j-1))/2;
	fy10=(m.z(i+1,j+1)-m.z(i+1,j-1))/2;
	fy01=m.z(i,j+1)-m.z(i,j);
	fy11=m.z(i+1,j+1)-m.z(i+1,j);
	}
else 
	{
	fy00=(m.z(i,j+1)-m.z(i,j-1))/2;
	fy10=(m.z(i+1,j+1)-m.z(i+1,j-1))/2;
	fy01=(m.z(i,j+2)-m.z(i,j))/2;
	fy11=(m.z(i+1,j+2)-m.z(i+1,j))/2;
	}

x.affect(4,0, fx00);//fx(0,0)
x.affect(5,0, fx10);//fx(1,0)
x.affect(6,0, fx01);//fx(0,1)
x.affect(7,0, fx11);//fx(1,1)

x.affect(8,0, fy00);//fy(0,0)
x.affect(9,0, fy10);//fy(1,0)
x.affect(10,0,fy01);//fy(0,1)
x.affect(11,0,fy11);//fy(1,1)

//x.affect(12,0, x.el(4, 0)*x.el(8, 0));//fxy(0,0)=fx(0,0)*fy(0,0)
//x.affect(13,0, x.el(5, 0)*x.el(9, 0));//fxy(1,0)=fx(1,0)*fy(1,0)
//x.affect(14,0, x.el(6, 0)*x.el(10, 0));//fxy(0,1)=fx(0,1)*fy(1,0)
//x.affect(15,0, x.el(7, 0)*x.el(11, 0));//fxy(1,1)=fx(1,1)*fy(1,1)

x.affect(12,0, 0);//fxy(0,0)
x.affect(13,0, 0);//fxy(1,0)
x.affect(14,0, 0);//fxy(0,1)
x.affect(15,0, 0);//fxy(1,1)

//calc the bicubic coefs:
a=Ainv.multiply(x);

return a;
}


/**
 * 
 * @param a the coefs of the bicubic f
 * @param p the (x,y) coord
 * @return f(x,y)
 */
private double bicubic(FullMatrix a,double[] p)
{
//alpha=[ a00 a10 a20 a30   a01 a11 a21 a31  a02 a12 a22 a32   a03 a13 a23 a33 ]T
double r;
double x=p[0];
double y=p[1];
double x2=x*x;
double y2=y*y;
double x3=x2*x;
double y3=y2*y;
//r= 	a00+ a10*x+ a20*x2+ a30*x3+
//	a01*y  +  a11*x*y + a21*x2*y  + a31*x3
//	a02*y2 + a12*x*y2 + a22*x2*y2 + a32*x3*y2+
//	a03*y3 + a13*x*y3 + a23*x2*y3 + a33*x3*y3 ;

r=	a.el(0, 0)+ a.el(1, 0)*x+ a.el(2, 0)*x2+ a.el(3, 0)*x3+
	a.el(4, 0)*y+ a.el(5, 0)*x*y+ a.el(6, 0)*x2*y+ a.el(7, 0)*x3*y+
	a.el(8, 0)*y2+ a.el(9, 0)*x*y2+ a.el(10, 0)*x2*y2+a.el(11, 0)*x3*y2+
	a.el(12, 0)*y3+ a.el(13, 0)*x*y3+ a.el(14, 0)*x2*y3 + a.el(15, 0)*x3*y3;
return r;
}

/**
 * give the derivative fx and fy of the bicubic f
 * @param a : the coefs of the bicubic
 * @param p the (x,y) coord
 * @return the derivativ fx,fy
 */
private double[] bicubicDerivative(FullMatrix a,double[] p)
{
//alpha=[ a00 a10 a20 a30   a01 a11 a21 a31  a02 a12 a22 a32   a03 a13 a23 a33 ]T
double x=p[0];
double y=p[1];
double x2=x*x;
double y2=y*y;
double x3=x2*x;
double y3=y2*y;
double[] der=new double[2];

//derivative in x
der[0]=	  0         + a.el(1, 0)+     a.el(2, 0)*2*x+      a.el(3, 0)*3*x2+
	        0       + a.el(5, 0)*y+   a.el(6, 0)*2*x*y+    a.el(7, 0)*3*x2*y+
	        0       + a.el(9, 0)*y2+  a.el(10, 0)*2*x*y2+  a.el(11, 0)*3*x2*y2+
	        0       + a.el(13, 0)*y3+ a.el(14, 0)*2*x*y3 + a.el(15, 0)*3*x2*y3;
//derivative in y
der[1]=	   0+
           a.el(4, 0)   + a.el(5, 0)*x   + a.el(6, 0)*x2    + a.el(7, 0)*x3+
           a.el(8, 0)*2*y  + a.el(9, 0)*x*2*y  + a.el(10, 0)*x2*2*y  + a.el(11, 0)*x3*2*y+
           a.el(12, 0)*3*y2 + a.el(13, 0)*x*3*y2 + a.el(14, 0)*x2*3*y2  + a.el(15, 0)*x3*3*y2;
return der;
}





private FullMatrix getAinv()
{
FullMatrix Ainv=new FullMatrix(16,16);
String s="";
s+="1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0";s+="\n";
s+="0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0";s+="\n";
s+="-3	3	0	0	-2	-1	0	0	0	0	0	0	0	0	0	0";s+="\n";
s+="2	-2	0	0	1	1	0	0	0	0	0	0	0	0	0	0";s+="\n";
s+="0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0";s+="\n";
s+="0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0";s+="\n";
s+="0	0	0	0	0	0	0	0	-3	3	0	0	-2	-1	0	0";s+="\n";
s+="0	0	0	0	0	0	0	0	2	-2	0	0	1	1	0	0";s+="\n";
s+="-3	0	3	0	0	0	0	0	-2	0	-1	0	0	0	0	0";s+="\n";
s+="0	0	0	0	-3	0	3	0	0	0	0	0	-2	0	-1	0";s+="\n";
s+="9	-9	-9	9	6	3	-6	-3	6	-6	3	-3	4	2	2	1";s+="\n";
s+="-6	6	6	-6	-3	-3	3	3	-4	4	-2	2	-2	-2	-1	-1";s+="\n";
s+="2	0	-2	0	0	0	0	0	1	0	1	0	0	0	0	0";s+="\n";
s+="0	0	0	0	2	0	-2	0	0	0	0	0	1	0	1	0";s+="\n";
s+="-6	6	6	-6	-4	-2	4	2	-3	3	-3	3	-2	-1	-2	-1";s+="\n";
s+="4	-4	-4	4	2	2	-2	-2	2	-2	2	-2	1	1	1	1";s+="\n";
double[][] data=StringSource.fillMatrix(s,false);
for (int col=0;col<16;col++)
	for (int row=0;row<16;row++) Ainv.affect(row, col, data[row][col]);
return Ainv;
}





}
