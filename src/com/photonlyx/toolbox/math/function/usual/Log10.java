
// Author Laurent Brunel

package com.photonlyx.toolbox.math.function.usual;

import com.photonlyx.toolbox.math.function.Function1D1D;


/** fonction a*ln(x)+b */
public  class Log10 extends Function1D1D
{

private double a,b;

protected static String defaultDefinition = null;

public Log10()
{
	
}

public Log10(double a,double b)
{
this.a=a;
this.b=b;
}
public double getA() {
	return a;
}

public void setA(double a) {
	this.a = a;
}

public double getB() {
	return b;
}

public void setB(double b) {
	this.b = b;
}


public static double getDistanceToPoint(double a, double b, double x, double y)
	{
	return Math.abs((y-b-a*x)/Math.sqrt(a*a+1));
	}

public static double getDistanceToPointSigned(double a, double b, double x, double y)
	{
	return (y-b-a*x)/Math.sqrt(a*a+1);
	}

public double getDistanceToPoint(double x, double y)
	{
	return Math.abs((y-b-a*x)/Math.sqrt(a*a+1));
	}

public double a() {return a;}
public double b() {return b;}





public  double f(double x)
{
double r;
r=a*Math.log(x)+b;
return r;
}



}
