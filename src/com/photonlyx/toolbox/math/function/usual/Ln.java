
// Author Laurent Brunel

package com.photonlyx.toolbox.math.function.usual;

import com.photonlyx.toolbox.math.function.Function1D1D;


/** function a*log10(x)+b */
public  class Ln extends Function1D1D
{

private double a,b;

protected static String defaultDefinition = null;

public Ln()
{
	
}

/**
 * function a*log10(x)+b
 * @param a
 * @param b
 */
public Ln(double a,double b)
{
this.a=a;
this.b=b;
}
public double getA() {
	return a;
}

public void setA(double a) {
	this.a = a;
}

public double getB() {
	return b;
}

public void setB(double b) {
	this.b = b;
}



public double a() {return a;}
public double b() {return b;}





public  double f(double x)
{
double r;
r=a*Math.log10(x)+b;
return r;
}

}
