
// Author Laurent Brunel

package com.photonlyx.toolbox.math.function;




/**function from R² to R*/
public abstract class Function2D1D  extends FunctionND1D
{
private double[] h=new double[3];//used for the simplex minimisation
private double[][] tr=new double[3][2];//used for the simplex minimisation, triangle in the 2D object space
private double derivationStep=0.0000001;
private double[] temp=new double[2];

public abstract double f(double[] in);

public double f(double x,double y)
{
temp[0]=x;
temp[1]=y;
return f(temp);
}

public double[] derivate(double[] in)
{
return derivate(in[0],in[1]);
}

public double[] derivate(double x,double y)
{
double[] d=new double[2];
double[] p1=new double[2];
double[] p2=new double[2];
p1[0]=x-derivationStep;
p1[1]=y;
p2[0]=x+derivationStep;
p2[1]=y;
d[0]=(f(p2)-f(p1))/(2*derivationStep);
p1[0]=x;
p1[1]=y-derivationStep;
p2[0]=x;
p2[1]=y+derivationStep;
d[1]=(f(p2)-f(p1))/(2*derivationStep);
return d;
}


public double getDerivationStep()
{
return derivationStep;
}



public void setDerivationStep(double derivationStep)
{
this.derivationStep = derivationStep;
}



/**
return  the local minimum of the function with the "Downhill simplex method in 2 dimensions.
start is the starting point.
step is the size of the first triangle taken
maxcount if the max number of iterations
diffstop is the the dfference between 2 consecutive z when the iteration is stopped
*/
public double[] minimiseWithSimplex(double[] start,double[] step,int maxcount,double diffStop)
{
tr[0]=start;
tr[1][0]=start[0]+step[0];
tr[1][1]=start[1];
tr[2][0]=start[0];
tr[2][1]=start[1]+step[1];
//System.out.println(print(tr));

boolean arret=false;
int compteur=0;
rangeTriangle();
double hprevious=h[0];
//while (!arret)
while ((compteur<maxcount)&&(!arret))
	{
	compteur++;
	//System.out.println(compteur+" ***************************");
	rangeTriangle();
	//System.out.println(compteur+" "+tr[0][0]+" "+tr[0][1]);
	double[] d=getDimTriangle();
//	double diff=Math.abs(h[2]-hprevious);
	double diff=d[0]+d[1];
	//System.out.print(compteur+" ***********************\n");
	//System.out.print(println(tr));
	//System.out.println(compteur+" triangle: "+d[0]+" "+d[1]+" .Diff="+diff);
	//System.out.println(println(tr));
	if (diff<diffStop) arret=true;
//	hprevious=h[2];
	double ztest=symetry(-1);
	//System.out.print(println(tr));
	//System.out.println(" sym -1 ->"+" z="+ztest);
	if (ztest<h[0])
		{
		ztest=symetry(2);
	//System.out.print(println(tr));
		}
	else if (ztest>h[1])
		{
		double save=h[2];
		ztest=symetry(0.5);
	//System.out.print(println(tr));
		if (ztest>save)
			{
			symetry(-1);
	//System.out.print(println(tr));
			}
		}
	//System.out.println(print(tr));
	}
System.out.println("Function2D1D"+" "+compteur+" iterations");
return tr[0];
}

/**
range the triangle such that the first has the lowest value, etc..
*/
private void rangeTriangle()
{
double[] p;
h[0]=f(tr[0]);
h[1]=f(tr[1]);
h[2]=f(tr[2]);
int[] index={0,1,2};
boolean swaped=true;
while (swaped)
	{
	swaped=false;
	for (int i=0;i<2;i++) if (h[index[i+1]]<h[index[i]])
		{//swap
		//System.out.println("swap "+index[i+1]+" "+index[i]);
		int mem=index[i];
		index[i]=index[i+1];
		index[i+1]=mem;
		swaped=true;
		}
	}
double[][] newtriangle=new double[3][];
newtriangle[0]=tr[index[0]];
newtriangle[1]=tr[index[1]];
newtriangle[2]=tr[index[2]];
tr=newtriangle;
}

private double[] getDimTriangle()
{
double[] dim=new double[2];
double min=Math.min(tr[2][0],Math.min(tr[0][0],tr[1][0]));
double max=Math.max(tr[2][0],Math.max(tr[0][0],tr[1][0]));
dim[0]=max-min;
min=Math.min(tr[2][1],Math.min(tr[0][1],tr[1][1]));
max=Math.max(tr[2][1],Math.max(tr[0][1],tr[1][1]));
dim[1]=max-min;
return dim;
}

/**
replace the first point by its symetric wrt the 2 others. symetric if coef=-1, extension if coef=-2, compression if coef=0.5
*/
private double symetry(double coef)
{
//System.out.println("symetry "+coef);
double AC0=tr[2][0]-tr[0][0];
double AC1=tr[2][1]-tr[0][1];
double AB0=tr[1][0]-tr[0][0];
double AB1=tr[1][1]-tr[0][1];
//System.out.println("AB "+AB0+" "+AB1);
//System.out.println("AC "+AC0+" "+AC1);
double nAB2=AB0*AB0+AB1*AB1;
double ACpsAB=AC0*AB0+AC1*AB1;
//System.out.println("ACpsAB "+ACpsAB);
//System.out.println("nAB2 "+nAB2);
double IC0=AC0-ACpsAB*AB0/nAB2;
double IC1=AC1-ACpsAB*AB1/nAB2;
//System.out.println("IC "+IC0+" "+IC1);
tr[2][0]=tr[2][0]+(coef-1)*IC0;
tr[2][1]=tr[2][1]+(coef-1)*IC1;
return f(tr[2]);
}

/**
replace the first point by its symetric wrt the 2 others. symetric if coef=-1, extension if coef=-2, compression if coef=0.5
*/
private double[] symetryold(double[][] triangle,double coef)
{
double AC0=triangle[2][0]-triangle[0][0];
double AC1=triangle[2][1]-triangle[0][1];
double AB0=triangle[1][0]-triangle[0][0];
double AB1=triangle[1][1]-triangle[0][1];
//System.out.println("AB "+AB0+" "+AB1);
//System.out.println("AC "+AC0+" "+AC1);
//double nAC=Math.sqrt(AC0*AC0+AC1*AC1);
double nAB2=AB0*AB0+AB1*AB1;
double ACpsAB=(AC0*AB0+AC1*AB1)/nAB2;
//System.out.println("ACpsAB "+ACpsAB);
double Vp0=ACpsAB*AB0;
double Vp1=ACpsAB*AB1;
//System.out.println("Vp "+Vp0+" "+Vp1);
double V0=AC0-Vp0;
double V1=AC1-Vp1;
//System.out.println("V "+V0+" "+V1);
double[] tryPoint=new double[2];
tryPoint[0]=Vp0+coef*V0+triangle[0][0];
tryPoint[1]=Vp1+coef*V1+triangle[0][1];
return tryPoint;
}

/**
replace the last point by its symetric wrt the 2 others. symetric if coef=-1, extension if coef=-2, compression if coef=0.5
*/
private double symetryold1(double coef)
{
System.out.println("symetry "+coef);
double[] p=new double[2];
p[0]=tr[2][0]+coef*(2*tr[2][0]-tr[1][0]-tr[0][0]);
p[1]=tr[2][1]+coef*(2*tr[2][1]-tr[1][1]-tr[0][1]);
double htest=f(p);
//if (htest<h[2])
	{
	//System.out.println("succ�");
	tr[2]=p;
	}
return htest;
}





/**
make a contraction (extension if coef>1) around the lowest point (here the first one)
*/
private void contract(double coef)
{
System.out.println("contract "+coef);
double AC0=tr[2][0]-tr[0][0];
double AC1=tr[2][1]-tr[0][1];
double AB0=tr[1][0]-tr[0][0];
double AB1=tr[1][1]-tr[0][1];
tr[1][0]=tr[0][0]+AB0*coef;
tr[1][1]=tr[0][1]+AB1*coef;
tr[2][0]=tr[0][0]+AC0*coef;
tr[2][1]=tr[0][1]+AC1*coef;
}

private String println(double[][] triangle)
{
StringBuffer sb=new StringBuffer();
for (int i=0;i<3;i++)
	{
	sb.append(i+" "+triangle[i][0]+" "+triangle[i][1]+" z="+f(triangle[i])+"\n");
	}
return sb.toString();
}



/**
 * trapeze integration
 * xmin : min boundary, xmax sup boundary,n sampling
 * */
public double integral(double xmin,double xmax,double ymin,double ymax,int n)
{
double r=0;
double intervx=(xmax-xmin)/n;
double intervy=(ymax-ymin)/n;
for (int i=0;i<n;i++)
	for (int j=0;j<n;j++)
	{
	double x1=xmin+intervx*i;
	double x2=x1+intervx;
	double y1=ymin+intervy*j;
	double y2=y1+intervy;
	r+=(f(x1,y1)+f(x1,y2)+f(x2,y2)+f(x2,y1));
	}
r=r/4*intervx*intervy;
return r;
}



}
