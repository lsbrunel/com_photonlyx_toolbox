package com.photonlyx.toolbox.math.function.usual2D1D;

import com.photonlyx.toolbox.math.function.Function2D1D;

public class Gaussian extends Function2D1D
{
private double coef=1,sigma=1,xcentre=0,ycentre=0;

public Gaussian()
{
	
}

public Gaussian(double coef,double sigma,double xcentre,double ycentre)
{
super();
this.coef = coef;
this.sigma = sigma;
this.xcentre = xcentre;
this.ycentre = ycentre;
}

@Override
public double f(double[] in)
{
double r2=Math.pow(in[0]-xcentre,2)+Math.pow(in[1]-ycentre,2);
return coef*Math.exp(-r2/Math.pow(sigma, 2));
}

public double getSigma()
{
return sigma;
}

public void setSigma(double sigma)
{
this.sigma = sigma;
}

}
