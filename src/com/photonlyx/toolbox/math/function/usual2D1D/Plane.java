package com.photonlyx.toolbox.math.function.usual2D1D;

import com.photonlyx.toolbox.math.function.Function2D1D;
import com.photonlyx.toolbox.math.geometry.Vecteur;

public class Plane extends Function2D1D
{
private Vecteur o,n;//a point a the plane and the normal to the plane

/**
 * 
 * @param o a point of the plane
 * @param n normalised normal of the plane
 */
public Plane(Vecteur o,Vecteur n)
{
super();
this.o = o;
this.n = n;
}

@Override
public double f(double[] in)
{
double z;
z=o.z()-((in[0]-o.x())*n.x()+(in[1]-o.y())*n.y())/n.z();
return z;
}



}
