
// Author Laurent Brunel


package com.photonlyx.toolbox.math.function;




/**
function that gives the derivative  of the son Function1D1D
*/
public class Function1D1Dderivative  extends Function1D1D
{
//links
private Function1D1D function1D1D;
//params
private double stepForDerivative;

//protected static String defaultDef = Global.defaultdefs.getDefaultDef("fa.reuse.math.function.Function1D1Dderivative");

/**construct */
public Function1D1Dderivative()
{
}

/**
construct with the son
@param f the son Function1D1D
@param stepForDerivative the step used to calculate the derivative
*/
public Function1D1Dderivative(Function1D1D f,double _stepForDerivative)
{
function1D1D=f;
stepForDerivative=_stepForDerivative;
}



public double f(double x)
{
return function1D1D.derivative(x,stepForDerivative);
}





}
