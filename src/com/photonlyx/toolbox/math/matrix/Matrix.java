package com.photonlyx.toolbox.math.matrix;


import java.awt.Graphics;



public interface Matrix
{
public int getNbRows();
public int getNbColumns();
public void put(int row,int column,double value);
public double get(int row,int column);
}
