// Author Laurent Brunel

package com.photonlyx.toolbox.math.spline;

import java.text.*;
import java.util.*;

import com.photonlyx.toolbox.math.matrix.*;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DSource;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;
import com.photonlyx.toolbox.util.Messager;


/**
Adjust a curve defined  by control points to a cloud of points by adjusting vertically the control points.
Use the least Square method (solve the system A tA=At F, A matrix of derivatives, F "fermeture" matrix).
Beware: the son ControlPointsCurve1D must have its control points in a son of class Signal1D1DXY.
The sons are: fa.reuse.math.spline.SplineCubic and fa.reuse.math.signal.Signal1D1DSource of name "data" (sorted in x)
*/
public  class ControlPoints1D1DFitLeastSquare1D 
{
//links
private Signal1D1DSource ss;//the cloud of points
private ControlPointsCurve1D curve;//the spline function
//params 
private int nbIter;//nb of iteration for the least square
private double xmin,xmax;//min and max x value of the data points
private boolean dontAdjustBoundaries;//if true the boundaries equal the data boundaries and are not adjusted
private boolean prepareCtrlPts;//if true the ctrp pts are spread regularly along the x's of the data

//internal
FullMatrix F=null;
FullMatrix A=null;



/**dont' use it!*/
public ControlPoints1D1DFitLeastSquare1D()
{
}

/**
fit a ControlPointsCurve1D
The data must be sorted in x. 
Use work to really do the fit !
@param data the signal to fit
@param nbControlPoints the number of control points to use
@param _dontAdjustBoundaries if true the boundaries are not adjusted and equalized to the data boundaries
*/
public ControlPoints1D1DFitLeastSquare1D(Signal1D1D _data,ControlPointsCurve1D _curve,
		boolean _dontAdjustBoundaries, int _nbIter,boolean _prepareCtrlPts)
{
this.ss= _data;
this.curve= _curve;
this.dontAdjustBoundaries= _dontAdjustBoundaries;
this.nbIter= _nbIter;
this.prepareCtrlPts= _prepareCtrlPts;
}





//here the measure is the total difference between data points y to their vertically near point in the spline
/**
do the fit
*/
public void update() 
{
NumberFormat nf3 = NumberFormat.getInstance(Locale.ENGLISH);
nf3.setMaximumFractionDigits(3);
nf3.setMinimumFractionDigits(3);
nf3.setGroupingUsed(false);

Signal1D1D data=ss.getSignal1D1D();
if (data==null) return;
if (data.getDim()==0) return;


// System.out.println(getClass()+" data:");
// for (int i=0;i<data.getDim();i++) System.out.println(data.x(i)+" "+data.y(i));

//detect the min and max x values of the data points
xmin=data.xmin();
xmax=data.xmax();


Signal1D1DXY ctrl=curve.getControlPoints();
//initialise nbCtrlPts control points equaly sampled in x with zero values in y
//ctrl.fillWith(new SignalDiscret1D1D(xmin.val,xmax.val,ctrl.getDim()));

if (prepareCtrlPts)
	{
	Signal1D1D sig=ss.getSignal1D1D();
	double xmin=sig.xmin();
	double xmax=sig.xmax();
	ctrl.copy(new SignalDiscret1D1D(xmin,xmax,ctrl.getDim()));
	}

if ((ctrl.xmax()-ctrl.xmin())==0)
	{
	Messager.messErr(getClass()+" The starting control points has all the same abcisses. Fit aborted"); 
	//getNode().setWorkCanceled(true);
	return;
	}
if (ctrl.getDim()>(data.getDim()))
	{
	Messager.messErr(getClass()+" Not enough data point to fit the curve "+curve.getClass()+". Fit aborted"); 
	//getNode().setWorkCanceled(true);
	return;
	}

int m=ctrl.getDim();//nb of control points
int n=data.getDim();//nb of data points of the cloud used to adjust the spline


// System.out.println(getClass()+" ctrl points:");
// System.out.println(ctrl);


//******************************************************************************
//do the least square adjustment	
//*******************************************************************************
		



for (int k=0;k<nbIter;k++)
	{
	//fill the A matrix (matrix of the derivatives)
	
	
	/*
	If we note :
	Cj the jth control points (m control points j from 0 to m-1)
	Xi the ith data point (n data points i from 0 to n-1)
	Pi the point on the spline curve having the same x co-ordinate than Xi
	the A matrix is a n*m matrix
	(                                                      )
	(                                                      )
	(                                                      )
	(                                                      )
	(          dPiy                                        )
	(   ...   _____    ...                                 )
	(          dCjy                                        )
	(                                                      )
	(                                                      )
	(                                                      )
	(                                                      )
	(                                                      )
	(                                                      )
	
	
	*/
	
	A=new FullMatrix(n,m);
	for (int i=0;i<n;i++)//list the data points
		{
		double x=data.x(i);
		for (int j=0;j<m;j++)//list the control points
			{
			double pd=partialDerivative(j,x);
			A.put(i,j,pd);
			}
		}
		
	
//	System.out.println("A");
//	System.out.println(A.toString());
//	System.out.println();
	
	

	//fill the F matrix (fermeture). F is the vector (matrix n x 1) of the Xi-Pi
	/*
	
	
	   (                 )
	   (                 )
	   (                 )
	   (                 )	
	   (    ...          )
	F= (   (Xi)x-(Pi)x   )
	   (    ...          )
	   (                 )
	   (                 )
	   (                 )
	   (                 )
	   
	*/
	
	 F=new FullMatrix(n,1);
	for (int i=0;i<n;i++)//list the data points
		{
		F.put(i,0,curve.f(data.x(i))-data.y(i));
		}
		
	
/*	System.out.println("F");
	System.out.println(F.toString());
	System.out.println();*/
	
	//calculate the normal matrix
	FullMatrix At=A.transpose();
	
/*	System.out.println("At");
	System.out.println(At.toString());
	System.out.println();*/
	
	
	FullMatrix N=At.multiply(A);
	
	
/*	System.out.println("N");
	System.out.println(N.toString());
	System.out.println();
	
	System.out.println("N-1");
	System.out.println(MatricePleine.inverse_lu(N).toString());
	System.out.println();*/
	
	
	FullMatrix AtF=At.multiply(F);
	
/*	System.out.println("AtF");
	System.out.println(AtF.toString());
	System.out.println();*/
	
	
	//now solve the equation:  AtA dX = AtF
	int[] indx=new int[2*n];
	FullMatrix.decompose_lu(N,indx);//N is destroyed!
	FullMatrix.systeme_lu(N,indx,AtF);//now the solution is in AtF!
	
	//apply the correction:
	if (dontAdjustBoundaries) 
		{
		for (int j=1;j<m-1;j++)//list the control points
			{
			double Y=ctrl.y(j);
			double dY=AtF.el(j,0);
// 			System.out.println("Ctrl point num "+j+"\tcorrection dY="+nf3.format(dY));
			ctrl.setValue(j,Y-dY);
			}
		ctrl.setValue(0,data.y(0));
		ctrl.setValue(m-1,data.y(n-1));
		}
	else
		{
		for (int j=0;j<m;j++)//list the control points
			{
			double Y=ctrl.y(j);
			double dY=AtF.el(j,0);
// 			System.out.println("Ctrl point num "+j+"\tcorrection dY="+nf3.format(dY));
			ctrl.setValue(j,Y-dY);
			}
		}
	
	
	
	}
	
/*System.out.println(getClass()+" corrected ctrl points:");
System.out.println(ctrl);*/


}
	
	
/**
gives the partial derivative of the co-ordinate y of the point of abcisse x of the curve  with respect to the Y coord of the control point of index ctrlIndex
@param ctrlIndex index of the control point
@param x the x co-ordinate
@return the value of the derivative
*/
private double partialDerivative(int ctrlIndex,double x)
{
double dY,absdiff;
double der1;
double der2=0;
double y0;//initial y coord of the spline point
double y;// y coord of the spline point

Signal1D1DXY ctrl=curve.getControlPoints();


absdiff=1;
dY=0.001;

//get the initial position of the control point
double Y0=ctrl.y(ctrlIndex);

//get the initial y coord of the spline point
curve.update();
y0=curve.f(x);
// System.out.println("d0="+d0);

while ((absdiff>1e-5))
	{
	ctrl.setValue(ctrlIndex,Y0+dY);
	curve.update();
	y=curve.f(x);
	der1=(y-y0)/dY;
	dY=dY/2;
	ctrl.setValue(ctrlIndex,Y0+dY);
	curve.update();
	y=curve.f(x);
	der2=(y-y0)/dY;
	absdiff=Math.abs(der2-der1);
	} 

//replace the control point at its initial position
ctrl.setValue(ctrlIndex,Y0);
curve.update();
return der2;
}



/**
 * return the fermeture: matrice difference between data to fit and curve
 * @return
 */
public FullMatrix getF(){return F;}



/**
 * calculate and return the rms of the differences between data points and spline curve
 */
public double getFitQualityRMS()
{
Signal1D1D data=ss.getSignal1D1D();
int n=data.getDim();
double r=0;
for (int i=0;i<n;i++)//list the data points
	{
	r+=Math.pow(curve.f(data.x(i))-data.y(i),2);
	}
r=Math.sqrt(r/n);
return r;
}


/**
 * calculate and return the max difference (abs) between data points and spline curve
 */
public double getFitQualityMaxDiff()
{
Signal1D1D data=ss.getSignal1D1D();
int n=data.getDim();
double r=0;
for (int i=0;i<n;i++)//list the data points
	{
	double v=Math.abs(curve.f(data.x(i))-data.y(i));
	if (v>r) r=v;
	}
return r;
}







}