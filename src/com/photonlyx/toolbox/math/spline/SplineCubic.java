// Author Laurent Brunel
package com.photonlyx.toolbox.math.spline;

import com.photonlyx.toolbox.math.Sorter;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DSource;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.util.Messager;





/**
function based on a Signal1D1XY that returns a cubic spline interpolation of this signal</p>
from "numerical recipies in c" re-touched to  have arrays from 0 to n-1 instead of 1 to n</p>
the control points are taken from the son <code>Signal1D1D</code>
*/
public class SplineCubic extends  ControlPointsCurve1D
{
private double leftDerivative,rightDerivative;
private boolean adaptTangents=true;//if true the tangents are adapted  to the first and last control pts segments
private boolean natural=false;//if true the second derivative at limits is set to zero

//spline specific
private double[] xvalues,yvalues,y2;
private int n;
private Signal1D1DXY ctrl;

public SplineCubic()
{
}

/**
Create a new spline cubic with the specified control points 
@param ctrl a signal with the control points
*/
public SplineCubic(int nbCtrlPts)
{
adaptTangents=true;
natural=false;
ctrl=new Signal1D1DXY(nbCtrlPts);
}

/**
Create a new spline cubic with the specified control points 
@param ctrl a signal with the control points
*/
public SplineCubic(Signal1D1DXY ctrl)
{
this.ctrl=ctrl;

}

/**
Create a new spline cubic with the specified control points 
@param ctrl a signal with the control points
@param yp1 left tangents if _adaptTangents false
@param ypn right tangents if _adaptTangents false
@param _adaptTangents if true the tangents are variable: the extreme segments made by the control points
*/
public SplineCubic(Signal1D1DXY ctrl,double yp1, double ypn,boolean _adaptTangents)
{
this.ctrl=ctrl;
leftDerivative=yp1;
rightDerivative=ypn;
adaptTangents=_adaptTangents;
natural=false;
}
/**
Create a new spline cubic with the specified control points 
@param ctrl a signal with the control points
@param yp1 left tangents if _adaptTangents and _natural false (1e30 if second derivative null)
@param ypn right tangents if _adaptTangents and _natural false (1e30 if second derivative null)
@param _adaptTangents if true the tangents are variable: the extreme segments made by the control points
@param _natural if true and _adaptTangents false the second derivative a limits is set to zero
*/
public SplineCubic(Signal1D1DXY ctrl,double yp1, double ypn,boolean _adaptTangents,boolean _natural)
{
this.ctrl=ctrl;
leftDerivative=yp1;
rightDerivative=ypn;
adaptTangents=_adaptTangents;
natural=_natural;
}

/** give the arrays of : x values, y values, size of these arrays , left derivative, right derivative.*/
public SplineCubic(double[] x, double[] y, int n, double yp1, double ypn)
{
initControlPoints(x,y,n,yp1,ypn);
}

public void initControlPoints(double[] x, double[] y, int n, double yp1, double ypn)
{
xvalues=x;
yvalues=y;
this.n=n;
y2=new double[n];
spline(xvalues,yvalues,n,yp1,ypn,y2);

/*
System.out.println(getClass()+" "+ getNode().getName());
System.out.println("i  x[i] y[i]");
for (int i=0;i<n;i++)
	{
	System.out.println(i+" "+x[i]+" "+y[i]);
	}
*/
}




/**calculate the spline*/
public void update()
{
//Retrieve the control points
int n=ctrl.getDim();
xvalues=new double[n];
yvalues=new double[n];
for (int i=0;i<n;i++)
	{
	xvalues[i]=ctrl.x(i);
	yvalues[i]=ctrl.y(i);
	}
//sort the xvalues:
int[] indices=Sorter.sort(xvalues);
for (int i=0;i<n;i++)
	{
	xvalues[i]=ctrl.x(indices[i]);
	yvalues[i]=ctrl.y(indices[i]);
	}

//xvalues=signal.getXarray();
//yvalues=signal.getYarray();
//n=signals.getSignal1D1D().getDim();
y2=new double[n];
//System.out.println(getClass()+" "+n+" control points");
if (n<=2)
	{
	Messager.messErr(getClass()+" Not enough points to fit with splines");
	return;
	}
if (adaptTangents)
	{
	double _leftDerivative=(ctrl.value(1)-ctrl.value(0))/(ctrl.x(1)-ctrl.x(0));
	double _rightDerivative=(ctrl.value(n-1)-ctrl.value(n-2))/(ctrl.x(n-1)-ctrl.x(n-2));
	spline(xvalues,yvalues,n,_leftDerivative,_rightDerivative,y2);
	}
else if (natural)
	{
	double _leftDerivative=1e30;
	double _rightDerivative=1e30;
	spline(xvalues,yvalues,n,_leftDerivative,_rightDerivative,y2);
	}
else spline(xvalues,yvalues,n,leftDerivative,rightDerivative,y2);
}




public double f(double x)
{
if (xvalues==null)return 0;
double y=splint(xvalues,yvalues,y2,xvalues.length,x);
return y;
}

/**
 * (NUMERICAL RECIPIES in C p115) code modified for indices 0...n-1
Given arrays x[1..n] and y[1..n] containing a tabulated function, i.e., yi = f(xi), with
x1 < x2 < . . . < xN, and given values yp1 and ypn for the first derivative of the interpolating
function at points 1 and n, respectively, this routine returns an array y2[1..n] that contains
the second derivatives of the interpolating function at the tabulated points xi. If yp1 and/or
ypn are equal to 1x10e30 or larger, the routine is signaled to set the corresponding boundary
condition for a natural spline, with zero second derivative on that boundary.
*/
private  void spline(double[] x, double[] y, int n, double yp1, double ypn, double[] y2)
{
int i,k;
double p,qn,sig,un;
double[] u=new double [n];
if (yp1 > 0.99e30)    //The lower boundary condition is set either to be "natural"
	{
	y2[0]=0.0;    //or else to have a specified first derivative.
	u[0]=0.0;
	}
else
	{
	y2[0] = -0.5;
	u[0]=(3.0/(x[1]-x[0]))*((y[1]-y[0])/(x[1]-x[0])-yp1);
	}

for (i=1;i<=n-2;i++)
	{                   //This is the decomposition loop of the tridiagonal al-
	sig=(x[i]-x[i-1])/(x[i+1]-x[i-1]);    //gorithm. y2 and u are used for tem-
	p=sig*y2[i-1]+2.0;                    //porary storage of the decomposed
	y2[i]=(sig-1.0)/p;                    //factors.
	u[i]=(y[i+1]-y[i])/(x[i+1]-x[i]) - (y[i]-y[i-1])/(x[i]-x[i-1]);
	u[i]=(6.0*u[i]/(x[i+1]-x[i-1])-sig*u[i-1])/p;
	}
if (ypn > 0.99e30)             //The upper boundary condition is set either to be
		qn=un=0.0;     //"natural"
else
	{                        //or else to have a specified first derivative.
	qn=0.5;
	un=(3.0/(x[n-1]-x[n-2]))*(ypn-(y[n-1]-y[n-2])/(x[n-1]-x[n-2]));
	}
y2[n-1]=(un-qn*u[n-2])/(qn*y2[n-2]+1.0);
for (k=n-2;k>=0;k--)    //This is the backsubstitution loop of the tridiagonal
	y2[k]=y2[k]*y2[k+1]+u[k];    //algorithm.
//free_vector(u,1,n-1);

/*System.out.println(getClass());
for (k=0;k<y2.length;k++) System.out.print(y2[k]+" ");
System.out.println();*/
}




/**
 * (NUMERICAL RECIPIES in C p115) code modified for indices 0...n-1
Given the arrays xa[1..n] and ya[1..n], which tabulate a function (with the xai's in order),
and given the array y2a[1..n], which is the output from spline above, and given a value of
x, this routine returns a cubic-spline interpolated value y.
*/
private double splint(double xa[], double ya[], double y2a[], int n, double x)
{
//void nrerror(char error_text[]);
int klo,khi,k;
double h,b,a;
klo=0;                       // We will find the right place in the table by means of
khi=n-1;                       //bisection. This is optimal if sequential calls to this
while (khi-klo > 1)
	{                     //routine are at random values of x. If sequential calls
	k=(khi+klo) >> 1;     //are in order, and closely spaced, one would do better
	if (xa[k] > x) khi=k;  //to store previous values of klo and khi and test if
	else klo=k;            //they remain appropriate on the next call.
	}                      //klo and khi now bracket the input value of x.
h=xa[khi]-xa[klo];
if (h == 0.0) //nrerror("Bad xa input to routine splint");   // The xa's must be dis-
	{
	System.out.println(getClass()+" Bad xa input to routine splint");
	return 0.0;
	}
a=(xa[khi]-x)/h;                                           //tinct.
b=(x-xa[klo])/h;                            //Cubic spline polynomial is now evaluated.
return a*ya[klo]+b*ya[khi]+((a*a*a-a)*y2a[klo]+(b*b*b-b)*y2a[khi])*(h*h)/6.0;
}

/**
return the control points
@return the control points
*/
// public  Signal1D1D getSignal1D1D()
// {
// return new Signal1D1DXY(xvalues,yvalues);
// }


/**return the control points of the curve*/
public Signal1D1DXY getControlPoints()
{
return (Signal1D1DXY) ctrl;
}

}
