// Author Laurent Brunel

package com.photonlyx.toolbox.math.spline;

import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;





/** 
Uniform B-spline  curve built with control points (son)
Eric W. Weisstein. "B-Spline." From MathWorld--A Wolfram Web Resource. http://mathworld.wolfram.com/B-Spline.html 
*/
public class Bspline extends ControlPointsCurve2D 
{
//links
private Signal1D1DXY ctrlPts;//the control points
//params
private int p;//degree of the Bspline:p=m-n-1 with m: nb of knots and n nb of control points 
//internal
private int m; //nb of knots-1
private int n; //nb of control points -1
private double[] k;//array of knots


public  Bspline()
{

}

/**
 * 
 * @param nbCtrlPts the nb of control points
 * @param order the degree of the spline
 */
public Bspline(int nbCtrlPts,int degree)
{
ctrlPts=new Signal1D1DXY(nbCtrlPts);
p=degree;
//af("degree="+p.val);
//af("nbCtrlPts="+nbCtrlPts);
}


public Bspline(Signal1D1D _ctrlPts,int degree)
{
ctrlPts=_ctrlPts.transfertTo1D1DXY();
p=degree;
//af("degree="+p.val);
//af("nbCtrlPts="+nbCtrlPts);
}



/**
update the internal matrices from the control points (son)
*/
public void work()
{
//Signal1D1D ctrlPts=ctrlPtsS.getSignal1D1D();
n=ctrlPts.getDim()-1;
m=n+p+1;
k=new double[m+1];
//  System.out.println(getClass()+" degree="+(int)p.val);
//  System.out.println(getClass()+" "+(m+1)+" knots");
//  System.out.println(getClass()+" "+(n+1)+" control points");

//the first knot and the last knot must be of multiplicity p+1. This will generate the so-called clamped B-spline curves
int pp=p;
if (pp>=n) 
	{
// 	Messager.messErr(getClass()+" Degree of Bspline too high or to few control points");
	System.out.println(getClass()+" Degree of Bspline too high or to few control points");
	}
int ni=(m+1)-2*(pp);//nb of internal knots
for (int i=0;i<pp;i++) k[i]=0;
for (int i=0;i<ni;i++) k[i+pp]=(double)i/((double)(ni-1));;
for (int i=m;i>m-pp;i--) k[i]=1;

	
//for (int i=0;i<=m;i++) k[i]=(double)i/(double)(m);;

//for (int i=0;i<=m;i++) System.out.println("k "+i+"="+k[i]);
// for (int i=0;i<=n;i++)	System.out.println(getClass()+" pts ctrl x="+ctrlPts.x(i)+" pts ctrl y="+ctrlPts.y(i));

}




public double[] f(double t)
{
double[] r=new double[2];
if (t<0) 
	{
// 	Messager.messErr(getClass()+" "+getNode().getName()+" t="+t+"<0!");
	System.out.println(getClass()+" "+" t="+t+"<0!");
	return r;
	}
if (t>1) 
	{
// 	Messager.messErr(getClass()+" "+getNode().getName()+" t="+t+">1!");
	System.out.println(getClass()+" "+" t="+t+">1!");
	return r;
	}
//System.out.println(getClass()+" t="+t);
//Signal1D1D ctrlPts=ctrlPtsS.getSignal1D1D();
if (p>=n) return r;
double[] coef=coef(p,t);
for (int i=0;i<=n;i++) 
	{
	//double N=N(i,(int)p.val,t);
	double N=coef[i];
	r[0]+=ctrlPts.x(i)*N;
	r[1]+=ctrlPts.y(i)*N;
/*	System.out.println(getClass()+"f() t= "+t+" pts ctrl "+i+"  N="+N);
	System.out.println(getClass()+"f() pts ctrl x="+ctrlPts.x(i)+" pts ctrl y="+ctrlPts.y(i));*/
	}
return r;
}


/**Cox deBoor recursion. Not used since it is time consuming*/
private double N(int i,int p,double t)
{
double r=0;
if (p==0) 
	{
	if ((t>=k[i])&&(t<k[i+1])&&(k[i]<k[i+1])) r=1.;
	else r= 0.;
	}
else //r= ((t-k[i])/(k[i+p]-k[i])*N(i,p-1,t) + (k[i+p+1]-t)/(k[i+p+1]-k[i+1])*N(i+1,p-1,t));
	{
	double n1=N(i,p-1,t);
	double n2=N(i+1,p-1,t);
	if (n1!=0) r=(t-k[i])/(k[i+p]-k[i])*n1;
	if (n2!=0) r+=(k[i+p+1]-t)/(k[i+p+1]-k[i+1])*n2;
	}
//System.out.println("N"+i+" "+p+"("+t+")="+r);
return r;
}


/**
 * Dr. Ching-Kuang Shene 
Associate Professor 
 Department of Computer Science 
 Michigan Technological University 
http://www.cs.mtu.edu/~shene/COURSES/cs3621/NOTES/notes.html
@param p 
@param t
@return the array of the coefficient N from i=0 to n for the degree p and parameter t (between 0 and 1)
*/

private double[] coef(int p,double t)
{
double[] N=new double[n+1];

//case when t is 0 or 1
if (t==0) 
	{
	N[0]=1;
	return N;
	}
if (t==1) 
	{
	N[n]=1;
	return N;
	}
//look for the knot span where t is
int kk=0;
//System.out.println("ADRIAN:t:" + t);
for (int i=0;i<=m-1;i++) 
	{
	//System.out.println("ADRIAN:k[" + i + "]:" + k[i] + ",k[" + (i+1) + "]:" + k[i+1]);
	if ((t>=k[i])&&(t<k[i+1])) 
		{
		kk=i;
		break;
		}
	}
if (kk==0)
	{
	System.out.println("kk=0!");
	}
N[kk]=1;// degree 0 coefficient 
for (int d=1;d<=p;d++)
	{
	// right (south-west corner) term only 
	N[kk-d] = (k[kk+1]-t)
	 / 
	 (k[kk+1]-k[kk-d+1]) 
	 * N[(kk-d)+1]; 
	 // compute internal terms
	for (int i=kk-d+1;i<=kk-1;i++)
		{
		N[i]= (t-k[i])/(k[i+d]-k[i])*N[i] + (k[i+d+1]-t)/(k[i+d+1]-k[i+1])*N[i+1];
		}
	// left (north-west corner) term only 
	N[kk]=(t-k[kk])/(k[kk+d]-k[kk])*N[kk];
	}
return N;
}


/**return the control points of the curve*/
public Signal1D1DXY getControlPoints()
{
return ctrlPts;
}




}




