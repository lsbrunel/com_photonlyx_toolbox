// Author Laurent Brunel

package com.photonlyx.toolbox.math.signal;

import com.photonlyx.toolbox.math.function.Function1D2D;
import com.photonlyx.toolbox.math.function.Function1D2DSource;



/**
represent a sampled parametric curve. filled by sampling a Function1D2D given by a son Function1D2DSource
*/
public class Signal1D2D extends Signal1D1DXY
{
//links
private Function1D2DSource fs;//the parametric function to sample
//params
private int dim;
private double xmin,xmax;//boundaries of the sampling

public Signal1D2D()
{}

public Signal1D2D(double _xmin,double _xmax,int _dim)
{
//	params
this.dim=_dim;
xmin=_xmin;
xmax=_xmax;
init(dim);
}




public void work()
{
Function1D2D f=fs.getFunction1D2D();
fillWithFunction1D2D(f);

}

public void fillWithFunction1D2D(Function1D2D f)
{
init((int)dim);
double st=(xmax-xmin)/(getDim()-1);
for (int i=0;i<getDim();i++)
	{
	double t=xmin+i*st;
	double[] d=f.f(t);
	setValue(i,d[0],d[1]);
	}
}




}//end of class




