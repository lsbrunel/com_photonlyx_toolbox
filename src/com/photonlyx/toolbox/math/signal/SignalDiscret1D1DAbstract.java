package com.photonlyx.toolbox.math.signal;



/**
 * signal with x values non randomly spread
 * @author laurent
 *
 */
public abstract class  SignalDiscret1D1DAbstract extends Signal1D1D
{
public abstract void init(int dim,double xmin,double xmax);
}
