// Author Laurent Brunel

package com.photonlyx.toolbox.math.signal;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.photonlyx.toolbox.math.function.Function3D1D;



/**
represent a 3 dimension signal with floating boundaries </p>
*/
public class Signal3D1D extends Signal  
{

private double[][][] array=new double[2][2][2];
private double xmin=0,xmax=1,ymin=0,ymax=1,zmin=0,zmax=1;
private double TINY=1E-10;
//private double samplingx,samplingy,samplingz;
private static int fileHeaderSize=1024;//put spare bytes in case of new data that has to be stored
private double sx,sy,sz;//sampling

public Signal3D1D()
{
}

public Signal3D1D(double xmin,double xmax,double ymin,double ymax,double zmin,double zmax,int dimx,int dimy,int dimz)
{
init(xmin, xmax, ymin, ymax, zmin,zmax,dimx, dimy,dimz);
}
public Signal3D1D(int dimx,int dimy,int dimz)
{
init(0, 1, 0, 1, 0,1,dimx, dimy,dimz);
}

/**
set the size and boundaries of the image
*/
public void init(double xmin,double xmax,double ymin,double ymax,double zmin,double zmax,int dimx,int dimy,int dimz)
{
array=new double[dimx][dimy][dimz];
this.xmin=xmin;
this.xmax=xmax;
this.ymin=ymin;
this.ymax=ymax;
this.zmin=zmin;
this.zmax=zmax;
sx=((xmax-xmin)/(dimx()-1));
sy=((ymax-ymin)/(dimy()-1));
sz=((zmax-zmin)/(dimz()-1));

}

///**
//set the size  of the image
//*/
//public void init(int dimx,int dimy,int dimz)
//{
//y=new double[dimx][dimy][dimz];
////samplingx=((xmax-xmin)/(dimx()-1));
////samplingy=((ymax-ymin)/(dimy()-1));
////samplingz=((zmax-zmin)/(dimz()-1));
//}



public double val(int i,int j,int k)
{
return array[i][j][k];
}

public void setVal(int i,int j,int k,double v)
{
array[i][j][k]=v;
}

public void _annulate()
{
init( xmin(), xmax(), ymin(), ymax(), zmin(), zmax(), dimx(), dimy(),dimz());
}

public double getValue(double x,double y,double z)
{
int i=(int) ((x-xmin)/sx);
int j=(int) ((y-ymin)/sy);
int k=(int) ((z-zmin)/sz);
return array[i][j][k];
}


public int dimy()
{
if (array==null) return 0;
if (array.length==0) return 0;
return array[0].length;
}

public int dimz()
{
if (array==null) return 0;
if (array.length==0) return 0;
if (array[0].length==0) return 0;
return array[0][0].length;
}
/**
 * nb of pixels horizontally
 * @return
 */
public int dimx()
{
if (array!=null) return array.length;else return 0;
}



public double xmin() { return xmin; }
public double ymin() { return ymin; }
public double xmax() { return xmax; }
public double ymax() { return ymax; }
public double zmin() { return zmin; }
public double zmax() { return zmax; }


public double[] min()
{
double[] r=new double[2];
r[0]=xmin;
r[1]=ymin;
return r;
}

public double[] max()
{
double[] r=new double[2];
r[0]=xmax;
r[1]=ymax;
return r;
}


public void setxmin(double r){xmin=r;}
public void setxmax(double r){xmax=r;}
public void setymin(double r){ymin=r;}
public void setymax(double r){ymax=r;}



public void fillWithFunction(Function3D1D f)
{
double sx=(xmax()-xmin())/(dimx()-1);
double sy=(ymax()-ymin())/(dimy()-1);
double sz=(zmax()-zmin())/(dimz()-1);
double[] in=new double[3];
if (f!=null)
	for (int i=0;i<dimx();i++)
		{
		in[0]=xmin+i*sx;
		for (int j=0;j<dimy();j++)
			{
			in[1]=ymin+j*sy;
			for (int k=0;k<dimz();k++)
				{
				in[2]=zmin+k*sz;
				array[i][j][k]=f.f(in);
				//System.out.println(i+" "+j+" "+in[0]+" "+in[1]+" "+y[i][j]+"\n");
				}
			}
		}
}

/**
 * add the function value at each point of the signal
 * @param f
 */
public void addFunction(Function3D1D f)
{
double sx=(xmax()-xmin())/(dimx()-1);
double sy=(ymax()-ymin())/(dimy()-1);
double sz=(zmax()-zmin())/(dimz()-1);
double[] in=new double[3];
if (f!=null)
	for (int i=0;i<dimx();i++)
		{
		in[0]=xmin+i*sx;
		for (int j=0;j<dimy();j++)
			{
			in[1]=ymin+j*sy;
			for (int k=0;k<dimz();k++)
				{
				in[2]=zmin+k*sz;
				array[i][j][k]+=f.f(in);
				//System.out.println(i+" "+j+" "+in[0]+" "+in[1]+" "+y[i][j]+"\n");
				}
			}
		}
}

/**return the max  value()*/
public double maxValue()
{
if (array==null) return 0;
double r=-1e300;
for (int i=0;i<dimx();i++)
	for (int j=0;j<dimy();j++) 
		for (int k=0;k<dimz();k++) if (array[i][j][k]>r) r=array[i][j][k];
return r;
}
/**return the max  value()*/
public double minValue()
{
if (array==null) return 0;
double r=1e300;
for (int i=0;i<dimx();i++)
	for (int j=0;j<dimy();j++) 
		for (int k=0;k<dimz();k++) if (array[i][j][k]<r) r=array[i][j][k];
return r;
}

public void _copy(Signal3D1D sig)
{
for (int i=0;i<dimx();i++)
	for (int j=0;j<dimy();j++) 
		for (int k=0;k<dimz();k++) array[i][j][k]=sig.array[i][j][k];
}

public Signal3D1D getAcopy()
{
Signal3D1D scopy=new 	Signal3D1D();
scopy.init(xmin, xmax, ymin, ymax, zmin, zmax, this.dimx(), this.dimx(), this.dimx());
scopy._copy(this);
return scopy;
}

/**
 * divide by sig voxel by voxel, do not divide if sig is 0
 * @param sig
 */
public void _div(Signal3D1D sig)
{
double v;
for (int i=0;i<dimx();i++)
	for (int j=0;j<dimy();j++) 
		for (int k=0;k<dimz();k++) 
			{
			v=sig.array[i][j][k];
			if (v!=0) array[i][j][k]=array[i][j][k]/v;
			}
}

public double calcIntegral()
{
double v=0;
for (int i=0;i<dimx();i++)
	for (int j=0;j<dimy();j++) 
		for (int k=0;k<dimz();k++) 
			{
			v+=this.array[i][j][k];
			}
double sx=(xmax-xmin)/(dimx()-1);
double sy=(ymax-ymin)/(dimy()-1);
double sz=(zmax-zmin)/(dimz()-1);
v*=sx*sy*sz;
return v;
}


public Signal3D1D calcCumulatedIntegral()
{
Signal3D1D s2=new Signal3D1D();
s2.init(xmin, xmax, ymin, ymax, zmin, zmax, dimx(), dimy(), dimz());
double v=0;
for (int i=0;i<dimx();i++)
	for (int j=0;j<dimy();j++) 
		for (int k=0;k<dimz();k++) 
			{
			v+=this.array[i][j][k];
			s2.setVal(i, j, k, v);
			}
double sx=(xmax-xmin)/(dimx()-1);
double sy=(ymax-ymin)/(dimy()-1);
double sz=(zmax-zmin)/(dimz()-1);
s2._scmul(sx*sy*sz);
return s2;
}



/**
 * used to sort a x,y,z position in space according to this 3D function
 * @return
 */
public SignalDiscret1D1D calcCumulatedIntegralPDFinverse()
{
int length=dimx()*dimy()*dimz();
SignalDiscret1D1D sig=new SignalDiscret1D1D(0,length,length);
//double[] vals=new double[length];
double v=0;
int l=0;
for (int i=0;i<dimx();i++)
	for (int j=0;j<dimy();j++) 
		for (int k=0;k<dimz();k++) 
			{
			v+=this.array[i][j][k];
			sig.setValue(l, v);
			//vals[l]=v;
			l++;
			}
//for (l=0;l<length;l++) vals[l]/=vals[length-1];
sig._multiply(1/v);

return sig.invert().resampleWithLinearInterpolation(50000);
}



public Signal2D1D getZSlice(int k)
{
Signal2D1D s2D=new Signal2D1D(xmin,xmax,ymin,ymax,dimx(),dimy());
for (int i=0;i<dimx();i++)
	for (int j=0;j<dimy();j++) 
			{
			s2D.setVal(i, j, this.val(i, j, k));
			}
return s2D;
}

public Signal2D1D getYSlice(int j)
{
Signal2D1D s2D=new Signal2D1D(xmin,xmax,zmin,zmax,dimx(),dimz());
for (int i=0;i<dimx();i++)
	for (int k=0;k<dimz();k++) 
			{
			s2D.setVal(i, k, this.val(i, j, k));
			}
return s2D;
}

public  void getZSlice(Signal2D1D s2D,int k)
{
for (int i=0;i<dimx();i++)
	for (int j=0;j<dimy();j++) 
			{
			s2D.setVal(i, j, this.val(i, j, k));
			}
}

public void _scmul(double d) {
	for (int i=0;i<dimx();i++)
		for (int j=0;j<dimy();j++) 
			for (int k=0;k<dimz();k++) 
				{
				this.setVal(i, j,k, this.val(i,j,k)*d);
				}	
}


/**
 * normalise : divide by integral
 */
public void _normalise() 
{
this._scmul(1/this.calcIntegral());
}


/**
 * store the image in the disk as a raw array of float
 * @param fullFilename
 * @return true if ok 
 */
public boolean saveAsRawFloat(String path,String filename)
{
int w=dimx();
int h=dimy();
int d=dimz();

//header
ByteBuffer buf=ByteBuffer.allocate(fileHeaderSize+w*h*d*Float.SIZE);
buf.putFloat((float)w);
buf.putFloat((float)h);
buf.putFloat((float)d);
buf.putFloat((float)xmin());
buf.putFloat((float)xmax());
buf.putFloat((float)ymin());
buf.putFloat((float)ymax());
buf.putFloat((float)zmin());
buf.putFloat((float)zmax());
int size=Float.BYTES*9;
buf.put(new byte[fileHeaderSize-size]);

//data
float[] frameFloat=new float[w*h*d];
for (int i=0;i<w;i++) for (int j=0;j<h;j++) for (int k=0;k<d;k++) frameFloat[k*w*h+j*w+i]=(float)val(i, j,k);

for (int k=0;k<frameFloat.length;k++)
		{
		buf.putFloat(frameFloat[k]);
		}
try
	{
	if (path.endsWith("/")) path=path.substring(0, path.length()-2);
	String fullFilename=path+File.separator+filename;
	BufferedOutputStream bw=new BufferedOutputStream(new FileOutputStream(fullFilename));
	bw.write(buf.array());
	System.out.println("float image file "+fullFilename+" stored ("+buf.array().length+"bytes)");
	bw.close();
	}
catch (Exception e)
	{
	System.err.println(" PROBLEM writing file: "+path+filename+" "+e);
	return false;
	}

return true;
}





/**
 * read an image encoded in floats
 * @param fullFilename
 * @param littleEndian false for java, true for C
 * @return
 */
public  boolean loadRawFloat(String path,String filename,boolean littleEndian)
{
byte[] header=new byte[fileHeaderSize];

//byte[] b0=new byte[(6)*4];
BufferedInputStream br;
try
	{
	br=new BufferedInputStream(new FileInputStream(path+File.separator+filename));
	}
catch (Exception e)
	{
	System.err.println(" PROBLEM reading raw float image ");
	e.printStackTrace();
	return false;
	}
//read header:
try
	{
	br.read(header,0,header.length);
	}
catch (Exception e)
	{
	System.err.println(" PROBLEM reading raw float image ");
	e.printStackTrace();
	try {br.close();} catch (Exception e1) {};
	return false;
	}
ByteBuffer buf0=ByteBuffer.wrap(header);
if (littleEndian) buf0.order(ByteOrder.LITTLE_ENDIAN); 
int w=(int)buf0.getFloat();
int h=(int)buf0.getFloat();
int d=(int)buf0.getFloat();
float xmin=buf0.getFloat();
float xmax=buf0.getFloat();
float ymin=buf0.getFloat();
float ymax=buf0.getFloat();
float zmin=buf0.getFloat();
float zmax=buf0.getFloat();
//System.out.println("littleEndian ="+littleEndian);
//System.out.println("w="+w+" h="+h+" xmin="+xmin+" xmax="+xmax+" ymin="+ymin+" ymax="+ymax);
if ((w<=0)||(h<=0)) 
	{
	System.err.println("Error w or h <=0");
	try {br.close();} catch (Exception e1) {};
	return false;
	}
init(xmin,xmax,ymin,ymax,zmin,zmax,w,h,d);

//read data:
//int w=dimx();
//int h=dimy();
float[] frameFloat=new float[w*h*d];
byte[] b=new byte[(w*h*d)*Float.BYTES];
//System.out.println("read "+((w*h)*4)+" bytes in file:"+fullFilename+" ...");
try
	{
	br.read(b,0,b.length);
	br.close();
	}
catch (Exception e)
	{
	System.err.println(" PROBLEM reading raw float image ");
	e.printStackTrace();
	return false;
	}
System.out.println("3D signal float  file "+path+filename+" loaded");
ByteBuffer buf=ByteBuffer.wrap(b);
if (littleEndian) buf.order(ByteOrder.LITTLE_ENDIAN); 
for (int k=0;k<w*h*d;k++) frameFloat[k]=buf.getFloat();
//init(w,h);
for (int i=0;i<w;i++) for (int j=0;j<h;j++)  for (int k=0;k<d;k++) setVal(i,j,k,frameFloat[k*w*h+j*w+i]);
return true;
}

public void _add(Signal3D1D s1) 
{
	for (int i=0;i<dimx();i++)
		for (int j=0;j<dimy();j++) 
			for (int k=0;k<dimz();k++) 
				{
				this.setVal(i, j,k, this.val(i,j,k)+s1.val(i, j, k));
				}	
	
}











}//end of class




