// Author Laurent Brunel

package com.photonlyx.toolbox.math.signal;

import java.awt.image.*;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.photonlyx.toolbox.gui.PaletteInterface;
import com.photonlyx.toolbox.gui.PalettePanel;
import com.photonlyx.toolbox.math.Complex;
import com.photonlyx.toolbox.math.analyse2D.RadialIntegrator;
import com.photonlyx.toolbox.math.analyse2D.SpotCentreDetector;
import com.photonlyx.toolbox.math.function.Function1D1D;
import com.photonlyx.toolbox.math.function.Function2D1D;
import com.photonlyx.toolbox.math.function.usual2D1D.Gaussian;
import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.ImageUtils;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;



/**
represent a 2 dimension signal with flounting boundaries </p>
can read gif jpg and nip (special 16bits fuga) file format
 */
public class Signal2D1D extends Signal  implements Signal2D1DSource
{

	private double[][] y=new double[2][2];
	private double xmin=0,xmax=1,ymin=0,ymax=1;
	private double TINY=1E-10;
	private double samplingx,samplingy;

	public Signal2D1D()
	{
	}

	public Signal2D1D(double xmin,double xmax,double ymin,double ymax,int dimx,int dimy)
	{
		init(xmin, xmax, ymin, ymax, dimx, dimy);
	}
	public Signal2D1D(int dimx,int dimy)
	{
		init(0, 1, 0, 1, dimx, dimy);
	}

	/**
set the size and boundaries of the image
	 */
	public void init(double xmin,double xmax,double ymin,double ymax,int dimx,int dimy)
	{
		y=new double[dimx][dimy];
		this.xmin=xmin;
		this.xmax=xmax;
		this.ymin=ymin;
		this.ymax=ymax;
		updateSamplings();

	}

	/**
set the size  of the image
	 */
	public void init(int dimx,int dimy)
	{
		y=new double[dimx][dimy];
		updateSamplings();
	}

	public void updateSamplings()
	{
		samplingx=((xmax-xmin)/(dimx()-1));
		samplingy=((ymax-ymin)/(dimy()-1));
	}


	/**
set all values to zero
	 */
	public void erase()
	{
		y=new double[dimx()][dimy()];
	}


	public double[] out(int[] index)
	{
		double[] r=new double[1];
		r[0]=y[index[0]][index[1]];
		return r;
	}

	/**
	 * nb of pixels vertically
	 * @return
	 */
	public int dimy()
	{
		if (y==null) return 0;
		if (y.length==0) return 0;
		return y[0].length;
	}

	/**
	 * nb of pixels horizontally
	 * @return
	 */
	public int dimx()
	{
		if (y!=null) return y.length;else return 0;
	}

	public int[] dim()
	{
		int[] d=new int[2];
		d[0]=dimx();
		d[1]=dimy();
		return d;
	}

	public double xmin() { return xmin; }
	public double ymin() { return ymin; }
	public double xmax() { return xmax; }
	public double ymax() { return ymax; }
	public double zmin() { return this.minValue(); }
	public double zmax() { return this.maxValue(); }


	public double[] min()
	{
		double[] r=new double[2];
		r[0]=xmin;
		r[1]=ymin;
		return r;
	}

	public double[] max()
	{
		double[] r=new double[2];
		r[0]=xmax;
		r[1]=ymax;
		return r;
	}


	public void setxmin(double r){xmin=r;}
	public void setxmax(double r){xmax=r;}
	public void setymin(double r){ymin=r;}
	public void setymax(double r){ymax=r;}

	/**set a new array , please indicate the array size in dx and dy*/
	public void setArray(double[][] array/*,int dx,int dy*/)
	{
		y=array;
		//dimy=array[0].length;
		//dimx=array.length;
		//dimx=dx;
		//dimy=dy;
	}

	public void affectRow(int rowindex,double[] col)
	{
		for (int i=0;i<dimx();i++) y[i][rowindex]=col[i];
	}

	public void affectCol(int colindex,double[] row)
	{
		for (int j=0;j<dimy();j++) y[colindex][j]=row[j];
	}

	/**get the matrix of double values*/
	public double[][] getArray()
	{
		return y;
	}

	public double[] out(double[] in)
	{
		double[] r=new double[1];
		int indicex=(int) ( xmin+in[0]*samplingx() );
		int indicey=(int) ( ymin+in[1]*samplingy() );
		r[0]=y[indicex][indicey];
		return r;
	}

	public void set(int[] cell,double[] in)
	{
		y[cell[0]][cell[1]]=in[0];
	}



	public void fillWithFonction(Function2D1D fonction)
	{
		//double sx=samplingx();
		//double sy=samplingy();
		double sx=(xmax()-xmin())/(dimx()-1);
		double sy=(ymax()-ymin())/(dimy()-1);
		double[] in=new double[2];
		if (fonction!=null)
			for (int i=0;i<dimx();i++)
			{
				in[0]=xmin+i*sx;
				for (int j=0;j<dimy();j++)
				{
					in[1]=ymin+j*sy;
					y[i][j]=fonction.f(in);
					//System.out.println(i+" "+j+" "+in[0]+" "+in[1]+" "+y[i][j]+"\n");
				}
			}
	}


	/** fill the signal with image file (jpeg or gif) value*/
	protected void fillWithFile(String filename)
	{
		System.out.println(getClass()+": load image "+filename);

		if ((filename.endsWith(".jpg"))||(filename.endsWith(".JPG")) || (filename.endsWith(".GIF"))||(filename.endsWith(".gif"))   )
		{
			Image img=null;
			try
			{
				img = Toolkit.getDefaultToolkit().getImage(filename);
			}
			catch (Exception e)
			{
				System.out.println("Erreur chargement image"+e);
			}
			fillWithImage(img,"R",1);
		}

	}

	/**
	 * store the image in the disk as a raw array of float
	 * @param path
	 * @param filename
	 * @return
	 */
	public boolean saveAsRawFloatImage(String path,String filename)
	{
		return saveAsRawFloatImage( path+File.separator+filename);
	}
	/**
	 * store the image in the disk as a raw array of float
	 * @param fullFilename
	 * @return true if ok 
	 */
	public boolean saveAsRawFloatImage(String fullFilename)
	{
		int w=dimx();
		int h=dimy();
		float[] frameFloat=new float[w*h];
		for (int i=0;i<w;i++) for (int j=0;j<h;j++) frameFloat[j*w+i]=(float)getValue(i, j);

		ByteBuffer buf=ByteBuffer.allocate(frameFloat.length*4+6*4);
		buf.putFloat((float)w);
		buf.putFloat((float)h);
		buf.putFloat((float)xmin());
		buf.putFloat((float)xmax());
		buf.putFloat((float)ymin());
		buf.putFloat((float)ymax());
		for (int k=0;k<frameFloat.length;k++)
		{
			buf.putFloat(frameFloat[k]);
		}
		try
		{
			BufferedOutputStream bw=new BufferedOutputStream(new FileOutputStream(fullFilename));
			bw.write(buf.array());
			//System.out.println("float image file "+fullFilename+" stored ("+buf.array().length+"bytes)");
			bw.close();
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM writing file: "+fullFilename+" "+e);
			return false;
		}

		return true;
	}






	public  boolean readRawFloatImageInResources(String filename)
	{

		byte[] b0=new byte[(6)*4];
		BufferedInputStream br;
		try
		{
			URL url=getClass().getClassLoader().getResource(filename);
			URLConnection connection = url.openConnection();
			InputStream input = connection.getInputStream();
			br=new BufferedInputStream(input);
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading raw float image ");
			e.printStackTrace();
			return false;
		}
		//read header:
		try
		{
			br.read(b0,0,b0.length);
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading raw float image ");
			e.printStackTrace();
			return false;
		}
		ByteBuffer buf0=ByteBuffer.wrap(b0);
		int w=(int)buf0.getFloat();
		int h=(int)buf0.getFloat();
		float xmin=buf0.getFloat();
		float xmax=buf0.getFloat();
		float ymin=buf0.getFloat();
		float ymax=buf0.getFloat();
		System.out.println("xmin="+xmin+" xmax="+xmax+" ymin="+ymin+" ymax="+ymax);
		init(xmin,xmax,ymin,ymax,w,h);

		//read data:
		//int w=dimx();
		//int h=dimy();
		float[] frameFloat=new float[w*h];
		byte[] b=new byte[(w*h)*4];
		System.out.println("read "+((w*h)*4)+" bytes in resource file:"+filename+" ...");
		try
		{
			br.read(b,0,b.length);
			br.close();
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading raw float image ");
			e.printStackTrace();
			return false;
		}
		System.out.println("float image file "+filename+" loaded");
		ByteBuffer buf=ByteBuffer.wrap(b);
		for (int k=0;k<w*h;k++) frameFloat[k]=buf.getFloat();
		//init(w,h);
		for (int i=0;i<w;i++) for (int j=0;j<h;j++) setValue(i,j,frameFloat[j*w+i]);
		return true;

	}




	public  boolean readRawFloatImage(String fullFilename)
	{
		return this.readRawFloatImage(fullFilename,false);
	}

	public  boolean readRawFloatImage(String fullFilename,boolean littleEndian)
	{
		return this.readRawFloatImage(fullFilename, littleEndian, true);
	}

	/**
	 * read an image encoded in floats
	 * @param fullFilename
	 * @param littleEndian
	 * @return
	 */
	public  boolean readRawFloatImage(String fullFilename,boolean littleEndian,boolean verbose)
	{

		byte[] b0=new byte[(6)*4];
		BufferedInputStream br;
		try
		{
			br=new BufferedInputStream(new FileInputStream(fullFilename));
		}
		catch (Exception e)
		{
			if (verbose) System.err.println(" PROBLEM reading raw float image ");
			//e.printStackTrace();
			return false;
		}
		//read header:
		try
		{
			br.read(b0,0,b0.length);
		}
		catch (Exception e)
		{
			if (verbose) System.err.println(" PROBLEM reading raw float image ");
			//e.printStackTrace();
			try {br.close();} catch (Exception e1) {};
			return false;
		}
		ByteBuffer buf0=ByteBuffer.wrap(b0);
		if (littleEndian) buf0.order(ByteOrder.LITTLE_ENDIAN); 
		int w=(int)buf0.getFloat();
		int h=(int)buf0.getFloat();
		float xmin=buf0.getFloat();
		float xmax=buf0.getFloat();
		float ymin=buf0.getFloat();
		float ymax=buf0.getFloat();
//		System.out.println("littleEndian ="+littleEndian);
//		System.out.println("w="+w+" h="+h+" xmin="+xmin+" xmax="+xmax+" ymin="+ymin+" ymax="+ymax);
		if ((w<=0)||(h<=0)) 
		{
			if (verbose) System.err.println("Error w or h <=0");
			try {br.close();} catch (Exception e1) {};
			return false;
		}
		init(xmin,xmax,ymin,ymax,w,h);

		//read data:
		//int w=dimx();
		//int h=dimy();
		float[] frameFloat=new float[w*h];
		byte[] b=new byte[(w*h)*4];
		//System.out.println("read "+((w*h)*4)+" bytes in file:"+fullFilename+" ...");
		try
		{
			br.read(b,0,b.length);
			br.close();
		}
		catch (Exception e)
		{
			if (verbose) System.err.println(" PROBLEM reading raw float image ");
			//e.printStackTrace();
			return false;
		}
		if (verbose) System.out.println("float image file "+fullFilename+" loaded");
		ByteBuffer buf=ByteBuffer.wrap(b);
		if (littleEndian) buf.order(ByteOrder.LITTLE_ENDIAN); 
		for (int k=0;k<w*h;k++) frameFloat[k]=buf.getFloat();
		//init(w,h);
		for (int i=0;i<w;i++) for (int j=0;j<h;j++) setValue(i,j,frameFloat[j*w+i]);
		return true;
	}

	/**
	 * fill with a raw file of unsigned integers (32bits/4 bytes)
	 * @param fullFilename
	 * @param littleEndian
	 * @return
	 */
	public  boolean readRaw32bitsUnsignedIntImage(String fullFilename,boolean littleEndian)
	{
		int w=dimx();
		int h=dimy();
		byte[] b=new byte[w*h*4];
		System.out.println("read "+(w*h*3)+" bytes in file:"+fullFilename+" ...");
		try
		{
			BufferedInputStream br=new BufferedInputStream(new FileInputStream(fullFilename));
			br.read(b,0,b.length);
			br.close();
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading raw float image ");
			e.printStackTrace();
			return false;
		}
		System.out.println("32bits unsigned int image file "+fullFilename+" loaded");
		ByteBuffer buf=ByteBuffer.wrap(b);
		if (littleEndian) buf.order(ByteOrder.LITTLE_ENDIAN); 
		init(w,h);
		for (int j=0;j<h;j++) for (int i=0;i<w;i++) //strange pixels order!!!!!!
		{
			int ii=buf.getInt();
			//if (ii>10000) System.out.println(i+" "+j+" "+ii);
			setValue(i,j,ii);
		}
		return true;
	}
	/** fill the signal with an image
	 * @param im
	 * @param channel can be "R" "G" "B" "alpha" "H" "S" "Br"
	 * @param scale  scale factor in unit/pixel
	 * @return
	 *
	 *
	 */
	public void fillWithImage(Image img,String channel,double scale)
	{
		copy(Signal2D1D.getSignal2D1DFromImage(img, channel, scale));
	}

	/** fill the signal with movie object*/
	public void fillWithImageOLD(Image img)
	{
		if (img==null)
		{
			Messager.messErr("No image ");
			return;
		}
		Frame f=new Frame();
		try
		{
			MediaTracker tracker = new MediaTracker(f);
			tracker.addImage(img, 0);
			tracker.waitForID(0);
		}
		catch (Exception e) {}

		int w = img.getWidth(f);
		int h = img.getHeight(f);
		System.out.println("Image "+w+"x"+h);

		if ((w==-1)||(h==-1))
		{
			Messager.messErr("Problem with image ");
			return;
		}

		//dimx=w;
		//dimy=h;
		/*xmin=0;
xmax=w;
ymin=0;
ymax=h;*/
		/*xmin=getNode().lookForParam("xminInit",0,false);
xmax=getNode().lookForParam("xmaxInit",w,false);
ymin=getNode().lookForParam("yminInit",0,false);
ymax=getNode().lookForParam("ymaxInit",h,false);*/
		y=new double[w][h];

		int[] pixels = new int[w * h];
		PixelGrabber pg = new PixelGrabber(img, 0, 0, w, h, pixels, 0, w);
		try
		{
			pg.grabPixels();
		}
		catch (InterruptedException e)
		{
			System.err.println("interrupted waiting for pixels!");
			return;
		}
		if ((pg.getStatus() & ImageObserver.ABORT) != 0)
		{
			System.err.println("image fetch aborted or errored");
			return;
		}

		//for (int i = 0; i < w*h; i++) System.out.println("pixel  "+i+" val "+pixels[i]);


		for (int i = 0; i < w; i++)
		{
			for (int j = 0; j < h; j++)
			{
				//int alpha = (pixels[j*w+i] >> 24) & 0xff;//voir doc jdk PixelGrabber
				int red   = (pixels[j*w+i] >> 16) & 0xff;
				//System.out.println((j*w+i)+" pixel col "+i+"lig "+j+" val "+pixels[j*w+i]);
				//System.out.println("pixel col "+i+"lig "+j+" val "+red);
				//y[i][j]=red;
				y[i][h-1-j]=red;
			}
		}


	}






	/**transfert all data from the given Signal2D1D.BEWARE: no copy of matrix data just point to the same matrix data*/
	public void transfert(Signal2D1D signal)
	{
		y=signal.y;
		xmin=signal.xmin;
		xmax=signal.xmax;
		ymin=signal.ymin;
		ymax=signal.ymax;
		//dimx=signal.dimx;
		//dimy=signal.dimy;
	}


	/**
	 * return a new instance of Signal2D1D that is a copy of this one 
	 * */
	public Signal2D1D copy()
	{
		Signal2D1D newsignal=new Signal2D1D();
		newsignal.init(this.xmin(),this.xmax(),this.ymin(),this.ymax(),this.dimx(),this.dimy());
		for (int i=0;i<newsignal.dim()[0];i++)
			for (int j=0;j<newsignal.dim()[1];j++)
			{
				newsignal.setValue(i,j,getValue(i,j));
			}
		return newsignal;
	}


	/**Copy the signal s*/
	public void copy(Signal2D1D s)
	{
		init(s.xmin(),s.xmax(),s.ymin(),s.ymax(),s.dimx(),s.dimy());
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,s.getValue(i,j));
			}
	}



	/**return the min value()*/
	public double minValue()
	{
		if (y==null) return 0;
		double r=1e300;
		for (int i=0;i<dimx();i++) for (int j=0;j<dimy();j++)  if (y[i][j]<r) r=y[i][j];
		return r;
	}


	public double minValue(int x0,int y0,int w0,int h0)
	{
		if (y==null) return 0;
		double r=1e300;
		for (int i=x0;i<x0+w0;i++) for (int j=y0;j<y0+h0;j++)  if (y[i][j]<r) r=y[i][j];
		return r;
	}

	/**return the pixel having  min value()*/
	public int[] minValuePixel()
	{
		if (y==null) return null;
		int[] p=new int[2];
		double r=1e300;
		for (int i=0;i<dimx();i++) 
			for (int j=0;j<dimy();j++)  
				if (y[i][j]<r) 
				{
					r=y[i][j];
					p[0]=i;
					p[1]=j;
				}
		return p;
	}
	

	
	public double[] minValueCoords()
	{
		int[] ij=minValuePixel();
		double[] xy=new double[2];
		xy[0]=xmin+this.samplingx()*ij[0];
		xy[1]=ymin+this.samplingy()*ij[1];
		return xy;
	}

	
	/**return the min value() among the positive values*/
	public double minValueStrictlyPositive()
	{
		if (y==null) return 0;
		double r=1e300;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
				if (y[i][j]>0) if (y[i][j]<r)  r=y[i][j];
		return r;
	}


	/**return the max  value()*/
	public double maxValue()
	{
		if (y==null) return 0;
		double r=-1e300;
		for (int i=0;i<dimx();i++) for (int j=0;j<dimy();j++)  if (y[i][j]>r) r=y[i][j];
		return r;
	}

	public double maxValue(int x0,int y0,int w0,int h0)
	{
		if (y==null) return 0;
		double r=-1e300;
		for (int i=x0;i<x0+w0;i++) for (int j=y0;j<y0+h0;j++) if (y[i][j]>r) r=y[i][j];
		return r;
	}

	/**return the max value() among the negative values*/
	public double maxValueStrictlyNegative()
	{
		if (y==null) return 0;
		double r=-1e300;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
				if (y[i][j]<0) if (y[i][j]>r)  r=y[i][j];
		return r;
	}

	/**return the distance in the x axis between 2 samples*/
	public double samplingx()
	{
		return samplingx;
	}

	/**return the distance in the y axis between 2 samples*/
	public double samplingy()
	{
		return samplingy;
	}


	/**set a value*/
	public void setValue(int i,int j,double val)
	{
		if (y==null) return;
		y[i][j]=val;
	}

	/**set a value*/
	public void setVal(int i,int j,double val)
	{
		y[i][j]=val;
	}

	public boolean inRange(int i,int j)
	{
		return ((i>=0)||(i<dimx())||(j>=0)||(j<dimy())) ;

	}

	/**get a value*/
	public double getValue(int i,int j)
	{
		if (y==null) return 0;
		return y[i][j];
	}

	/**get a value*/
	public double z(int i,int j)
	{
		if (y==null) return 0;
		return y[i][j];
	}

	/**get a value*/
	public double val(int i,int j)
	{
		return y[i][j];
	}

	/**get a value*/
	public double getValue(double xx,double yy)
	{
		if (y==null) return 0;
		int i=getSampleIndexX(xx);
		int j=getSampleIndexY(yy);
		if ((i<0)||(i>=dimx())) return 0;
		if ((j<0)||(j>=dimy())) return 0;
		return y[i][j];
	}
	/**get a value*/
	public double getValue(double[] p)
	{
		return getValue(p[0],p[1]);
	}




	/**get a value*/
	public double getValueInterpol(double xx,double yy)
	{
		if (y==null) return 0;
		int cellx=(int)Math.floor((xx-xmin)/samplingx);
		int celly=(int)Math.floor((yy-ymin)/samplingy);
		if ((cellx<0)||(cellx>=(dimx()-1))) return 0;
		if ((celly<0)||(celly>=(dimy()-1))) return 0;

		//return (y[cellx][celly]*(samplingx*(cellx+1)-xx)+
		//			y[cellx+1][celly]*(xx-samplingx*cellx))	/samplingx  +
		//		(y[cellx][celly+1]*(samplingy*(celly+1)-yy)+
		//				y[cellx+1][celly+1]*(yy-samplingy*celly))  /samplingy;
		return (interpol(xx,samplingx*cellx,y[cellx][celly],samplingx*(cellx+1),y[cellx+1][celly])+
				interpol(yy,samplingy*celly,y[cellx][celly],samplingy*(celly+1),y[cellx][celly+1]) )/2;
	}

	/**
	 * return linear interpolated value a x (which is between x1 and x2)
	 * @param x  where we want an interpolation
	 * @param x1 x of previous point
	 * @param y1 y of previous point
	 * @param x2 x of next point
	 * @param y2 y of next point
	 * @return
	 */
	private double interpol(double x,double x1,double y1,double x2,double y2)
	{
		return ( (x2-x)*y1 + (x-x1)*y2 ) / (x2-x1);
	}

	/*
 private void handlesinglepixel(int x, int y, int pixel) {
        int alpha = (pixel >> 24) & 0xff;
        int red   = (pixel >> 16) & 0xff;
        int green = (pixel >>  8) & 0xff;
        int blue  = (pixel      ) & 0xff;
        // Deal with the pixel as necessary...
 }

	 */


	/**return the barycentre calculated over the indexes from i1 to i2 included and j1 to j2 included*/
	public double[] barycentre(int x0,int y0,int w0,int h0)
	{
		if (y==null) return null;
		//System.out.println(getName()+" imin="+imin+" jmin="+jmin+" imax="+imax+" jmax="+jmax);
		double[] r=new double[2];
		double s=0;
		double v;
		for (int i=x0;i<x0+w0;i++)
			for (int j=y0;j<y0+h0;j++)
			{
				v=y[i][j];
				r[0]+=i*v;
				r[1]+=j*v;
				s+=v;
				//System.out.println(getName()+" r "+r[0]+" "+r[1]);
			}
		//System.out.println(getName()+" s="+s);
		if (s==0)
		{
			r[0]=(x0+w0/2.0)*samplingx();
			r[1]=(y0+h0/2.0)*samplingy();
		}
		else
		{
			//	r[0]=r[0]/s;
			//	r[1]=r[1]/s;
			r[0]=xmin+(r[0]/s+0.5)*samplingx();
			r[1]=ymin+(r[1]/s+0.5)*samplingy();
		}
		return r;
	}

	/**return the barycentre of order 1 calculated over the full signal*/
	public double[] barycentre()
	{
		if (y==null) return null;
		double[] r=new double[2];
		double s=0;
		double v;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				v=y[i][j];
				r[0]+=i*v;
				r[1]+=j*v;
				s+=v;
				//System.out.println(getName()+" r "+r[0]+" "+r[1]);
				//setValue(i,j,10);
			}
		//System.out.println(getName()+" s="+s);
		if (s==0)// case black image, return the middle....
		{
			r[0]=(xmin+xmax)/2;
			r[1]=(ymin+ymax)/2;
		}
		else
		{
			r[0]=xmin+(r[0]/s+0.5)*samplingx();
			r[1]=ymin+(r[1]/s+0.5)*samplingy();
		}
		return r;
	}




	/**return the barycentre of a window of corners 1 and 2*/
	public double[] barycentre(double x1,double y1,double x2,double y2)
	{
		int x0=getSampleIndexX(x1);
		int y0=getSampleIndexY(y1);
		int w0=getSampleIndexX(x2)-x0;
		int h0=getSampleIndexY(y2)-y0;
		return barycentre(x0,y0,w0,h0);
	}


	/**return the barycentre of the image. Value bellow threshold are set to 0
	 */
	public double[] barycentreThresholdRelative(double thresholdrelative) 
	{
		double lemin =minValue();
		double lemax =maxValue();
		double treel=lemin+(lemax-lemin)*thresholdrelative;

		if (y==null) return null;
		double[] r=new double[2];
		double s=0;
		double v;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				v=y[i][j];
				if (v<=treel) v=0;
				r[0]+=i*v;
				r[1]+=j*v;
				s+=v;
				//System.out.println(getName()+" r "+r[0]+" "+r[1]);
			}
		//System.out.println(getName()+" s="+s);
		if (s==0)// case black image, return the middle....
		{
			r[0]=(xmin+xmax)/2;
			r[1]=(ymin+ymax)/2;
		}
		else
		{
			r[0]=xmin+(r[0]/s+0.5)*samplingx();
			r[1]=ymin+(r[1]/s+0.5)*samplingy();
		}
		return r;
	}

	/**
	 * return the barycentre of the image. Value bellow threshold are set to 0
	 */
	/**
	 * 
	 * @param r array of 2 double : barycentre coord
	 * @param objectSize array of 1 double
	 * @param maxValue array of 1 double max grey value
	 * @param thresholdrelative relative threshold (0 to 1) used to find object
	 */
	public void barycentreThresholdRelative(double[] r,double[] objectSize ,double[] maxValue,double thresholdrelative) 
	{
		double lemin =minValue();
		double lemax =maxValue();
		double treel=lemin+(lemax-lemin)*thresholdrelative;

		if (y==null) return ;
		double s=0;
		double v;
		objectSize[0]=0;
		maxValue[0]=-1e30;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				v=y[i][j];
				if (v>maxValue[0]) maxValue[0]=v;
				if (v<=treel) v=0;else objectSize[0]+=1;
				r[0]+=i*v;
				r[1]+=j*v;
				s+=v;
				//System.out.println(getName()+" r "+r[0]+" "+r[1]);
			}
		//System.out.println(getName()+" s="+s);
		if (s==0)// case black image, return the middle....
		{
			r[0]=(xmin+xmax)/2;
			r[1]=(ymin+ymax)/2;
		}
		else
		{
			r[0]=xmin+(r[0]/s+0.5)*samplingx();
			r[1]=ymin+(r[1]/s+0.5)*samplingy();
		}
	}
	/**
	 * return the barycentre of the image ROI. Value bellow threshold are set to 0
	 * @param thresholdrelative
	 * @param x0 x of the ROI corner
	 * @param y0 y of the ROI corner
	 * @param w0 width of the ROI
	 * @param h0 height of the ROI
	 * @return
	 */
	public double[] barycentreThresholdRelative(double thresholdrelative,int x0,int y0,int w0,int h0) 
	{
		double lemin =minValue(x0, y0, w0, h0);
		double lemax =maxValue(x0, y0, w0, h0);
		double treel=lemin+(lemax-lemin)*thresholdrelative;
		//	System.out.println("Thresholdrelative="+thresholdrelative);
		//	System.out.println("lemin="+lemin);
		//	System.out.println("lemax="+lemax);
		//	System.out.println("treel="+treel);

		if (y==null) return null;
		double[] r=new double[2];
		double s=0;
		double v;
		for (int i=x0;i<x0+w0;i++)
			for (int j=y0;j<y0+h0;j++)
			{
				v=y[i][j];
				if (v<=treel) v=0;
				r[0]+=i*v;
				r[1]+=j*v;
				s+=v;
				//System.out.println(getName()+" r "+r[0]+" "+r[1]);
			}
		//System.out.println(getName()+" s="+s);
		if (s==0)// case black image, return the middle....
		{
			r[0]=(x0+w0/2.0)*samplingx();
			r[1]=(y0+h0/2.0)*samplingy();
		}
		else
		{
			r[0]=xmin+(r[0]/s+0.5)*samplingx();
			r[1]=ymin+(r[1]/s+0.5)*samplingy();
		}
		//System.out.println(" r "+r[0]+" "+r[1]);
		return r;
	}


	/**
	 * return the barycentre of the image ROI. Value bellow threshold are set to 0
	 * @param threshold in grey level
	 * @param x0 x of the ROI corner
	 * @param y0 y of the ROI corner
	 * @param w0 width of the ROI
	 * @param h0 height of the ROI
	 * @return
	 */
	public double[] barycentreThreshold(double threshold,int x0,int y0,int w0,int h0) 
	{

		if (y==null) return null;
		double[] r=new double[2];
		double s=0;
		double v;
		for (int i=x0;i<x0+w0;i++)
			for (int j=y0;j<y0+h0;j++)
			{
				v=y[i][j];
				if (v<=threshold) v=0;
				r[0]+=i*v;
				r[1]+=j*v;
				s+=v;
				//System.out.println(getName()+" r "+r[0]+" "+r[1]);
			}
		//System.out.println(getName()+" s="+s);
		if (s==0)// case black image, return the middle....
		{
			r[0]=(x0+w0/2.0)*samplingx();
			r[1]=(y0+h0/2.0)*samplingy();
		}
		else
		{
			r[0]=xmin+(r[0]/s+0.5)*samplingx();
			r[1]=ymin+(r[1]/s+0.5)*samplingy();
		}
		//System.out.println(" r "+r[0]+" "+r[1]);
		return r;
	}


	public int binaryObjectSize(double thresholdrelative,int x0,int y0,int w0,int h0)
	{
		Signal2D1D s=this.copy();
		s._thresholdRelative(thresholdrelative, x0, y0, w0, h0);
		int size=0;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++) if (s.getValue(i, j)==1) size++;
		return size;

	}

	/**add a value to all the pixels*/
	public void add(double val)
	{
		if (y==null) return;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				y[i][j]=y[i][j]+val;
			}
	}

	/**add a value to all the pixels*/
	public void _add(double r) 
	{
		add(r);	
	}


	/**
	 * transform in binary image with a threshold
	 * 
	 * @param threshold 
	 * @param x0, y0, w0, h0 zone where threshold is done
	 */
	public void _threshold(double threshold,int x0,int y0,int w0,int h0)
	{
		if (y==null) return;
		for (int i=x0;i<x0+w0;i++)
			for (int j=y0;j<y0+h0;j++)
			{
				if (y[i][j]>threshold) y[i][j]=1.0; else y[i][j]=0.0;
			}
	}



	/**
	 * transform in binary image with a threshold
	 * 
	 * @param threshold 
	 */
	public void _threshold(double threshold)
	{
		_threshold(threshold,0,0,dimx(),dimy());	
	}

	/**
	 * transform in binary image with a threshold. The threshold=0 corresponds to min value and
the threshold=1 corresponds to max value
	 */
	public void _thresholdRelative(double thresholdrelative,int x0,int y0,int w0,int h0)
	{
		double lemin =minValue(x0,y0,w0,h0);
		double lemax =maxValue(x0,y0,w0,h0);
		double treel=lemin+(lemax-lemin)*thresholdrelative;
		//System.out.println("Thresholdrelative="+thresholdrelative);
		//System.out.println("lemin="+lemin);
		//System.out.println("lemax="+lemax);
		//System.out.println("treel="+treel);
		_threshold(treel,x0,y0,w0,h0);
	}

	/**
	 * transform in binary image with a threshold. The threshold=0 corresponds to min value and
the threshold=1 corresponds to max value
	 */
	public void _thresholdRelative(double thresholdrelative)
	{
		_thresholdRelative(thresholdrelative,0,0,dimx(),dimy());	
	}


	/** take the log at any base for each pixel, <=0 value are not touched*/
	public void _log(double base)
	{
		if (y==null) return;
		double r=Math.log(base);
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				if (y[i][j]>0) y[i][j]=Math.log(y[i][j])/r;
			}
	}


	/**get the sample index close to the x position*/
	public int getSampleIndexX(double x)
	{
		int cell=(int) ((x-xmin)/((xmax-xmin)/dimx()));
		return cell;
	}
	/**get the sample index close to the y position*/
	public int getSampleIndexY(double y)
	{
		int cell=(int) ((y-ymin)/((ymax-ymin)/dimy()));
		return cell;
	}

	/**replace by a sub-rectangle of the signal, the pixels min an max are included*/
	public void _crop(double nxmin,double nymin,double nxmax,double nymax )
	{
		if (nxmin<xmin()) nxmin=xmin;
		if (nxmax>=xmax()) nxmax=xmax-TINY;
		if (nymin<ymin()) nymin=ymin;
		if (nymax>=ymax()) nymax=ymax-TINY;
		int indicexmin=getSampleIndexX(nxmin);
		int indicexmax=getSampleIndexX(nxmax);
		int indiceymin=getSampleIndexY(nymin);
		int indiceymax=getSampleIndexY(nymax);

		// System.out.println("Crop from(" +nxmin+ "," +nymin+ ") to (" +nxmax+ "," +nymax+ ")" );
		// System.out.println("then.crop from(" +indicexmin+ "," +indiceymin+ ") to (" +indicexmax+ "," +indiceymax+ ")" );

		double newxmin=xmin()+samplingx()*(indicexmin);
		double newxmax=xmin()+samplingx()*(indicexmax);
		double newymin=ymin()+samplingy()*(indiceymin);
		double newymax=ymin()+samplingy()*(indiceymax);
		int newdimx=indicexmax-indicexmin+1;
		int newdimy=indiceymax-indiceymin+1;
		double[][] newy=new double[newdimx][newdimy];
		for (int i=0;i<newdimx;i++)
			for (int j=0;j<newdimy;j++)
			{
				newy[i][j]=y[indicexmin+i][indiceymin+j];
			}
		y=newy;
		xmin=newxmin;
		xmax=newxmax;
		ymax=newymax;
		ymin=newymin;
		samplingx=((xmax-xmin)/(dimx()-1));
		samplingy=((ymax-ymin)/(dimy()-1));
	}

	/**return a sub-rectangle of the signal, the pixels min an max are included*/
	public Signal2D1D crop(double nxmin,double nymin,double nxmax,double nymax )
	{
		Signal2D1D newsignal=copy();
		newsignal._crop(nxmin,nymin,nxmax,nymax );
		return newsignal;
	}


	/**replace by a sub-rectangle of the signal, the pixels min an max are included*/
	public void _crop(int x0,int y0,int w0,int h0 )
	{

		double newxmin=xmin()+samplingx()*(x0);
		double newxmax=xmin()+samplingx()*(x0+w0);
		double newymin=ymin()+samplingy()*(y0);
		double newymax=ymin()+samplingy()*(y0+h0);
		double[][] newy=new double[w0][h0];
		for (int i=0;i<w0;i++)
			for (int j=0;j<h0;j++)
			{
				newy[i][j]=y[x0+i][y0+j];
			}
		y=newy;
		xmin=newxmin;
		xmax=newxmax;
		ymax=newymax;
		ymin=newymin;
		samplingx=((xmax-xmin)/(dimx()-1));
		samplingy=((ymax-ymin)/(dimy()-1));
	}

	/**replace by a sub-rectangle of the signal, the pixels min an max are included*/
	public  Signal2D1D crop(int x0,int y0,int w0,int h0 )
	{
		Signal2D1D newsignal=copy();
		newsignal._crop(x0,y0,w0,h0 );
		return newsignal;
	}


	public double integrale()
	{
		double r=0;
		samplingx=((xmax-xmin)/(dimx()-1));
		samplingy=((ymax-ymin)/(dimy()-1));
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				r+=y[i][j];
			}
		return r*samplingx*samplingy;
	}





	//operation between 2 signals


	/** return a new signal with the sum with sig2 */
	public Signal2D1D add(Signal2D1D sig2)
	{
		Signal2D1D newsig=new Signal2D1D();
		newsig.copy(this);
		newsig._add(sig2);
		return newsig;
	}

	/** return a new signal with the sum with sig2 */
	public void _add(Signal2D1D sig2)
	{
		Signal2D1D sig1=this;
		if ((sig1.dimx()!=sig2.dimx())||(sig1.dimy()!=sig2.dimy())) 
		{
			Messager.messErr("Trying to add 2 Signal2D1D of different w or h");
		}
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,getValue(i,j)+sig2.getValue(i,j));
			}
	}

	public void _divide(Signal2D1D sig2)
	{
		double v2;
		Signal2D1D sig1=this;
		if ((sig1.dimx()!=sig2.dimx())||(sig1.dimy()!=sig2.dimy())) 
		{
			Messager.messErr("Trying to divide 2 Signal2D1D of different w or h (1:"
					+sig1.dimx()+"x"+sig1.dimy()
					+" 2:"+sig2.dimx()+"x"+sig2.dimy()
					+")"
					);
		}
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				v2=sig2.getValue(i,j);
				if (v2!=0)  setValue(i,j,getValue(i,j)/v2);else setValue(i,j,0);
			}
	}


	//	/**divide by  the signal s2 pixel by pixel (must have the same size)*/
	//	public void _div(Signal2D1D s2) 
	//	{
	//		double v2;
	//		for (int i=0;i<dimx();i++)
	//			for (int j=0;j<dimy();j++)
	//			{
	//				v2=s2.getValue(i,j);
	//				if (v2!=0) setValue(i,j,getValue(i,j)/v2);else setValue(i,j,0);
	//			}
	//	}


	/** return a new signal with the difference with this-sig2 */
	public Signal2D1D sub(Signal2D1D sig2)
	{
		Signal2D1D sig1=this;
		if ((sig1.dimx()!=sig2.dimx())||(sig1.dimy()!=sig2.dimy())) return this;
		Signal2D1D newsig=new Signal2D1D();

		newsig.y= new double[sig1.dimx()][sig1.dimy()];
		//newsig.dimx=sig1.dimx();
		//newsig.dimy=sig1.dimy();
		newsig.xmin=sig1.xmin();
		newsig.xmax=sig1.xmax();
		newsig.ymin=sig1.ymin();
		newsig.ymax=sig1.ymax();

		for (int i=0;i<sig1.dimx();i++)
			for (int j=0;j<sig1.dimy();j++)
			{
				newsig.setValue(i,j,sig1.getValue(i,j)-sig2.getValue(i,j));
			}
		return newsig;
	}

	/** return a new signal with the product with sig2 */
	public Signal2D1D multiply(Signal2D1D sig2)
	{
		Signal2D1D sig1=this;
		if ((sig1.dimx()!=sig2.dimx())||(sig1.dimy()!=sig2.dimy())) return this;
		Signal2D1D newsig=new Signal2D1D();

		newsig.y= new double[sig1.dimx()][sig1.dimy()];
		//newsig.dimx=sig1.dimx();
		//newsig.dimy=sig1.dimy();
		newsig.xmin=sig1.xmin();
		newsig.xmax=sig1.xmax();
		newsig.ymin=sig1.ymin();
		newsig.ymax=sig1.ymax();

		for (int i=0;i<sig1.dimx();i++)
			for (int j=0;j<sig1.dimy();j++)
			{
				newsig.setValue(i,j,sig1.getValue(i,j)*sig2.getValue(i,j));
			}
		return newsig;
	}


	/** replace all the values by their absolute value
@return this
	 */
	public Signal2D1D _abs()
	{
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,Math.abs(getValue(i,j)));
			}
		return this;
	}



	/**
 multiply by a value
@param val the value to multiply
@return this
	 */
	public Signal2D1D _multiply(double val)
	{
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,getValue(i,j)*val);
			}
		return this;
	}



	public Signal2D1D _multiplyWithSaturation(double val,double minValue,double maxValue)
	{
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,Math.max(Math.min(getValue(i,j)*val,maxValue),minValue));
			}
		return this;
	}


	public Signal2D1D _addWithSaturation(double val,double minValue,double maxValue)
	{
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,Math.max(Math.min(getValue(i,j)+val,maxValue),minValue));
			}
		return this;
	}




	/**return the mean value calculated over all area*/
	public double mean()
	{
		return mean(0,0,dimx()-1,dimy()-1);
	}

	/**
return the standard deviation  grey level
	 */
	public  double rms(double mean){return sigma(mean);}


	/**
return the standard deviation  grey level
	 */
	public  double sigma(double mean)
	{
		double s=0;
		int n=dimx()*dimy();;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				double v=z(i,j)-mean;
				s+=v*v;
			}
		return Math.sqrt(s/n);
	}




	/**return the autocorrelation at the point i0,j0*/
	public double autocor(int i0,int j0)
	{
		double somme=0;
		int dimx=dimx();
		int dimy=dimy();
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				if ((((i-i0)>0)&&((i-i0)<dimx))&&(((j-j0)>0)&&((j-j0)<dimy)))
					somme+=getValue(i,j)*getValue(i-i0,j-j0);
			}
		//System.out.println("autocorel:"+i0+" "+j0+"="+somme);
		return somme;
	}

	/**return the intercorrelation  between this and signal2 at the point i0,j0*/
	public double intercor(Signal2D1D signal2,int i0,int j0)
	{
		double somme=0;
		int dimx2=signal2.dimx();
		int dimy2=signal2.dimy();
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				if ((((i-i0)>0)&&((i-i0)<dimx2))&&(((j-j0)>0)&&((j-j0)<dimy2)))
					somme+=getValue(i,j)*signal2.getValue(i-i0,j-j0);
			}
		//System.out.println("intercorel:"+i0+" "+j0+"="+somme);
		return somme;
	}



	/**return the mean value calculated over the indexes from i1 to i2 included and j1 to j2 included*/
	public double mean(int imin,int jmin,int imax,int jmax)
	{
		double r=0;
		for (int i=imin;i<=imax;i++)
			for (int j=jmin;j<=jmax;j++) r+=y[i][j];
		double nb=(imax-imin+1)*(jmax-jmin+1);
		return r/nb;
	}


	/**
normalise the signal such that the min is at zero and the max at 1
	 */
	public void _normalise()
	{
		double min=minValue();
		double max=maxValue();
		System.out.println("min="+min+" max="+max);
		double amp=max-min;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				double v=getValue(i,j);
				setValue(i,j,(v-min)/amp);
			}
	}


	/**return the distance D2 between the 2 signals (square root of the sum of pixel to pixel difference)*/
	public double distanceD2(Signal2D1D signal2)
	{
		double somme=0;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				double diff=getValue(i,j)-signal2.getValue(i,j);
				somme+=diff*diff;
			}
		return Math.sqrt(somme);
	}

	/**return the distance D1 between the 2 signals (sum of absolute value of pixel to pixel difference)*/
	public double distanceD1(Signal2D1D signal2)
	{
		double somme=0;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				double diff=getValue(i,j)-signal2.getValue(i,j);
				somme+=Math.abs(diff);
			}
		return (somme);
	}


	public String toString()
	{
		StringBuffer sb=new StringBuffer();
		double sx=samplingx();
		double sy=samplingy();
		sb.append("//image "+dimx()+"x"+dimy()+"\n");
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				double xx=xmin+i*sx;
				double yy=ymin+j*sy;
				sb.append(xx+"\t"+yy+"\t"+y[i][j]+"\n");
			}
		return sb.toString();
	}

	/**
transform this signal into an image. put log=true if log scale must be applied.
	 */
	public Image toImage(PaletteInterface palette,boolean autoscale,boolean log)
	{
		Signal2D1D copy=copy();;
		if (log)
		{
			copy._log(10);
		}

		int dimx=copy.dimx();
		int dimy=copy.dimy();

		int[] pix=new int[dimx*dimy];

		if (palette==null) return null;

		if (autoscale)
		{
			palette.setVmin((float)copy.minValue());
			palette.setVmax((float)copy.maxValue());
		}

		for (int i = 0; i < dimx; i++)
			for (int j = 0; j < dimy; j++)
			{
				float val = (float)copy.getValue(i,j);
				pix[dimx*(dimy-1-j)+i] = palette.getRGB(val);
			}

		Image img = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(dimx,dimy,pix,0,dimx));
		return img;

	}

	/**
	 * save to a png file after normalisation
	 * @param path
	 * @param filename
	 */
	public void saveAsPNG(String path,String filename)
	{
		BufferedImage bim=this.toImage(new PalettePanel(PalettePanel.GRAY_SCALE), true, 0, this.zmax(),false);
		CImage cim1=new CImage(bim);
		cim1.saveImage(path,filename);
	}
	
	/**
	 * transform this signal into an image. put log=true if log scale must be applied.
	 * @param palette
	 * @param fixedLimitsZ
	 * @param zmin
	 * @param zmax
	 * @param logz
	 * @return
	 */
	public BufferedImage toImage(PaletteInterface palette,boolean fixedLimitsZ,double zmin,double zmax,boolean logz)
	{
		//af("logz="+logz+" fixedLimitsZ="+fixedLimitsZ);
		Signal2D1D copy=copy();;
		if (logz)
		{
			copy._log(10);
		}
		int dimx=copy.dimx();
		int dimy=copy.dimy();
		int[] pix=new int[dimx*dimy];
		if (palette==null) return null;
		if (!fixedLimitsZ)
		{
			double _zmin=copy.minValue();
			double _zmax=copy.maxValue();
			palette.setVmin((float)_zmin);
			palette.setVmax((float)_zmax);
		}
		else
		{
			if (logz)
			{
				palette.setVmin((float)Math.log10(zmin));
				palette.setVmax((float)Math.log10(zmax));
			}
			else
			{
				palette.setVmin((float)(zmin));
				palette.setVmax((float)(zmax));		
			}
		}
		for (int i = 0; i < dimx; i++)
			for (int j = 0; j < dimy; j++)
			{
				float val = (float)copy.getValue(i,j);
				pix[dimx*(dimy-1-j)+i] = palette.getRGB(val);
				//af("val="+val+" "+"pix="+pix[dimx*(dimy-1-j)+i]);
			}


		BufferedImage bim=new BufferedImage(dimx,dimy,BufferedImage.TYPE_INT_RGB);
		bim.setRGB(0, 0,dimx,dimy, pix, 0, dimx);
		//try{
		//ImageIO.write(bim, "PNG", new File(newDir.getAbsolutePath()+sep+imageFileName.replaceAll("jpg", "png"))); }
		//catch (Exception e){e.printStackTrace();}

		//Image img = Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(dimx,dimy,pix,0,dimx));
		return bim;
	}

	public SignalDiscret1D1D getColumn(double x)
	{
		int col=getSampleIndexX(x);
		if ((col<0)||(col>dimx()-1)) return null;
		return getColumn(col);
	}

	public SignalDiscret1D1D getColumn(int col)
	{
		SignalDiscret1D1D signal=new SignalDiscret1D1D(ymin(),ymax(),dimy());
		for (int j=0;j<signal.getDim();j++) signal.setValue(j,getValue(col,j));
		return signal;
	}

	public SignalDiscret1D1D getRow(int row)
	{
		SignalDiscret1D1D signal=new SignalDiscret1D1D(ymin(),ymax(),dimx());
		for (int j=0;j<signal.getDim();j++) signal.setValue(j,getValue(j,row));
		return signal;
	}


	public SignalDiscret1D1D getRow(double y)
	{
		int col=getSampleIndexY(y);
		return getRow(col);
	}


	/**return itself*/
	public Signal2D1D getSignal2D1D()
	{
		return this;
	}

	public void _annulate()
	{
		init( xmin(), xmax(), ymin(), ymax(), dimx(), dimy());
	}


	public  void removeAllPoints() {_annulate();}

	/** 
	 * fft2D using paul brouke code
	 * (return a new Signal2D1)
	 * */

	public Signal2D1D  fft2d()
	{
		//System.out.println("dimx="+dimx());
		//System.out.println("dimy="+dimy());
		int newdimx=(int)Math.pow(2,(int)(Math.ceil(Math.log(dimx())/Math.log(2))));
		int newdimy=(int)Math.pow(2,(int)(Math.ceil(Math.log(dimy())/Math.log(2))));
		//System.out.println("newdimx="+newdimx);
		//System.out.println("newdimy="+newdimy);

		//adapt for fourier transform, leave the unused pixels to 0
		Complex[][] co=new Complex[newdimx][newdimy];
		for (int i=0;i<newdimx;i++)//
			for (int j=0;j<newdimy;j++)//
			{
				if ((i<dimx())&&(j<dimy())) co[i][j]=new Complex(this.getValue(i, j),0);
				else co[i][j]=new Complex(0,0);
			}

		//do the FFT:
		FFT.FFT2D(co, 1);

		//see https://www.youtube.com/watch?time_continue=203&v=z7X6jgFnB6Y
		//intervalls of sampling:
		double deltax=(xmax()-xmin())/(dimx());
		double deltay=(ymax()-ymin())/(dimy());
		//sampling frequencies
		double fx=1/deltax;
		double fy=1/deltay;
		//Nyquist frequency:
		double fmax_x=fx/2;
		double fmax_y=fy/2;


		Signal2D1D fft=new Signal2D1D(-fmax_x,fmax_x,-fmax_y,fmax_y,newdimx,newdimy);

		//rearrange the array to get the (,0,0) frequency at the middle:
		int ii,jj;
		for (int i=0;i<newdimx;i++)//
			for (int j=0;j<newdimy;j++)//
			{
				ii=i-newdimx/2;
				jj=j-newdimy/2;
				if (ii<0) ii+=newdimx;
				if (jj<0) jj+=newdimy;
				fft.setValue(i, j, co[ii][jj].norm());
			}
		return fft;
	}


	public Signal2D1D fft()
	{
		System.out.println("dimx="+dimx());
		System.out.println("dimy="+dimy());
		double deltax=(xmax()-xmin())/(dimx()-1);
		double deltay=(ymax()-ymin())/(dimy()-1);
		int newdimx=(int)Math.pow(2,(int)(Math.ceil(Math.log(dimx())/Math.log(2))));
		int newdimy=(int)Math.pow(2,(int)(Math.ceil(Math.log(dimy())/Math.log(2))));
		double newxmin=0;
		double newxmax=deltax*newdimx;
		double newymin=0;
		double newymax=deltay*newdimy;
		System.out.println("newdimx="+newdimx);
		System.out.println("newdimy="+newdimy);
		Signal2D1D fft=new Signal2D1D(newxmin,newxmax,newymin,newymax,newdimx,newdimy);
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
				fft.setValue(i, j, this.z(i, j));
		//the rest are zeros...
		fft._fft();
		return fft;
	}


	/**
give a new signal with the 2D fourier transform.
Only valid for dimx and dimy power of 2
	 */
	public void _fft()
	{
		double deltax=(xmax()-xmin())/(dimx()-1);
		double deltay=(ymax()-ymin())/(dimy()-1);
		double fmx=1/(2*deltax);
		double fmy=1/(2*deltay);
		//Signal2D1D f=new Signal2D1D(-fmx,fmx,-fmy,fmy,dimx(),dimy());
		float[] data=new float[2*dimx()*dimy()+1];
		int ndim=2;
		int[] nn=new int[ndim+1];
		nn[1]=dimx();
		nn[2]=dimy();
		int isign=1;

		for (int i = 0; i < dimx() ; i++)
			for (int j = 0; j < dimy() ; j++)
			{
				data[1+2*(j*dimx()+i)]=(float)getValue(i,j);
				data[1+2*(j*dimx()+i)+1]=0;
			}
		fourn(data,nn,ndim,isign);
		for (int i = 0; i < dimx() ; i++)
			for (int j = 0; j < dimy() ; j++)
			{
				double real=data[1+2*(j*dimx()+i)];
				double im=data[1+2*(j*dimx()+i)+1];
				double n=Math.sqrt(Math.pow(real,2)+Math.pow(im,2));
				setValue(i,j,n);
			}
		xmin=-fmx;
		xmax=fmx;
		ymin=-fmy;
		ymax=fmy;
	}


	//http://www.brainflux.org/java/classes/FFT2DApplet.html
	// http://www.ulib.org/webRoot/Books/Numerical_Recipes/bookcpdf/c12-4.pdf
	void fft2d_A(float data[], int ndim, int isign) {
		//System.out.println("Doing FFT");
		int idim;
		int i1, i2, i3, i2rev, i3rev, ip1, ip2, ip3, ifp1, ifp2;
		int ibit, k1, k2, n, nprev, nrem, ntot;
		float tempi, tempr, temp;
		double theta, wi, wpi, wpr, wr, wtemp;
		int SIZE=(int)Math.pow(2, 8);
		int SIZEsqrd=SIZE*SIZE;
		ntot = SIZEsqrd;
		nprev=1;
		for (idim=ndim; idim>=1; idim--) {
			n=SIZE;// SIZE = 2^POW
			nrem=ntot/(n*nprev);
			ip1=nprev<<1;
			ip2=ip1*n;
			ip3=ip2*nrem;
			i2rev=1;
			for (i2=1; i2<=ip2; i2+=ip1) {  // Bit reversal part
				if (i2 < i2rev) {
					for (i1=i2; i1<=i2+ip1-2; i1+=2) {
						for (i3=i1; i3<=ip3; i3+=ip2) {
							i3rev=i2rev+i3-i2;
							temp=data[i3-1]; data[i3-1]=data[i3rev-1]; data[i3rev-1]=temp;
							temp=data[i3]; data[i3]=data[i3rev]; data[i3rev]=temp;
						}
					}
				}
				ibit=ip2 >> 1;
						while (ibit >= ip1 && i2rev > ibit) {
							i2rev -= ibit;
							ibit >>= 1;
						}
						i2rev += ibit;
			}
			ifp1=ip1;  // Here begins the real fft code.
			while (ifp1 < ip2) {
				ifp2=ifp1<<1;
				theta=isign*6.28318530717959/(ifp2/ip1);
				wtemp=Math.sin(0.5*theta);
				wpr = -2.0*wtemp*wtemp;
				wpi=Math.sin(theta);
				wr=1.0;
				wi=0.0;
				for (i3=1; i3<=ifp1; i3+=ip1) {
					for (i1=i3; i1<=i3+ip1-2; i1+=2) {
						for (i2=i1; i2<=ip3; i2+=ifp2) {
							k1=i2;
							k2=k1+ifp1;
							//System.out.println("k2="+k2);
							tempr=(float)wr*data[k2-1]-(float)wi*data[k2];
							tempi=(float)wr*data[k2]+(float)wi*data[k2-1];
							data[k2-1]=data[k1-1]-tempr;
							data[k2]=data[k1]-tempi;
							data[k1-1] += tempr;
							data[k1] += tempi;
						}
					}
					wr=(wtemp=wr)*wpr-wi*wpi+wr;
					wi=wi*wpr+wtemp*wpi+wi;
				}
				ifp1=ifp2;
			}
			nprev *= n;
		}
		// Rescale data back down.
		for (int off=0; off<SIZEsqrd<<1; off++) {
			data[off] /= (float)SIZE;
		}
	}


	//http://perso.telecom-paristech.fr/~tupin/ANIM/PROJ_SSBDES/util_maths.c
	/**
	 * 	
	 * @param inverse
	 * @return
	 */
	public Signal2D1D  _fft2d_B()
	{
		float []tab_in;
		float []tab_out;
		int ind_col,ind_lig;
		int inverse=1;

		System.out.println("dimx="+dimx());
		System.out.println("dimy="+dimy());
		double deltax=(xmax()-xmin())/(dimx()-1);
		double deltay=(ymax()-ymin())/(dimy()-1);
		int newdimx=(int)Math.pow(2,(int)(Math.ceil(Math.log(dimx())/Math.log(2))));
		int newdimy=(int)Math.pow(2,(int)(Math.ceil(Math.log(dimy())/Math.log(2))));
		double newxmin=0;
		double newxmax=deltax*newdimx;
		double newymin=0;
		double newymax=deltay*newdimy;
		System.out.println("newdimx="+newdimx);
		System.out.println("newdimy="+newdimy);

		float[][] image=new float[newdimy][newdimx*2];
		float[][] spectre=new float[newdimy][newdimx*2];

		for (int c=0;c<dimx();c++)//col
			for (int r=0;r<dimy();r++)//row
			{
				image[r][2*c]=(float)this.getValue(c,r);//real part
				image[r][2*c+1]=(float)0;//imaginary part
			}


		//  tab_in=(float *)malloc(sizeof(float)*(2*nb_col*nb_lig));
		//  tab_out=(float *)malloc(sizeof(float)*(2*nb_col*nb_lig));

		tab_in=new float[2*newdimx*newdimy];
		tab_out=new float[2*newdimx*newdimy];

		for (ind_lig=0;ind_lig<newdimy;ind_lig ++)
		{
			for (ind_col=0;ind_col<newdimx;ind_col++)
			{
				//	  tab_in[2*ind_col+2*ind_lig*newdimx]=image[ind_lig][2*ind_col];
				//	  tab_in[2*ind_col+1+2*ind_lig*newdimx]=image[ind_lig][2*ind_col+1];
				tab_in[2*ind_col+2*ind_lig*newdimx]=image[ind_lig][2*ind_col];//real part
				tab_in[2*ind_col+1+2*ind_lig*newdimx]=0;//imaginary part
			}
		}

		xima_fft2d2(tab_in,tab_out,(int)inverse*newdimx,(int)inverse*newdimy);

		for (ind_lig=0;ind_lig<newdimy;ind_lig ++)
		{
			for (ind_col=0;ind_col<newdimx;ind_col++)
			{
				spectre[ind_lig][2*ind_col]=(float)(tab_out[2*ind_col+2*ind_lig*newdimx]/(Math.sqrt((double)1.0*newdimx*newdimy)));
				spectre[ind_lig][2*ind_col+1]=(float)(tab_out[2*ind_col+1+2*ind_lig*newdimx]/(Math.sqrt((double)1.0*newdimx*newdimy)));
			}
		}

		Signal2D1D fft=new Signal2D1D(newxmin,newxmax,newymin,newymax,newdimx,newdimy);
		float im,re;
		for (int c=0;c<newdimx;c++)//col
			for (int r=0;r<newdimy;r++)//row
			{
				re=spectre[r][c*2];
				im=spectre[r][c*2+1];
				fft.setValue(c,r,Math.sqrt(re*re+im*im));
			}
		return fft;

		//  free(tab_in);
		//  free(tab_out);
	}



	private void  xima_fft2d2( float[] tab_in, float [] four_tab,int nnxp,int nnyp)
	//float *tab_in;
	//float *four_tab;
	//int nnxp, nnyp;
	{
		int nnx, nny, nn0, nn2x, nn2y, ix, iy, isignx, isigny;
		float []four_workx;
		float  []four_worky;


		if(nnxp >0) {
			isignx = 1;
			nnx = nnxp;
		}
		else {
			isignx = -1;
			nnx = -nnxp;
		}


		if(nnyp >0) {
			isigny = 1;
			nny = nnyp;
		}
		else {
			isigny = -1;
			nny = -nnyp;
		}

		//four_workx = (float *) malloc(2*(nnx+1)*sizeof(float));
		//four_worky = (float *) malloc(2*(nny+1)*sizeof(float));

		four_workx=new float[2*(nnx+1)];
		four_worky=new float[2*(nnx+1)];

		nn2x = 2*nnx;
		nn2y = 2*nny;


		nn0 = 0;

		for(iy=0;iy<nny;iy++){

			for(ix=0;ix<nn2x;ix=ix+2)
			{
				//*(four_workx+ix+1) = *(tab_in+ix+nn0);
				four_workx[ix+1]=tab_in[ix+nn0];
				//*(four_workx+ix+2) = *(tab_in+ix+nn0+1);
				four_workx[ix+1]=tab_in[ix+nn0+1];
			}
			four1_numrec(four_workx, nnx, isignx);

			for(ix=0;ix<nn2x;ix=ix+2)
			{
				//*(four_tab+nn0+ix) = *(four_workx+ix+1);
				four_tab[nn0+ix]=four_workx[ix+1];
				//*(four_tab+nn0+ix+1) = *(four_workx+ix+2);		
				four_tab[nn0+ix+1]=four_workx[ix+2];
			}


			nn0 = nn0 + nn2x;
		}

		//printf("Boucle de FFT OK pour Ox\n");




		for(ix=0;ix<nnx;ix++){


			for(iy=0;iy<nny;iy++){
				//*(four_worky+2*iy+1) = *(four_tab+iy*nn2x+2*ix);
				four_worky[2*iy+1]=four_tab[iy*nn2x+2*ix];
				//*(four_worky+2*iy+2) = *(four_tab+iy*nn2x+2*ix+1);
				four_worky[2*iy+2]=four_tab[iy*nn2x+2*ix+1];
			}
			four1_numrec(four_worky, nny,  isigny);

			for(iy=0;iy<nny;iy++){
				//*(four_tab+iy*nn2x+2*ix) = *(four_worky+2*iy+1) ;
				four_tab[iy*nn2x+2*ix]=four_worky[2*iy+1];
				//*(four_tab+iy*nn2x+2*ix+1) = *(four_worky+2*iy+2) ;
				four_tab[iy*nn2x+2*ix+1]=four_worky[2*iy+2];
			}

		}


		System.out.println("Boucle de FFT OK pour Oy\n");

		//free(four_workx);
		//free(four_worky);



	}




	void four1_numrec(float[] data,int nn, int isign)
	//float data[];
	//long int nn;
	//int isign;
	{
		int  n,mmax,m,j,istep,i;
		double wtemp,wr,wpr,wpi,wi,theta;
		float tempr,tempi;

		n=2*nn;
		j=1;
		for (i=1;i<n;i+=2) {
			if (j > i) {
				SWAP(data[j],data[i]);
				SWAP(data[j+1],data[i+1]);
			}
			m=n >> 1;
			while (m >= 2 && j > m) {
				j -= m;
				m >>= 1;
			}
			j += m;
		}
		mmax=2;
		while (n > mmax) {
			istep=2*mmax;
			theta=6.28318530717959/(isign*mmax);
			wtemp=Math.sin(0.5*theta);
			wpr = -2.0*wtemp*wtemp;
			wpi=Math.sin(theta);
			wr=1.0;
			wi=0.0;
			for (m=1;m<mmax;m+=2) {
				for (i=m;i<=n;i+=istep) {
					j=i+mmax;
					tempr=(float)(wr*data[j]-wi*data[j+1]);
					tempi=(float)(wr*data[j+1]+wi*data[j]);
					data[j]=data[i]-tempr;
					data[j+1]=data[i+1]-tempi;
					data[i] += tempr;
					data[i+1] += tempi;
				}
				wr=(wtemp=wr)*wpr-wi*wpi+wr;
				wi=wi*wpr+wtemp*wpi+wi;
			}
			mmax=istep;
		}
	}




	/**
cf Numerical recipies (Cambridge)
Replaces data by its ndim-dimensional discrete Fourier transform, if isign is input as 1.
nn[1..ndim] is an integer array containing the lengths of each dimension (number of complex
values), which MUST all be powers of 2. 
data is a real array of length twice the product of these lengths, in which the data are stored 
as in a multidimensional complex array: real and imaginary parts of each element are in consecutive locations,
 and the rightmost index of the array increases most rapidly as one proceeds along data. 
For a two-dimensional array, this is equivalent to storing the array by rows. 
If isign is input as -1, data is replaced by its inverse
transform times the product of the lengths of all dimensions.
	 */
	private static void fourn(float data[],  int nn[], int ndim, int isign)
	{
		// #include <math.h>
		// #define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr
		// void fourn(float data[], unsigned long nn[], int ndim, int isign)



		int idim;
		int i1,i2,i3,i2rev,i3rev,ip1,ip2,ip3,ifp1,ifp2;
		int ibit,k1,k2,n,nprev,nrem,ntot;
		float tempi,tempr;
		//                                              Double precision for trigonometric recur-
		double theta,wi,wpi,wpr,wr,wtemp;
		//                                                  rences.
		//                                              Compute total number of complex val-
		for (ntot=1,idim=1;idim<=ndim;idim++)
			//                                                ues.
			ntot *= nn[idim];
		nprev=1;
		//                                              Main loop over the dimensions.
		for (idim=ndim;idim>=1;idim--) {
			n=nn[idim];
			nrem=ntot/(n*nprev);
			ip1=nprev << 1;
			ip2=ip1*n;
			ip3=ip2*nrem;
			i2rev=1;
			//                                              This is the bit-reversal section of the
			for (i2=1;i2<=ip2;i2+=ip1) {
				//                                                  routine.
				if (i2 < i2rev) {
					for (i1=i2;i1<=i2+ip1-2;i1+=2) {
						for (i3=i1;i3<=ip3;i3+=ip2) {
							i3rev=i2rev+i3-i2;
							SWAP(data[i3],data[i3rev]);
							SWAP(data[i3+1],data[i3rev+1]);
						}
					}
				}
				ibit=ip2 >> 1;
						while (ibit >= ip1 && i2rev > ibit) {
							i2rev -= ibit;
							ibit >>= 1;
						}
						i2rev += ibit;
			}
			//                                              Here begins the Danielson-Lanczos sec-
			ifp1=ip1;
			//                                                  tion of the routine.
			while (ifp1 < ip2) {
				ifp2=ifp1 << 1;
				//                                                           Initialize for the trig. recur-
				theta=isign*6.28318530717959/(ifp2/ip1);
				//                                                                rence.
				wtemp=Math.sin(0.5*theta);
				wpr = -2.0*wtemp*wtemp;
				wpi=Math.sin(theta);
				wr=1.0;
				wi=0.0;
				for (i3=1;i3<=ifp1;i3+=ip1) {
					for (i1=i3;i1<=i3+ip1-2;i1+=2) {
						for (i2=i1;i2<=ip3;i2+=ifp2) {
							//                                              Danielson-Lanczos formula:
							k1=i2;
							k2=k1+ifp1;
							tempr=(float)wr*data[k2]-(float)wi*data[k2+1];
							tempi=(float)wr*data[k2+1]+(float)wi*data[k2];
							data[k2]=data[k1]-tempr;
							data[k2+1]=data[k1+1]-tempi;
							data[k1] += tempr;
							data[k1+1] += tempi;
						}
					}
					wr=(wtemp=wr)*wpr-wi*wpi+wr; //Trigonometric recurrence.
					wi=wi*wpr+wtemp*wpi+wi;
				}
				ifp1=ifp2;
			}
			nprev *= n;
		}

	}

	private static void SWAP(float r1,float r2)
	{
		float temp=r1;
		r1=r2;
		r2=temp;
	}




	/**Apply a transfert function to each pixel*/
	public void applyATransfertFunction(Function1D1D f)
	{
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,f.f(getValue(i,j)));
			}
	}


	/**
	 * return a new smoothed image by averaging with a kernel of size kernelSize*kernelSize
	 * @param kernelSize
	 * @return
	 */
	public Signal2D1D smoothSquareKernel(int radius)
	{
		double r=0;
		int c=0;
		int w=dimx();
		int h=dimy();
		Signal2D1D s2=new Signal2D1D(xmin(),xmax(),ymin(),ymax(), dimx(), dimy());
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				r=0;
				c=0;
				for (int ii=-radius;ii<radius;ii++)
					for (int jj=-radius;jj<radius;jj++)
						if ( ((i+ii)>=0) && ((i+ii)<w) && ((j+jj)>=0) && ((j+jj)<h) )
						{
							r+=y[i+ii][j+jj];
							c++;
						}
				r/=c;
				s2.setValue(i, j, r);
			}
		return s2;
	}

	/**
	 * smooth this  image by averaging with a kernel of size kernelSize*kernelSize
	 * @param kernelSize
	 */
	public void  _smoothSquareKernel(int radius)
	{
		copy(smoothSquareKernel(radius));
	}

	/**
	 * calculate the spatial auto correlation of the  image
	 * @param deltaMax
	 * @param imCor
	 * @param profile
	 */
	public    Signal2D1D calcSpatialCorrelation(int deltaMax)
	{
		Signal2D1D imCor=new Signal2D1D();
		double sx=samplingx();
		double sy=samplingy();
		imCor.init(-deltaMax*sx,deltaMax*sx,-deltaMax*sy,deltaMax*sy,2*deltaMax+1,2*deltaMax+1);
		double mean=mean();
		double sigma=sigma(mean);
		for (int i=-deltaMax;i<=deltaMax;i++)
			for (int j=-deltaMax;j<=deltaMax;j++) 
			{
				//System.out.println("i="+i+" j="+j);
				double correlation=normalisedSpatialCorrelation(this,this,i,j,mean,sigma);
				imCor.setValue(i+deltaMax,j+deltaMax,correlation);
			}
		return imCor;
	}

	/**
return the correlation  between an im1 and im2 translated by dx,dy pixels
	 */
	public  static double normalisedSpatialCorrelation(Signal2D1D im1,Signal2D1D im2,int dx,int dy ,double mean,double sigma)
	{
		double s=0;
		int w=im1.dimx();
		int h=im1.dimy();
		int n=w*h;
		//int index=0;
		for (int j=0;j<im1.dimy();j++)
			for (int i=0;i<im2.dimx();i++)
			{
				double v1=im1.z(i, j)-mean;//     im1[index] -mean;
				double  v2=translated(im2,w,h,dx,dy,i,j)-mean;
				//System.out.println("i="+i+" j="+j+" dx="+dx+" dy="+dy+" v1="+v1+" v2="+v2);
				s+=v1*v2;
				//		index++;
			}
		return s/n/sigma/sigma;
	}


	/**
return the value of the image translated by dx,dy pixels
	 */
	private static double translated(Signal2D1D im,int w,int h,int dx,int dy,int i,int j)
	{
		int ii=i-dx;
		if (ii<0) ii=w+ii;
		if (ii>=w) ii=ii-w;
		int jj=j-dy;
		if (jj<0) jj=h+jj;
		if (jj>=h) jj=jj-h;
		return im.z(ii,jj);//im[jj*w+ii];
	}

	/**
A radial profile around a center of a 2D1D signal.Only the biggest
circle inserted in the signal is counted</p>
	 */

	public  Signal1D1DXY radialIntegrator(double spotCentrex,double spotCentrey, int dimProfile,double rmin)
	{
		Signal2D1D im=this;
		Signal1D1DXY profil=new Signal1D1DXY();

		//compute the radius of the biggest inserted circle
		double rx=Math.min(spotCentrex-im.xmin(),im.xmax()-spotCentrex);
		double ry=Math.min(spotCentrey-im.ymin(),im.ymax()-spotCentrey);
		double rmax=Math.min(rx,ry);

		//af("spotCentrex"+spotCentrex);
		//af("spotCentrey"+spotCentrey);
		//af("im.xmax()"+im.xmax());
		//af("im.ymax()="+im.ymax());
		//af("rx="+rx);
		//af("ry="+ry);
		//af("rmax="+rmax);

		//create the empty profile
		profil.init(dimProfile);
		//start the profile calculation
		double profileSampling=(rmax-rmin)/(dimProfile-1);
		double sampx=im.samplingx();
		double sampy=im.samplingy();
		double xx,yy,d;
		int cell=-1;
		double[] compteur=new double[dimProfile];
		for (int i=0;i<im.dimx();i++)//scan all the pixels
			for (int j=0;j<im.dimy();j++)
			{
				xx=im.xmin()+i*sampx-spotCentrex;//co-ordinate of the current pixel wrt image centre
				yy=im.ymin()+j*sampy-spotCentrey;
				d=Math.sqrt(xx*xx+yy*yy);//distance of current pixel from the centre
				if ((d<rmax)&&(d>rmin))//check if pixel is the good ring sector
				{
					//calculate the profile cell corresponding to the current pixel
					cell=(int)((d-rmin)/(rmax-rmin)*dimProfile);
					//increment the profile cell value with pixel value
					profil.setValue(cell,profil.value(cell)+im.getValue(i,j));
					compteur[cell]++;//count the pixel added to this cell
					//af("xx="+xx+" yy="+yy);
					//af(" d="+d+" cell="+cell+" valim="+im.getValue(i,j));
				}

				//else im.setValue(i,j,0);//the pixels out of the ring sector are put to 0
				//else System.out.println();
			}

		//now the mean value are calculated thanks to compteur values
		for (int i=0;i<dimProfile;i++)
		{
			double yval;
			if (compteur[i]!=0) yval=profil.value(i)/compteur[i];else yval=0;
			profil.setValue(i,(rmin+profileSampling*i),yval);
		}

		//af(profil);
		//System.out.println(this);
		return profil;
	}

	/**invert the constrast (min becomes max and max becomes min*/
	public Signal2D1D _invertContrast() 
	{
		double min=this.minValue();
		double max=this.maxValue();
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,min+max-getValue(i,j));
			}
		return this;

	}

	/**
	 * translate all points by (dx,dy,dy)
	 * @param dx
	 * @param dy
	 * @param dz
	 */
	public void _translate(double dx, double dy, double dz) 
	{
		xmin+=dx;
		xmax+=dx;
		ymin+=dy;
		ymax+=dy;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,getValue(i,j)+dz);
			}
	}

	/**
	 * translate the pixels of the image with the vector (di,dj)
	 * @param di
	 * @param dj
	 */
	public void _translate(int di,int dj)
	{
		int w=dimx();
		int h=dimy();
		Signal2D1D s2=new Signal2D1D(xmin,xmax,ymin,ymax,w,h);
		int i2,j2;
		for (int i=0;i<w;i++) 
			for (int j=0;j<h;j++) 
			{
				i2=i+di;
				j2=j+dj;
				if ((i2<0)||(j2<0)||(i2>=w)||(j2>=h)) continue;
				s2.setValue(i2, j2, this.getValue(i, j));
			}
		this.copy(s2);
	}

	public void _setXYBoudaries(double xmin, double xmax, double ymin, double ymax) 
	{
		this.xmin=xmin;	
		this.xmax=xmax;	
		this.ymin=ymin;	
		this.ymax=ymax;	
		samplingx=((xmax-xmin)/(dimx()-1));
		samplingy=((ymax-ymin)/(dimy()-1));
	}



	/**
	 * create a SIgnal2D1D and fill it with an image 
	 * @param im
	 * @param channel can be R G B alpha H S Br
	 * @param scale  scale factor in unit/pixel
	 * @return
	 */
	public static Signal2D1D  getSignal2D1DFromImage(Image im,String channel,double scale)
	{
		int c=0;
		if (channel.compareTo("R")==0) c=0;
		if (channel.compareTo("G")==0) c=1;
		if (channel.compareTo("B")==0) c=2;
		if (channel.compareTo("alpha")==0) c=3;
		if (channel.compareTo("H")==0) c=4;
		if (channel.compareTo("S")==0) c=5;
		if (channel.compareTo("Br")==0) c=6;
		int[] dimension=new int[2];

		int[] pix=ImageUtils.getRawPixelsFromImage(im,dimension);
		int w=dimension[0];
		int h=dimension[1];
		//double _scale=1;
		Signal2D1D signal=new Signal2D1D(0, w*scale, 0,h*scale, w, h);
		for (int i = 0; i < w; i++)
			for (int j = 0; j <h; j++)
			{
				double val=0;
				switch (c)
				{
				case 0:
					val=ImageUtils.R(pix[w*(h-1-j)+i]);
					break;
				case 1:
					val=ImageUtils.G(pix[w*(h-1-j)+i]);
					break;
				case 2:
					val=ImageUtils.B(pix[w*(h-1-j)+i]);
					break;
				case 3:
					val=ImageUtils.alpha(pix[w*(h-1-j)+i]);
					break;
				case 4:
					val=ImageUtils.H(pix[w*(h-1-j)+i]);
					break;
				case 5:
					val=ImageUtils.S(pix[w*(h-1-j)+i]);
					break;
				case 6:
					val=ImageUtils.Br(pix[w*(h-1-j)+i]);
				}
				//System.out.println(val);
				signal.setValue(i,j,val);
			}
		return signal;
	}


	/**add the signal s2 (must have the same size)*/
	public void _subtract(Signal2D1D s2) 
	{
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,getValue(i,j)-s2.getValue(i,j));
			}


	}


	public static Signal2D1D getSignal2D1DFromImage(CImage cim)
	{
		Signal2D1D signal2D1D=new Signal2D1D();
		signal2D1D.loadFromImage(cim);
		return signal2D1D;
	}

	public void loadFromImage(CImage cim)
	{
		int w=cim.w();
		int h=cim.h();
		init(w,h);
		int[] pix=cim.getRGBImage();
		for (int i = 0; i < w; i++)
			for (int j = 0; j <h; j++)
			{
				int index=i+w*j;
				Color c=new Color(pix[index]);
				double val=(c.getBlue()+c.getGreen()+c.getRed())/3.0;
				setValue(i,j,val);
			}
	}

	/**
	 * flip around X axis
	 */
	public void _flipX()
	{
		Signal2D1D newImage=this.copy();
		int h=dimy();
		for (int i = 0; i < dimx(); i++)
			for (int j = 0; j <dimy(); j++)
			{
				setValue(i,h-1-j,newImage.getValue(i,j));
			}
	}

	/**
	 * flip around Y axis
	 */
	public void _flipY()
	{
		Signal2D1D newImage=this.copy();
		int w=dimx();
		for (int i = 0; i < dimx(); i++)
			for (int j = 0; j <dimy(); j++)
			{
				setValue(w-1-i,j,newImage.getValue(i,j));
			}
	}

	/**
	 * rotate the image around the centre
	 * @param a
	 */
	//public void _rotate(double angle)
	//{
	//Signal2D1D newImage=this.copy();
	//int w=dimx();
	//int h=dimy();
	//double a,r,d;
	//int di,dj,ii,jj;
	//for (int i = 0; i < dimx(); i++)
	//	for (int j = 0; j <dimy(); j++)
	//		{
	//		//turn around image center
	//		di=i-w/2;
	//		dj=j-h/2;
	//		a=Math.atan2(dj,di);
	//		r=Math.sqrt(di*di+dj*dj);
	//		ii=w/2+(int)(r*Math.cos(a-angle));
	//		jj=h/2+(int)(r*Math.sin(a-angle));
	//		if ((ii<0)||(ii>=dimx())) d= 0;
	//		else if ((jj<0)||(jj>=dimy())) d= 0;
	//		else d=newImage.getValue(ii,jj);
	//		setValue(i,j,d);
	//		}
	//
	//}

	public void multiplyWithFonction(Function2D1D f)
	{
		double sx=(xmax()-xmin())/(dimx()-1);
		double sy=(ymax()-ymin())/(dimy()-1);
		double[] p=new double[2];
		for (int i = 0; i < dimx(); i++)
			for (int j = 0; j <dimy(); j++)
			{
				p[0]=xmin+i*sx;
				p[1]=ymin+j*sy;
				setValue(i,j,getValue(i,j)*f.f(p));
			}

	}

	/**
	 * return the center of a spot, for image having a single white spot
	 * @return
	 */
	public double[] getSpotCenter()
	{
		double[] center=new double[2];
		SpotCentreDetector spotCentreDetector=new SpotCentreDetector();
		spotCentreDetector.setImSource(this);
		spotCentreDetector.setThreshold(0.9);
		spotCentreDetector.calculate();
		center[0]=spotCentreDetector.getSpotCentrex();
		center[1]=spotCentreDetector.getSpotCentrey();
		return center;
	}


	/**
	 * get the profile of a spot . calculate automatically the center
	 * @return
	 */
	public Signal1D1DXY getProfile()
	{
		double[] center=new double[2];
		//detect the center:
		SpotCentreDetector spotCentreDetector=new SpotCentreDetector();
		spotCentreDetector.setImSource(this);
		spotCentreDetector.setThreshold(0.9);
		spotCentreDetector.calculate();
		center[0]=spotCentreDetector.getSpotCentrex();
		center[1]=spotCentreDetector.getSpotCentrey();
		return getProfile(center);
	}


	/**
	 * get the profile of a spot giving its center
	 * @return
	 */
	public Signal1D1DXY getProfile(double[] center)
	{
		return getProfile(center,this.xmax()-this.xmin());
	}

	/**
	 * get the profile of a spot giving its center and max radius
	 * @return
	 */

	public Signal1D1DXY getProfile(double[] center,double rmax)
	{
		return this.getProfile(center,rmax,Math.max(this.dimx(),this.dimy()));
	}


	/**
	 * get the profile of a spot
	 * 
	 * @param center output:allocated array to place the center location
	 * @return
	 */
	public Signal1D1DXY getProfile(double[] center,int dimProfile)
	{
		//SpotCentreDetector spotCentreDetector=new SpotCentreDetector();
		RadialIntegrator radialIntegrator=new RadialIntegrator();

		//spotCentreDetector.setImSource(this);
		//spotCentreDetector.setThreshold(0.9);
		//spotCentreDetector.calculate();
		//center[0]=spotCentreDetector.getSpotCentrex();
		//center[1]=spotCentreDetector.getSpotCentrey();

		Signal1D1DXY signalProfile=new Signal1D1DXY();

		double pixel=Math.max(this.samplingx(),this.samplingy());
		double rmax_x=Math.min(this.xmax()-center[0],center[0]-this.xmin());
		double rmax_y=Math.min(this.ymax()-center[1],center[1]-this.ymin());
		double rmax=Math.min(rmax_x,rmax_y);

		radialIntegrator.setIm(this);
		radialIntegrator.setProfile(signalProfile);
		radialIntegrator.setDimProfile(dimProfile);
		radialIntegrator.setRmin(2*pixel);
		radialIntegrator.setRmax(rmax);
		radialIntegrator.setSpotCentrex(center[0]);
		radialIntegrator.setSpotCentrey(center[1]);
		radialIntegrator.calculate();	
		return signalProfile;
	}


	/**
	 * get the profile of a spot
	 * 
	 * @param center allocated array to place the center location
	 * @return
	 */
	public Signal1D1DXY getProfile(double[] center,double rmax,int dimProfile)
	{
		//SpotCentreDetector spotCentreDetector=new SpotCentreDetector();
		RadialIntegrator radialIntegrator=new RadialIntegrator();

		//spotCentreDetector.setImSource(this);
		//spotCentreDetector.setThreshold(0.9);
		//spotCentreDetector.calculate();
		//center[0]=spotCentreDetector.getSpotCentrex();
		//center[1]=spotCentreDetector.getSpotCentrey();

		Signal1D1DXY signalProfile=new Signal1D1DXY();

		double pixel=Math.max(this.samplingx(),this.samplingy());
		radialIntegrator.setIm(this);
		radialIntegrator.setProfile(signalProfile);
		radialIntegrator.setDimProfile(dimProfile);
		radialIntegrator.setRmin(pixel);
		radialIntegrator.setRmax(rmax);
		radialIntegrator.setSpotCentrex(center[0]);
		radialIntegrator.setSpotCentrey(center[1]);
		radialIntegrator.calculate();	
		return signalProfile;
	}

	public void _square()
	{
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				setValue(i,j,Math.pow(getValue(i,j),2));
			}
	}

	public void _tiltx(double tiltx)
	{
		double sy=(ymax()-ymin())/(dimy()-1);
		double y;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				y=ymin+j*sy;
				setValue(i,j,getValue(i,j)+y*tiltx);
			}
	}

	public void _tilty(double tilty)
	{
		double sx=(xmax()-xmin())/(dimx()-1);
		double x;
		for (int i=0;i<dimx();i++)
			for (int j=0;j<dimy();j++)
			{
				x=xmin+i*sx;
				setValue(i,j,getValue(i,j)+x*tilty);
			}
	}

	public void _offset(int dx, int dy)
	{
		Signal2D1D s2=new Signal2D1D(dimx(),dimy());
		int x1,y1;
		for (int x=0;x<dimx();x++)
			for (int y=0;y<dimy();y++)
			{
				x1=x+dx;
				y1=y+dy;
				if((x1>=0)&&(x1<this.dimx())&&(y1>=0)&&(y1<this.dimy()))
					s2.setValue(x1,y1, this.getValue(x,y));
			}
		this.copy(s2);
	}



	/**
	 * rotation around center of image
	 * @param da in rad
	 */
	public void _rotateAroundCenter(double da)
	{
		Signal2D1D s2=new Signal2D1D(dimx(),dimy());
		double alpha,r,alpha1;
		int x1,y1,x2,y2;
		int w=dimx();
		int h=dimy();
		for (int x=0;x<dimx();x++)
			for (int y=0;y<dimy();y++)
			{
				x1=x-w/2;
				y1=y-h/2;
				alpha=Math.atan2(y1,x1);
				r=Math.sqrt(x1*x1+y1*y1);
				alpha1=alpha+da;
				x2=(int)(r*Math.cos(alpha1))+w/2;
				y2=(int)(r*Math.sin(alpha1))+h/2;
				if((x2>=0)&&(x2<this.dimx())&&(y2>=0)&&(y2<this.dimy()))
					s2.setValue(x,y, this.getValue(x2,y2));
			}
		this.copy(s2);
	}

	public void _rotate90Right()
	{
		Signal2D1D s2=new Signal2D1D(ymin(),ymax(),xmin(),xmax(),dimy(),dimx());
		int w=dimx();
		int h=dimy();
		for (int x=0;x<w;x++)
			for (int y=0;y<h;y++)
			{
				s2.setValue(h-1-y,x, this.getValue(x,y));
			}
		this.copy(s2);
	}
	public void _rotate90Left()
	{
		Signal2D1D s2=new Signal2D1D(ymin(),ymax(),xmin(),xmax(),dimy(),dimx());
		int w=dimx();
		int h=dimy();
		for (int x=0;x<w;x++)
			for (int y=0;y<h;y++)
			{
				s2.setValue(y,w-1-x, this.getValue(x,y));
			}
		this.copy(s2);
	}
	/**
	 * symetry around x axis
	 */
	public void _flipAroundX()
	{
		Signal2D1D s2=this.copy();
		int h=dimy();
		for (int x=0;x<dimx();x++)
			for (int y=0;y<dimy();y++)
			{
				s2.setValue(x,y, this.getValue(x,h-1-y));
			}
		this.copy(s2);
	}

	/**
	 * symetry around y axis
	 */
	public void _flipAroundY()
	{
		Signal2D1D s2=this.copy();
		int w=dimx();
		for (int x=0;x<dimx();x++)
			for (int y=0;y<dimy();y++)
			{
				s2.setValue(x,y, this.getValue(w-1-x,y));
			}
		this.copy(s2);
	}


	public void _resample(int newWidth)
	{
		double coef= (double)newWidth/(double)this.dimx();
		int newHeight=(int)(this.dimy()*coef);
		Signal2D1D im2=new Signal2D1D(xmin(),xmax(),ymin(),ymax(),newWidth,newHeight);
		for (int i=0;i<newWidth;i++) 
			for (int j=0;j<newHeight;j++) 
			{
				int oldi=(int)(i/coef);
				int oldj=(int)(j/coef);
				im2.setValue(i, j, this.getValue(oldi,oldj));
			}
		this.copy(im2);
	}

	public void _binning(int binning)
	{
		if (binning==1) return;
		int newWidth=this.dimx()/binning;
		int newHeight=this.dimy()/binning;
		Signal2D1D s=new Signal2D1D(newWidth,newHeight);
		s.setxmin(this.xmin());
		s.setxmax(this.xmax());
		s.setymin(this.ymin());
		s.setymax(this.ymax());
		Signal2D1D.binning(s,this, binning);
		this.copy(s);
	}

	/**
	 * BEWARE: set correctly the images dimensions
	 * @param im
	 * @param imBinned
	 * @param binning
	 */
	public static void binning(Signal2D1D imBinned,Signal2D1D im,  int binning)
	{
		if (binning==1) 
		{
			imBinned.copy(im);
			return;
		}
		double r;
		double coef=Math.pow(binning, 2);
		for (int i=0;i<imBinned.dimx();i++) 
			for (int j=0;j<imBinned.dimy();j++) 
			{
				r=0;
				for (int ii=0;ii<binning;ii++) 
					for (int jj=0;jj<binning;jj++) 
					{
						r+=im.getValue(i*binning+ii, j*binning+jj);
					}
				imBinned.setValue(i, j, r/coef);
			}
	}

	/**
	 * add a frame arounf the image
	 * @param frameWidth
	 * @param value
	 */
	public void _addFrame(double frameWidth, double value)
	{
		Signal2D1D s=new Signal2D1D();
		double pixpermmx=(double)dimx()/(this.xmax()-this.xmin());
		double pixpermmy=(double)dimy()/(this.ymax()-this.ymin());
		int dx=(int)((double)frameWidth*pixpermmx);
		int dy=(int)((double)frameWidth*pixpermmy);
		s.init(this.xmin()-frameWidth,this.xmax()+frameWidth,this.ymin()-frameWidth,this.ymax()+frameWidth,
				this.dimx()+2*dx,this.dimy()+2*dy);

		for (int j=0;j<dy;j++)
		{
			for (int i=0;i<dimx()+2*dx;i++)	s.setValue(i,j,value);
		}

		for (int j=dy;j<dimy()+dy;j++)
		{
			for (int i=0;i<dx;i++) 					s.setValue(i,j,value);
			for (int i=dx;i<dimx()+dx;i++) 			s.setValue(i,j,getValue(i-dx,j-dy));
			for (int i=dimx()+dx;i<dimx()+2*dx;i++) s.setValue(i,j,value);
		}

		for (int j=dimy()+dy;j<dimy()+2*dy;j++)
		{
			for (int i=0;i<dimx()+2*dx;i++) s.setValue(i,j,value);
		}

		this.copy(s);
	}



	public Signal1D1DXY calcProjectionX()
	{
		double r=0;
		Signal1D1DXY signal=new Signal1D1DXY();
		signal.init(this.dimy());
		double sy=this.samplingy();
		for (int j=0;j<this.dimy();j++)
		{
			r=0;
			for (int i=0;i<this.dimx();i++)
			{
				r+=this.getValue(i, j);
			}
			r/=this.dimx();
			signal.setValue(j, this.ymin()+j*sy, r);
		}
		return signal;
	}




	public Signal1D1DXY calcProjectionY()
	{
		double r=0;
		Signal1D1DXY signal=new Signal1D1DXY();
		signal.init(this.dimx());
		double sx=this.samplingx();
		for (int i=0;i<this.dimx();i++)
		{
			r=0;
			for (int j=0;j<this.dimy();j++)
			{
				r+=this.getValue(i, j);
			}
			r/=this.dimy();
			signal.setValue(i, this.xmin()+i*sx, r);
		}
		return signal;
	}



	/**
	 * affect the same value to all pixels
	 * @param d
	 */
	public void setUniformValue(double d) 
	{
		for (int i=0;i<this.dimx();i++)
		{
			for (int j=0;j<this.dimy();j++)
			{
				this.setValue(i,j,d);
			}
		}

	}










}//end of class




