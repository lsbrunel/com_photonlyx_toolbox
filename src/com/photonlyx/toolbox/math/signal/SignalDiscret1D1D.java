
// Author Laurent Brunel

package com.photonlyx.toolbox.math.signal;

import java.io.*;
import java.nio.*;
import java.text.*;
import java.util.*;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.math.Complex;
import com.photonlyx.toolbox.math.FFT;
import com.photonlyx.toolbox.math.Util;
import com.photonlyx.toolbox.math.function.Function1D1D;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Messager;




/**
represent a one dimension signal with real boundaries and dim samplings from index 0 to index (dim-1)</p>
 */
public class SignalDiscret1D1D extends SignalDiscret1D1DAbstract 
{
	//in
	private double xmin,xmax;//boudaries of the signal
	private int dim;//number of samples

	//if true the y values correspond to positions at x values , nb y values= nb x values (default)
	//if false the y values correspond to cells between x values, nb y values +1 = nb x values 
	private boolean isPointsSignal=true;

	//internal
	private double[] y;
	private double samplingx;

	private int lastPointIndex = -1;

	//static private String defaultDef = null;



	public SignalDiscret1D1D()
	{
		lastPointIndex = -1;
	}

	/**
	 * 
	 * @param xmin
	 * @param xmax
	 * @param dim
	 */
	public SignalDiscret1D1D(double xmin,double xmax,int dim)
	{
		this.xmin=xmin;
		this.xmax=xmax;
		this.dim=dim;
		y=new double[dim];
		samplingx=sampling();
		lastPointIndex = -1;
	}

	public SignalDiscret1D1D(double[] y,double xmin,double xmax)
	{
		//init();
		this.xmin=xmin;
		this.xmax=xmax;
		this.dim=y.length;
		this.y=y;
		samplingx=sampling();
		lastPointIndex = -1;
	}

	/**
	 * 
	 * @param xmin
	 * @param xmax
	 * @param dim
	 * @param f function to fill with
	 */
	public SignalDiscret1D1D(double xmin,double xmax,int dim,Function1D1D f)
	{
		//init();
		this.xmin=xmin;
		this.xmax=xmax;
		this.dim=dim;
		y=new double[dim];
		samplingx=sampling();
		if (f!=null)
		{
			for (int i=0;i<getDim();i++) y[i]=f.f(x(i));
		}
		lastPointIndex = -1;
	}


	///**
	//refill if can. processing. calculate output.
	//*/
	//public void init()
	//{
	//setNewArray();
	//samplingx=sampling();
	//lastPointIndex = -1;
	//}


	//public void work()
	//{
	//lastPointIndex = -1;
	//
	//y=new double[dimInit()];
	//samplingx=sampling();
	//}


	/**
	 * @deprecated
alocate the number of samples (filled with zeros) and the boundaries
@param dim : the number of samples
@param xmin : the low x boundary
@param xmax : the high x boundary
	 */
	public void init(int _dim,double _xmin,double _xmax)
	{
		xmin=_xmin;
		xmax=_xmax;
		dim=_dim;
		y=new double[(int)dim];
		samplingx=sampling();
		lastPointIndex = -1;
	}

	/**
alocate the number of samples (filled with zeros) and the boundaries
@param dim : the number of samples
@param xmin : the low x boundary
@param xmax : the high x boundary
	 */
	public void init1(double _xmin,double _xmax,int _dim)
	{
		xmin=_xmin;
		xmax=_xmax;
		dim=_dim;
		y=new double[(int)dim];
		samplingx=sampling();
		lastPointIndex = -1;
	}


	public boolean isPointsSignal()
	{
		return isPointsSignal;
	}


	public void setPointsSignal(boolean isPointsSignal)
	{
		this.isPointsSignal = isPointsSignal;
		samplingx=sampling();
		lastPointIndex = -1;
	}


	/** return an array containing the x values*/
	public double[] getXarray()
	{
		double[] x=new double[getDim()];
		double s=sampling();
		for (int i=0;i<getDim();i++) x[i]=xmin()+s*i;
		return x;
	}

	/** return the y values array */
	public double[] getYarray()
	{
		return y;
	}

	/**same as getYarray()*/
	public double[] getYvalues()
	{
		return y;
	}

	public void fillWithFunction(Function1D1D fonction)
	{
		for (int i=0;i<getDim();i++)
		{
			y[i]=fonction.f(x(i));
		}
		lastPointIndex = -1;
	}

	/**read a text file with 2 columns containing x and y real data</p>
fill the  y array and get xmin and xmax at the first and last row*/
	public void fillWithFile(String filename)
	{
		if (filename==null) return;
		String s=TextFiles.readFile(filename).toString();
		fillWithString(s);
		lastPointIndex = -1;
	}



	/**
return the number of couple (x,y) in a string (c++ comments removed)
	 */
	private int NbPointInString(String s)
	{
		StringReader r;
		StreamTokenizer st;
		r= new StringReader(s);
		st= new StreamTokenizer(r);
		st.wordChars(0x26,0x26);
		st.wordChars(0x5F,0x5F);
		st.wordChars(0x7B,0x7B);
		st.wordChars(0x7D,0x7D);
		st.slashSlashComments(true);
		st.slashStarComments(true);
		int compteur=0;
		try
		{
			while (st.ttype!=StreamTokenizer.TT_EOF)
			{
				st.nextToken();
				//System.out.println(compteur+" "+st.nval);
				if (st.ttype==StreamTokenizer.TT_NUMBER) compteur++;
			}
		}
		catch(Exception e) {;}
		return compteur/2;
	}


	/**read a string with 2 columns containing x and y real data
fill the x and y arrays*/
	public void fillWithString(String string)
	{
		StringReader r;
		StreamTokenizer st;


		r= new StringReader(string);

		st= new StreamTokenizer(r);

		st.wordChars(0x26,0x26);
		st.wordChars(0x5F,0x5F);
		st.wordChars(0x7B,0x7B);
		st.wordChars(0x7D,0x7D);
		st.slashSlashComments(true);
		st.slashStarComments(true);

		int dim=NbPointInString(string);
		double[] x=new double[dim];//temporary
		y=new double[dim];//the array of the signal

		try
		{
			for (int i=0;i<dim;i++)
			{
				if (st.ttype==StreamTokenizer.TT_EOF)
				{
					Messager.messErr("La chaine  est trop courte");
					xmin=x[0];
					xmax=x[i-1];
					_trunc(0,i-1);
					return ;
				}
				while (st.ttype!=StreamTokenizer.TT_NUMBER) st.nextToken();
				x[i]=st.nval;
				st.nextToken();
				while (st.ttype!=StreamTokenizer.TT_NUMBER) st.nextToken();
				y[i]=st.nval;
				st.nextToken();
			}
		}
		catch (Exception e)
		{
			Messager.messErr(Messager.getString("Probleme_lecture_chaine") +"\n"+e);
			return ;
		}

		xmin=x[0];
		xmax=x[x.length-1];
		lastPointIndex = -1;
	}



	public int getDim()
	{
		if (y!=null) return y.length; else return 0;
	}

	/**
	 * return the raw x value of index i
	 * if the signal is points like give the x of the point</p>
	 * if the signal is cell like, give the x of the left point of cell 
	 * */
	public double x(int i)
	{
		if (i>getDim()-1) return 0;
		if (i<0) return 0;
		//System.out.println("x function"+" i="+i+" samplingx="+samplingx+" isPointsSignal="+isPointsSignal);
		//if (isPointsSignal) return xmin+samplingx*i;
		//else return xmin+samplingx*(i+0.5);
		return xmin+samplingx*i;
	}

	/**
	 * return the x value for the sample of index i</p>
	 * if the signal is points like give the x of the point</p>
	 * if the signal is cell like, give the middle point of cell in log scale
	 * */
	public double xSample(int i)
	{
		if (i>getDim()-1) return 0;
		if (i<0) return 0;
		//System.out.println("x function"+" i="+i+" samplingx="+samplingx+" isPointsSignal="+isPointsSignal);
		if (isPointsSignal) return xmin+samplingx*i;
		else return xmin+samplingx*(i+0.5);
	}

	public void copy(Signal1D1D signal)
	{
		if (signal instanceof SignalDiscret1D1D) this.copy((SignalDiscret1D1D)signal);
		else
		{
			Messager.messErr(getClass()+" Atempt to copy a signal that is not a SignalDiscret1D1D");
		}
	}

	/**equalise to the signal, make a copy of data in a new instance*/
	public void copy(SignalDiscret1D1D signal)
	{
		/*xmin=new Param("xmin",signal.xmin());
xmax=new Param("xmax",signal.xmax());*/
		xmin=signal.xmin();
		xmax=signal.xmax();
		y=new double[signal.getDim()];
		for (int i=0;i<getDim();i++)
		{
			y[i]=signal.value(i);
		}
		lastPointIndex = -1;
		samplingx=sampling();
	}

	/** make a copy of data in a new instance*/
	public SignalDiscret1D1D getAcopy()
	{
		SignalDiscret1D1D signal=new SignalDiscret1D1D();
		signal.copy(this);
		return signal;
	}

	/**get a copy of the signal, make a copy of data in a new instance*/
	public SignalDiscret1D1D getCopy()
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,value(i));
		return newsig;
	}

	/**equalise to the signal; BEWARE : there is no copy of matrix data : just point to the same*/
	public void transfert(SignalDiscret1D1D signal)
	{
		y=signal.y;
		xmin=signal.xmin();
		xmax=signal.xmax();
		lastPointIndex = -1;
	}


	/**
return the value at the cell i. If i is out of range,return 0
	 */
	public double value(int i)
	{
		double cor=1;
		if ((i<0)||(i>=getDim())) return 0.0;
		return y[i]*cor;
	}
	/**
return the value at the cell i. If i is out of range,return 0
	 */
	public double y(int i)
	{
		double cor=1;
		if ((i<0)||(i>=getDim())) return 0.0;
		return y[i]*cor;
	}


	/**
	 * return the value at the x position </p>
	 * if the signal is points like give the value of the closest point</p>
	 * if the signal is cell like, give the value of the cell containing the x value</p>
	 * 0 is returned if out of boundaries</p>
	 */
	public double value(double x)
	{
		//if ((x<xmin())||(x>xmax())) return 0;
		//int cell=getSampleIndex(x);
		//return value(cell);
		if ((x<xmin())||(x>=xmax())) return 0;
		if (isPointsSignal) 
		{
			int i=getSampleIndex(x);
			return value(i);
		}
		else
		{
			int cell=getSampleIndexFloor(x);
			return value(cell);
		}
	}

	/**
return the value at the x position 
approximated with a linear interpolation,
0 is returned if out of boundaries
	 */
	// public double valueInterpol(double x)
	// {
	// double v=0;
	// if ((x<xmin())||(x>xmax())) return 0;
	// int i=(int)Math.floor((x-xmin())/sampling());
	// double x1=x(i);
	// double x2=x(i+1);
	// double y1=y(i);
	// double y2=y(i+1);
	// double y=y1+(y2-y1)/(x2-x1)*(x-x1);
	// /*System.out.println(getClass()+" x1= "+x1+" x2= "+x2);
	// System.out.println(getClass()+" y1= "+y1+" y2= "+y2);
	// System.out.println(getClass()+" x= "+x+" y= "+y	);*/
	// return y;
	// }

	/** set a value (not corrected)*/
	public void setValue(int i,double v)
	{
		if ( (i<0) || (i>=getDim()) )  return;
		if (y!=null)
		{
			y[i]=v;
		}

		if (i < lastPointIndex)
		{
			// If a point is inserted, all the curve will be redraw
			//
			lastPointIndex = -1;
		}
	}




	//public int dimInit()
	//{
	//return dim;
	//}

	//public void setDim(int _dim)
	//{
	//dim=_dim;
	//}


	/**return the distance in the x axis between 2 samples*/
	public double sampling()
	{
		double ss;
		if (isPointsSignal) 
			{
			if (dim==1) ss=1;
			else ss= (xmax()-xmin())/(dim-1);//signal known by some points
			}
		else ss= (xmax()-xmin())/dim;//signal known by values inside cells
		return ss;
		//if (y!=null)
		//	{
		//	if (getDim()>1)
		//		return ((xmax()-xmin())/(getDim()-1)); else return 0;
		//	}
		//else return 0;
	}

	/**return the distance in the x axis between 2 samples*/
	public double sampling(int i)
	{
		return sampling();
	}


	/**return the x min value*/
	public double xmin()
	{
		return xmin;
	}

	/**return the x max value*/
	public double xmax()
	{
		return xmax;
	}

	/**return the x min value*/
	public void setXmin(double r)
	{
		xmin=r;
	}

	/**return the x max value*/
	public void setXmax(double r)
	{
		xmax=r;
	}
	///**set the x min value*/
	//public void setxmin(double x)
	//{xmin=x;}
	//
	///**set the x max value*/
	//public void setxmax(double x)
	//{xmax=x;}
	//
	//public void setNewArray()
	//{
	//y=new double[(int)dim];
	//}

	//public void setArray(double [] array)
	//{
	//y=array;
	//lastPointIndex = -1;
	//}

	/**
get the xi closest to the x position
return 0 if x==xmin, (dim-1) if x=max,  -1 if out of range
	 */
	public int getSampleIndex(double x)
	{
		if ((x<xmin)||(x>=xmax)) return 0;
		int i=(int)Math.round((x-xmin())/sampling());
		//if (cell<0) cell=-1;
		//if (cell>=getDim()) cell=-1;
		return i;
	}

	/**get the sample index just lower to the x position, </p>
	 * return 0 if x==xmin, (dim-1) if x=max,  -1 if out of range*/
	public int getSampleIndexFloor(double x)
	{
		if (x<xmin) return -1;
		if (x>=xmax) return -1;
		int i=(int) Math.floor((x-xmin)/samplingx);
		//System.out.println("x="+x+" "+i+ " x1="+x(cell)+" x2="+x(i));
		return i;
	}


	/**return a String with x and y in 2 columns*/
	public String toString()
	{
		NumberFormat nf1 = NumberFormat.getInstance(Locale.ENGLISH);
		nf1.setMaximumFractionDigits(15);
		nf1.setMinimumFractionDigits(15); 
		nf1.setGroupingUsed(false);

		StringBuffer s=new StringBuffer();
		//double samp=sampling();
		for (int i=0;i<getDim();i++)
		{
			//double x=(xmin+i*samp);
			s.append(nf1.format(x(i))+"\t"+nf1.format(value(i))+"\n");
		}
		return s.toString();
	}


	//******************************************************************************
	//* JoloPlotXY optimization
	//******************************************************************************

	public void markEnd()
	{
		lastPointIndex=getDim() - 1 ;
	}

	/** 
	 * Tells if the Signal needs to be entirely redrawn or if only the last
	 * part has to be redrawn
	 * 
	 * @return true
	 */
	public boolean needsRedrawAll()
	{
		// TODO
		// On doit retourner true si l'un des nouveaux points est compris entre xmin et xmax
		//
		if (lastPointIndex == -1) 
		{
			return true;
		}

		return false;
	}

	/** 
	 * Froce the signal to be entirely redrawn the next time it will be drawn 
	 */
	public void redrawAll() {lastPointIndex = -1;}

	/** 
	 * Returns the part of the signal thas has not yet been drawn 
	 * 
	 * @return this
	 */
	public Signal1D1D getSignalEnd()
	{
		int newLastPointIndex = getDim() - 1;
		int firstPointIndex;

		if (lastPointIndex == -1) 
		{
			firstPointIndex = 0;
		}
		else
		{
			firstPointIndex = lastPointIndex;
		}

		int endSignalDim = getDim() - firstPointIndex;

		SignalDiscret1D1D endSignal=new SignalDiscret1D1D();
		//signal.copy(this);
		// TODO les parametres xmin et xmax sont passe a 0
		endSignal.init(endSignalDim,0,0);

		int dataIndex = firstPointIndex;
		for (int i=0; i < endSignalDim ;i++) 
		{
			endSignal.setValue(i,value(dataIndex));
			dataIndex++;
		}

		lastPointIndex = newLastPointIndex;

		return endSignal;
	}




	//*************************************************************************
	//*****************  analyse and processing   *****************************
	//*************************************************************************






	/** return the index of the min y value*/
	public int yminIndex()
	{
		double r=1e300;
		int ii=0;
		for (int i=0;i<getDim();i++) 
		{
			double v=value(i);
			if (v<r)
			{
				r=v;
				ii=i;
			}
		}
		return ii;
	}

	/** return the index of the max y value*/
	public int ymaxIndex()
	{
		double r=-1e300;
		int ii=0;
		for (int i=0;i<getDim();i++) 
		{
			double v=value(i);
			if (v>r) 
			{
				r=v;
				ii=i;
			}
		}
		return ii;
	}

	public double integral()
	{
		//double s=0;
		//for (int i=0;i<getDim();i++) s+=value(i);
		//return s*sampling();

		if (isPointsSignal) 
		{
			double s=0;
			for (int i=0;i<getDim()-1;i++)
			{
				s+=y[i]+y[i+1];
			}
			return s/2.0*(samplingx);
		}
		else
		{
			double s=0;
			for (int i=0;i<getDim();i++)
			{
				s+=y[i];
			}
			return s*samplingx;
		}


	}

	/*public double rms()
{
double s=0;
double m=mean();
for (int i=0;i<getDim();i++)
	{
	double v=value(i)-m;
	s+=v*v;
	}
return Math.sqrt(s/getDim());
}*/

	/**put the integral at the value 1*/
	public SignalDiscret1D1D normaliseSurface()
	{
		double s=integral();
		//System.out.println("Surface:"+s);
		SignalDiscret1D1D newsig=this.multiply(1/s);
		return newsig;
	}


	/**return a new SignalDiscret1D1D after multiply all values by a factor*/
	public SignalDiscret1D1D multiply(double s)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,value(i)*s);
		return newsig;
	}

	/**return a new SignalDiscret1D1D after multiply all values by a factor*/
	public SignalDiscret1D1D multiply_x(double s)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin()*s,xmax()*s,getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,value(i));
		return newsig;
	}





	/**return a new SignalDiscret1D1D after multiply all values by the fonction*/
	public SignalDiscret1D1D multiply(Function1D1D f)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,value(i)*f.f(x(i)));
		return newsig;
	}

	/**return a new SignalDiscret1D1D after add a value to all values*/
	public SignalDiscret1D1D add(double s)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,value(i)+s);
		return newsig;
	}

	/**add a value to all values*/
	public void _add(double s)
	{
		for (int i=0;i<getDim();i++) setValue(i,value(i)+s);
	}

	/**return a new SignalDiscret1D1D after square root all values*/
	public SignalDiscret1D1D sqrt()
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,Math.sqrt(value(i)));
		return newsig;
	}

	/**return a new SignalDiscret1D1D after elevating all values by a power */
	public SignalDiscret1D1D pow(double puiss)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,Math.pow(value(i),puiss));
		return newsig;
	}

	/**replace all values by ( base power of value) */
	public void _exp(double base)
	{
		for (int i=0;i<getDim();i++) setValue(i,Math.pow(base,value(i)));
	}


	/**return a new SignalDiscret1D1D after elevating all values at the puissance of the base */
	public SignalDiscret1D1D exp(double base)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,Math.pow(base,value(i)));
		return newsig;
	}



	/**return a new SignalDiscret1D1D after elevating all values at the puissance of the base */
	public SignalDiscret1D1D log(double base)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		double r=Math.log(base);
		for (int i=0;i<getDim();i++) newsig.setValue(i,Math.log(value(i))/r);
		return newsig;
	}

	/**elevate all values at the puissance of the base */
	public void _log(double base)
	{
		double r=Math.log(base);
		for (int i=0;i<getDim();i++) y[i]=Math.log(y[i])/r;
		lastPointIndex = -1;
	}


	/**return a new SignalDiscret1D1D after get the absolute value */
	public SignalDiscret1D1D abs()
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,Math.abs(value(i)));
		return newsig;
	}

	/**return a new SignalDiscret1D1D after mirror the  values. keep the same xmin and xmax*/
	public SignalDiscret1D1D mirror()
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,value(getDim()-1-i));
		return newsig;
	}


	/**give back a new signal with part of the signal from the indexes i1 to i2 included*/
	public SignalDiscret1D1D trunc(int i1,int i2)
	{
		double x1=xmin()+i1*sampling();
		double x2=xmin()+i2*sampling();
		SignalDiscret1D1D sig=new SignalDiscret1D1D(x1,x2,i2-i1);
		for (int i=0;i<sig.getDim();i++) sig.setValue(i,value(i1+i));
		return sig;
	}

	/**keep part of the signal from the indexes i1 to i2 included*/
	public void _trunc(int i1,int i2)
	{
		double x1=x(i1);
		double x2=x(i2);
		int newdim=i2-i1;
		if (newdim<0) newdim=0;
		double[] newy= new double[newdim];
		for (int i=0;i<newdim;i++) newy[i]=y[i1+i];
		y=newy;
		xmin=x1;
		xmax=x2;
		lastPointIndex = -1;
	}

	/**return a new SignalDiscret1D1D after concatenate 2 signals.</p>
(xmax+sampling) of the first must be xmin of the second and </p>
the sampling must be the same to get real sense operation)*/
	public SignalDiscret1D1D concatenate(SignalDiscret1D1D sig2)
	{
		SignalDiscret1D1D sig1=this;
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(sig1.xmin(),sig2.xmax(),getDim()+sig2.getDim());
		for (int i=0;i<sig1.getDim();i++) newsig.setValue(i,sig1.value(i));
		for (int i=0;i<sig2.getDim();i++) newsig.setValue(i+sig1.getDim(),sig2.value(i));
		return newsig;
	}


	/**
tranlate the signal in the x direction
@param xoffset : the offset to add to each x  value
@return a new SignalDiscret1D1D with   the x values translated
	 */
	public SignalDiscret1D1D translate(double xoffset)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin()+xoffset,xmax()+xoffset,getDim());
		for (int i=0;i<getDim();i++) newsig.setValue(i,value(i));
		return newsig;
	}

	/**
tranlate the signal in the x direction
@param xoffset : the offset to add to each x  value
	 */
	public void _translate(double xoffset)
	{
		xmin=xmin()+xoffset;
		xmax=xmax()+xoffset;
	}


	/**
return a new SignalDiscret1D1D with the onvolution of the  signal by s2 (s2 assumed periodic)
	 */
	public SignalDiscret1D1D convolution(SignalDiscret1D1D s2)
	{
		double r;
		int n=getDim();
		int n2=s2.getDim();
		int indice;
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),n);
		for (int i=0;i<n;i++)
		{
			r=0;
			for (int j=0;j<n2;j++) 
			{
				indice=i-j;
				if (indice<0) indice+=n2;
				r+=value(j)*s2.value(indice);
			}
			newsig.setValue(i,r);
		}
		return newsig;
	}


	/**return the average value from min to max (0 correspond to xmin, 1 to xmax)*/
	public double mean3(double min,double max)
	{
		double r=0;
		int i1=(int) Math.floor(min*getDim());
		int i2=(int) Math.floor(max*getDim());
		for (int i=i1;i<i2;i++) r+=value(i);
		return r/(i2-i1);
	}

	/**return the average value from xmin to xmax */
	public double mean2(double xmin,double xmax)
	{
		double r=0;
		int i1=getSampleIndex(xmin);
		int i2=getSampleIndex(xmax);
		for (int i=i1;i<i2;i++) r+=value(i);
		return r/(i2-i1);
	}

	/**affect a value from xmin to xmax */
	public void affect(double xmin,double xmax,double value)
	{
		int i1=getSampleIndex(xmin);
		int i2=getSampleIndex(xmax);
		for (int i=i1;i<i2;i++) setValue(i,value);
	}

	/**trunc from x1 to x2. return a new instance*/
	public SignalDiscret1D1D trunc(double x1,double x2)
	{
		if (x2<x1) return null;
		if (x1>xmax()) return null;
		if (x2<xmin()) return null;
		if (x1<xmin()) x1=xmin();
		if (x2>xmax()) x2=xmax();
		int i1=getSampleIndex(x1);
		int i2=getSampleIndex(x2);
		//System.out.println ("i1="+i1+" i2="+i2);
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(x1,x2,i2-i1+1);
		for (int i=i1;i<=i2;i++) newsig.setValue(i-i1,value(i));
		return newsig;
	}


	/**trunc from x1 to x2*/
	public void _trunc(double x1,double x2)
	{
		int i1=getSampleIndex(x1);
		int i2=getSampleIndex(x2);
		_trunc(i1,i2);
	}


	/**trunc from x1 to x2*/
	public void _truncx(double x1,double x2)
	{
		_trunc(x1,x2);
	}

	/**return the moment of order 1 */
	public double barycentre()
	{
		double samp=sampling();
		double r=0,s=0;
		for (int i=0;i<getDim();i++)
		{
			r+=(xmin()+i*samp)*value(i);
			s+=value(i);
		}
		return r/s;
	}


	/** make a spectral analysis with eventually a smaller analysis sample. The result is then averaged. The spectrum (number of points dimAnalyse) is returned*/
	public SignalDiscret1D1D analyseSpectrale(int dimAnalyse)
	{
		int na=dimAnalyse;
		int ma=(int)Math.floor(Math.log(na)/Math.log(2));
		na=(int)Math.pow(2,ma);

		int nbsig=getDim()/na;
		Complex[] X=new Complex[na+1];
		double[] moy=new double[na];

		for (int j=0;j<nbsig;j++)
		{
			for (int i=1;i<=na;i++) 
			{
				X[i]=new Complex(value(j*na+i-1),0);
			}
			FFT.fft(X,true);
			for (int i=1;i<=na;i++) 
			{
				moy[i-1]+=X[i].norm()/nbsig;
			}
		}
		double tech=sampling();//sampling time of the signal
		double fech=1/tech;//sampling frequency of the signal
		System.out.println("Frequence echantillonnage:"+fech+"Hz");
		SignalDiscret1D1D spectre=new SignalDiscret1D1D(moy,0,fech);
		return spectre;
	}


	/** make a spectral analysis with a smaller analysis sample swept each time by "decalage" samples.</p>
 The result is then averaged. The spectrum (number of points dimAnalyse) is returned*/
	public SignalDiscret1D1D analyseSpectraleGlisse(int dimAnalyse,int decalage)
	{
		int na=dimAnalyse;
		int ma=(int)Math.floor(Math.log(na)/Math.log(2));
		na=(int)Math.pow(2,ma);

		int nbsig=(getDim()-na)/decalage;
		Complex[] X=new Complex[na+1];
		double[] moy=new double[na];

		for (int j=0;j<nbsig;j++)
		{
			for (int i=1;i<=na;i++) 
			{
				X[i]=new Complex(value(j*decalage+i-1),0);
			}
			FFT.fft(X,true);
			for (int i=1;i<=na;i++) 
			{
				moy[i-1]+=X[i].norm()/nbsig;
			}
		}
		double tech=sampling();//sampling time of the signal
		double fech=1/tech;//sampling frequency of the signal
		System.out.println("Frequence echantillonnage:"+fech+"Hz");
		SignalDiscret1D1D spectre=new SignalDiscret1D1D(moy,0,fech);
		return spectre;
	}



	/**give back the fft transform*/
	public SignalDiscret1D1D fft()
	{
		int na=getDim();
		Util.af("fft: echantillonage = "+na);
		int ma=(int)Math.ceil(Math.log(na)/Math.log(2));
		na=(int)Math.pow(2,ma);
		Util.af("fft: echantillonage final = "+na);
		Complex[] X=new Complex[na+1];
		for (int i=1;i<=na;i++)
		{
			X[i]=new Complex(value(i-1),0);
		}
		FFT.fft(X,true);
		//Messager.logln("fft: fmax"+1/(2*sampling()));

		SignalDiscret1D1D newsig=new SignalDiscret1D1D(0,1/(2*sampling()),na/2);
		for (int i=0;i<newsig.getDim();i++) newsig.setValue(i,X[i+1].norm());
		return newsig;
	}


	/**return a new SignalDiscret1D1D after extracting the peaks.</p>
dimkern is the number of points used to evaluated the local average (>=3).</p>
diff is the limit difference in percent from the kernel average for a point to be extracted or not
	 */
	public SignalDiscret1D1D extractPeaks(int dimkern, double diff)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim()-dimkern+1;i++)
		{
			double average=mean(i,i+dimkern-1);
			if ((Math.abs(value(i)-average)/average*100)>=diff)
			{
				newsig.setValue(i,mean(i+1,i+dimkern-1));
				System.out.println("Un pic a l'indice "+i);
			}
			else newsig.setValue(i,value(i));
		}
		for (int i=getDim()-dimkern+1;i<getDim();i++)
		{
			newsig.setValue(i,value(i));
		}
		return newsig;
	}



	/**
return a new SignalDiscret1D1D after smoothing with a uniform kernel of size kern
	 */
	//public SignalDiscret1D1D smoothing(int dimkern)
	//{
	//SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
	//for (int i=0;i<getDim()-dimkern+1;i++)
	//	{
	//	double average=mean(i,i+dimkern-1);
	//	newsig.setValue(i,average);
	//	}
	////set the last values to the last mean
	//for (int i=getDim()-dimkern+1;i<getDim();i++)
	//	{
	//	newsig.setValue(i,value(getDim()-dimkern));
	//	}
	//return newsig;
	//}

	/**
return a new undersampled SignalDiscret1D1D replacing npts points by average
	 */
	public SignalDiscret1D1D underSample(int nbpts)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim()/nbpts);
		for (int i=0;i<newsig.getDim();i++)
		{
			double average=mean(i*nbpts,(i+1)*nbpts-1);
			newsig.setValue(i,average);
		}
		return newsig;
	}


	/**resample */
	public SignalDiscret1D1D resample(int newdim)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),newdim);
		for (int i=0;i<newsig.getDim();i++)
		{
			double x=newsig.x(i);
			newsig.setValue(i,value(x) );
		}
		return newsig;
	}

	/**invert the signal, like the bible the end becomes the start */
	public SignalDiscret1D1D inverse()
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim();i++)
		{
			newsig.setValue(i,value(getDim()-i-1) );
		}
		return newsig;
	}

	/**
return a new SignalDiscret1D1D binary (0 and 1 only) the threshold is in percent of max-min value from min value
	 */
	public SignalDiscret1D1D threshold(double thresholdpc)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		double ymax=ymax();
		double ymin=ymin();
		double threshold=thresholdpc/100.0*(ymax-ymin)+ymin;
		for (int i=0;i<getDim();i++)
		{
			double v=value(i);	
			if (v>threshold) newsig.setValue(i,1); else newsig.setValue(i,0);
		}
		return newsig;
	}

	public  SignalDiscret1D1D addZeros(int nbzeros)
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax()+sampling()*nbzeros,getDim()+nbzeros);
		for (int i=0;i<getDim();i++)
		{
			newsig.setValue(i,value(i));
		}
		return newsig;
	}

	/**return a new signal with the derivative*/
	public SignalDiscret1D1D derivate()
	{
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax()-sampling(),getDim()-1);
		double s=sampling();
		for (int i=0;i<getDim()-1;i++)
		{
			newsig.setValue(i,(value(i+1)-value(i))/s);
		}
		return newsig;
	}

	//***************************************************************
	//***********  2 signals processing   ***************************
	//****************************************************************


	//private boolean check(SignalDiscret1D1D sig1,SignalDiscret1D1D sig2,String operation)
	//{
	//if (  (sig1.xmin()!=sig2.xmin()) || (sig1.xmax()!=sig2.xmax())|| (sig1.getDim()!=sig2.getDim()) )
	//	{
	//	Messager.messErr(operation+"\n"+getClass()+" "+sig1.getNode().getName()+" and "+sig2.getNode().getName()+" \nThe 2 signals must be identical");
	//	return false;
	//	}
	//return true;
	//}
	//

	/**
add a new SignalDiscret1D1D (of same dim)
@param sig2 the signal to add
@return a new signal added
	 */
	public SignalDiscret1D1D add(SignalDiscret1D1D sig2)
	{
		SignalDiscret1D1D sig1=this;
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(sig1.xmin(),sig1.xmax(),getDim());
		for (int i=0;i<sig1.getDim();i++) newsig.setValue(i,sig1.value(i)+sig2.value(i));
		return newsig;
	}


	/**
add a new SignalDiscret1D1D 
@param sig2 the signal to add
@return a new signal with the 1st & 2nd signal catenated
	 */
	public SignalDiscret1D1D concatSignal(SignalDiscret1D1D sig2)
	{
		SignalDiscret1D1D sig1=this;
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(sig1.xmin(),sig2.xmax(),(sig1.getDim()+sig2.getDim()));
		for (int i=0;i<sig1.getDim();i++) newsig.setValue(i,sig1.value(i));
		for (int i=sig1.getDim();i<(sig1.getDim()+sig2.getDim());i++) newsig.setValue(i,sig2.value(i));
		return newsig;
	}

	/**
add a SignalDiscret1D1D (of same dim)
@param sig2 the signal to add
	 */
	public void  _add(SignalDiscret1D1D sig2)
	{
		for (int i=0;i<getDim();i++) setValue(i,value(i)+sig2.value(i));
	}

	/**
substract sig2 </p>
The boundaries and sampling of sig2 MUST be the same .
	 */
	public void _subDirect(SignalDiscret1D1D sig2)
	{
		for (int i=0;i<getDim();i++)
		{
			setValue(i,value(i)-sig2.value(i)  );
		}
	}

	/**substract sig2.</p>
The boundaries and sampling of sig2 doesn't have to be the same .</p>
 Not matching values are interpolated.</p>
The final boundaries are the intersection of the 2 intervals */
	public void _sub(SignalDiscret1D1D sig2)
	{
		//double nxmin=Math.max(xmin(),sig2.xmin());
		//double nxmax=Math.min(xmax(),sig2.xmax());
		//int ndim=(int)((nxmax-nxmin)/sampling());
		for (int i=0;i<getDim();i++)
		{
			double x=x(i);
			setValue(i,value(x)-sig2.value(x)  );
		}
	}

	/**substract sig2 and put in a new SignalDiscret1D1D.</p>
The boundaries and sampling of sig2 doesn't have to be the same .</p>
 Not matching values are interpolated.</p>
The final boundaries are the intersection of the 2 intervals */
	public SignalDiscret1D1D sub(SignalDiscret1D1D sig2)
	{
		SignalDiscret1D1D sig1=this;
		double nxmin=Math.max(sig1.xmin(),sig2.xmin());
		double nxmax=Math.min(sig1.xmax(),sig2.xmax());
		int ndim=(int)((nxmax-nxmin)/sig1.sampling());
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(nxmin,nxmax,ndim);
		for (int i=0;i<newsig.getDim();i++)
		{
			double x=newsig.x(i);
			newsig.setValue(i,sig1.value(x)-sig2.value(x)  );
		}
		return newsig;
	}

	/**multiply by another SignalDiscret1D1D */
	public SignalDiscret1D1D mul(SignalDiscret1D1D sig2)
	{
		SignalDiscret1D1D sig1=this;
		double nxmin=Math.max(sig1.xmin(),sig2.xmin());
		double nxmax=Math.min(sig1.xmax(),sig2.xmax());
		int ndim=(int)((nxmax-nxmin)/sig1.sampling());
		SignalDiscret1D1D newsig=new SignalDiscret1D1D(nxmin,nxmax,ndim);
		for (int i=0;i<newsig.getDim();i++) 
		{
			double x=newsig.x(i);
			newsig.setValue(i,sig1.value(x)*sig2.value(x)  );
		}
		return newsig;
	}

	/**transfert to a Signal1D1DXY  */
	public Signal1D1DXY transfertTo1D1DXY()
	{
		double samp=sampling();
		double xmin=xmin();
		Signal1D1DXY signal=new Signal1D1DXY(getDim());
		for (int i=0;i<getDim();i++)
		{
			double x=(xmin+i*samp);
			signal.setValue(i,x,value(i));
		}
		return signal;
	}

	/**
	 * return the (first from start x value to right or left )
	 *  x value for this y value. linear interpolation.
	 * @param y
	 * @param start
	 * @param toRight
	 * @param found boolean[1]:put true if solution found, false if not. can be null
	 * @return
	 */
	public double solve(double y,double start,boolean toRight)
	{
		return solve( y, start, toRight,null);
	}


	/**
	 * return the (first from start x value to right or left )
	 *  x value for this y value. linear interpolation.
	 * @param y
	 * @param start
	 * @param toRight
	 * @param found boolean[1]:put true if solution found, false if not. can be null
	 * @return
	 */
	public double solve(double y,double start,boolean toRight,boolean[] found)
	{
		double x1,x2,y1,y2,x;
		boolean _found=false;
		double samp=sampling();
		double xmin=xmin();
		int i;
		if (start<=xmin()) i=0;
		else if (start>=xmax()) i=getDim()-1;
		else i=getSampleIndex(start);
		if (i==getDim()-1) i--;
		do
		{
			x1=(xmin+i*samp);
			x2=(xmin+(i+1)*samp);
			y1=value(i);
			y2=value(i+1);
			//	System.out.println("i="+i+"x1="+x1+" x2="+x2+" y1="+y1+" y2="+y2+" y="+y);
			if (((y1<=y)&&(y2>y))||((y2<y)&&(y1>=y)))
			{
				//System.out.println("found");
				_found=true;
				break;
			}
			if (toRight) i++;else i--;
		}
		while ((i<getDim()-1)&&(i>0));

		if (_found)
		{
			if (y2!=y1) x= ( x1*Math.abs(y2-y) + x2*Math.abs(y1-y)  ) / Math.abs(y2-y1);
			else x=(x1+x2)/2;
		}
		else x=start;
		if (found!=null) found[0]=_found;
		return x;
	}


	/**return the (first from start x value to right or left ) x value for this y value. linear interpolation.*/
	public double FindYValueAbove(double y,double start,boolean toRight)
	{
		double x1,x2,y1,y2,x;
		boolean found=false;
		double samp=sampling();
		double xmin=xmin();
		int i;
		if (start<=xmin()) i=0;
		else if (start>=xmax()) i=getDim()-1;
		else i=getSampleIndex(start);
		if (i==getDim()-1) i--;
		do
		{
			x1=(xmin+i*samp);

			y1=value(i);

			if (y1>=y)
			{
				System.out.println("found @ " + x1);
				found=true;
				break;
			}	
			if (toRight) i++;else i--;
		}
		while ((i<getDim()-1)&&(i>0));

		if (found)
		{
			x=x1;
		}
		else x=-1;
		System.out.println("found x value of " + x);

		return x;
	}



	/**return the (first from start x value to right or left ) x value for this y value. linear interpolation.*/
	public double FindYValueBelow(double y,double start,boolean toRight)
	{
		double x1,x2,y1,y2,x;
		boolean found=false;
		double samp=sampling();
		double xmin=xmin();
		int i;
		if (start<=xmin()) i=0;
		else if (start>=xmax()) i=getDim()-1;
		else i=getSampleIndex(start);
		if (i==getDim()-1) i--;
		do
		{
			x1=(xmin+i*samp);

			y1=value(i);

			if (y1<=y)
			{
				System.out.println("found @ " + x1);
				found=true;
				break;
			}	
			if (toRight) i++;else i--;
		}
		while ((i<getDim()-1)&&(i>0));

		if (found)
		{
			x=x1;
		}
		else x=-1;
		System.out.println("found x value of " + x);

		return x;
	}

	/**
 y=f(x).linear interpolation betwen the points</p>
Return an vector of Double of the solutions
	 */
	public Vector<Double> solveComplet(double y)
	{
		Vector<Double> v=new Vector<Double>();
		double x,x1,x2,y1,y2;
		// System.out.println(getClass()+" xmin= "+xmin+" xmax= "+xmax+" y="+y);
		double step=sampling();
		x2=xmin();
		y2=y(0);
		for (int i=1;i<getDim();i++)
		{
			x1=x2;
			y1=y2;
			x2=x1+step;
			y2=y(i);
			/*	System.out.println(getClass()+" x1= "+x1+" x2= "+x2);
	System.out.println(getClass()+" y1= "+y1+" y2= "+y2);*/
			if (((y1<y)&(y2<y))||((y1>y)&(y2>y))) continue; 
			if (((y1<y)&(y<y2))||((y2<y)&(y<y1))) 
			{
				if (y2!=y1) x= ( x1*Math.abs(y2-y) + x2*Math.abs(y1-y)  ) / Math.abs(y2-y1);
				else x=(x1+x2)/2;
				/*		System.out.println(getClass()+" x1= "+x1+" x2= "+x2);
		System.out.println(getClass()+" y1= "+y1+" y2= "+y2);
		System.out.println(getClass()+" x= "+x);*/
				v.add(new Double(x));
			}
			else if (y1==y) v.add(new Double(x1));
			else if (y2==y) v.add(new Double(x2));
		}
		return v;
	}





	/**set the  point of index i to these new co-ordinates, if possible*/
	public void setPoint(int i,double[] p)
	{
		y[i]=p[1];

		if (i < lastPointIndex)
		{
			// If a point is inserted, all the curve will be redraw
			//
			lastPointIndex = -1;
		}
	}

	/**
update the state of the object from the data in the string s</p>
@param s the string where is stored the object's state
	 */
	public void updateStateFromString(String s)
	{
		// if (s==null) return;
		if ((s==null) ||(s.compareTo("")==0))
		{
			init(0,0,1);
			return;
		}
		//drop the first line
		s=s.substring(s.indexOf('\n')+1,s.indexOf(';')-1);
		// System.out.println(getClass()+"\n"+s);
		double[][] data2col;
		data2col=StringSource.fillWithTwoFirstColumns(s);
		if (data2col==null) return;
		xmin=data2col[0][0];
		xmax=data2col[0][data2col[0].length-1];
		y=data2col[1];
		samplingx=sampling();
	}



	public void updateFromXML(XMLElement xml) 
	{
		String data=xml.getContent();	
		Definition def=new Definition(data); 
		int _dim=def.dim()/2;
		double _xmin=new Double(def.word(0)).doubleValue();
		double _xmax=new Double(def.word(2*(_dim-1))).doubleValue();
		init(_dim,_xmin,_xmax);
		for (int i=0;i<getDim();i++) 
			this.setValue(i,  new Double(def.word(2*i+1)).doubleValue());
		//read property
		isPointsSignal=xml.getBooleanAttribute("isPointsSignal","true","false",false);	
	}



	/**
put the state of the object in a byte array
@return the string where is stored the object's state
	 */
	public byte[] storeStateInBytes()
	{
		//header size in bytes
		ByteBuffer buf = ByteBuffer.allocate(4+8+8+getDim()*8);
		buf.putInt(getDim());//4bytes
		buf.putDouble(xmin());//8bytes
		buf.putDouble(xmax());//8bytes
		for (int i=0;i<getDim();i++) buf.putDouble(y[i]);//8*dim bytes
		return buf.array();
	}


	/**
update the state of the object from the data in the byte array b</p>
@param b the byte array where is stored the object's state
	 */
	public void updateStateFromBytes(ByteBuffer buf)
	{
		if (buf==null)
		{
			y=new double[0];
			return;
		}
		int dim=buf.getInt();
		y=new double[dim];
		xmin=buf.getDouble();
		xmax=buf.getDouble();
		for (int i=0;i<dim;i++) y[i]=buf.getDouble();
		lastPointIndex = -1;
		samplingx=sampling();
	}



	/**
put the state of the object in a byte array with data coded as float number (loose some precision)
@return the string where is stored the object's state
	 */
	public byte[] storeStateInBytesCodedInFloat()
	{
		//header size in bytes
		ByteBuffer buf = ByteBuffer.allocate(4+4+4+getDim()*4);
		buf.putInt(getDim());//4bytes
		buf.putFloat((float)xmin());//4bytes
		buf.putFloat((float)xmax());//4bytes
		for (int i=0;i<getDim();i++) buf.putFloat((float)y[i]);//4*dim bytes
		return buf.array();
	}


	/**
update the state of the object from the data in the byte array b with data coded as float number (loose some precision)
@param b the byte array where is stored the object's state
	 */
	public void updateStateFromBytesCodedInFloat(ByteBuffer buf)
	{
		if (buf==null)
		{
			y=new double[0];
			return;
		}
		int dim=buf.getInt();
		y=new double[dim];
		xmin=buf.getFloat();
		xmax=buf.getFloat();
		for (int i=0;i<dim;i++) y[i]=buf.getFloat();
		lastPointIndex = -1;
	}



	public void _annulate()
	{
		//for (int i=0;i<getDim();i++) setValue(i,0);
		//y=new double[getDim()];
		init(getDim(),xmin(),xmax());
	}

	public  void removeAllPoints() { init(0,0,0);}

	/**multiply the x values by a factor*/
	public  void _multiply_x(double coef)
	{
		xmin*=coef;
		xmax*=coef;
		samplingx= sampling();
	}

	public void updateSamplingX()
	{
		samplingx= sampling();
	}


	@Override
	public void _inverse() 
	{
		for (int i=0;i<getDim();i++) setValue(i,1/value(i));
	}

	/**
replace the y values by the Log (base 10) of themselves
	 */
	public void  _logTheY()
	{
		for (int i=0;i<getDim();i++) setValue(i,Math.log10(y[i]));
		lastPointIndex = -1;
	}




	/**
return a new SignalDiscret1D1D after smoothing with a uniform kernel of size kern
	 */
	public SignalDiscret1D1D smooth(int dimkern)
	{

		SignalDiscret1D1D newsig=new SignalDiscret1D1D(xmin(),xmax(),getDim());
		for (int i=0;i<getDim()-dimkern+1;i++)
		{
			double average=mean(i,i+dimkern-1);
			newsig.setValue(i,average);
		}
		//set the last values with a kernel at the left
		for (int i=getDim()-dimkern+1;i<getDim();i++)
		{
			int imin=Math.max(0, i-dimkern+1);
			double average=mean(imin,i);
			newsig.setValue(i,average);
		}
		return newsig;

	}


}





