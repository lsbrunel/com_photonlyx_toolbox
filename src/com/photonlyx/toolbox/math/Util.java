package com.photonlyx.toolbox.math;


public class Util 
{
/**gives log(base10)(x)*/
public static double log10(double r) { return Math.log(r)/Math.log(10);}
/**gives 10^x*/
public static double p10(double r) { return Math.pow(10,r);}


/**return the closest lower among 1 2 and 5 and the decade in which we are . r positive
first value the left part of the scientific notation. Second value the power of 10.
*/
public static double[] closestLower125Positive(double r)
{
//calc the decade in which we are
double decade=(double)Math.floor(com.photonlyx.toolbox.math.Util.log10(r));
//then get the left part of the scientific notation
double lscientific=r/com.photonlyx.toolbox.math.Util.p10(decade);
//then get the closest lower among 1 2 and 5
double closest;
if (lscientific>=5) closest=5; else if (lscientific>=2) closest=2; else  closest=1;
double[] result=new double[2];
result[0]=closest;
result[1]=decade;
return result;
}

/**return the closest upperer among 1 2 and 5 and the decade in which we are. r positive
first value the left part of the scientific notation. Second value the power of 10.
*/
public static double[] closestUpperer125Positive(double r)
{
//calc the decade in which we are
double decade=(double)Math.floor(com.photonlyx.toolbox.math.Util.log10(r));
//then get the left part of the sceintific notation
double lscientific=r/com.photonlyx.toolbox.math.Util.p10(decade);
//then get the closest lower among 1 2 and 5
double closest;
if (lscientific<=1) closest=1; else if (lscientific<=2) closest=2; else  closest=5;
double[] result=new double[2];
result[0]=closest;
result[1]=decade;
return result;
}

/**
 * tells if s contains a double value
 * @param s
 * @return
 */
public static boolean isANumericalValue(String s)
{
	double val;
	try
		{
		val=new Double(s.replace(',','.')).doubleValue();
		return true;
		}
	catch (NumberFormatException e)
		{
		return false;
		}
}



public static void af(Object s)
{
System.out.println(s.toString());
//Global.log(getClass()+" "+s.toString());
}

}
