// Author Laurent Brunel

package com.photonlyx.toolbox.math.analyse;

import com.photonlyx.toolbox.math.function.Function1D2D;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DSource;


/**
function that gives the 2D linear interpolation of the Signal1D1D from the Signal1D1DSource son. 
The curve coordinates goes from 0 to 1 and respects the cartesian distances.
*/
public class LinearInterpolation2D extends  Function1D2D
{
//links
private Signal1D1D sdaq;

//params 
private String controlPointsRepartition;

//internal
private double[] tt;


public LinearInterpolation2D()
{

}


/**
create a function that linearly interpol the signal. 
Normalise the curve abcisse such that it goes from 0 to 1.
@param signalsource: the Signal1D1Dsource (assumed to be sorted) to interpol
@param scontrolPointsRepartition if "distance" the curve abscisse is calculated from the distance 
		between each point, 
	if "index" the curve abscisse is calculated from the index each point
	, if "distance*index" the curve abscisse is calculated from the product distance*index of each point
*/
public LinearInterpolation2D(Signal1D1D signal,String scontrolPointsRepartition)
{
controlPointsRepartition=scontrolPointsRepartition;
sdaq=signal;
work();
}




/**
calculate the curve abcisse for each sample
*/
public void work()
{
Signal1D1D signal=sdaq;
int n=signal.getDim();
if (n==0) return;
// System.out.println(getClass()+" "+n);
tt=new double[n];
tt[0]=0;

int cas=0;
if (controlPointsRepartition.compareTo("distance")==0) cas=0;
if (controlPointsRepartition.compareTo("index")==0) cas=1;
if (controlPointsRepartition.compareTo("distance*index")==0) cas=2;

for (int i=1;i<n;i++)
	{
	if (cas==0)
		{
		//sample the curve absisse
		double dx=signal.x(i)-signal.x(i-1);
		double dy=signal.value(i)-signal.value(i-1);
		tt[i]+=tt[i-1]+Math.sqrt(dx*dx+dy*dy);
		}
	
	//sample with the point index
	if (cas==1) tt[i]=i;
	
	//sample with pointindex*x
	if (cas==2) tt[i]+=i*(signal.x(i)-signal.x(0));
	
	}

//renormalise such that t goes from 0 to 1
for (int i=1;i<n;i++) tt[i]/=tt[n-1];
//for (int i=1;i<n;i++) System.out.println(tt[i]);

}





/**
gives a 2D linear  interpolation at any t value 
assume that the signal is x sorted
@param t the curve abcisse value
@return the linear interpolation x,y for this t, respecting cartesian distance
*/
public double[] f(double t)
{
//System.out.println(t);
Signal1D1D signal=sdaq.getSignal1D1D();
if (signal==null) return null;
int n=signal.getDim();
int index=0;
double[] val=new double[2];
if (n==0) return val;
if (t<0)
	{
	val[0]=signal.x(0);
	val[1]=signal.value(0);
	}
else if (t>tt[n-1])
	{
	val[0]=signal.x(n-1);
	val[1]=signal.value(n-1);	
	}
else for (int i=0;i<n-1;i++)
	if ((tt[i]<=t)&&(t<=tt[i+1])) 
		{
		index=i;
		double t1=tt[i];
		double t2=tt[i+1];
		double x1=signal.x(i);
		double x2=signal.x(i+1);
		double y1=signal.value(index);
		double y2=signal.value(index+1);
		//System.out.println("x1="+x1+" x2="+x2+" y1="+y1+" y2="+y2);
		val[0]=x1+(x2-x1)*(t-t1)/(t2-t1);
		val[1]=y1+(y2-y1)*(t-t1)/(t2-t1);
		break;
		}
//System.out.println(val);
return val;
}










}
