package com.photonlyx.toolbox.math.analyse;

import com.photonlyx.toolbox.math.function.usual.PolynomeN;
import com.photonlyx.toolbox.math.matrix.FullMatrix;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DSource;


/**
fit he polynome son to the data of the signal1D1D son with the least square method

*/
public class FitPolynomeN 
{
//links
private Signal1D1DSource daqs;
private PolynomeN p;



public FitPolynomeN()
{
}



/**
construct with the daq to fit
@param daq the data to fit 
@param polynome the polynome used to fit the data
*/
public FitPolynomeN(Signal1D1D daq,PolynomeN polynome)
{
p=polynome;
}



/**
 * fit polynome of order 
 * @param signal
 * @param order
 * @return the list of the (order+1) coefs of the polynome
 */
public static PolynomeN fitPolynomeN(Signal1D1D signal, int order)
	{
	int m=order+1;
	int n=signal.getDim();

	//calculate the design matrix A
	FullMatrix A=new FullMatrix(n,m);
	//the same weight is given to each data sample
	for (int l=0;l<n;l++)
		for (int c=0;c<m;c++)
			A.put(l,c,X(c,signal.x(l)));
	// System.out.println("A");
	// System.out.println(A);

	//calculate the b vector (y of the data)
	FullMatrix b=new FullMatrix(n,1);		
	for (int i=0;i<n;i++) b.put(i,0,signal.value(i));

	//calculate the normal matrix
	FullMatrix At=A.transpose();
	FullMatrix N=At.multiply(A);
	/*System.out.println("N");
	System.out.println(N);*/
			
	//calculate the right part of the system to solve
	FullMatrix Atb=At.multiply(b);
			
	int[] indx=new int[m];
	FullMatrix.decompose_lu(N,indx);//N is destroyed!
	FullMatrix.systeme_lu(N,indx,Atb);//now the solution is in AtB!

	double[] result = new double[m];

	for (int i=0;i<m;i++) result[i]=Atb.get(i,0);
	/*for (int i=0;i<m;i++) System.out.println(Atb.get(i,0));*/
		
	return new PolynomeN(result);
	}

/**
do the fitting
*/
public void fit()
{
Signal1D1D daq=daqs.getSignal1D1D();
double[] coefs = fitPolynomeN(daq,p.getOrder()).getCoefs();
for (int i=0;i<p.getOrder()+1;i++) p.setCoef(i,coefs[i]);
/*for (int i=0;i<m;i++) System.out.println(Atb.get(i,0));*/
		
}

/**
basis function
*/
private static double X(int j,double x)
{
return Math.pow(x,j);
}


public PolynomeN getPolynomeN(){return p;}

}