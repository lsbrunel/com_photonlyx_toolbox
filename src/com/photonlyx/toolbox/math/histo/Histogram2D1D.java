package com.photonlyx.toolbox.math.histo;

import com.photonlyx.toolbox.math.signal.Signal2D1D;



/**
histogram that receive 2D values
*/
public  class Histogram2D1D extends Signal2D1D implements Histogram
{
//params
private int nbHits;
private double samplingx,samplingy;


public  Histogram2D1D()
{
init( 0, 1, 0, 1, 100, 100);
}

public  Histogram2D1D(double xmin,double xmax,double ymin,double ymax,int dimx,int dimy)
{
init( xmin, xmax, ymin, ymax, dimx, dimy);
}

public void init(double xmin,double xmax,double ymin,double ymax,int dimx,int dimy)
{
super.init(xmin, xmax, ymin, ymax, dimx, dimy);
//here the data correspond to a cell, not a point:
samplingx=((xmax()-xmin())/dimx());
samplingy=((ymax()-ymin())/dimy());
nbHits=0;
}

/**
return the number of occurences acquired
*/
public int getNbOccurences()
{
return nbHits;
}

public int getNbHits() {
	return nbHits;
}

public void setNbHits(int nbHits) {
	this.nbHits = nbHits;
}

/**
add the propability dp at the occurence cell occ
*/
public void addOccurence(double[] occ,double dp)
{
//calculate the cell where to put the occurence
int cellx=(int)((occ[0]-xmin())/samplingx);
if ((cellx<0)||(cellx>=dimx())) return;

int celly=(int)((occ[1]-ymin())/samplingy);
if ((celly<0)||(celly>=dimy())) return;

setValue(cellx,celly,getValue(cellx,celly)+dp);
nbHits++;

//System.out.println("add occurence "+occ);
}

/**
add the propability dp at the occurence cell occ
*/
public void addOccurence(double x,double y,double dp)
{
//calculate the cell where to put the occurence
int cellx=(int)((x-xmin())/samplingx);
if ((cellx<0)||(cellx>=dimx())) return;

int celly=(int)((y-ymin())/samplingy);
if ((celly<0)||(celly>=dimy())) return;

setValue(cellx,celly,getValue(cellx,celly)+dp);
nbHits++;

//System.out.println("add occurence "+occ);
}

public void _annulate()
{
init( xmin(), xmax(), ymin(), ymax(), dimx(), dimy());
}





}





