package com.photonlyx.toolbox.math.histo;

import com.photonlyx.toolbox.math.signal.SignalDiscret1D1DAbstract;

public interface Histogram1D1DInterface  extends Histogram
{
public SignalDiscret1D1DAbstract getSignalDiscret1D1DAbstract();
public void addOccurence(double d,double dp);
public void init(int dim,double xmin,double xmax);
}
