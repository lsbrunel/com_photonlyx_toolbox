// Author Laurent Brunel


package com.photonlyx.toolbox.math.image;

import java.io.File;
import javax.swing.*;
import javax.swing.filechooser.*;

import com.photonlyx.toolbox.io.Utils;
import com.photonlyx.toolbox.util.Messager;


/**
file filter for png image
*/
public class ImageFileFilter extends FileFilter implements com.photonlyx.toolbox.io.FileFilterWithExtension
{


// Accept all directories and all gif, jpg, or tiff files.
public boolean accept(File f)
{
if (f.isDirectory())
	{
	return true;
	}

String sextension = Utils.getExtension(f);
if 	(
	(("png").equals(sextension))
	||	(("PNG").equals(sextension))
	||	(("jpg").equals(sextension))
	||	(("jpeg").equals(sextension))
	||	(("JPG").equals(sextension))
	||	(("gif").equals(sextension))
	||	(("GIF").equals(sextension))
	) return true;
else
	{
	return false;
    	}

}

/**
The description of this filter
@return The description of this filter
*/
public String getDescription()
{
return Messager.getString("png jpg gif files");
}

/**
The extension of this filter
@return The extension of this filter
*/
public static String getExtension()
{
return "png";
}

public String getFileExtension() 
	{
	return getExtension();
	}




}
