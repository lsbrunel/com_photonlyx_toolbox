package com.photonlyx.toolbox.math.image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;

import com.photonlyx.toolbox.math.function.usual.Line;
import com.photonlyx.toolbox.math.histo.Histogram1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1D;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;

/**
 * list of indexes of pixels forming a connected zone in the image
 * @author laurent
 *
 */
public class Zone implements Iterable<Integer>
{
	private Vector<Integer> l=new Vector<Integer>();
	//private CImageProcessing cim;
	private  int w,h;

	private static int whiteCode=Color.white.getRGB();
	private static int redCode=Color.white.getRGB();

	//static final Comparator<Integer> X_ORDER = new Comparator<Integer>() 
	//{
	//public int compare(Integer i1, Integer i2) 
	//	{
	//	int x1=i1%w;
	//	int x2=i1%w;
	//    double diff=x2-x1;
	//    int i=0;
	//    if (diff<0) i= -1;
	//    if (diff==0) i= 0;
	//    if (diff>0) i= 1;
	//    return i;
	//    }
	//};



	public Iterator<Integer> iterator() 
	{
		return l.iterator();
	}

	//public Zone(CImageProcessing cim)
	//{
	//this.cim=cim;
	//w=cim.w();
	//}

	//public Zone(int w)
	//{
	//this.w=w;
	//}

	public Zone(int w,int h)
	{
		this.w=w;
		this.h=h;
	}

	public void add(Integer i)
	{
		l.add(i);
	}

	public void add(Zone z)
	{
		for (int i:z) l.add(i);
	}

	//public CImageProcessing getImage()
	//{
	//return cim;
	//}
	//



	public int x(int index)
	{
		//w=cim.w();
		int y=index/w;
		return index-y*w;
	}

	public int y(int index)
	{
		return index/w;
	}

	//public int blue(int index)
	//{
	//BufferedImage bim=cim.getBufferedImage();
	//int[] pix=bim.getRGB(0, 0, bim.getWidth(),bim.getHeight(), null, 0, bim.getWidth());
	//return  new Color(pix[index]).getBlue();
	//}
	//
	//public int red(int index)
	//{
	//BufferedImage bim=cim.getBufferedImage();
	//int[] pix=bim.getRGB(0, 0, bim.getWidth(),bim.getHeight(), null, 0, bim.getWidth());
	//return  new Color(pix[index]).getRed();
	//}

	/**
	 * get the size in pixels
	 */
	public int size()
	{
		return l.size();
	}

	/**
	 * return the barycentre position
	 * @param w
	 * @return
	 */
	public double[] calcBarycentre()
	{
		double[] b=new double[2];
		//int w=cim.w();
		//int h=cim.h();
		int x,y;
		double n=this.size();
		for (int index:this)
		{
			y=index/w;
			x=index-y*w;
			b[0]+=x;
			b[1]+=y;
			//System.out.println("index="+index+" x="+x+" y="+y+" b="+b[0]+" "+b[1]);
		}
		b[0]/=n;
		b[1]/=n;
		//System.out.println(" b="+b[0]+" "+b[1]);
		return b;
	}

	/**
	 * return the mean radius (root mean square of pixel distance from barycentre)
	 * @param w width of image
	 * @return
	 */
	public double calcMeanRadius(double[] barycentre)
	{
		int x,y;
		double n=this.size();
		double r2=0;
		for (int index:this)
		{
			y=index/w;
			x=index-y*w;
			r2+=Math.pow(x-barycentre[0],2)+Math.pow(y-barycentre[1],2);
		}
		r2=Math.sqrt(r2/n);
		return r2;
	}


	/**
	 * cal the box inserting the zone
	 * @param xmin_xmax_ymin_ymax
	 * @return
	 */
	public void calcBox(int[] xmin_xmax_ymin_ymax)
	{
		int xmin=w*10,xmax=0,ymin=w*10,ymax=0;
		int x,y;
		for (int index:this)
		{
			y=index/w;
			x=index-y*w;
			if (x<xmin) xmin=x;
			if (x>xmax) xmax=x;
			if (y<ymin) ymin=y;
			if (y>ymax ) ymax=y;
		}
		xmin_xmax_ymin_ymax[0]=xmin;
		xmin_xmax_ymin_ymax[1]=xmax;
		xmin_xmax_ymin_ymax[2]=ymin;
		xmin_xmax_ymin_ymax[3]=ymax;
	}




	public String toString()
	{
		StringBuffer sb=new StringBuffer();
		for (int i:this) sb.append(i+" "+"("+x(i)+","+y(i)+") ");
		return sb.toString();
	}


	/**
	 * fit a line to the pixels and sort the pixel with the position wrt this line 
	 * pas fini ?
	 */
	public void sortAlongAline()
	{
		int x,y;
		Signal1D1DXY sig=new Signal1D1DXY();
		for (int index:this)
		{
			y=index/w;
			x=index-y*w;
			sig.addPoint(x,y);
		}
		Line line=sig.getFittedLine(false, false);

	}

	/**
	 * get the index of first point encountered close to (x1,y1)
	 * @param x1
	 * @param y1
	 * @param tolerance
	 * @return -1 if not close point found
	 */
	public int getIndexOfPointCloseTo(int w,int x1, int y1,int tolerance)
	{
		int x2,y2,d,index;
		for (int i=0;i<this.size();i++)
		{
			index=l.elementAt(i);
			y2=index/w;
			x2=index-y2*w;
			//distance between the 2 points
			d=(int)(Math.abs(x1-x2)+Math.abs(y1-y2));
			if (d<tolerance) return i;
		}
		return -1;
	}


	//
	//public void sortByX()
	//{
	//w=cim.w();
	////StringBuffer sb=new StringBuffer();for (int i:this) sb.append(i%w+" ");System.out.println(sb);
	//Collections.sort(this, X_ORDER);
	////StringBuffer sb2=new StringBuffer();for (int i:this) sb2.append(i%w+" ");System.out.println(sb2);
	//}


	/**
	 * max distance from a point in the zone to the zone barycentre
	 */
	public double maxDistanceFromCentre(double[] barycentre)
	{
		double d,m=0;
		int x,y;
		for (int i:this)
		{
			y=i/w;
			x=i-y*w;
			d=Math.pow(x-barycentre[0],2)+Math.pow(y-barycentre[1],2);
			if (d>m) m=d;
		}
		return Math.sqrt(m);
	}


	///**
	// * inflate the zone by one pixel
	// */
	//public void inflateq()
	//{
	//int[] pix=cim.getRawPixels();
	//for (int i:this)
	//	{
	//	
	//		pix[i]=Color.red.getRGB();
	//		
	//	}	
	//cim.setPixelsDirect(pix, cim.w(), cim.h());
	//}


	///**
	// * inflate the zone by one pixel
	// */
	//public void inflate()
	//{
	//System.out.println("before: "+this.size());
	//Vector<Integer> l1=getExternalPixels();
	//System.out.println(l1.size()+" external pixels");
	//for (int k:l1) this.add(k);
	//int[] pix=cim.getRawPixels();
	//for (int k:l1) pix[k]=redCode;
	//cim.setPixelsDirect(pix, cim.w(), cim.h());
	//}


	/**
	 * inflate the zone by one pixel
	 */
	public Zone getExternalPixels(CImageProcessing cim)
	{
		Zone l1=new Zone(w,h);
		//Zone z1=this.getAcopy();
		int[] pix=cim.getRawPixels();
		for (int i:this)
		{
			//z1.add(i);
			Vector<Integer> ns=cim.getNotNeighboursWithDiags(i, pix);
			if (ns.size()>1) 
			{
				//int y=i/w;
				//int x=i-w*y;		
				//System.out.print(" pix ("+x+","+y+") "+ns.size()+" not neighbours ");
				for (int k:ns) //if (pix[k]!=whiteCode)
				{
					//int y1=k/w;
					//int x1=k-w*y1;		
					//System.out.print(" pix ("+x1+","+y1+") "+pix[k]);
					//pix[k]=whiteCode;
					l1.add(k);
				}
				//System.out.println();
			}
		}	
		//System.out.println("white code="+whiteCode);
		//cim.setPixelsDirect(pix, cim.w(), cim.h());
		//z1.getImage().setPixelsDirect(pix, cim.w(), cim.h());
		return l1;
	}

	public Zone getAcopy()
	{
		Zone z=new Zone(w,h);
		return z;
	}

	public Integer elementAt(int i)
	{
		return l.elementAt(i);
	}

	public void add(int i, int index)
	{
		l.add(i,index);
	}

	public Zone translate(int dx,int dy)
	{
		Zone z1=new Zone(w,h);
		int x,y,x1,y1,index1;
		for (int i:this) 
		{
			y=i/w;
			x=i-y*w;
			x1=x+dx;
			y1=y+dy;
			index1=y1*w+x1;
			z1.add(index1);
		}
		return z1;
	}

	/**
	 * tells if this zone has the pixel of index index
	 * @param index
	 * @return
	 */
	public boolean has(int index)
	{
		for (int i:this) if (i==index) return true;
		return false;
	}


	private static double diff(byte[] im1,byte[] im2)
	{
		double d=0;
		for (int i=0;i<im1.length;i++) d+=im1[i]-im2[i]; 
		return d;
	}

	public static double difference(Zone z1,Zone z2)
	{
		int index1max=z1.getIndexMax();
		int index2max=z2.getIndexMax();
		int max=Math.max(index1max,index2max);
		byte[] im1=new byte[max+1];
		byte[] im2=new byte[max+1];
		System.out.println("max="+max+" length="+im1.length);
		for (int i:z1) 
		{
			System.out.println("zone1: "+i);
			im1[i]=1;
		}
		for (int i:z2) im2[i]=1;
		return diff(im1,im2);
	}



	public int getIndexMax()
	{
		int indexMax=-1;
		for (int i:this) if (i>indexMax) indexMax=i; 
		return indexMax;
	}


	public boolean touchTheBorder()
	{
		int x,y;
		for (int i:this) 
		{
			y=i/w;
			x=i-y*w;
			if ((x==0)||(x==w-1)||(y==0)||(y==h-1)) return true;
		}
		return false;
	}

	/**
	 * width of the image containing the zone
	 * @return
	 */
	public int getW() {
		return w;
	}

	/**
	 * height of the image containing the zone
	 * @return
	 */
	public int getH() {
		return h;
	}


	/**
	 * apply threshold (on brightness) on the zone only
	 * @param cimp
	 * @param threshold
	 */
	public void _threshold(CImageProcessing cimp,int threshold) 
	{
		int[] pix=cimp.getRawPixels();
		float[] hsbvals=new float[3];
		for (int i:this) 
		{	
			Color c=new Color(pix[i]);
			Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);		
			if (hsbvals[2]*255.0>threshold) pix[i]=Color.WHITE.getRGB();else  pix[i]=Color.BLACK.getRGB();
		}
		cimp.setPixelsDirect(pix, cimp.w(), cimp.h());
	}

	/**
	 * calc the mean brightness of the zone
	 * @param cimp
	 * @return
	 */
	public double meanBrightness(CImageProcessing cimp) 
	{
		int[] pix=cimp.getRawPixels();
		float[] hsbvals=new float[3];
		double mean=0;
		for (int i:this) 
		{
			Color c=new Color(pix[i]);
			Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
			mean+=hsbvals[2];
		}
		return mean/this.size()*255.0;
	}


	/**
	 * calc the mean hue of the zone (0-255)
	 * @param cimp
	 * @return
	 */
	public double meanHue(CImageProcessing cimp) 
	{
		int[] pix=cimp.getRawPixels();
		float[] hsbvals=new float[3];
		double mean=0;
		for (int i:this) 
		{
			Color c=new Color(pix[i]);
			Color.RGBtoHSB(c.getRed(), c.getGreen(), c.getBlue(), hsbvals);
			mean+=hsbvals[0];
		}
		return mean/this.size()*255.0;
	}

	public int getXmin()
	{
		int xmin=10000000;
		for (int i:this) 
		{
			int x=x(i);
			if (x<xmin) xmin=x;
		}
		return xmin;
	}
	/**
	 * get the pixel of the zone more at the left	
	 * @return
	 */
	public int getIndexOfMinX()
	{
		int xmin=10000000;
		int ii=0;
		for (int i:this) 
		{
			int x=x(i);
			if (x<xmin) 
			{
				xmin=x;
				ii=i;
			}
		}
		return ii;
	}

	/**
	 * get the pixel of the zone more at the right	
	 * @return
	 */
	public int getIndexOfMaxX()
	{
		int xmax=-1;
		int ii=0;
		for (int i:this) 
		{
			int x=x(i);
			if (x>xmax) 
			{
				xmax=x;
				ii=i;
			}
		}
		return ii;
	}

	public Zone getPixelsOfX(int x) 
	{
		Zone z=new Zone(w,h);
		for (int i:this) 
		{
			if (x(i)==x)z.add(i);
		}
		return z;
	}
	public Zone getPixelsOfY(int y) 
	{
		Zone z=new Zone(w,h);
		for (int i:this) 
		{
			if (y(i)==y) z.add(i);
		}
		return z;
	}

	public int getIndexOfMinY() 
	{
		int ymin=1000000000;
		int jj=0;
		for (int j:this) 
		{
			int y=y(j);
			if (y<ymin) 
			{
				ymin=y;
				jj=j;
			}
		}
		return jj;
	}

	public int getIndexOfMaxY() 
	{
		int ymax=-1;
		int jj=0;
		for (int j:this) 
		{
			int y=y(j);
			if (y>ymax) 
			{
				ymax=y;
				jj=j;
			}
		}
		return jj;
	}

	public Zones splitVertical(int x0) 
	{
		Zones zs=new Zones();
		Zone z1=new Zone(w,h);
		Zone z2=new Zone(w,h);
		zs.add(z1);
		zs.add(z2);
		for (int i:this)
		{
			int x=x(i);
			if (x<x0) z1.add(i);else z2.add(i);
		}
		return zs;
	}




}
