package com.photonlyx.toolbox.math.image;

import java.awt.Color;
import java.util.Vector;

import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.math.image.Zones;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.math.signal.SignalDiscret1D1D;

/**
 * image encoded in bytes
 * @author laurent
 *
 */

public class ByteImage 
{
	private static byte seenCode=(byte)0x80;	
	private static byte whiteCode=(byte)0xFF;	
	byte[] data;
	int w,h;

	public ByteImage()
	{
		this.w=0;
		this.h=0;
		data=new byte[w*h];
	}


	public ByteImage(int w,int h)
	{
		this.w=w;
		this.h=h;
		data=new byte[w*h];
	}

	public int w() {return w;}
	public int h() {return h;}

	public int data(int i) {

		return data[i];
	}


	public int get(int x,int y)
	{
		return data[x+w*y] & 0xFF;
	}
	public void set(int x,int y,byte b)
	{
		data[x+w*y]=b;
	}

	public byte[] getData() {return data;}

	/**
	 * copy data of raw2 into raw1 (raw1 and raw2 are supposed to have the same size !)	
	 * @param raw1
	 * @param raw2
	 */
	public static void copy(ByteImage raw1,ByteImage raw2)	
	{
		if ((raw1.w()!=raw2.w())||(raw1.h()!=raw2.h()))
		{
			raw1.data=new byte[raw2.w()*raw2.h()];
			raw1.w=raw2.w();
			raw1.h=raw2.h();
		}
		for (int k=0;k<raw1.w * raw1.h;k++)
		{
			raw1.data[k]=raw2.data[k];
		}
	}


	/**
	 * 
	 * @param raw1 the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 * @return the copy
	 */
	public static ByteImage getAcopy(ByteImage raw1)	
	{
		ByteImage raw2=new ByteImage(raw1.w,raw1.h);
		for (int k=0;k<raw1.w*raw1.h;k++)
		{
			raw2.data[k]=raw1.data[k];
		}
		return raw2;
	}


	/**
	 * add raw2 to raw1	
	 * @param raw1
	 * @param raw2
	 */
	public static void add(ByteImage raw1,ByteImage raw2)	
	{
		for (int k=0;k<raw1.w*raw1.h;k++)
		{
			raw1.data[k]+=raw2.data[k];
		}
	}

	/**
	 * subtract raw2 to raw1. raw1=raw1-raw2	
	 * @param raw1
	 * @param raw2
	 */
	public static void sub(ByteImage raw1,ByteImage raw2)	
	{
		int v1,v2,v;
		for (int k=0;k<raw1.w*raw1.h;k++)
		{
			v1=raw1.data[k] & 0xFF;
			//v1=(int)raw1[k];if (v1<0) v1+=255;

			v2=raw2.data[k] & 0xFF;
			//v2=(int)raw2[k];if (v2<0) v2+=255;
			v=v1-v2;
			if (v<0) raw1.data[k]=0;else raw1.data[k]=(byte)v;
		}
	}

	/**
	 * apply threshold to raw 
	 * @param raw1
	 * @param raw2
	 */
	public static void threshold(ByteImage raw,int threshold)	
	{
		int v;	
		for (int k=0;k<raw.w*raw.h;k++)
		{
			v=raw.data[k] & 0xFF;
			//v=(int)raw[k];if (v<0) v+=255;
			if (v<threshold) raw.data[k]=(byte)0;else raw.data[k]=(byte)255;
		}
	}

	public static int min(ByteImage raw)
	{
		int v,vmin=Integer.MAX_VALUE;	
		for (int k=0;k<raw.w*raw.h;k++)
		{
			v=raw.data[k] & 0xFF;
			if (v<vmin) vmin=v;
		}
		return vmin;
	}	
	public static int max(ByteImage raw)
	{
		int v,vmax=Integer.MIN_VALUE;	
		for (int k=0;k<raw.w*raw.h;k++)
		{
			v=raw.data[k] & 0xFF;
			if (v>vmax) vmax=v;
		}
		return vmax;
	}

	/**
	 * 
	 * @param pix the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 * @param xs x coordinate of top left corner of crop zone
	 * @param ys y coordinate of top left corner of crop zone
	 * @param ws width of crop zone
	 * @param hs height of crop zone
	 * @return the cropped byte image
	 */
	public static byte[] crop(ByteImage pix,int xs,int ys,int ws,int hs)
	{
		byte[] cropped=new byte[ws*hs];
		for (int ic=0;ic<ws;ic++) 
			for (int jc=0;jc<hs;jc++) 
			{
				cropped[ic+ws*jc]=pix.data[(xs+ic)+pix.w*(ys+jc)];
			}
		return cropped;
	}

	/**
	 * 
	 * @param pix the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 * @param sortBySize
	 * @return
	 */
	public  static Zones segmentation2(ByteImage pix,boolean sortBySize)
	{
		Zones zones=new Zones();
		for (int i=0;i<pix.w*pix.h;i++) 
		{
			if (pix.data[i]==whiteCode)
			{
				Zone zone=new Zone(pix.w,pix.h);
				zones.add(zone);
				//System.out.println("follow zone "+(zones.size()-1));
				followZone2(true,i,zone,zones,pix);
				//System.out.println("Zone "+(zones.size()-1)+" "+zone.size()+" pixels");
			}
		}
		//repaint all zones in white
		for (int k=0;k<pix.w*pix.h;k++) if (pix.data[k]==seenCode) pix.data[k]=whiteCode;

		//System.out.println(zones.size()+" zones");
		if (sortBySize) zones.sortBySizedescending();

		return zones;	
	}




	/**
	 * recursive method to follow the connected white pixels
	 * @param diag if true follow also the neighbors in diagonal directions
	 * @param k
	 * @param zone
	 * @param zones
	 * @param pix the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 */
	public  static void followZone2(boolean diag,int k,Zone zone,Zones zones,ByteImage pix)
	{
		Vector<Integer> v;
		Vector<Integer> pixels2visit=new Vector<Integer>();
		pixels2visit.add(k);
		while (pixels2visit.size()>0)
		{
			int i=pixels2visit.elementAt(0);

			//	System.out.print("Pixel "+i);
			//	System.out.print(": list of pixels to visit: ");
			//	for (int pp:pixels2visit) System.out.print(pp+" ");
			//	System.out.println();
			//	
			//mark as seen:
			pix.data[i]=seenCode;
			//add this pixel to the list:
			//if (zone.has(i)) System.out.println("pixels "+i+" already in zone");
			zone.add(i);
			pixels2visit.remove(0);
			if (diag) v=getNeighboursWithDiags(i,pix); else v=getNeighbours(i,pix); 

			//	System.out.print(v.size()+" neighbours: ");
			//	for (int pp:v) System.out.print(pp+" ");
			//	System.out.println();

			for (int pp:v) if (!pixels2visit.contains(pp)) pixels2visit.add(pp);

			//	System.out.print("Zone: ");
			//	for (int pp:zone) System.out.print(pp+" ");
			//	System.out.println();


		}


	}






	/**
	 * neighbours north south east west
	 * @param index
	 * @param pix the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 * @return
	 */
	private  static Vector<Integer> getNeighbours(int index,ByteImage pix)
	{
		Vector<Integer> v=new Vector<Integer>();
		int y=index/pix.w;
		int x=index-pix.w*y;
		int p;
		if (x>0) 
		{
			p=(x-1)+pix.w*(y);
			if (pix.data[p]==whiteCode) v.add(p);
		}
		if (x<pix.w-1) 
		{
			p=(x+1)+pix.w*(y);
			if (pix.data[p]==whiteCode) v.add(p);
		}
		if (y>0) 
		{
			p=(x)+pix.w*(y-1);
			if (pix.data[p]==whiteCode) v.add(p);
		}
		if (y<pix.h-1) 
		{
			p=(x)+pix.w*(y+1);
			if (pix.data[p]==whiteCode) v.add(p);
		}
		return v;
	}





	/**
	 * get Neighbors having red canal with value 255 north south east west and diags
	 * @param index pixel position
	 * @param pix the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 * @return
	 */
	public static Vector<Integer> getNeighboursWithDiags(int index,ByteImage pix)
	{
		Vector<Integer> v=new Vector<Integer>();
		int y=index/pix.w;
		int x=index-pix.w*y;
		int _index;
		//if (x>0) if (pix[(x-1)+w*(y)]==whiteCode) v.add((x-1)+w*(y));
		//if (x<w-1) if (pix[(x+1)+w*(y)]==whiteCode) v.add((x+1)+w*(y));
		//if (y>0) if (pix[(x)+w*(y-1)]==whiteCode) v.add((x)+w*(y-1));
		//if (y<h-1) if (pix[(x)+w*(y+1)]==whiteCode) v.add((x)+w*(y+1));
		if (x>0) 
		{
			_index=(x-1)+pix.w*(y);
			if (pix.data[_index]==whiteCode) v.add(_index);
		}
		if (x<pix.w-1) 
		{
			_index=(x+1)+pix.w*(y);
			if (pix.data[_index]==whiteCode) v.add(_index);
		}
		if (y>0) 
		{
			_index=(x)+pix.w*(y-1);
			if (pix.data[_index]==whiteCode) v.add(_index);
		}
		if (y<pix.h-1) 
		{
			_index=(x)+pix.w*(y+1);
			if (pix.data[_index]==whiteCode) v.add(_index);
		}
		//diagonal left up:
		if ((x>0)&&(y>0)) 
		{
			_index=(x-1)+pix.w*(y-1);
			if (pix.data[_index]==whiteCode) v.add(_index);
		}
		//diagonal right up:
		if ((x<pix.w-1)&&(y>0)) 
		{
			_index=(x+1)+pix.w*(y-1);
			if (pix.data[_index]==whiteCode) v.add(_index);
		}
		//diagonal left down:
		if ((x>0)&&(y<pix.h-1)) 
		{
			_index=(x-1)+pix.w*(y+1);
			if (pix.data[_index]==whiteCode) v.add(_index);
		}
		//diagonal right down:
		if ((x<pix.w-1)&&(y<pix.h-1)) 
		{
			_index=(x+1)+pix.w*(y+1);
			if (pix.data[_index]==whiteCode) v.add(_index);
		}
		return v;
	}



	/**
	 * load the frame of index imageIndex and put it in the CImage cim
	 * @param pix the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 * @param cim
	 */
	public static void transfertRawToCImage(ByteImage raw, CImage cim)
	{
		int[] pix=new int[raw.w*raw.h];
		//transfert the image to java color format,inverting y coord
		int index,val,alpha=255;
		for (int k=0;k<raw.w*raw.h;k++)
		{
			val=raw.data[k]&0xFF;
			pix[k]=(alpha << 24)| (val << 16) | (val << 8) | (val) ;
		}
		cim.setPixelsDirect(pix, raw.w, raw.h);
	}




	//
	///** 
	// * transfert data from raw to cim
	// * @param raw
	// * @param cimp
	// */
	//public static  void transfertRawToCImage(byte[] raw,int w,int h,CImage cimp)
	//{
	//if ( (cimp.w()*cimp.h())<raw.length) return;
	////transfert the image to java color format,inverting y coord
	//int index,val,alpha=255;
	//int[] pix=cimp.getRawPixelsFromImage();
	//for (int i=0;i<w;i++)
	//	for (int j=0;j<h;j++)
	//		{
	//		//index=i+(h0-1-j)*w0;
	//		index=i+j*w;
	//		//val=raw[index]&0xFF;
	//		val=(int)raw[index];if (val<0) val+=255;
	//		pix[index]=(alpha << 24)| (val << 16) | (val << 8) | (val) ;
	//		}
	//cimp.setPixelsDirect(pix, w, h);
	//}
	//





	/** 
	 * Transfer data from cim (red channel) to raw
	 * @param cim
	 * @param pix the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 */
	public static void transfertCImageToRaw(CImage cim,ByteImage raw)
	{
		raw.w=cim.w();
		raw.h=cim.h();
		raw.data=new byte[raw.w*raw.h];
		int n=raw.w*raw.h;
		int[] pix=cim.getRawPixelsFromImage();
		byte b;
		for (int i=0;i<n;i++) 
		{
			b=(byte)new Color(pix[i]).getRed();
			raw.data[i]=b;
		}
	}

	
	
	/** 
	 * Transfer data from im to raw. value lower than 0 are set to 0. Values higher than 255 are set to 255.
	 * @param im the image of double
	 */
	public static ByteImage transfertSignal2D1DToByteImage(Signal2D1D im)
	{
		ByteImage bi=new ByteImage(im.dimx(),im.dimy());
		int index;
		int v;
		for (int i=0;i<bi.w;i++) 
			for (int j=0;j<bi.h;j++) 
			{
				v=(int)im.getValue(i, j);
				if (v<0) v=0;
				if (v>255) v=255;
				//b=(byte)(v&0xFF);
				//System.out.print(b+" ");
				index=i+bi.w*j;
				bi.data[index]=(byte)(v);
			}
		return bi;
	}


	/** 
	 * Transfer data from im to raw. value lower than 0 are set to 0. Values higher than 255 are set to 255.
	 * @param im the image of double
	 */
	public static Signal2D1D transfertByteImageToSignal2D1D(ByteImage bi)
	{
		Signal2D1D sig=new Signal2D1D(bi.w(),bi.h());
		int index;
		for (int i=0;i<bi.w;i++) 
			for (int j=0;j<bi.h;j++) 
			{
				index=i+bi.w*j;
				sig.setVal(i, j, bi.data[index]&0xFF);
			}
		return sig;
	}


	/**
	 * calc the means along the columns in red channel
	 * @param pix the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 * @return
	 */
	public  static SignalDiscret1D1D meanY(ByteImage pix)
	{
		SignalDiscret1D1D proj= new SignalDiscret1D1D();
		meanY(pix,proj);
		return proj;
	}


	/**
	 * calc the means along the columns in red channel
	 * @param pix the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 * @param proj
	 */
	public static void meanY(ByteImage pix,SignalDiscret1D1D proj)
	{
		double[] meanY=new double[pix.w];
		for (int i=0;i<pix.w;i++) 
			for (int j=0;j<pix.h;j++) 
			{
				meanY[i]+=(int)(pix.data[i+pix.w*j]&0xFF);
			}
		proj.init1(0,pix.w,pix.w);
		for (int i=0;i<pix.w;i++) proj.setValue(i, meanY[i]/pix.h);
	}




	/**
	 * return a new smoothed image by averaging with a kernel of size kernelSize*kernelSize
	 * @param pix the raw byte array representing the image
	 * @param w width of the image
	 * @param h height of the image
	 * @param radius kernelSize
	 */
	public static byte[] smoothSquareKernel(ByteImage raw,int radius)
	{
		double r=0;
		int c=0;
		byte[] pix2=new byte[raw.w*raw.h];
		int i2,j2,index2;
		int code2;
		for (int i=0;i<raw.w;i++)
			for (int j=0;j<raw.h;j++)
			{
				r=0;
				c=0;
				for (int ii=-radius;ii<radius;ii++)
					for (int jj=-radius;jj<radius;jj++)
					{
						i2=i+ii;
						j2=j+jj;
						index2=j2*raw.w+i2;
						if ( (i2>=0) && (i2<raw.w) && (j2>=0) && (j2<raw.h) )
						{
							code2=raw.data[index2]&0xFF;
							r+=code2;
							c++;
						}
					}
				r/=c;
				pix2[j*raw.w+i]=(byte)r;
			}
		return pix2;
	}



	/**
	 *  subtract the image pix2 to pix1 and add an offset(to keep values >0)
	 * @param pix1 the image 
	 * @param w width
	 * @param h height
	 * @param pix2 the image to subtract
	 * @param offset
	 */
	public static void _subtract(ByteImage pix1,ByteImage pix2,int offset)
	{
		int r1,r2,r3;
		for (int k=0;k<pix1.w*pix1.h;k++) 
		{
			r1=pix1.data[k]&0xFF;
			r2=pix2.data[k]&0xFF;
			r3=r1-r2+offset;	
			if (r3<0) r3=0;
			if (r3>255) r3=255;
			pix1.data[k]=(byte)r3;
		}
	}


	public static double calcMean(ByteImage pix, int xc, int yc, int wc, int hc)
	{
		double mean=0;
		for (int ic=0;ic<wc;ic++) 
			for (int jc=0;jc<hc;jc++) 
			{
				mean+=pix.data[(xc+ic)+pix.w*(yc+jc)]&0xFF;
			}
		return mean/(wc*hc);
	}



	/**
	 * 
	 * @param bi image to resample
	 * @param w1 new width
	 * @param h1 new height
	 */
	public static ByteImage resample(ByteImage bi, int w1, int h1) 
	{
		ByteImage bi1=new ByteImage(w1,h1);
		double coefw= (double)w1/(double)bi.w;
		double coefh= (double)h1/(double)bi.h;
		for (int x1=0;x1<w1;x1++) 
			for (int y1=0;y1<h1;y1++) 
			{
				int oldi=(int)(x1/coefh);
				int oldj=(int)(y1/coefw);
				bi1.data[x1+w1*y1]= bi.data[oldi+bi.w*oldj];
			}
		return bi1;
	}

	/**
	 * set min to 0 and max to 256
	 */
	public void _equaliseHistogram() 
	{
		int minRed=min(this);
		int maxRed=max(this);
		double a=256.0/(maxRed-minRed);
		double b=-minRed*a;
		double r2;
		for (int k=0;k<w*h;k++) 
		{
			r2=(a*(data[k]&0xFF)+b);
			if (r2<0) r2=0;
			if (r2>255) r2=(byte)255;
			data[k]=(byte)r2;
		}
	}

}
