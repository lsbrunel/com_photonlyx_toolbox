package com.photonlyx.toolbox.math.image;


import java.awt.image.*;
import java.awt.*;


import java.io.*;


/**
some tools for image manipulation
*/
public class Utils
{
//internal
private	static float[] hsbvals=new float[3];





public static BufferedImage getBufferedImageFromComponent(Component comp)
	{
	int w=comp.getWidth();
	int h=comp.getHeight();
	BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_BGR);
	//BufferedImage bi = new BufferedImage(w,h,BufferedImage.TYPE_BYTE_INDEXED);
	Graphics2D big = bi.createGraphics();
	comp.paintAll(big);

	return bi;
	}

public static void saveBufferedImageToPNG(String directory, String filename, Image bi)
{
saveBufferedImageToPNG(directory+filename,bi);
}
/**
 * 
 * @param directory
 * @param filename
 * @param bi
 * @param compLevel
 */
public static void saveBufferedImageToPNG(String directory, String filename, Image im,int compLevel)
{
saveBufferedImageToPNG(directory+File.separator+filename,im,compLevel);
}

public static void saveBufferedImageToPNG  (String fullFilename, Image bi)
{
saveBufferedImageToPNG(fullFilename,bi,1);
}

/**
 * save Image as png
 * @param fullFilename
 * @param bi
 * @param compLevel 0 to 9
 */
public static void saveBufferedImageToPNG  (String fullFilename, Image bi,int compLevel)
	{
	File file=null;
	FileOutputStream out=null;
	//System.out.println("Save  png image:  "+fullFilename);
	try
		{
		file = new File(fullFilename);
		out = new FileOutputStream(file);
		}
	catch(Exception e)
		{
		System.out.println(e);
		return;
		}


	// encodes bi as a PNG data stream
	PngEncoder encoder = new PngEncoder(bi,PngEncoder.NO_ALPHA, PngEncoder.FILTER_NONE ,compLevel);
	try
		{
		byte[] pngbytes=encoder.pngEncode();
		out.write( pngbytes );
		}
	catch(Exception e)
		{
		System.out.println(e);
		}
	try
		{
		out.close();
		}
	catch(Exception e)
		{
		System.out.println(e);
		return;
		}
	}

public static void saveAsImagePNG(String directory, String filename,Component comp)
{
BufferedImage bi = getBufferedImageFromComponent(comp);

saveBufferedImageToPNG(directory, filename, bi);
}






/**
get the pixels and size of an image
@param img the image to read
@param dimension the returned size: dimension[0] has the width and dimension[1] has the height
@return the pixels array in a one dimensional array 
*/
public static int[] getRawPixelsFromImage(Image img,int[] dimension)
{
Frame f=new Frame();
try
	{
	MediaTracker tracker = new MediaTracker(f);
	tracker.addImage(img, 0);
	tracker.waitForID(0);
        }
catch (Exception e) {}

int w = img.getWidth(f);
int h = img.getHeight(f);
int[] pixels = new int[w * h];
dimension[0]=w;
dimension[1]=h;
PixelGrabber pg = new PixelGrabber(img, 0, 0, w, h, pixels, 0, w);
try
	{
	pg.grabPixels();
        }
catch (InterruptedException e)
	{
        System.err.println("interrupted waiting for pixels!");
        return null;
        }
return pixels;
}

/**
return an image form raw pixels
@param pixels an array of int of siez w*h
@param w width of the image
@param h height of the image
@return the created Image 
*/
public static Image  getImageFromRawPixels(int[] pixels,int w,int h)
{
return Toolkit.getDefaultToolkit().createImage(new MemoryImageSource(w,h,pixels,0,w));
}
	
	



/**
 *  convert the byte into a java encoded image
 * @param byBufImg
 * @param javapix
 */	
public static  void convertByteChannelToJavaImage(byte byBufImg[],int[] javapix)
{
//convert to java Image
int alpha=255;
for (int i=0;i<byBufImg.length;i++)
	{
	int y=byBufImg[i]&0xff;
	//if (byBufImg[i]==0) System.out.println(byBufImg[i]);
	int red=y;
	int green=y;
	int blue=y;
	int coul=(alpha << 24)| (red << 16) | (green << 8) | (blue) ;
	javapix[i]=coul;
	}
}




/**
 * return a byte array with one of channels (R G B H S B alpha)
 * @param im
 * @param channel (R G B H S B or alpha)
 * @return
 */
public static  byte[]  getChannel (Image im,String channel)
{
int c=0;
if (channel.compareTo("R")==0) c=0;
if (channel.compareTo("G")==0) c=1;
if (channel.compareTo("B")==0) c=2;
if (channel.compareTo("alpha")==0) c=3;
if (channel.compareTo("H")==0) c=4;
if (channel.compareTo("S")==0) c=5;
if (channel.compareTo("B")==0) c=6;
int[] dimension=new int[2];

int[] pix=Utils.getRawPixelsFromImage(im,dimension);
int w=dimension[0];
int h=dimension[1];


byte[] im2=new byte[w*h];
for (int index= 0; index < w*h; index++)
		{
		int val=0;
		//int index=w*(h-1-j)+i;
		switch (c)
			{
			case 0:
			val=R(pix[index]);
			break;
			case 1:
			val=G(pix[index]);
			break;
			case 2:
			val=B(pix[index]);
			break;
			case 3:
			val=alpha(pix[index]);
			break;
			case 4:
			val=H(pix[index]);
			break;
			case 5:
			val=S(pix[index]);
			break;
			case 6:
			val=Br(pix[index]);
			}
		im2[index]=(byte)val;
		}

return im2;
}

public static int alpha(int pix)
{
return ((pix>>24)&0xff);
}

public static int R(int pix)
{
return  ((pix>>16)&0xff);
}

public static int G(int pix)
{
return  ((pix>>8)&0xff);
}

public static int B(int pix)
{
return  ((pix)&0xff);
}

///**Hue*/
//public static byte H(int pix)
//{
//Color.RGBtoHSB((int)R(pix),(int)G(pix),(int)B(pix),hsbvals);
//return (byte)(hsbvals[0]*255);
//}
//
///**saturation*/
//public static byte S(int pix)
//{
//Color.RGBtoHSB((int)R(pix),(int)G(pix),(int)B(pix),hsbvals);
//return (byte)(hsbvals[1]*255);
//}

///**brightness*/
//public static byte Br(int pix)
//{
//Color.RGBtoHSB((int)R(pix),(int)G(pix),(int)B(pix),hsbvals);
//return (byte)(hsbvals[2]*255);
//}



/**Hue*/
public static int H(int pix)
{
Color.RGBtoHSB((int)R(pix),(int)G(pix),(int)B(pix),hsbvals);
return (int)(hsbvals[0]*255);
}

/**saturation*/
public static int S(int pix)
{
Color.RGBtoHSB((int)R(pix),(int)G(pix),(int)B(pix),hsbvals);
return (int)(hsbvals[1]*255);
}

/**brightness*/
public static int Br(int pix)
{
Color.RGBtoHSB((int)R(pix),(int)G(pix),(int)B(pix),hsbvals);
return (int)(hsbvals[2]*255);
}




}
