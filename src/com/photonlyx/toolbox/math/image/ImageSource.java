package com.photonlyx.toolbox.math.image;

import java.awt.*;

/**
can provide an image
*/
public interface ImageSource
{
public Image getImage();

public CImage getCImage();

}
