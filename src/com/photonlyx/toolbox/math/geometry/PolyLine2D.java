package com.photonlyx.toolbox.math.geometry;

import java.util.Iterator;
import java.util.Vector;




public class PolyLine2D implements Iterable<Vecteur2D>
{
private Vector<Vecteur2D> list=new Vector<Vecteur2D>();
private Vecteur2D box2DMin=new Vecteur2D();
private Vecteur2D box2DMax=new Vecteur2D();

public PolyLine2D()
{
// TODO Auto-generated constructor stub
}

public void addPoint(Vecteur2D v)
{
list.add(v);
updateBox(v);
}


public void addPoint( int i,Vecteur2D p)
{
list.add(i,p);
updateBox(p);
}

public Vector<Vecteur2D> getList()
{
return list;
}

public Iterator<Vecteur2D> iterator() 
{
return list.iterator();
}

public Vecteur2D first()
{
if (list.size()>0) return list.firstElement();else return null;
}

public Vecteur2D last()
{
if (list.size()>0) return list.lastElement();else return null;
}


/**
 * get the distance of a point to the polyline
 * @param point
 * @return
 */
public double getDistance( double[] point)
{
double[] intersection=new double[2];
double dmin=1e30;
for (int i=0;i<list.size()-1;i++)
	{
	Vecteur2D p1=list.elementAt(i);
	Vecteur2D p2=list.elementAt(i+1);
	double d=Segment2D.segdist2D(p1.getCoord(), p2.getCoord(),point,intersection);
	if (d<dmin) dmin=d;
	}
return dmin;
}

/**
 * update the occupancy box
 */
public void updateBox()
{
box2DMin.affect(1e30,1e30);;
box2DMax.affect(-1e30,-1e30);;
for (Vecteur2D v:list)
	{
	if (v.x()<box2DMin.x()) box2DMin.setX(v.x());
	if (v.y()<box2DMin.y()) box2DMin.setY(v.y());
	if (v.x()>box2DMax.x()) box2DMax.setX(v.x());
	if (v.y()>box2DMax.y()) box2DMax.setY(v.y());
	}
}
/**
 * update the occupancy box, just considering this new point
 */

public void updateBox(Vecteur2D newPoint)
{
box2DMin.affect(1e30,1e30);;
box2DMax.affect(-1e30,-1e30);;
Vecteur2D v=newPoint;
	{
	if (v.x()<box2DMin.x()) box2DMin.setX(v.x());
	if (v.y()<box2DMin.y()) box2DMin.setY(v.y());
	if (v.x()>box2DMax.x()) box2DMax.setX(v.x());
	if (v.y()>box2DMax.y()) box2DMax.setY(v.y());
	}
}



/**
 * check if the polyline intersect with the segment2D made by the 2 points
 * (just return the first intersection found)
 * @param p1
 * @param p2
 * @return null if no intersection
 */
public Vecteur2D intersect2D(Vecteur2D p1, Vecteur2D p2,int[] indexInter)
{
indexInter[0]=-1;
//System.out.println("Polyline: check intersection with seg: ["+p1+"-"+p2+"]");
//System.out.println("Polyline: boxmin="+box2DMin+" boxmax="+box2DMax);
if ( (p1.x()>box2DMax.x()) & (p2.x()>box2DMax.x()) ) return null;
if ( (p1.x()<box2DMin.x()) & (p2.x()<box2DMin.x()) ) return null;
if ( (p1.y()>box2DMax.y()) & (p2.y()>box2DMax.y()) ) return null;
if ( (p1.y()<box2DMin.y()) & (p2.y()<box2DMin.y()) ) return null;
for (int i=0;i<list.size()-1;i++)
	{
	Vecteur2D _p1=list.elementAt(i);
	Vecteur2D _p2=list.elementAt(i+1);
	//System.out.println(i+" Check with seg: ["+_p1+"-"+_p2+"]");
	if ( (p1.x()>_p1.x()) & (p1.x()>_p2.x()) & (p2.x()>_p1.x()) & (p2.x()>_p2.x()) ) continue;
	if ( (p1.x()<_p1.x()) & (p1.x()<_p2.x()) & (p2.x()<_p1.x()) & (p2.x()<_p2.x()) ) continue;
	if ( (p1.y()>_p1.y()) & (p1.y()>_p2.y()) & (p2.y()>_p1.y()) & (p2.y()>_p2.y()) ) continue;
	if ( (p1.y()<_p1.y()) & (p1.y()<_p2.y()) & (p2.y()<_p1.y()) & (p2.y()<_p2.y()) ) continue;
	//System.out.println("Could intersect ....");
	Segment2D seg1=new Segment2D(p1,p2);
	Segment2D seg2=new Segment2D(_p1,_p2);
	//check segment intersection
	Line2D l1=new Line2D(seg1);
	Line2D l2=new Line2D(seg2);
	Vecteur2D inter=l1.getIntersectionPointWith(l2);
	if ((seg1.intersectionIsOnSegment(inter))&(seg2.intersectionIsOnSegment(inter)))//segments intersect
		{
		indexInter[0]=i;
		return inter;
		}
	}
return null;
}



}
