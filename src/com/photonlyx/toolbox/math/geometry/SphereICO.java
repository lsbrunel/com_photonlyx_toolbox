package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;

public class SphereICO implements Object3DFrame,TriangleMeshSource
{
	private double r=1;//sphere radius 
	private int recursionLevel=0;
	private TriangleMesh localMesh=new TriangleMesh();
	private TriangleMesh globalMesh=new TriangleMesh();
	private Frame frame=new Frame();//local frame 

	public SphereICO()
	{
		init();
	}

	public SphereICO(double r, int recursionLevel)
	{
		this.r=r;
		this.recursionLevel=recursionLevel;
		init();
	}


	@Override
	public void updateGlobal()
	{
		//calc the global coordinates:
		for (int i=0;i<globalMesh.nbTriangles();i++)
		{
			frame.global(globalMesh.getTriangle(i),localMesh.getTriangle(i));
		}
	}

	//public TriangleMesh getTriangles() {return tris;}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		globalMesh.checkOccupyingCube(cornerMin, cornerMax);

	}
	@Override
	public void draw(Graphics g, Projector proj) 
	{
		globalMesh.draw(g, proj);
	}

	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		return globalMesh.getDistanceToScreen(proj);
	}
	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		for (Triangle3D tri:globalMesh) 
			if (tri.isOn(p, proj)) return true;
		return false;
	}


	@Override
	public Frame getFrame()
	{
		return frame;
	}



	@Override
	/**
	 * 
	 */
	public TriangleMesh getTriangles() {
		return globalMesh;
	}





	private Vecteur correctRadius(Vecteur p)
	{
		double length = p.norme();
		return new Vecteur(p.x()/length*r, p.y()/length*r, p.z()/length*r);
	}



	//  http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
	//calc the local frame triangles
	public void init()
	{
		localMesh.removeAllElements();
		// create 12 vertices of a icosahedron
		double t = (1.0 + Math.sqrt(5.0)) / 2.0;//nombre d'or

		Vector<Vecteur> v=new Vector<Vecteur>();
		v.add(correctRadius(new Vecteur(-1,  t,  0)));
		v.add(correctRadius(new Vecteur( 1,  t,  0)));
		v.add(correctRadius(new Vecteur(-1, -t,  0)));
		v.add(correctRadius(new Vecteur( 1, -t,  0)));

		v.add(correctRadius(new Vecteur( 0, -1,  t)));
		v.add(correctRadius(new Vecteur( 0,  1,  t)));
		v.add(correctRadius(new Vecteur( 0, -1, -t)));
		v.add(correctRadius(new Vecteur( 0,  1, -t)));

		v.add(correctRadius(new Vecteur( t,  0, -1)));
		v.add(correctRadius(new Vecteur( t,  0,  1)));
		v.add(correctRadius(new Vecteur(-t,  0, -1)));
		v.add(correctRadius(new Vecteur(-t,  0,  1)));


		// create 20 triangles of the icosahedron
		//var faces = new List<TriangleIndices>();
		Vector<Triangle3D> tris=  new Vector<Triangle3D>();

		// 5 faces around point 0
		tris.add(new Triangle3D(v.elementAt(0), v.elementAt(11), v.elementAt(5)));
		tris.add(new Triangle3D(v.elementAt(0), v.elementAt(5), v.elementAt(1)));
		tris.add(new Triangle3D(v.elementAt(0), v.elementAt(1), v.elementAt(7)));
		tris.add(new Triangle3D(v.elementAt(0), v.elementAt(7), v.elementAt(10)));
		tris.add(new Triangle3D(v.elementAt(0), v.elementAt(10), v.elementAt(11)));

		// 5 adjacent faces 
		tris.add(new Triangle3D(v.elementAt(1), v.elementAt(5), v.elementAt(9)));
		tris.add(new Triangle3D(v.elementAt(5), v.elementAt(11), v.elementAt(4)));
		tris.add(new Triangle3D(v.elementAt(11), v.elementAt(10), v.elementAt(2)));
		tris.add(new Triangle3D(v.elementAt(10), v.elementAt(7), v.elementAt(6)));
		tris.add(new Triangle3D(v.elementAt(7), v.elementAt(1), v.elementAt(8)));

		// 5 faces around point 3
		tris.add(new Triangle3D(v.elementAt(3), v.elementAt(9), v.elementAt(4)));
		tris.add(new Triangle3D(v.elementAt(3), v.elementAt(4), v.elementAt(2)));
		tris.add(new Triangle3D(v.elementAt(3), v.elementAt(2), v.elementAt(6)));
		tris.add(new Triangle3D(v.elementAt(3), v.elementAt(6), v.elementAt(8)));
		tris.add(new Triangle3D(v.elementAt(3), v.elementAt(8), v.elementAt(9)));

		// 5 adjacent faces 
		tris.add(new Triangle3D(v.elementAt(4), v.elementAt(9), v.elementAt(5)));
		tris.add(new Triangle3D(v.elementAt(2), v.elementAt(4), v.elementAt(11)));
		tris.add(new Triangle3D(v.elementAt(6), v.elementAt(2), v.elementAt(10)));
		tris.add(new Triangle3D(v.elementAt(8), v.elementAt(6), v.elementAt(7)));
		tris.add(new Triangle3D(v.elementAt(9), v.elementAt(8), v.elementAt(1)));



		// refine triangles
		for (int i = 0; i < recursionLevel; i++)
		{
			//var faces2 = new List<TriangleIndices>();
			Vector<Triangle3D> faces2 = new Vector<Triangle3D>();
			for (Triangle3D tri:tris)
			{
				// replace triangle by 4 triangles
				Vecteur a = Vecteur.getMiddlePoint(tri.p1(), tri.p2());
				Vecteur b = Vecteur.getMiddlePoint(tri.p2(), tri.p3());
				Vecteur c = Vecteur.getMiddlePoint(tri.p3(), tri.p1());
				Vecteur a2=correctRadius(a);
				Vecteur b2=correctRadius(b);
				Vecteur c2=correctRadius(c);
				faces2.add(new Triangle3D(tri.p1().copy(), a2, c2));
				faces2.add(new Triangle3D(tri.p2().copy(), b2, a2));
				faces2.add(new Triangle3D(tri.p3().copy(), c2, b2));
				faces2.add(new Triangle3D(a2, b2, c2));
			}
			//tris = faces2;
			// done, now add triangles to mesh
			//oreach (var tri in faces)
			for (Triangle3D tri:faces2)
			{
				localMesh.add(tri);
			}
		}


		for (Triangle3D tri:localMesh) globalMesh.add(tri.copy());
		updateGlobal();

	}


	public String saveToOBJ() 
	{
		return globalMesh.saveToOBJ();
	}




	public static void main(String[] args) 
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();
		graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);


		SphereICO s=new SphereICO(3,1);
		s.getFrame()._translate(new Vecteur(1,0,0));
		s.updateGlobal();

//		//add to a set:
//		Object3DColorSet set=new Object3DColorSet();
//		set.add(s.getTriangles());
//		graph3DPanel.addColorSet(set);
		
		//add directly to the graph 3D:
		graph3DPanel.add(s.getTriangles());
		

		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());
	}




}





