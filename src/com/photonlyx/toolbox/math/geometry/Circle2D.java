package com.photonlyx.toolbox.math.geometry;

public class Circle2D 
{
private Vecteur2D centre;
private double r;//radius

public Circle2D(double xc,double yc, double r)
{
centre=new Vecteur2D(xc,yc);
this.r=r;	
}

public Circle2D(Vecteur2D centre, double r)
{
this.centre=centre;	
this.r=r;	
}

public static Circle2D getCircleFrom3Points(Vecteur2D p1,Vecteur2D p2,Vecteur2D p3)
{
Line2D l1=new Segment2D(p1,p2).getBisectorLine();
Line2D l2=new Segment2D(p2,p3).getBisectorLine();
Vecteur2D i=l1.getIntersectionPointWith(l2);
double radius=p1.subn(i).norme();
return new Circle2D(i,radius);
}

}
