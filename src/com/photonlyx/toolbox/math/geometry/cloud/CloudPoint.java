package com.photonlyx.toolbox.math.geometry.cloud;

import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Vecteur;

public class CloudPoint implements XMLstorable, Object3D
{
	private Vecteur p;
	private Voxel voxel;
	private static int sizeOnScreen=2;
	private Vector<CloudPoint> segments=null;

	public CloudPoint()
	{
	}

	public CloudPoint(Vecteur p)
	{
		this.p=p;
	}


	public Vecteur getP()
	{
		return p;
	}

	public Voxel getVoxel()
	{
		return voxel;
	}

	public void setVoxel(Voxel v){voxel=v;}

	/**
	 * get the  neighbors closer than maxDistance
	 * @param maxDistance
	 * @return
	 */
	public Cloud getNeighbors(Cloud cloud,double maxDistance)
	{
		//first locate the closest voxels:
		Vector<Voxel> list=cloud.getVoxelsCloseTo(getP(),maxDistance);
		//second look inside these voxels
		Cloud cn=new Cloud();
		for (Voxel vox:list) 
		{
			for (CloudPoint cp:vox.getCloudPoints()) 
				if (Vecteur.distance(cp.getP(),getP())<=maxDistance) 
					cn.add(cp);
		}
		return cn;
	}


	/**
	 * get the  neighbors closer than maxDistance
	 * @param cloud 
	 * @param neihgbors
	 * @param distances distances of the points of the neighbors cloud
	 * @param maxDistance
	 * @return
	 */
	public void getNeighbors(Cloud cloud,Cloud neighbors,Vector<Double> distances,double maxDistance)
	{
		neighbors.removeAllElements();
		distances.removeAllElements();
		//first locate the closest voxels:
		Vector<Voxel> list=cloud.getVoxelsCloseTo(getP(),maxDistance);
		//second look inside these voxels
		double dist;
		for (Voxel vox:list) 
		{
			for (CloudPoint cp:vox.getCloudPoints())
			{
				dist= Vecteur.distance(cp.getP(),getP());
				if (dist<=maxDistance) 
				{
					neighbors.add(cp);
					distances.add(dist);
				}
			}
		}
		//System.out.println(neighbors.size()+" neighbors");
	}

	public void addSegment(CloudPoint cp)
	{
		if (segments==null) segments=new Vector<CloudPoint>();
		segments.add(cp);
	}

	public void removeAllSegments()
	{
		if (segments!=null) segments.removeAllElements();
	}
	/**
	 * tells the cloud point is connected to this by a segment
	 * @param cp
	 * @return
	 */
	public boolean isConnectedTo(CloudPoint cp)
	{
		if (segments==null) return false;
		if (segments.contains(cp))return true;
		return false;
	}



	@Override
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		el.setAttribute("px",p.x() );
		el.setAttribute("py", p.y());
		el.setAttribute("pz",p.z() );
		return el;
	}



	@Override
	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;
		double x,y,z;
		x=xml.getDoubleAttribute("px",0);	
		y=xml.getDoubleAttribute("py",0);	
		z=xml.getDoubleAttribute("pz",0);
		p=new Vecteur(x, y, z);
	}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
	{
		for (int k=0;k<3;k++) 
		{
			double r=p.coord(k);
			if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
			if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
		}	

	}

	
	@Override
	public void draw(Graphics g, Projector proj)
	{
		double[] cooScreen=new double[2];
		cooScreen=proj.calcCoorEcran(p);
		g.fillRect((int)(cooScreen[0]-sizeOnScreen/2),(int)( cooScreen[1]-sizeOnScreen/2), sizeOnScreen, sizeOnScreen);
		//System.out.println(p+"    "+cooScreen[0]+" "+cooScreen[1]);

		if (segments!=null)
		{
			double[] cooScreen1;
			for (CloudPoint cp:segments)
			{
				cooScreen1=proj.calcCoorEcran(cp.getP());
				g.drawLine((int)cooScreen[0], (int)cooScreen[1],(int) cooScreen1[0], (int)cooScreen1[1]);
			}
		}
	}
	
	


	@Override
	public double getDistanceToScreen(Projector proj)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public CloudPoint getACopy()
	{
		return new CloudPoint(this.getP());
	}


	public boolean isNaN()
	{
		boolean b=false;
		for (int i=0;i<3;i++) b=b||(new Double(p.coord(i)).isNaN()); 
		return b;
	}

	@Override
	public boolean isOn(Point p, Projector proj)
	{
		// TODO Auto-generated method stub
		return false;
	}



}
