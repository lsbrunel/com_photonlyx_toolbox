package com.photonlyx.toolbox.math.geometry;

import java.awt.Graphics;
import java.awt.Point;
import java.io.File;
import java.util.Vector;

import com.photonlyx.toolbox.math.function.usual2D1D.Gaussian;
import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.threeD.gui.Graph3DWindow;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.txt.TextFiles;

/**
 * mesh of triangles having a local frame
 * @author laurent
 *
 */
public class TriangleMeshFrame  implements Object3DFrame,TriangleMeshSource
{
	protected Frame frame=new Frame();
	protected TriangleMesh local=new TriangleMesh();
	protected TriangleMesh global=new TriangleMesh();


	public TriangleMeshFrame()
	{
	}

	public TriangleMeshFrame(TriangleMesh t)
	{
		local=t;
		createGlobalTriangles();
	}

	@Override
	public Frame getFrame() 
	{
		return frame;
	}

	public void add(Triangle3D t) 
	{
		local.add(t);
		Triangle3D tglobal=t.copy();
		global.add(tglobal);
		frame.global(tglobal,t);
	}
	
	public void add(Quadrilatere3D q) 
	{
		Triangle3D t1=new Triangle3D(q.p1(),q.p2(),q.p3());
		local.add(t1);
		Triangle3D t2=new Triangle3D(q.p1(),q.p3(),q.p4());
		local.add(t2);
		Triangle3D tglobal1=t1.copy();
		Triangle3D tglobal2=t2.copy();
		global.add(tglobal1);
		global.add(tglobal2);
		frame.global(tglobal1,t1);
		frame.global(tglobal2,t2);
	}
	

	private void createGlobalTriangles()
	{
		//create global triangles
		global.removeAllElements();
		for (Triangle3D t:local) 
		{
			Triangle3D t2=new Triangle3D(t.p1().copy(),t.p2().copy(),t.p3().copy());
			global.add(t2);
		}
	}

	@Override
	/**
	 * update coord of global triangles
	 */
	public void updateGlobal() 
	{
		for (int i=0;i<global.nbTriangles();i++)
		{
			frame.global(global.getTriangle(i),local.getTriangle(i));
		}
	}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) {
		global.checkOccupyingCube(cornerMin, cornerMax);

	}

	@Override
	public void draw(Graphics g, Projector proj) {
		global.draw(g, proj);

	}

	@Override
	public double getDistanceToScreen(Projector proj) {
		return global.getDistanceToScreen(proj);
	}

	@Override
	public boolean isOn(Point p, Projector proj) {
		return global.isOn(p, proj);
	}

	public void readOBJ(String s)
	{
		local.readOBJ(s);
		frame.copy(new Frame());		
		createGlobalTriangles();
	}
	
	/**
	 * load mesh from OBJ ascii file
	 * @param path
	 * @param file
	 */
	public void readOBJ(String path, String file)
	{
		String s=TextFiles.readFile(path+File.separator+file).toString();
		local.readOBJ(s);
		frame.copy(new Frame());		
		createGlobalTriangles();
	}


	public String saveToOBJ()
	{
		return global.saveToOBJ();
	}
	
	public void saveToOBJfile(String path, String file)
	{
		TextFiles.saveString(path+File.separator+file,this.saveToOBJ(),false);
	}
	

	public void readSTL(String s)
	{
		local.readSTL(s);
		frame.copy(new Frame());
		createGlobalTriangles() ;		
	}
	public void readSTLasciiFile(String path,String filename)
	{ 
		String s=TextFiles.readFile(path+File.separator+filename).toString();
		this.readSTL(s);
		frame.copy(new Frame());
		createGlobalTriangles() ;		
	}
	public void readSTLinBinaryFile(String path,String filename)
	{ 
		local.updateFromBinarySTLfile(path+File.separator+filename);
		frame.copy(new Frame());
		createGlobalTriangles() ;		
	}

	public String saveToSTL()
	{
		return global.saveToSTL();
	}
	
	public void saveToSTLfile(String path, String file)
	{
		TextFiles.saveString(path+File.separator+file,this.saveToSTL(),false);
	}
	
	public void readSignal2D1D(Signal2D1D im)
	{
		local.importSignal2D1D(im);
		frame.copy(new Frame());
		createGlobalTriangles() ;		
	}


	@Override
	public TriangleMesh getTriangles() {
		return global;
	}


	public Vector<Segment3D> getIntersectedSegments(Plane plane)
	{
		TriangleMesh mesh=this.getTriangles();
		Vector<Segment3D> list=new	Vector<Segment3D>();
		for (int i=0;i<mesh.nbTriangles();i++)
		{
			Triangle3D t=mesh.getTriangle(i);
			Segment3D seg=t.getIntersectedSegment(plane);
			if (seg!=null) list.add(seg);
		}
		return list;
	}

	



public TriangleMesh getLocal() {
		return local;
	}

	public TriangleMesh getGlobal() {
		return global;
	}

public static void main(String[] args)
{
	Signal2D1D im=new Signal2D1D(100,100);
//	Gaussian f=new Gaussian(0.1,0.3,0.5,0.5);
//	im.fillWithFonction(f);
	
	String path="/home/laurent/fabserver/cloud1/photonlyx/projets/2022_05_17_Joconde/2022_11_22_couche_rayée";
	String filename="peinture_rayee.png";
	double w=8;
	double dz=0.05;
	im.loadFromImage(new CImage(path,filename,false));
	double scaleXY=w/im.dimx();
	im.setxmin(0);
	im.setxmax(im.dimx()*scaleXY);
	im.setymin(0);
	im.setymax(im.dimy()*scaleXY);
	im._multiply(dz/(im.zmax()-im.zmin()));
	im._flipAroundX();
	
	TriangleMeshFrame mesh=new TriangleMeshFrame();
	mesh.local.importSignal2D1D(im,0.05);
	mesh.frame.copy(new Frame());
	mesh.createGlobalTriangles() ;
	//rotate around Y:
	mesh.getFrame()._rotate(new Vecteur(0,Math.PI/2,0));
	mesh.getFrame()._translate(new Vecteur(0,-w/2,-w/2));
	//translate:
	mesh.getFrame()._translate(new Vecteur(-0.06,0,0));
	//rotate around y:
	//mesh.getFrame()._rotate(new Vecteur(0,Math.PI,0,0));
	mesh.updateGlobal();
	
	//Graph3DWindow g3d=new Graph3DWindow();
	//g3d.getGraph3DPanel().add(mesh);
	
	mesh.saveToOBJfile(path,filename.replace("png","obj"));
	
}

public void addLocalTriangles(TriangleMesh triangles) 
{
	local.add(triangles);
	this.createGlobalTriangles();
}


}
