package com.photonlyx.toolbox.math.geometry;

import java.awt.Graphics;
import java.awt.Point;



public interface Object3D 
{
/**
 * update the boundaries vectors cornerMin and cornerMax if the object is out of it
 * @param cornerMin
 * @param cornerMax
 */
public void checkOccupyingCube(Vecteur cornerMin,Vecteur cornerMax);
public void draw(Graphics g,Projector proj);
public double getDistanceToScreen(Projector proj);
public boolean isOn(Point p,Projector proj);

}
