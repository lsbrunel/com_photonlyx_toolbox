package com.photonlyx.toolbox.math.geometry;

import java.util.Iterator;
import java.util.Vector;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;

public class TriangleMeshMatrix2D extends  Shape   implements XMLstorable,Iterable<Triangle3D>
{
double x,y,z;
double ax,ay,az;
private Vecteur centre=new Vecteur();
private Vecteur X;//x axis of the base plane
private Vecteur Y;//y axis of the base plane
private Vecteur Z;//z axis of the base plane
private String filename;//file stl with the triangles
private Vector<Triangle3D> list=new Vector<Triangle3D>();
private Vecteur pminLocal,pmaxLocal;//boundary box
private double sx,sy;//pixel size
private int w,h;//matrix size
private Triangle3D[][] matrix1,matrix2;//the matrices of triangles (2 triangles by pixels)
private Vecteur lastIntersection;

public TriangleMeshMatrix2D()
{
centre=new Vecteur(x,y,z);
Rotator rot=new Rotator(ax,ay,az);
X=rot.turn(new Vecteur(1,0,0));
Y=rot.turn(new Vecteur(0,1,0));
Z=rot.turn(new Vecteur(0,0,1));
}


private void init()
{
centre=new Vecteur(x,y,z);
Rotator rot=new Rotator(ax,ay,az);
X=rot.turn(new Vecteur(1,0,0));
Y=rot.turn(new Vecteur(0,1,0));
Z=rot.turn(new Vecteur(0,0,1));
//readSTLFile(Global.path+filename);
Signal2D1D s2=new Signal2D1D();
s2.readRawFloatImage(Global.path+filename);
convertRawImage(s2);
}

@Override
public double distance(Vecteur pos, Vecteur dir)
{
Vecteur i=intersection(pos,dir);
return pos.sub(i).norme(); 
}

public Vecteur intersection(Vecteur pos, Vecteur dir)
{
//return intersectionUsingMatrix(pos,dir);
return intersectionBruteForce(pos,dir);
}



/**
 * find intersection in spiral starting with i0,j0
  * @param pos
 * @param dir
 * @param i0 starting pixel x
 * @param j0 starting pixel y
 * @return
 */
public Triangle3D findIntersectedTriangleSpiral(Vecteur pos, Vecteur dir,int i0,int j0)
{
int k=1;int sign=1;
int dx,dy;
int i1,j1;
Triangle3D t;
for (;;)
	{
	dx=k*sign;
	dy=0;
	i1=i0+dx;
	j1=j0+dy;
	//System.out.println("spiral k="+k+" ("+dx+","+dy+")"+" ("+i1+","+j1+")");
	if ((i1<0)||(j1<0)||(i1>=w)||(j1>=h)) return null;
	t=getIntersectedTriangleInAPixel(i1,j1,pos,dir);
	if (t!=null) return t;
	
	dx=0;
	dy=-k*sign;
	i1=i0+dx;
	j1=j0+dy;
	//System.out.println("spiral k="+k+" ("+dx+","+dy+")"+" ("+i1+","+j1+")");
	if ((i1<0)||(j1<0)||(i1>=w)||(j1>=h)) return null;
	t=getIntersectedTriangleInAPixel(i1,j1,pos,dir);
	if (t!=null) return t;
	
	k++;
	sign=-sign;
	}


}



/**
 * brute force intersection
 * */
public Triangle3D findIntersectedTriangleBruteForce(Vecteur pos, Vecteur dir)
{
//System.out.println(getClass()+" findIntersectedTriangleBruteForce");
//System.out.println(getClass()+" Check intersection:");
//System.out.println(list.size()+" triangles");
Vector<Triangle3D> v=new Vector<Triangle3D>();
Vector<Vecteur> i=new Vector<Vecteur>();
Vector<Double> dis=new Vector<Double>();
for (Triangle3D t:list)
	{
	Vecteur ii=t.intersection(pos, dir);
	if (ii!=null) 
		{
		v.add(t);
		i.add(ii);
		dis.add(pos.sub(ii).norme());
		}
	}

//if no triangle intersect the line, return null
if (v.size()==0) return null;
else //check which intersection is the closest 
	{
	double r=1e30;
	int index=-1;
	for (int j=0;j<v.size();j++)
		{
		double dd=dis.elementAt(j);
		if (dd<r) 
			{
			r=dd;
			index=j;
			}
		}
	lastIntersection=i.elementAt(index);
	return v.elementAt(index);
	}
}



/**
 *  find intersected triangle by a ray, using the matrix arrangement trick 
 * */
public Triangle3D findIntersectedTriangleUsingMatrix(Vecteur pos, Vecteur dir)
{
//System.out.println(getClass()+" findIntersectedTriangleUsingMatrix");
//take the intersection with the base plane
Vecteur int0=Plane.intersectionPlaneLine(centre, Z, pos, dir);
//local coordinates of i0 in the base plane frame:
double x0=int0.mul(X);
double y0=int0.mul(Y);
//find the pixel
int i0=(int)Math.floor((x0-pminLocal.x())/sx);
int j0=(int)Math.floor((y0-pminLocal.y())/sy);
if ((i0<0)||(j0<0)||(i0>=w)||(j0>=h)) 
	{
	lastIntersection=int0;	
	return null;
	}
Triangle3D t=getIntersectedTriangleInAPixel(i0,j0,pos,dir);
if (t!=null) return t;
//here could use a kd tree algorithm to find the intersected triangle:
//....

//then go to brute force !
//return this.findIntersectedTriangleBruteForce(pos, dir);

//use spiral search:
return this.findIntersectedTriangleSpiral(pos, dir,i0,j0);
}


private Triangle3D getIntersectedTriangleInAPixel(int i0,int j0,Vecteur pos, Vecteur dir)
{
//take the triangle 1 of the pixel:
Triangle3D t01=matrix1[i0][j0];
//check if this is the good triangle:
Vecteur p1=t01.intersection(pos, dir);
if (p1!=null) 
	{
	lastIntersection=p1;	
	return t01;
	}
//take the triangle 2 of the pixel:
Triangle3D t02=matrix2[i0][j0];
//check if this is the good triangle:
Vecteur p2=t02.intersection(pos, dir);
if (p2!=null) 
	{
	lastIntersection=p2;	
	return t02;
	}
return null;
}

public Vecteur intersectionBruteForce(Vecteur pos, Vecteur dir)
{
Triangle3D t=this.findIntersectedTriangleBruteForce(pos, dir);
if (t==null) return Plane.intersectionPlaneLine(centre, Z, pos, dir);
else return lastIntersection;
}
/**
 * intersection using matrices
 * @param pos
 * @param dir
 * @return
 */
public Vecteur intersectionUsingMatrix(Vecteur pos, Vecteur dir)
{
this.findIntersectedTriangleUsingMatrix(pos, dir);
return lastIntersection;

}



private void createVoxelsTree()
{
Voxel vtop=new Voxel(null,pminLocal,pmaxLocal);

int i1,i2,i3;
int j1,j2,j3;

i1=0;
i2=w/2;
i3=w-1;

j1=0;
j2=h/2;
j3=h-1;




}

private void createVoxelsTreeRecursive(Voxel voxel,int i1,int i2,int j1,int j2)
{
int dx=i2-i1;
int dy=j2-j1;

if (dx>dy)//voxel bigger in x than in y
	{
	if ((i2-i1)<2)
		{
		//separate the voxel in 2 voxels along x
		int i3=(i1+i2)/2;
		double x1=pminLocal.x()+i1*sx;
		double x2=pminLocal.x()+i2*sx;
		double x3=pminLocal.x()+i3*sx;
		double y1=pminLocal.y()+j1*sy;
		double y2=pminLocal.y()+j2*sy;
		Vecteur pmin1=new Vecteur(x1,y1,pminLocal.z());
		Vecteur pmax1=new Vecteur(x3,y2,pmaxLocal.z());
		Voxel v1=new Voxel(voxel,pmin1,pmax1);
		Vecteur pmin2=new Vecteur(x3,y1,pminLocal.z());
		Vecteur pmax2=new Vecteur(x2,y2,pmaxLocal.z());
		Voxel v2=new Voxel(voxel,pmin2,pmax2);
		voxel.setSons(v1,v2);
		createVoxelsTreeRecursive(v1,i1,i3,j1,j2);
		createVoxelsTreeRecursive(v2,i3,i2,j1,j2);
		}
	}
else//voxel bigger in y than in x
	{	
	if ((j2-j1)<2)
		{
		//separate the voxel in 2 voxels along y
		int j3=(j1+j2)/2;
		double x1=pminLocal.x()+i1*sx;
		double x2=pminLocal.x()+i2*sx;
		double y1=pminLocal.y()+j1*sy;
		double y2=pminLocal.y()+j2*sy;
		double y3=pminLocal.y()+j3*sy;
		Vecteur pmin1=new Vecteur(x1,y1,pminLocal.z());
		Vecteur pmax1=new Vecteur(x2,y3,pmaxLocal.z());
		Voxel v1=new Voxel(voxel,pmin1,pmax1);
		Vecteur pmin2=new Vecteur(x1,y3,pminLocal.z());
		Vecteur pmax2=new Vecteur(x2,y2,pmaxLocal.z());
		Voxel v2=new Voxel(voxel,pmin2,pmax2);
		voxel.setSons(v1,v2);
		createVoxelsTreeRecursive(v1,i1,i2,j1,j3);
		createVoxelsTreeRecursive(v2,i1,i2,j3,j2);
		}
	}


}



@Override
public Vecteur centre()
{
return centre;
}

@Override
public Vecteur normal(Vecteur i)
{
Triangle3D t=this.findIntersectedTriangleUsingMatrix(i, Z);
if (t!=null) return t.getNormal();
//if no triangle intersect the line, return intersection with base plane
else return Z;
}

/*
public  void readSTLFile(String stlFullFileName)
{
System.out.println(getClass()+" open "+stlFullFileName);
String s=TextFiles.readFile(stlFullFileName).toString();
readSTL(s);
}


private  void readSTL(String stl)
{
list.removeAllElements();
//Triangle3DSet set=new Triangle3DSet();
String[] lines=StringSource.readLines(stl);
int lineNb=0;
Definition def;
if (new Definition(lines[lineNb]).word(0).compareTo("solid")!=0)
	{
	Messager.messErr(getClass()+" Can't find solid in first line");
	return ;
	}
lineNb++;//go to 1rst facet
while (new Definition(lines[lineNb]).word(0).compareTo("endsolid")!=0)
	{
	if (new Definition(lines[lineNb]).word(0).compareTo("facet")!=0)
		{
		Messager.messErr(getClass()+" Line "+lineNb+" Can't find facet in:"+lines[lineNb]);
		return ;
		}
	lineNb++;//go to outer loop
	lineNb++;//go to 1rst vertex
	Vecteur[] p=new Vecteur[3];
	int index=0;
	while ((def=new Definition(lines[lineNb])).word(0).compareTo("endloop")!=0)
		{
		double x=new Double(def.word(1)).doubleValue();
		double y=new Double(def.word(2)).doubleValue();
		double z=new Double(def.word(3)).doubleValue();
		p[index]=new Vecteur(x,y,z);
		index++;
		lineNb++;//go to next vertex or to end loop
		}
	lineNb++;//go to endfacet
	Triangle3D t=new Triangle3D(p[0],p[1],p[2]);
	list.add(t);
	lineNb++;//go to facet or endsolid
	//System.out.println(lineNb+" "+lines[lineNb]);
	}
}

*/

@Override
public XMLElement toXML()
{
XMLElement el = new XMLElement();
el.setName(getClass().getSimpleName().toString().replace("class ", ""));
el.setDoubleAttribute("x",centre.x());
el.setDoubleAttribute("y",centre.y());
el.setDoubleAttribute("z",centre.z());
el.setDoubleAttribute("ax",ax);
el.setDoubleAttribute("ay",ay);
el.setDoubleAttribute("az",az);
el.setAttribute("filename",filename);
return el;
}

@Override
public void updateFromXML(XMLElement xml)
{
filename=xml.getStringAttribute("filename");
x=xml.getDoubleAttribute("x",0);
y=xml.getDoubleAttribute("y",0);
z=xml.getDoubleAttribute("z",0);
ax=xml.getDoubleAttribute("ax",0);
ay=xml.getDoubleAttribute("ay",0);
az=xml.getDoubleAttribute("az",0);
init();
}

public  void convertRawImage(Signal2D1D im)
{
w=im.dimx()-1;
h=im.dimy()-1;
matrix1=new Triangle3D[w][h];
matrix2=new Triangle3D[w][h];
pminLocal=new Vecteur(im.xmin(),im.ymin(),im.zmin());
pmaxLocal=new Vecteur(im.xmax(),im.ymax(),im.zmax());
sx=((im.xmax()-im.xmin())/(w));
sy=((im.ymax()-im.ymin())/(h));
for (int i=0;i<w;i++)
	for (int j=0;j<h;j++)
		{
		double x=im.xmin()+i*sx;
		double y=im.ymin()+j*sy;
		double z1=im.z(i, j);
		double z2=im.z(i+1, j);
		double z3=im.z(i+1, j+1);
		double z4=im.z(i, j+1);
		Vecteur p1=new Vecteur(x,y,z1);
		Vecteur p2=new Vecteur(x+sx,y,z2);
		Vecteur p3=new Vecteur(x+sx,y+sy,z3);
		Vecteur p4=new Vecteur(x,y+sy,z4);
		Triangle3D t1=new Triangle3D(p1,p2,p4);
		Triangle3D t2=new Triangle3D(p2,p3,p4);
		matrix1[i][j]=t1;
		matrix2[i][j]=t2;
		list.add(t1);
		list.add(t2);
		}
}


public Iterator<Triangle3D> iterator() {
	return list.iterator();
}





}
