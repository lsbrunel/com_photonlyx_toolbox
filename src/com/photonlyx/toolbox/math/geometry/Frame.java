package com.photonlyx.toolbox.math.geometry;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import com.photonlyx.toolbox.math.matrix.FullMatrix;

public class Frame implements Object3D
{
	private Vecteur[] axis=new Vecteur[3];//axis X, Y and Z
	private Vecteur O=new Vecteur();//centre
	
	private double axisLength=2;

	public Frame()
	{
		axis[0]=new Vecteur(1,0,0);
		axis[1]=new Vecteur(0,1,0);
		axis[2]=new Vecteur(0,0,1);
		O=new Vecteur(0,0,0);
	}

	public Frame(Vecteur centre,Vecteur X,Vecteur Y,Vecteur Z)
	{
		axis[0]=X;
		axis[1]=Y;
		axis[2]=Z;
		O=centre;
	}

	public void init()
	{
		axis[0]=new Vecteur(1,0,0);
		axis[1]=new Vecteur(0,1,0);
		axis[2]=new Vecteur(0,0,1);
		O=new Vecteur(0,0,0);
	}



	public Vecteur axis(int coord)
	{
		return axis[coord];
	}
	public Vecteur getAxis(int coord)
	{
		return axis[coord];
	}
	public Vecteur x()
	{
		return axis[0];
	}
	public Vecteur y()
	{
		return axis[1];
	}
	public Vecteur z()
	{
		return axis[2];
	}


	public void setAxis(int coord,Vecteur v)
	{
		axis[coord]=v;
	}

	public Vecteur getCentre(){return O;}

	/** 
	 * copy centre to c
	 * @param c
	 */
	public void setCentre(Vecteur c){O.copy(c);}


	/**
	 *to change the coordinates from frame (X,Y,Z) to frame (X',Y',Z').
	 * if a point P has coordinates (x,y,z) in frame (X,Y,Z) ,
	 * 
	 * @param the frame (X',Y',Z') expressed in (X,Y,Z)
	 * ([0]: vector X' expressed in frame (X,Y,Z))
	 * ([1]: vector Y' expressed in frame (X,Y,Z))
	 * ([2]: vector Z' expressed in frame (X,Y,Z))
	 * 
	 * @return the frame (X,Y,Z) expressed in (X',Y',Z')
	 */
	public  Frame invertFrameCoordinates()
	{
		FullMatrix m=new FullMatrix(3,3);
		m.affect(0,0,axis[0].x()); m.affect(0,1,axis[0].y()); m.affect(0,2,axis(0).z());
		m.affect(1,0,axis[1].x()); m.affect(1,1,axis[1].y()); m.affect(1,2,axis[1].z());
		m.affect(2,0,axis[2].x()); m.affect(2,1,axis[2].y()); m.affect(2,2,axis[2].z());
		FullMatrix m2=FullMatrix.inverse_lu3(m);
		Frame frame2=new Frame();
		frame2.axis[0]=new Vecteur(m2.el(0, 0),m2.el(0, 1),m2.el(0, 2));
		frame2.axis[1]=new Vecteur(m2.el(1, 0),m2.el(1, 1),m2.el(1, 2));
		frame2.axis[2]=new Vecteur(m2.el(2, 0),m2.el(2, 1),m2.el(2, 2));
		return frame2;
	}

	/**
	 * return a Vecteur with coordinates expressed in the new Frame
	 * change the coordinates  from (x,y,z), expressed in (X,Y,Z), to (x',y',z') expressed in (X',Y',Z')
	 * @param Vecteur with coordinates expressed in the old Frame
	 * @return
	 */
	public Vecteur newCoordinates(Vecteur vold)
	{
		Vecteur v2=new Vecteur(axis[0].mul(vold),axis[1].mul(vold),axis[2].mul(vold));
		return v2;
	}

	/**
	 * vnew has the global coordinates
	 * @param vGlobal
	 * @param vold
	 */
	public void global(Vecteur vGlobal,Vecteur vLocal)
	{
		Vecteur v1=axis[0].scmul(vLocal.x());
		v1._add(axis[1].scmul(vLocal.y()));
		v1._add(axis[2].scmul(vLocal.z()));
		v1._add(O);
		vGlobal.copy(v1);
	}

	/**
	 * change the coordinates to the one expressed in the new Frame
	 * change the coordinates  from (x,y,z), expressed in (X,Y,Z), to (x',y',z') expressed in (X',Y',Z')
	 * @param Vecteur with coordinates expressed in this Frame
	 * @return
	 */
	public void _newCoordinatesBAAD(Vecteur v)
	{
		v.u[0]=axis[0].mul(v);
		v.u[1]=axis[1].mul(v);
		v.u[2]=axis[2].mul(v);
		v._add(O);
	}


	/**
	 * change the coordinates to the one expressed in the new Frame
	 * change the coordinates  from (x,y,z), expressed in (X,Y,Z), to (x',y',z') expressed in (X',Y',Z')
	 * @param Vecteur with coordinates expressed in this Frame
	 * @return
	 */
	public void _newCoordinates(Vecteur v)
	{
		Vecteur v1=axis[0].scmul(v.x());
		v1._add(axis[1].scmul(v.y()));
		v1._add(axis[2].scmul(v.z()));
		v1._add(O);
		v.affect(v1);
	}

	/**
	 * change the coordinates to the one expressed in the new Frame
	 * change the coordinates  from (x,y,z), expressed in (X,Y,Z), to (x',y',z') expressed in (X',Y',Z')
	 * @param Vecteur with coordinates expressed in this Frame
	 * @return
	 */
	public void _newCoordinatesVect(Vecteur v)
	{
		Vecteur v1=axis[0].scmul(v.x());
		v1._add(axis[1].scmul(v.y()));
		v1._add(axis[2].scmul(v.z()));
		v.affect(v1);
	}


	/**
	 * change the coordinates of the frame from local to global
	 * @param vold
	 */
	public void _newCoordinates(Frame f)
	{
		for (int i=0;i<3;i++) 
		{
			this._newCoordinatesVect(f.axis[i]);	
		}
		this._newCoordinates(f.getCentre());
	}


	/** compute a change of coordinates
	 * get the Vecteur coords from its coords expressed in the frame (X,Y,Z)
	 * @param vnew third vector of the frame
	 * @return a new vector with the old coordinates
	 */
	public Vecteur oldCoordinates(Vecteur vnew)
	{
		Vecteur v=new Vecteur();
		for (int i=0;i<3;i++)  v.u[i]=vnew.u[0]*axis[0].coord(i)+vnew.u[1]*axis[1].coord(i)+vnew.u[2]*axis[2].coord(i);
		return v;
	}

	public  void global(Triangle3D tnew,Triangle3D told)
	{
		for (int j=0;j<3;j++) global(tnew.p[j],told.p[j]);
	}


	public  Triangle3D newCoordinates(Triangle3D told)
	{
		Vecteur[] ps=new Vecteur[3];
		for (int j=0;j<3;j++) ps[j]=newCoordinates(told.p(j));
		return new Triangle3D(ps);
	}

	public void _newCoordinates(Quadrilatere3D q1) {
		for (int j=0;j<4;j++) _newCoordinates(q1.p(j));

	}

	public  void _newCoordinates(Triangle3D told)
	{
		for (int j=0;j<3;j++) _newCoordinates(told.p(j));
	}


	public  String toString()
	{
		String s="";
		s+="center="+O.toString()+"\n";
		s+="X'="+axis[0].x()+" X + "+ axis[0].y()+" Y + "+ axis[0].z()+" Z"+"\n";
		s+="Y'="+axis[1].x()+" X + "+ axis[1].y()+" Y + "+ axis[1].z()+" Z"+"\n";
		s+="Z'="+axis[2].x()+" X + "+ axis[2].y()+" Y + "+ axis[2].z()+" Z"+"\n";
		return s;
	}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void draw(Graphics g, Projector proj)
	{
		this.draw(g, proj, Color.black, axisLength);
	}



	public void draw(Graphics g, Projector proj,Color color,double axisSize)
	{
		g.setColor(color);
		double a=axisSize;
		Vecteur p1;
		Vecteur p2;
		double[] cooScreen2;
		double[] cooScreen1;
		p1=axis[0].scmul(a).addn(axis[1].scmul(a)).addn(O);
		p2=axis[0].scmul(-a).addn(axis[1].scmul(a)).addn(O);
		cooScreen1=proj.calcCoorEcran(p1);
		cooScreen2=proj.calcCoorEcran(p2);
		g.drawLine((int)cooScreen1[0],(int)cooScreen1[1], (int)cooScreen2[0],(int)cooScreen2[1]);

		p1=axis[0].scmul(-a).addn(axis[1].scmul(a)).addn(O);
		p2=axis[0].scmul(-a).addn(axis[1].scmul(-a)).addn(O);
		cooScreen1=proj.calcCoorEcran(p1);
		cooScreen2=proj.calcCoorEcran(p2);
		g.drawLine((int)cooScreen1[0],(int)cooScreen1[1],(int) cooScreen2[0],(int)cooScreen2[1]);

		p1=axis[0].scmul(-a).addn(axis[1].scmul(-a)).addn(O);
		p2=axis[0].scmul(a).addn(axis[1].scmul(-a)).addn(O);
		cooScreen1=proj.calcCoorEcran(p1);
		cooScreen2=proj.calcCoorEcran(p2);
		g.drawLine((int)cooScreen1[0],(int)cooScreen1[1], (int)cooScreen2[0],(int)cooScreen2[1]);

		p1=axis[0].scmul(a).addn(axis[1].scmul(-a)).addn(O);
		p2=axis[0].scmul(a).addn(axis[1].scmul(a)).addn(O);
		cooScreen1=proj.calcCoorEcran(p1);
		cooScreen2=proj.calcCoorEcran(p2);
		g.drawLine((int)cooScreen1[0],(int)cooScreen1[1], (int)cooScreen2[0],(int)cooScreen2[1]);

		//x axis:
		p1=axis[0].scmul(0).addn(axis[1].scmul(0)).addn(O);
		p2=axis[0].scmul(a).addn(axis[1].scmul(0)).addn(O);
		cooScreen1=proj.calcCoorEcran(p1);
		cooScreen2=proj.calcCoorEcran(p2);
		g.drawLine((int)cooScreen1[0],(int)cooScreen1[1], (int)cooScreen2[0],(int)cooScreen2[1]);

		//Z axis
		p1=O;
		p2=axis[2].scmul(a).addn(O);
		cooScreen1=proj.calcCoorEcran(p1);
		cooScreen2=proj.calcCoorEcran(p2);
		g.drawLine((int)cooScreen1[0],(int)cooScreen1[1], (int)cooScreen2[0],(int)cooScreen2[1]);


	}


	@Override
	public double getDistanceToScreen(Projector proj)
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public void copy(Frame f)
	{
		for (int i=0;i<3;i++)  axis[i].copy(f.axis(i));
		this.getCentre().copy(f.getCentre());

	}

	@Override
	public boolean isOn(Point p, Projector proj)
	{
		// TODO Auto-generated method stub
		return false;
	}

	public void _rotate(Vecteur angles)
	{
		Rotator r=new Rotator(angles);
		//for (int i=0;i<3;i++) 
		r._turn(this);
	}

	public void _scale(Vecteur scale) 
	{
		for (int i=0;i<3;i++)  axis[i]._scmul(scale.coord(i));
	}

	public void _translate(Vecteur dr)
	{
		O._add(dr);
	}


	public double getAxisLength() {
		return axisLength;
	}

	public void setAxisLength(double axisLength) {
		this.axisLength = axisLength;
	}

	/**
	 * rotate the frame such that X axis points to a view point
	 */
	public void rotateSuchXpoints(Vecteur viewPoint)
	{
		Vecteur framex=viewPoint.sub(O);
		if (framex.norme()==0) return;
		framex._normalise();
		Vecteur z=new Vecteur(0,0,1);
		Vecteur framey=z.vectmul(framex);
		framey._normalise();
		Vecteur framez=framex.vectmul(framey);
		this.setAxis(0,framex);
		this.setAxis(1,framey);
		this.setAxis(2,framez);
	}

	/**
	 * rotate the frame such that X axis points to a view point
	 */
	public void rotateSuchZpoints(Vecteur viewPoint)
	{
		Vecteur z=viewPoint.sub(O);
		Frame f=buildAframeHavingZ(z);
		this.copy(f);
//		Vecteur framex,framey,framez;
//		framez=z;
//		if (framez.norme()==0) return;
//		framez._normalise();
//		Vecteur x=new Vecteur(1,0,0);
//		framey=framez.vectmul(x);
//		if (framey.norme()!=0)
//		{		
//			framey._normalise();
//			framex=framey.vectmul(framez);
//		}
//		else
//		{
//			Vecteur y=new Vecteur(0,1,0);
//			framex=y.vectmul(framez);
//			framex._normalise();
//			framey=framez.vectmul(framex);
//		}
//		this.setAxis(0,framex);
//		this.setAxis(1,framey);
//		this.setAxis(2,framez);
	}
	public void buildSuchThatZaxisIs(Vecteur z)
	{
		Frame f=buildAframeHavingZ(z);
		this.copy(f);
	}
	
	public static Frame buildAframeHavingZ(Vecteur z1)
	{
		Vecteur framex,framey,framez;
		framez=z1.getNormalised();
		if (framez.norme()==0) return null;
		Vecteur x=new Vecteur(1,0,0);
		framey=framez.vectmul(x);
		if (framey.norme()!=0)
		{		
			framey._normalise();
			framex=framey.vectmul(framez);
		}
		else
		{
			Vecteur y=new Vecteur(0,1,0);
			framex=y.vectmul(framez);
			framex._normalise();
			framey=framez.vectmul(framex);
		}
		Frame f=new Frame();
		f.setAxis(0,framex);
		f.setAxis(1,framey);
		f.setAxis(2,framez);
		return f;
	}
}
