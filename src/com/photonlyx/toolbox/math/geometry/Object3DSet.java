package com.photonlyx.toolbox.math.geometry;

import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;

public class Object3DSet extends Vector<Object3D> implements Object3D
{

	public void add(Object3DSet set)
	{
		for (Object3D o:set) this.add(o);
	}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (Object3D o:this) o.checkOccupyingCube(cornerMin, cornerMax);	
	}

	@Override
	public void draw(Graphics g, Projector proj) 
	{
		for (Object3D o:this) o.draw(g, proj);	
	}

	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		for (Object3D o:this) if (o.isOn(p, proj)) return true;
		return false;
	}

	public static Object3DSet readOBJ(String s)
	{
		Object3DSet o3ds=new Object3DSet();
		o3ds.addObjObjects(s);
		return o3ds;
	}

	/**
	 * add objects from the OBJ format
	 * https://en.wikipedia.org/wiki/Wavefront_.obj_file
	 * @param s
	 */
	public void addObjObjects(String s)
	{
		// see spec in http://www.martinreddy.net/gfx/3d/OBJ.spec
		String[] li=StringSource.readLines(s,false);
		Vector<Vecteur> vertices=new Vector<Vecteur> ();
		//Vector<Vecteur> points=new Vector<Vecteur> ();
		//Vector<Vecteur> faces=new Vector<Vecteur> ();
		//Vector<Vecteur> lines=new Vector<Vecteur> ();
		//System.out.println(li.length+" lines");
		for (String line:li)
		{
			//System.out.println(l);
			if (line.length()<=1) continue;
			if (line.charAt(0)=='#') continue;
			Definition def=new Definition(line);
			//System.out.println(def);
			if (line.startsWith("vt")) continue;
			if (line.startsWith("vn")) continue;
			if (line.charAt(0)=='v')
			{
				Vecteur v=new Vecteur(def.getValue(1,0),def.getValue(2,0),def.getValue(3,0));
				Point3D t=new Point3D(v,1);
				this.add(t);
				vertices.add(v);
			}
			if (line.charAt(0)=='f')
			{
				if (def.dim()==4)//triangle
				{
					int i1=new Integer(removeNormal(def.word(1)));
					int i2=new Integer(removeNormal(def.word(2)));
					int i3=new Integer(removeNormal(def.word(3)));
					//			int i1=new Integer(def.word(1).replace("//",""));
					//			int i2=new Integer(def.word(2).replace("//",""));
					//			int i3=new Integer(def.word(3).replace("//",""));
					Triangle3D t=new Triangle3D(vertices.elementAt(i1-1),
							vertices.elementAt(i2-1),vertices.elementAt(i3-1));
					this.add(t);
				}
				if (def.dim()==5)
				{
					int i1=new Integer(removeNormal(def.word(1)));
					int i2=new Integer(removeNormal(def.word(2)));
					int i3=new Integer(removeNormal(def.word(3)));
					int i4=new Integer(removeNormal(def.word(4)));
					Quadrilatere3D t=new Quadrilatere3D(vertices.elementAt(i1-1),
							vertices.elementAt(i2-1),vertices.elementAt(i3-1),
							vertices.elementAt(i4-1));
					this.add(t);
				}
			}
			if (line.charAt(0)=='l')//if (def.dim()==3)
			{
				//System.out.println("Line: "+line);
				PolyLine3D pol=new PolyLine3D();
				pol.setSeePoints(false);
				for (int i=1;i<def.dim();i++)
				{
					int k=new Integer(def.word(i));
					pol.addPoint(vertices.elementAt(k-1));
					//System.out.println(i+" "+vertices.elementAt(k-1));
				}
				this.add(pol);
			}
			if (line.charAt(0)=='p')if (def.dim()==2)
			{
				int i1=new Integer(def.word(1));
				Point3D t=new Point3D(vertices.elementAt(i1-1),1);
				this.add(t);
			}
		}

	}

	private static String removeNormal(String s)
	{
		int i=s.indexOf("/");
		if (i!=-1)s=s.substring(0,i);
		return s;
	}


	public TriangleMesh transformToTriangles()
	{
		//transform quads in triangle for STL
		TriangleMesh set=new TriangleMesh();
		for (Object3D o:this) 
		{
			if (o instanceof Quadrilatere3DColor)
			{
				Quadrilatere3D q=(Quadrilatere3D)o;
				Vecteur p1=q.p1();
				Vecteur p2=q.p2();
				Vecteur p3=q.p3();
				Vecteur p4=q.p4();
				Triangle3D t1=new Triangle3D(p1,p2,p4);
				Triangle3D t2=new Triangle3D(p2,p3,p4);
				set.add(t2);
				set.add(t1);
			}
			if (o instanceof Quadrilatere3D)
			{
				Quadrilatere3D q=(Quadrilatere3D)o;
				Vecteur p1=q.p1();
				Vecteur p2=q.p2();
				Vecteur p3=q.p3();
				Vecteur p4=q.p4();
				Triangle3D t1=new Triangle3D(p1,p2,p4);
				Triangle3D t2=new Triangle3D(p2,p3,p4);
				set.add(t2);
				set.add(t1);
			}
			if (o instanceof Triangle3D)
			{
				set.add((Triangle3D)o);
			}
		}
		return set;
	}
}
