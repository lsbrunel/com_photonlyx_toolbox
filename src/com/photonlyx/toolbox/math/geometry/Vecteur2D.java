package com.photonlyx.toolbox.math.geometry;

import java.text.DecimalFormat;
import java.text.NumberFormat;



//*******************************************************************************
//*************************** CLASSE VECTEUR2D*****************************
//****************************************************************************



/**class for manipulation of 2D vectors*/
public class Vecteur2D
{
	//private static NumberFormat nf=new DecimalFormat("0.000E0");
	private static NumberFormat nf=new DecimalFormat("0.###");
	public double u[];


	/**
	 * (1,0) base vecteur
	 */
	public static Vecteur2D baseX=new Vecteur2D(1,0);
	/**
	 * (0,1) base vecteur
	 */
	public static Vecteur2D baseY=new Vecteur2D(0,1);


	public Vecteur2D()
	{
		u=new double[2];
	}

	public Vecteur2D(double x,double y)
	{
		u=new double[2];
		u[0]=x;
		u[1]=y;
	}
	
	public Vecteur2D(double[] p)
	{
		u=p;
	}


	public void xAffect(double x) {u[0]=x;}
	public void yAffect(double y) {u[1]=y;}
	public void coordAffect(int i,double d) {u[i]=d;}

	/**
	 * get coordinates as double array 
	 * @return
	 */
	public double[] getCoord(){return u;}


	/**equals the co-ordinates to the ones of v*/
	public void affect(Vecteur2D v) 
	{
		int i;
		for (i=0;i<2;i++) u[i]=v.u[i];
	}

	/**duplicates the vector*/
	public Vecteur2D copy()
	{
		Vecteur2D v=new Vecteur2D();
		v.affect(this);
		return v;
	}


	public void affect(double x,double y)
	{
		u[0]=x;
		u[1]=y;
	}

	/**print the vector like x TAB y TAB z*/
	public String print(String decalage)
	{
		String s= new String(decalage+nf.format(u[0])+" \t"+nf.format(u[1]));
		//String s= new String(decalage+u[0]+" "+u[1]+" "+u[2]);
		return s;
	}

	public String print()
	{
		return this.print("");
	}

	public String toStringOLD()
	{
		return print();
	}

	public String toString()
	{
		return "("+nf.format(u[0])+";"+nf.format(u[1])+")";
	}

	public void annulle()
	{
		int i;
		for(i=0;i<2;i++) u[i]=0.;
	}

	public double x()	{return u[0];}
	public double y()	{return u[1];}
	public double coord(int i) {return u[i];}

	/**set to (1,0,0)*/
	public void affectX()
	{
		u[0]=1.;
		u[1]=0.;
	}

	/**set to (0,1,0)*/
	public void affectY()
	{
		u[0]=0.;
		u[1]=1.;
	}


	/**get the L2 norm*/
	public double norme()
	{
		double r;
		r=Math.sqrt(u[0]*u[0]+u[1]*u[1]);
		return r;
	}


	/**get the square of the L2 norm*/
	public double normeSquare()
	{
		double r;
		r=u[0]*u[0]+u[1]*u[1];
		return r;
	}

	/**return the scalar product of this Vecteur with v*/
	public double mul(Vecteur2D v) 
	{
		int i;
		double retval=0.0;
		for(i=0;i<2;i++) retval+=u[i]*v.u[i];
		return(retval);
	}



	/**subtract with  v*/
	public void sub (Vecteur2D v) 
	{
		int i;
		for(i=0;i<2;i++) u[i]=u[i]-v.u[i];
	}

	/**return a new Vecteur subtracted with  v*/
	public Vecteur2D subn (Vecteur2D v) 
	{
		int i;
		Vecteur2D rv=new Vecteur2D();
		for(i=0;i<2;i++) rv.u[i]=u[i]-v.u[i];
		return rv;
	}

	/**return a new Vecteur  with  v1-v2*/
	public static Vecteur2D sub (Vecteur2D v1,Vecteur2D v2) 
	{
		int i;
		Vecteur2D rv=new Vecteur2D();
		for(i=0;i<2;i++) rv.u[i]=v1.u[i]-v2.u[i];
		return rv;
	}

	/**add the Vecteur v. return the same  added Vecteur!*/
	public Vecteur2D add (Vecteur2D v) 
	{
		int i;
		for(i=0;i<2;i++) u[i]=u[i]+v.u[i];
		return this;
	}

	/**add the Vecteur of coord (x,y) . return the same  added Vecteur!*/
	public Vecteur2D add (double x,double y) 
	{
		int i;
		u[0]+=x;
		u[1]+=y;
		return this;
	}


	/**add the Vecteur v. return a new Vecteur!*/
	public Vecteur2D addn (Vecteur2D v) 
	{
		Vecteur2D rv=new Vecteur2D(0,0);
		for(int i=0;i<2;i++) rv.u[i]=u[i]+v.u[i];
		return rv;
	}

	/**return a new Vecteur multiplied by the scalar*/
	public Vecteur2D scmul(double scalar) 
	{
		int i;
		Vecteur2D rv=new Vecteur2D();
		for (i=0;i<2;i++) rv.u[i]=u[i]*scalar;
		return rv;
	}

	/**set the norm to 1,return a new Vector*/
	public void normalise()
	{
		double n;
		n=norme();
		u[0]=u[0]/n;
		u[1]=u[1]/n;
	}

	/**set the norm to 1,return a new Vector*/
	public Vecteur2D normalisen()
	{
		Vecteur2D rv=new Vecteur2D();
		double n;
		n=norme();
		rv.u[0]=u[0]/n;
		rv.u[1]=u[1]/n;
		return rv;
	}


	/** compute a change of coordinates*/
	/**return a new vector with the new coordinates*/
	/** X,Y,Z contain the old frame vectors expressed in the new frame*/
	public Vecteur2D changeCoordinates(Vecteur2D X,Vecteur2D Y)
	{
		Vecteur2D v=new Vecteur2D();
		for (int i=0;i<2;i++)  v.u[i]=u[0]*X.coord(i)+u[1]*Y.coord(i);
		return v;
	}


	/**
	 * return a perpendicular Vector
	 * @param clockwise if true, the perp vector is turned clockwise from originbal vector
	 * @return
	 */
	public Vecteur2D getPerpVector(boolean clockwise)
	{
		if (clockwise) return new Vecteur2D(-y(),x());
		else return new Vecteur2D(y(),-x());
	}



	public double scalarProduct(Vecteur2D v)
	{
		return x()*v.x()+y()*v.y();
	}


	public boolean equals(Vecteur2D v)
	{
		return ((this.x()==v.x())&(this.y()==v.y()))	;
	}

	/**
	 * polar angle of the vector (angle from  Ox axis)
	 * @return
	 */
	public double polarAngle()
	{
		return Math.atan2(y(), x());	
	}

	public void setX(double r) 
	{
		u[0]=r;
	}

	public void setY(double r) 
	{
		u[1]=r;
	}

	public String toString(int i)
	{
		NumberFormat nff=new DecimalFormat("0.###");
		nff.setMaximumFractionDigits(i);
		return "("+nff.format(u[0])+";"+nff.format(u[1])+")";
	}



} // end class Vector2D
