package com.photonlyx.toolbox.math.geometry;

import java.awt.Graphics;
import java.awt.Point;

public class Lens implements Object3DFrame,TriangleMeshSource
{
private double r1=300,r2=100,d=25,tc=5; //first and second radii (plane for r=0) thickness at center , diameter and refractive index
private int dim=50;//nb of mesh points along d

private SphericalCap cap1,cap2;
private CylinderMesh side;

private TriangleMesh localMesh=new TriangleMesh();
private TriangleMesh globalMesh=new TriangleMesh();
private boolean randomMesh=false;
private Frame frame=new Frame();//local frame of the cylinder

public Lens()
{
	init();
}

public Lens(double r1,double r2,double d,double tc,int dim)
{
	this.r1=r1;
	this.r2=r2;
	this.d=d;
	this.tc=tc;
	this.dim=dim;
	init();
}

private void init()
{
	cap1=new SphericalCap(new Vecteur(),r1,d,dim,randomMesh);
	cap2=new SphericalCap(new Vecteur(),r2,d,dim,randomMesh);
	cap2.getFrame().getCentre()._translate(0, 0, tc);
	cap2.updateGlobal();
	double thicknessAtSides=tc-cap1.getDepth()+cap2.getDepth();
	side=new CylinderMesh(d,thicknessAtSides,dim);
	side.getFrame().getCentre().setZ(cap1.getDepth());
	side.updateGlobal();
	allocateLocalMesh();
	allocateGlobalMesh();
}

private void allocateLocalMesh()
{
	localMesh.removeAllElements();
	for (Triangle3D t:cap1.getTriangles())  localMesh.add(t);
	for (Triangle3D t:cap2.getTriangles())  localMesh.add(t);
	for (Triangle3D t:side.getTriangles())  localMesh.add(t);
}

private void allocateGlobalMesh()
{
	globalMesh.removeAllElements();
	for (Triangle3D t:localMesh)  globalMesh.add(t);
}


@Override
public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
{
	globalMesh.checkOccupyingCube(cornerMin, cornerMax);
}




@Override
public void draw(Graphics g, Projector proj) 
{
	globalMesh.draw(g, proj);
}




@Override
public double getDistanceToScreen(Projector proj) {
	// TODO Auto-generated method stub
	return 0;
}




@Override
public boolean isOn(Point p, Projector proj) 
{
	return globalMesh.isOn(p, proj);
}




@Override
public TriangleMesh getTriangles() 
{
	return globalMesh;
}




@Override
public Frame getFrame() 
{
	return frame;
}




@Override
public void updateGlobal() 
{
	//calc the global coordinates:
	for (int i=0;i<globalMesh.nbTriangles();i++)
	{
		frame.global(globalMesh.getTriangle(i),localMesh.getTriangle(i));
	}
	
}





}
