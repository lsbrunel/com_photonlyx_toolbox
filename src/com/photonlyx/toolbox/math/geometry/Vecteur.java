package com.photonlyx.toolbox.math.geometry;


import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.matrix.FullMatrix;
import com.photonlyx.toolbox.txt.Definition;



//*******************************************************************************
//*************************** CLASSE VECTEUR*****************************
//****************************************************************************



/**class for manipulation of 3D vectors*/
public class Vecteur implements XMLstorable
{
	private static NumberFormat nf=new DecimalFormat("0.000E0");
	public double u[];
	private static NumberFormat nf10 = NumberFormat.getInstance(Locale.ENGLISH);

	static   //static initializer code
	{
		nf10.setMaximumFractionDigits(10);
		nf10.setMinimumFractionDigits(10);
		nf10.setGroupingUsed(false);
	} 


	/**
	 * (1,0,0) base vecteur
	 */
	//public static Vecteur baseX=new Vecteur(1,0,0);
	/**
	 * (0,1,0) base vecteur
	 */
	//public static Vecteur baseY=new Vecteur(0,1,0);
	/**
	 * (0,0,1) base vecteur
	 */
	//public static Vecteur baseZ=new Vecteur(0,0,1);



	public Vecteur()
	{
		u=new double[3];
	}

	public Vecteur(double x,double y,double z)
	{
		u=new double[3];
		u[0]=x;
		u[1]=y;
		u[2]=z;
	}
	public Vecteur(double[] v)
	{
		u=v;
	}

	public void setX(double d){u[0]=d;}
	public void setY(double d){u[1]=d;}
	public void setZ(double d){u[2]=d;}

	public void xAffect(double x) {u[0]=x;}
	public void yAffect(double y) {u[1]=y;}
	public void zAffect(double z) {u[2]=z;}

	/**
	 * affect the coor.
	 * @param i coord: 0 for X, 1 for Y , 2 for Z
	 * @param d
	 */
	public void coordAffect(int i,double d) {u[i]=d;}

	/**equals the co-ordinates to the ones of v*/
	public void affect(Vecteur v) 
	{
		int i;
		for (i=0;i<3;i++) u[i]=v.u[i];
	}

	/**duplicates the vector*/
	public Vecteur copy()
	{
		Vecteur v=new Vecteur();
		v.copy(this);
		return v;
	}

	/**
	 * copy coords of o
	 * @param o
	 */
	public void copy(Vecteur o) 
	{
		u[0]=o.u[0];
		u[1]=o.u[1];
		u[2]=o.u[2];	
	}

	public void affect(double x,double y,double z)
	{
		u[0]=x;
		u[1]=y;
		u[2]=z;
	}

	/**print the vector like x TAB y TAB z*/
	public String print(String decalage)
	{
		String s= new String(decalage+"("+nf10.format(u[0])+" \t"+nf10.format(u[1])+" \t"+nf10.format(u[2])+")"	);
		//String s= new String(decalage+u[0]+" "+u[1]+" "+u[2]);
		return s;
	}

	public String print()
	{
		return this.print("");
	}

	public String toString()
	{
		return print();
	}

	public String toString(DecimalFormat df)
	{
		String s= new String(df.format(u[0])+" \t"+df.format(u[1])+" \t"+df.format(u[2]));
		return s;
	}

	public void annulle()
	{
		int i;
		for(i=0;i<3;i++) u[i]=0.;
	}

	public double x()	{return u[0];}
	public double y()	{return u[1];}
	public double z()	{return u[2];}
	public double coord(int i) {return u[i];}

	/**set to (1,0,0)*/
	public void affectX()
	{
		u[0]=1.;
		u[1]=0.;
		u[2]=0.;
	}

	/**set to (0,1,0)*/
	public void affectY()
	{
		u[0]=0.;
		u[1]=1.;
		u[2]=0.;
	}

	/**set to (0,0,1)*/
	public void affectZ()
	{
		u[0]=0.;
		u[1]=0.;
		u[2]=1.;
	}

	/**get the L2 norm*/
	public double norme()
	{
		double r;
		r=Math.sqrt(u[0]*u[0]+u[1]*u[1]+u[2]*u[2]);
		return r;
	}


	/**get the square of the L2 norm*/
	public double normeSquare()
	{
		double r;
		r=u[0]*u[0]+u[1]*u[1]+u[2]*u[2];
		return r;
	}

	/**return the scalar product of this Vecteur with v*/
	public double mul(Vecteur v) 
	{
		int i;
		double retval=0.0;
		for(i=0;i<3;i++) retval+=u[i]*v.u[i];
		return(retval);
	}

	/**return the vectorial product of this Vecteur with v*/
	public Vecteur vectmul(Vecteur v) 
	{
		Vecteur w=new Vecteur();
		w.coordAffect(0,y()*v.z()-z()*v.y());
		w.coordAffect(1,z()*v.x()-x()*v.z());
		w.coordAffect(2,x()*v.y()-y()*v.x());
		return(w);
	}

	/**return a new Vecteur subtracted with  v*/
	public void _sub (Vecteur v) 
	{
		for(int i=0;i<3;i++) u[i]-=v.u[i];
	}


	/**return a new Vecteur subtracted with  v*/
	public Vecteur sub (Vecteur v) 
	{
		int i;
		Vecteur rv=new Vecteur();
		for(i=0;i<3;i++) rv.u[i]=u[i]-v.u[i];
		return rv;
	}

	/**return a new Vecteur  with  v1-v2*/
	public static Vecteur sub (Vecteur v1,Vecteur v2) 
	{
		int i;
		Vecteur rv=new Vecteur();
		for(i=0;i<3;i++) rv.u[i]=v1.u[i]-v2.u[i];
		return rv;
	}

	/**
	 * add the Vecteur v. return the same  added Vecteur!
	 * */
	public Vecteur _add (Vecteur v) 
	{
		int i;
		for(i=0;i<3;i++) u[i]=u[i]+v.u[i];
		return this;
	}
	/**add the Vecteur v. return the same  added Vecteur!*/
	//public Vecteur _add (Vecteur v) 
	//{
	//int i;
	//for(i=0;i<3;i++) u[i]=u[i]+v.u[i];
	//return this;
	//}

	/**add the Vecteur of coord (x,y,z) . return the same  added Vecteur!*/
	public Vecteur _add (double x,double y, double z) 
	{
		int i;
		u[0]+=x;
		u[1]+=y;
		u[2]+=z;
		return this;
	}


	/**add the Vecteur v. return a new Vecteur!*/
	public Vecteur addn (Vecteur v) 
	{
		Vecteur rv=new Vecteur(0,0,0);
		for(int i=0;i<3;i++) rv.u[i]=u[i]+v.u[i];
		return rv;
	}

	/**add the Vecteur v. return a new Vecteur!*/
	public Vecteur addn (double x,double y,double z) 
	{
		Vecteur rv=new Vecteur(u[0]+x,u[1]+y,u[2]+z);
		return rv;
	}

	/**return a new Vecteur multiplied by the scalar*/
	public Vecteur scmul(double scalar) 
	{
		int i;
		Vecteur rv=new Vecteur();
		for (i=0;i<3;i++) rv.u[i]=u[i]*scalar;
		return rv;
	}

	/**return a new Vecteur multiplied by the scalar*/
	public void _scmul(double scalar) 
	{
		for (int i=0;i<3;i++) u[i]*=scalar;
	}

	/**set the norm to 1*/
	public void _normalise()
	{
		double n;
		n=norme();
		u[0]=u[0]/n;
		u[1]=u[1]/n;
		u[2]=u[2]/n;
	}

	/**return a new vector normalized */
	public Vecteur getNormalised()
	{
		double n;
		n=norme();
		return new Vecteur(u[0]/n,u[1]/n,u[2]/n);
	}


	/**
	 * return a new Vecteur which is the intersection of a plane and a line
	 * @param pPlane point of the plane
	 * @param nPlane normal of the plane
	 * @param pLine point of the line
	 * @param nLine direction of the line
	 * @return the intersection point, null if line parallel to plane
	 */
	public static Vecteur intersectionPlaneLine
	(Vecteur pPlane,Vecteur nPlane,Vecteur pLine,Vecteur nLine)
	{
		double tdroite;
		Vecteur PdPp,u;
		double r;

		PdPp=pPlane.sub(pLine);
		r=nLine.mul(nPlane);
		if (r!=0) tdroite=PdPp.mul(nPlane)/r;  
		else 
		{
			//tdroite=1e50;
			return null;
		}
		u=nLine.scmul(tdroite);
		return u._add(pLine);
	}



	/**
	 * Calculate the symetrical of the this Vecteur with respect to
	 * a plane owning Pplan with normal vector nplan
	 */
	public Vecteur symetryPlane(Vecteur Pplan,Vecteur nplan)
	{
		Vecteur PS,PSimage;
		PS=sub(Pplan);
		PSimage=PS.sub( nplan.scmul(2.0*PS.mul(nplan)));
		return(PSimage._add(Pplan));
	}

	/**
	 * @deprecated use class Frame

	 * Calculate the symetrical of the this Vecteur with respect to
	 * a vectorial plane with normal vector nplan
	 */
	public Vecteur vectorialSymetryPlane(Vecteur nplan)
	{
		Vecteur Vimage;
		Vimage=this.sub( nplan.scmul(2.0*this.mul(nplan)));
		return(Vimage);
	}

	/** @deprecated use class Frame
	 * compute a change of coordinates
	 * get the Vecteur coords from its coords expressed in the frame (X,Y,Z)
	 * @param X first vector of the frame
	 * @param Y second vector of the frame
	 * @param Z third vector of the frame
	 * @return a new vector with the new coordinates
	 */
	public Vecteur changeCoordinates(Vecteur X,Vecteur Y,Vecteur Z)
	{
		Vecteur v=new Vecteur();
		for (int i=0;i<3;i++)  v.u[i]=u[0]*X.coord(i)+u[1]*Y.coord(i)+u[2]*Z.coord(i);
		return v;
	}

	/**
	 * gives the new coordinates of the vecteur expressed in the new frame X,Y,Z
	 * @param X first vector of the new frame
	 * @param Y second vector of the new frame
	 * @param Z third vector of the new frame
	 * @return a new vector with the new coordinates
	 */
	public Vecteur changeCoordinates2(Vecteur X,Vecteur Y,Vecteur Z)
	{
		Vecteur v=new Vecteur(X.mul(this),Y.mul(this),Z.mul(this));
		return v;
	}





	public XMLElement toXML()
	{
		StringBuffer sb=new StringBuffer();
		//sb.append(nf10.format(x())+" "+nf10.format(y())+" "+nf10.format(z()));
		XMLElement el = new XMLElement();	
		el.setName(getClass().toString().replace("class ", ""));
		//el.setContent(sb.toString());
		el.setDoubleAttribute("x",x());
		el.setDoubleAttribute("y",y());
		el.setDoubleAttribute("z",z());
		return el;	
	}


	public void updateFromXML(XMLElement xml) 
	{
		//String data=xml.getContent();	
		//Definition def=new Definition(data); 
		//System.out.println(xml);
		//System.out.println("#"+data+"#");
		//this.setX(new Double(def.word(0)).doubleValue());
		//this.setY(new Double(def.word(1)).doubleValue());
		//this.setZ(new Double(def.word(2)).doubleValue());
		this.setX(xml.getDoubleAttribute("x",0));
		this.setY(xml.getDoubleAttribute("y",0));
		this.setZ(xml.getDoubleAttribute("z",0));
	}



	public void _translate(double dx, double dy, double dz)
	{
		u[0]+=dx;
		u[1]+=dy;
		u[2]+=dz;
	}
	public void _translate(Vecteur v)
	{
		u[0]+=v.u[0];
		u[1]+=v.u[1];
		u[2]+=v.u[2];
	}

	public Vecteur translate(double dx, double dy, double dz)
	{
		Vecteur v=new Vecteur();
		v.u[0]+=dx;
		v.u[1]+=dy;
		v.u[2]+=dz;
		return v;
	}


	public void swapYZ()
	{
		double r=u[2];
		setZ(u[1]);
		setY(r);
	}

	public void swapXZ()
	{
		double r=u[2];
		setZ(u[0]);
		setX(r);
	}

	public static double distance(Vecteur p1, Vecteur p2)
	{
		double x,y,z;
		x=p2.u[0]-p1.u[0];
		y=p2.u[1]-p1.u[1];
		z=p2.u[2]-p1.u[2];
		return Math.sqrt(x*x+y*y+z*z);
	}

	
	public static Vecteur getMiddlePoint(Vecteur a,Vecteur b)
	{
		return new Vecteur((a.x()+b.x())/2,(a.y()+b.y())/2,(a.z()+b.z())/2);
	}


} // fin classe vecteur
