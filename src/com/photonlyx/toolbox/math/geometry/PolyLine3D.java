package com.photonlyx.toolbox.math.geometry;

import java.awt.Graphics;
import java.awt.Point;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;

import nanoxml.XMLElement;


public class PolyLine3D implements Iterable<Vecteur>,Object3D
{
	private Vector<Vecteur> list=new Vector<Vecteur>();
	private boolean seePoints=true;

	public PolyLine3D()
	{
	}

	public int nbPoints()
	{
		return list.size();
	}

	public void addPoint(Vecteur _v)
	{
		list.add(_v);
	}

	public void removeAllPoints()
	{
		list.removeAllElements();
	}

	public Vector<Vecteur> getList()
	{
		return list;
	}
	public Vecteur get(int i)
	{
		return list.elementAt(i);
	}

	public Iterator<Vecteur> iterator() 
	{
		return list.iterator();
	}

	public Vecteur first()
	{
		if (list.size()>0) return list.firstElement();else return null;
	}

	public Vecteur last()
	{
		if (list.size()>0) return list.lastElement();else return null;
	}

	public Vecteur getPoint(int index)
	{
		return list.elementAt(index);
	}


	public boolean isSeePoints() {
		return seePoints;
	}

	public void setSeePoints(boolean seePoints) {
		this.seePoints = seePoints;
	}

	public XMLElement toXML() 
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		for (Vecteur p : list) el.addChild(p.toXML());
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		list.removeAllElements();
		if (xml==null) return;
		//System.out.println(xml);
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			Vecteur p=new Vecteur();
			p.updateFromXML(enumSons.nextElement());
			addPoint(p);
		}
	}

	/**
	 * sort the x values
	 */
	public void sortX()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.x());
		Collections.sort(vv);
	}


	/**
	 * sort they values
	 */
	public void sortY()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.y());
		Collections.sort(vv);
	}


	/**
	 * sort the x values
	 */
	public void sortZ()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.z());
		Collections.sort(vv);
	}


	/**
	 * sort the x+y values
	 */
	public void sortXplusY()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.x()+v.y());
		Collections.sort(vv);
	}

	/**
	 * sort the x+y values
	 */
	public void sortYplusZ()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.y()+v.z());
		Collections.sort(vv);
	}


	/**
	 * sort the x+z values
	 */
	public void sortXplusZ()
	{
		Vector<Double> vv=new Vector<Double>();
		for (Vecteur v:list) vv.add(v.x()+v.z());
		Collections.sort(vv);
	}



	public double ymin()
	{
		double d=1e30;//Double.MAX_VALUE;
		for (Vecteur v:list) if (v.y()<d) d=v.y();
		return d;	
	}

	public double ymax()
	{
		double d=-1e30;//Double.MIN_VALUE;
		for (Vecteur v:list) if (v.y()>d) d=v.y();
		return d;	
	}

	public String toString()
	{
		StringBuffer sb=new StringBuffer();
		for (Vecteur p:this) sb.append(p.toString()+"\n");
		return sb.toString();
	}


	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
	{
		for (Vecteur p:this)
			for (int k=0;k<3;k++) 
			{
				double r=p.coord(k);
				if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
				if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
			}
	}


	public void draw(Graphics g, Projector proj)
	{
		int n=this.getList().size();
		double[][] cooScreen=new double[2][];
		for (int i=0;i<n-1;i++)
		{
			Vecteur p1=this.getList().elementAt(i);
			Vecteur p2=this.getList().elementAt(i+1);
			cooScreen[0]=proj.calcCoorEcran(p1);
			cooScreen[1]=proj.calcCoorEcran(p2);
			g.drawLine((int)cooScreen[0][0], (int)cooScreen[0][1], (int)cooScreen[1][0], (int)cooScreen[1][1]);
			if (seePoints) g.fillRect((int)cooScreen[0][0]-1, (int)cooScreen[0][1]-1,3,3);
		}
		//last point
		if(this.getList().size()>0)
		{
			Vecteur p1=this.getList().elementAt(n-1);
			cooScreen[0]=proj.calcCoorEcran(p1);
			if (seePoints) g.fillRect((int)cooScreen[0][0]-1, (int)cooScreen[0][1]-1,3,3);
		}
	}


	public double getDistanceToScreen(Projector proj)
	{
		return 	proj.zCoord(this.first());
	}

	@Override
	public boolean isOn(Point p, Projector proj)
	{
		// TODO Auto-generated method stub
		return false;
	}

	public PolyLine3D copy() 
	{
		PolyLine3D p=new PolyLine3D();
		for (Vecteur v:this) p.addPoint(v.copy());
		return p;
	}

	public void add(PolyLine3D po) 
	{
		for (Vecteur p:po) this.addPoint(p);
	}



}
