package com.photonlyx.toolbox.math.geometry;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import com.photonlyx.toolbox.txt.TextFiles;

public class Pyramid implements Object3DFrame,TriangleMeshSource
{
	private double a=1;//side
	private double h=1;//height
	private Vector<Vecteur> points=new Vector<Vecteur>();
	private TriangleMesh localMesh=new TriangleMesh();
	private TriangleMesh globalMesh=new TriangleMesh();
	private Frame frame=new Frame();//local frame 

	public Pyramid()
	{
		init();
	}

	public Pyramid(double a,double h)
	{
		this.a=a;
		this.h=h;
		init();
	}


	@Override
	public void updateGlobal()
	{
		//calc the global coordinates:
		for (int i=0;i<globalMesh.nbTriangles();i++)
		{
			frame.global(globalMesh.getTriangle(i),localMesh.getTriangle(i));
		}
	}

	//public TriangleMesh getTriangles() {return tris;}

	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		globalMesh.checkOccupyingCube(cornerMin, cornerMax);

	}
	@Override
	public void draw(Graphics g, Projector proj) 
	{
		globalMesh.draw(g, proj);
	}

	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		return globalMesh.getDistanceToScreen(proj);
	}
	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		for (Triangle3D tri:globalMesh) 
			if (tri.isOn(p, proj)) return true;
		return false;
	}


	@Override
	public Frame getFrame()
	{
		return frame;
	}



	@Override
	/**
	 * 
	 */
	public TriangleMesh getTriangles() {
		return globalMesh;
	}






	//  http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
	//calc the local frame triangles
	public void init()
	{
		points.add(new Vecteur(a/2,a/2,0));
		points.add(new Vecteur(-a/2,a/2,0));
		points.add(new Vecteur(-a/2,-a/2,0));
		points.add(new Vecteur(a/2,-a/2,0));
		points.add(new Vecteur(0,0,h));
		
		
		localMesh.removeAllElements();
		//base:
		localMesh.add(new Triangle3D(points.get(0),points.get(3),points.get(1)));
		localMesh.add(new Triangle3D(points.get(1),points.get(3),points.get(2)));
		//sides:
		localMesh.add(new Triangle3D(points.get(3),points.get(4),points.get(2)));
		localMesh.add(new Triangle3D(points.get(4),points.get(3),points.get(0)));
		localMesh.add(new Triangle3D(points.get(1),points.get(4),points.get(0)));
		localMesh.add(new Triangle3D(points.get(2),points.get(4),points.get(1)));
		
		
		for (Triangle3D tri:localMesh) globalMesh.add(tri.copy());
		updateGlobal();

	}






	public double getA() {
		return a;
	}

	public void setA(double a) {
		this.a = a;
	}

	public double getH() {
		return h;
	}

	public void setH(double h) {
		this.h = h;
	}

	public static void main(String[] args) 
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();

		Pyramid s=new Pyramid(1,1);
		Object3DColorSet set=new Object3DColorSet();
		set.add(s.getTriangles());
		graph3DPanel.addColorSet(set);
		
		//graph3DPanel.addObject3D(s);
		
		Pyramid s2=new Pyramid(1,1);
		s2.getFrame()._translate(new Vecteur(2,0,0));
		s2.updateGlobal();
		//graph3DPanel.addObject3D(s2);
		
		for (Triangle3D tri:s2.getTriangles()) graph3DPanel.add(new Triangle3DColor(tri,Color.BLACK,true));		
		
		s2.getTriangles().saveToSTL("/home/laurent/temp/pyramid.stl");
		
		Vecteur p1=new Vecteur(2,2,0);
		Vecteur p2=new Vecteur(-2,2,0);
		Vecteur p3=new Vecteur(-2,-2,0);
		Vecteur p4=new Vecteur(2,-2,0);
		Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
		graph3DPanel.add(q);		
		
		
		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());
	}




}





