package com.photonlyx.toolbox.math.geometry;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;


public class Segment3D  implements Object3D
{
//private Vecteur p1=new Vecteur(),p2=new Vecteur();
private Vecteur[] points=new Vecteur[2];

public Segment3D()
{
//p1=new Vecteur();	
//p2=new Vecteur();	
points[0]=new Vecteur();	
points[1]=new Vecteur();	
}

public Segment3D(Vecteur _p1,Vecteur _p2)
{
//p1=_p1;	
//p2=_p2;	
points[0]=_p1;
points[1]=_p2;	
	
}


/**return the first segment extremity*/
public Vecteur p1(){return points[0];}
/**return the second segment extremity*/
public Vecteur p2(){return points[1];}

/**if one of the is out of the cube, extends the cube*/
public void checkOccupyingCube(Vecteur cornerMin,Vecteur cornerMax)
{
if( points[0]==null) return;
if (points[1]==null) return;
	for (int k=0;k<3;k++) 
		{
		double r=points[0].coord(k);
		if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
		if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
		r=points[1].coord(k);
		if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
		if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
		}	
}



public void draw(Graphics g,Projector proj)
{
double[][] cooScreen=new double[2][];
//System.out.println(p1+"    "+p2);
cooScreen[0]=proj.calcCoorEcran(points[0]);
cooScreen[1]=proj.calcCoorEcran(points[1]);
//System.out.println(cooScreen[0][0]+" "+cooScreen[0][1]+"     "+cooScreen[1][0]+" "+cooScreen[1][1]);
//g.setColor(Color.black);
g.drawLine((int)cooScreen[0][0], (int)cooScreen[0][1], (int)cooScreen[1][0], (int)cooScreen[1][1]);
}



public double getDistanceToScreen(Projector proj)
{
return 	proj.zCoord(points[0]);
}

public double getLength()
{
return points[1].sub(points[0]).norme();
}

public void _translate(double dx,double dy,double dz)
{
	points[0]._translate(dx,dy,dz);
	points[1]._translate(dx,dy,dz);
}

/**
 * get a copy
 * @return
 */
public Segment3D copy()
{
return new Segment3D(points[0].copy(),points[1].copy());
}

/**
 * get the intersection of this segment with a plane 
 * @param p
 * @return interection point or null if none 
 */
public Vecteur intersection(Plane p)
{
//System.out.println("Segment :"+p1+"  "+p2);
//System.out.println("   Intersection with plane: O:"+p.pPlane+" n:"+p.Z());
Vecteur p1p2=points[1].sub(points[0]);
//intersect the plane with the line
Vecteur i=p.intersectionLine(points[0], p1p2);
if (i==null) return null; //plane // line
//check if the point i in in the segment:
double localIpos=p1p2.mul(i.sub(points[0]));
if (localIpos<0) return null;
if (localIpos>p1p2.mul(p1p2)) return null;
//System.out.println("i="+i);
return i;

}

@Override
public boolean isOn(Point p, Projector proj)
{
// TODO Auto-generated method stub
return false;
}

public Vecteur point(int i) {
	return points[i];
}


public String toString()
{
return p1().toString()+"  "+p2().toString();
}



}
