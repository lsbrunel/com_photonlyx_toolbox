package com.photonlyx.toolbox.math.geometry;

import java.awt.Graphics;
import java.awt.Point;

import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Vecteur;

public class Point3D   extends Vecteur implements Object3D
{
private int sizeOnScreen=1;
private double[] cooScreen=new double[2];//screen coordinates

public Point3D()
{
super();
}

public Point3D(Vecteur _p)
{
this.affect(_p);
}


public Point3D(Vecteur _p,int sizeOnScreen)
{
this.affect(_p);
this.sizeOnScreen=sizeOnScreen;
}


public void draw(Graphics g,Projector proj)
{
cooScreen=proj.calcCoorEcran(this);
g.fillRect((int)cooScreen[0]-sizeOnScreen/2, (int)cooScreen[1]-sizeOnScreen/2, sizeOnScreen, sizeOnScreen);
}

public double[] getCooScreen(Projector proj)
{
return cooScreen;
}

/**
 * get a copy
 * @return
 */
public Point3D copy()
{
return new Point3D(this.copy());
}


/**if one of the is out of the cube, extends the cube*/

public void checkOccupyingCube(Vecteur cornerMin,Vecteur cornerMax)
{
	for (int k=0;k<3;k++) 
		{
		double r=this.coord(k);
		if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
		if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
		}	
}


public double getDistanceToScreen(Projector proj)
{
return 	proj.zCoord(this);
}

public Vecteur getP()
{
return this;
}

@Override
public boolean isOn(Point p, Projector proj) 
{
	double d=Math.pow(cooScreen[0]-p.x,2)+Math.pow(cooScreen[1]-p.y,2);
	return (d<3);
}




}
