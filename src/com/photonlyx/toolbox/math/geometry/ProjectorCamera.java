

package com.photonlyx.toolbox.math.geometry;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;

	
	
/**
 * 3D to screen projector
 * */
public class ProjectorCamera  implements Projector,Object3D
{
//params
private double focal=50;//in distance unit

//internal
protected Vecteur O;//center of the sensor
private Vecteur pX,pY,pZ;//pX is horizontal, pY is vertical, optical axis is pZ
private Vecteur lens;//lens in front of the sensor
private int screenW,screenH;
private double zoomStep=1.1;
private double[] pixelSize={0.005,0.005};//pixel size in mm in x and y


public ProjectorCamera()
{

O=new Vecteur();
pX=new Vecteur(1,0,0);
pY=new Vecteur(0,1,0);
pZ=new Vecteur(0,0,1);
setLens();	
}


public ProjectorCamera(double _focal,int _screenW,int _screenH)
{
focal=_focal;
screenW=_screenW;
screenH=_screenH;

O=new Vecteur();
pX=new Vecteur(1,0,0);
pY=new Vecteur(0,1,0);
pZ=new Vecteur(0,0,1);
setLens();	
}

//public void work()
//{
//O.affect(x, y, z);
//setLens();	
//}
public void initialise(Vecteur target,double distanceToTarget,double _focal,int _screenW,int _screenH)
{
this.initialise(new Vecteur(3*Math.PI/4,0,-0.3), target, distanceToTarget, _focal, _screenW, _screenH);
}

public void initialise(Vecteur angles,Vecteur target,double distanceToTarget,double _focal,int _screenW,int _screenH)
{
focal=_focal;

pX.affect(1,0,0);
pY.affect(0,1,0);
pZ.affect(0,0,1);
//Vecteur angles=new Vecteur(0,0,0);
//Vecteur angles=new Vecteur(3*Math.PI/4,0,-0.3);
Rotator r=new Rotator(angles);
r._turn(pX);
r._turn(pY);
r._turn(pZ);

O.affect(target.sub(pZ.scmul(distanceToTarget)));
//x=O.x();
//y=O.y();
//z=O.z();
setLens();

screenW=_screenW;
screenH=_screenH;
}

protected void setLens()
{
lens=O.addn(pZ.scmul(focal));	
}

	/** project a 3D point P on the plane of the Projecteur*/
public double[] projete(Vecteur P)
	{
	
	//Vecteur OP;
	double[] proj=new double[2];
	if (P==null) return proj;
	Vecteur dir=P.sub(lens);
	Vecteur	point=Vecteur.intersectionPlaneLine(O,pZ,lens,dir);
	if (point==null) return proj;
	Vecteur	vectloc=point.sub(O);
	proj[0]=-pX.mul(vectloc);
	proj[1]=pY.mul(vectloc);
	//System.out.println("P "+P);
	//System.out.println("O "+O);
	//System.out.println("lens "+lens);
	//System.out.println(proj[0]+" "+proj[1]);
	return proj;
	}


/**calculates the screen co-ordinates*/
public double[] calcCoorEcran(Vecteur P) 
{
double[] proj;
double[] coorEcran=new double[2]; 
proj=projete(P);
//coorEcran[0]=(int) ((proj[0]+0.5)/sensorW*ecranW);
//coorEcran[1]=(int) ((proj[1]+0.5)/sensorH*ecranH);
coorEcran[0]=((proj[0]/pixelSize[0])+0.5*screenW);
coorEcran[1]= ((proj[1]/pixelSize[1])+0.5*screenH);
coorEcran[1]=screenH-coorEcran[1];
return coorEcran;
}

/**calculates the screen co-ordinates in double precision*/
public double[] calcCoorEcranDouble(Vecteur P) 
{
double[] proj;
double[] coorEcran=new double[2]; 
proj=projete(P);
//coorEcran[0]=(int) ((proj[0]+0.5)/sensorW*ecranW);
//coorEcran[1]=(int) ((proj[1]+0.5)/sensorH*ecranH);
coorEcran[0]=((proj[0]/pixelSize[0])+0.5*screenW);
coorEcran[1]= ((proj[1]/pixelSize[1])+0.5*screenH);
coorEcran[1]=screenH-coorEcran[1];
return coorEcran;
}


/**calculates the screen co-ordinates in double precision*/
public void  calcCoorScreen(double[] cooScreen, Vecteur P) 
{
double[] proj;
proj=projete(P);
cooScreen[0]=((proj[0]/pixelSize[0])+0.5*screenW);
cooScreen[1]= ((proj[1]/pixelSize[1])+0.5*screenH);
cooScreen[1]=screenH-cooScreen[1];
}

public void calcCoorScreen(Vecteur2D cooScreen,Vecteur P)
{
	double[] proj;
	proj=projete(P);
	cooScreen.setX((proj[0]/pixelSize[0])+0.5*screenW);
	cooScreen.setY(screenH-((proj[1]/pixelSize[1])+0.5*screenH));
}


/** calc the z coordinate of the point P in the frame of the Projecteur*/
public double zCoord(Vecteur P)
	{
	Vecteur OP;
	double[] proj=new double[2];
	OP=P.sub(O);
	double z=OP.mul(pZ);
	return z;
	}

public void initAngles()
{
pX.affect(1,0,0);
pY.affect(0,1,0);
pZ.affect(0,0,1);
}




/**
 * turn the camera with respect to its frame angles in rad
 * */
public void turn(double ax,double ay,double az)
{
//System.out.println(pX);
//System.out.println(pY);
//System.out.println(pZ);
Vecteur angles=new Vecteur(ax,ay,az);
Rotator r=new Rotator(angles);
//r.tourner(pX);
//make a turned frame from main frame
Vecteur pX1=r.turn(new Vecteur(1,0,0));
Vecteur pY1=r.turn(new Vecteur(0,1,0));
Vecteur pZ1=r.turn(new Vecteur(0,0,1));
//System.out.println(pX1);
//System.out.println(pY1);
//System.out.println(pZ1);

//Express the turned frame in the camera frame:
Vecteur pX2=pX1.changeCoordinates(pX,pY,pZ);
Vecteur pY2=pY1.changeCoordinates(pX,pY,pZ);
Vecteur pZ2=pZ1.changeCoordinates(pX,pY,pZ);

//change the camera frame
pX.affect(pX2);
pY.affect(pY2);
pZ.affect(pZ2);
//System.out.println(pX);
//System.out.println(pY);
//System.out.println(getClass()+" pZ="+pZ);
//translateToSeeTarget();
setLens();
}


public void translateToSeeTarget(Vecteur target,double distanceToTarget)
{
//O.add(pX.scmul(localX(target)));
//O.add(pY.scmul(localY(target)));
//O.add(pZ.scmul(-distanceToTarget+localZ(target)));
O.affect(target.sub(pZ.scmul(distanceToTarget)));
//x=O.x();
//y=O.y();
//z=O.z();
setLens();
}

private double localX(Vecteur p)
{
return p.sub(O).mul(pX);	
}

private double localY(Vecteur p)
{
return p.sub(O).mul(pY);	
}

private double localZ(Vecteur p)
{
return p.sub(O).mul(pZ);	
}


/** move the camera, the coordinates refer to camera frame*/
public void translater(double dx,double dy,double dz)
{
O._add(pX.scmul(-dx));
O._add(pY.scmul(-dy));
O._add(pZ.scmul(-dz));
//x=O.x();
//y=O.y();
//z=O.z();
setLens();

//double G=focal.val/distanceToTarget.val;
//target.add(pX.scmul(-dx/G));
//target.add(pY.scmul(-dy/G));
//translateToSeeTarget();

//double angley=-dx/focal.val;
//double anglex=dy/focal.val;
//tourner(anglex,angley,0);
}

/** move the camera, the coordinates refer to camera frame*/
public void translater(Vecteur v)
{
O._add(pX.scmul(-v.x()));
O._add(pY.scmul(-v.y()));
O._add(pZ.scmul(-v.z()));
//x=O.x();
//y=O.y();
//z=O.z();
setLens();

}

/**zoom by a factor */
public void zoom() 
{
focal*=zoomStep;
setLens();
}

/**zoom by a factor */
public void unzoom() 
{
focal/=zoomStep;
setLens();
}

public void getCloser(double step)
{
O._add(pZ.scmul(step));
//x=O.x();
//y=O.y();
//z=O.z();
setLens();
}

public void getFarther(double step)
{
O._add(pZ.scmul(-step));
//x=O.x();
//y=O.y();
//z=O.z();
setLens();
}


public Plane getSensorPlane()
{
return new Plane(O,pZ);
}









public double getFocal()
{
return focal;
}

public void setFocal(double focal)
{
this.focal = focal;
this.setLens();
}

public Vecteur getO()
{
return O;
}

public void setO(Vecteur o)
{
O = o;
}

public Vecteur getpX()
{
return pX;
}

public void setpX(Vecteur pX)
{
this.pX = pX;
}

public Vecteur getpY()
{
return pY;
}

public void setpY(Vecteur pY)
{
this.pY = pY;
}

public Vecteur getpZ()
{
return pZ;
}

public void setpZ(Vecteur pZ)
{
this.pZ = pZ;
}

public Vecteur getLens()
{
return lens;
}

public void setLens(Vecteur lens)
{
this.lens = lens;
}

public int getScreenW()
{
return screenW;
}

public void setScreenW(int ecranW)
{
this.screenW = ecranW;
}

public int getScreenH()
{
return screenH;
}

public void setScreenH(int ecranH)
{
this.screenH = ecranH;
}


@Override
public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
{
checkOccupyingCube(cornerMin, cornerMax, O);
checkOccupyingCube(cornerMin, cornerMax, O.sub(pZ));
}

private void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax,Vecteur v)
{
for (int k=0;k<3;k++) 
	{
	double r=v.coord(k);
	if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
	if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
	}
}

@Override
public void draw(Graphics g, Projector proj)
{
Vecteur sensorCenter=O;
Polygon pol=new Polygon();
//System.out.println("quad filled="+filled+" sun="+sun);
double[] cooScreen;

double sensorW=pixelSize[0]*screenW;
double sensorH=pixelSize[1]*screenH;

//sensor:
cooScreen=proj.calcCoorEcran(sensorCenter.addn(pX.scmul(sensorW/2).addn(pY.scmul(sensorH/2))));
pol.addPoint((int)cooScreen[0],(int)cooScreen[1]);
cooScreen=proj.calcCoorEcran(sensorCenter.addn(pX.scmul(sensorW/2).addn(pY.scmul(-sensorH/2))));
pol.addPoint((int)cooScreen[0],(int)cooScreen[1]);
cooScreen=proj.calcCoorEcran(sensorCenter.addn(pX.scmul(-sensorW/2).addn(pY.scmul(-sensorH/2))));
pol.addPoint((int)cooScreen[0],(int)cooScreen[1]);
cooScreen=proj.calcCoorEcran(sensorCenter.addn(pX.scmul(-sensorW/2).addn(pY.scmul(sensorH/2))));
pol.addPoint((int)cooScreen[0],(int)cooScreen[1]);
g.setColor(Color.black);
g.drawPolygon(pol);

//sensor X axis:
pol=new Polygon();
cooScreen=proj.calcCoorEcran(sensorCenter);
pol.addPoint((int)cooScreen[0],(int)cooScreen[1]);
cooScreen=proj.calcCoorEcran(sensorCenter.addn(pX.scmul(sensorW/2)));
pol.addPoint((int)cooScreen[0],(int)cooScreen[1]);
g.setColor(Color.black);
g.drawPolygon(pol);

//sensor Y axis
pol=new Polygon();
cooScreen=proj.calcCoorEcran(sensorCenter);
pol.addPoint((int)cooScreen[0],(int)cooScreen[1]);
cooScreen=proj.calcCoorEcran(sensorCenter.addn(pY.scmul(sensorH/2)));
pol.addPoint((int)cooScreen[0],(int)cooScreen[1]);
g.setColor(Color.black);
g.drawPolygon(pol);

//camera  axis from sensor to lens
pol=new Polygon();
cooScreen=proj.calcCoorEcran(sensorCenter);
pol.addPoint((int)cooScreen[0],(int)cooScreen[1]);
cooScreen=proj.calcCoorEcran(sensorCenter.addn(pZ.scmul(focal)));
pol.addPoint((int)cooScreen[0],(int)cooScreen[1]);
g.setColor(Color.black);
g.drawPolygon(pol);

//field from corner to lens extended by 30 focals
double[] cooScreen2;
Vecteur corner,end;
for (int i=-1;i<=1;i+=2)for (int j=-1;j<=1;j+=2)
	{
	corner=sensorCenter.addn(pX.scmul(i*sensorW/2).addn(pY.scmul(j*sensorH/2)));
	end=corner.addn(lens.sub(corner).scmul(30));
	cooScreen=proj.calcCoorEcran(corner);
	cooScreen2=proj.calcCoorEcran(end);
	g.setColor(Color.black);
	g.drawLine((int)cooScreen[0],(int)cooScreen[1], (int)cooScreen2[0],(int)cooScreen2[1]);
	}
//point for the lens
int size=6;
cooScreen=proj.calcCoorEcran(lens);
g.fillOval((int)cooScreen[0]-size/2,(int)cooScreen[1]-size/2, size, size);

}


@Override
public double getDistanceToScreen(Projector proj)
{
// TODO Auto-generated method stub
return 0;
}

///**
// * get the pixel size in um
// * @return
// */
//public double[] getPixelSize()
//{
//return pixelSize;
//}
/**
 * get the pixel size in um
 * @return
 */
public double getPixelSizeX_um()
{
return pixelSize[0]*1000;
}
/**
 * get the pixel size in um
 * @return
 */
public double getPixelSizeY_um()
{
return pixelSize[1]*1000;
}

///**
// * set the pixel size
// * @param pixelSizeX in um
// * @param pixelSizeY in um
// */
//public void setPixelSize(double pixelSizeX_um,double pixelSizeY_um)
//{
//this.pixelSize[0] = pixelSizeX_um/1000;
//this.pixelSize[1] = pixelSizeY_um/1000;
//}


/**
 * set the pixel size
 * @param pixelSizeX in um
 * @param pixelSizeY in um
 */
public void setPixelSizeX_um(double pixelSize_um)
{
this.pixelSize[0] = pixelSize_um/1000;
}

/**
 * set the pixel size
 * @param pixelSizeX in um
 * @param pixelSizeY in um
 */
public void setPixelSizeY_um(double pixelSize_um)
{
this.pixelSize[1] = pixelSize_um/1000;
}


@Override
public boolean isOn(Point p, Projector proj)
{
// TODO Auto-generated method stub
return false;
}






} 
