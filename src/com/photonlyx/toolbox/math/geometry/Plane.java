package com.photonlyx.toolbox.math.geometry;

import java.util.Enumeration;
import java.util.Vector;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.cloud.Cloud;
import com.photonlyx.toolbox.math.geometry.cloud.CloudPoint;
import com.photonlyx.toolbox.math.matrix.FullMatrix;


/**
Simulate a plane </p>
*/

public class Plane  extends Shape implements XMLstorable
{
protected Vecteur pPlane=new Vecteur();//the centre point of the plane
protected Vecteur nPlane=new Vecteur(0,0,1);//local frame of the plane Z is normal


public  Plane()
{
pPlane.affect(0,0,0);
nPlane.affect(0,0,1);
}
/**
 * 
 * @param x point of the plane, x
 * @param y point of the plane, y
 * @param z point of the plane, z
 * @param ax frame FIRST turned by ax radian around x vector
 * @param ay frame SECOND turned by ay radian around y vector
 * @param az frame THIRD turned by az radian around z vector
 */
public  Plane(double x,double y,double z,double ax,double ay,double az)
{
pPlane.affect(x,y,z);
Rotator rot=new Rotator(ax,ay,az);
nPlane.affect(rot.turn(new Vecteur(0,0,1)));
}

/**
 * 
 * @param pPlane point of the plane
 * @param angles angles in rad, turn first X then Y then Z
 */
public  Plane(Vecteur pPlane,double[] angles)
{
this.pPlane.affect(pPlane);
Rotator rot=new Rotator(angles[0],angles[1],angles[2]);
nPlane.affect(rot.turn(new Vecteur(0,0,1)));
}

/**
 * create a plane with a point and a normal vector
 * @param pPlane
 * @param normal
 */
public  Plane(Vecteur _pPlane,Vecteur normal)
{
pPlane.affect(_pPlane);
nPlane.affect(normal);
}



/**give the distance to the plane  from the point pos in direction of dir*/
public double distance(Vecteur pos,Vecteur dir)
{
//intersection point between the ray and the surface
Vecteur i=Vecteur.intersectionPlaneLine(pPlane,nPlane,pos,dir);

//rayon parallele au plan
if ( i.sub(pos).mul(dir)<=1e-9) return 1e10;
//System.out.println("procedure distance a plan photon direction: "+dir;
//System.out.println("procedure distance a plan photon norme: "+dir.norme());
//System.out.println("i="+i);

//vector from photon to intersection
Vecteur pi=i.sub(pos);
//System.out.println("pi="+pi);

double sc=dir.mul(pi);
//System.out.println("sc="+sc);

//if (sc<=1e-5) return 1e10;
return sc;
}

/**give the shortest distance to the plane  from the point pos */
public double distance(Vecteur p)
{
return Math.abs(nPlane.mul(p.sub(pPlane)));
}

public double distanceWithSign(Vecteur p)
{
return nPlane.mul(p.sub(pPlane));
}

/**return the centre point of the plane*/
public Vecteur centre()
{
return pPlane;
}

/**return the normal vector at the point i (that must belong to the plane!)*/
public Vecteur normal(Vecteur i)
{
return nPlane;
}


/**return the normal of the plane*/
public Vecteur Z(){return nPlane;}



/**
 * return a new Vecteur which is the intersection of this plane with a line.
 * @param Pdroite
 * @param ndroite
 * @return null if plane // line
 */
public Vecteur intersectionLine(Vecteur Pdroite,Vecteur ndroite)
{
double tdroite;
Vecteur PdPp,u;
double r;

PdPp=pPlane.sub(Pdroite);
r=ndroite.mul(nPlane);
if (r!=0) tdroite=PdPp.mul(nPlane)/r;  else return null;
u=ndroite.scmul(tdroite);
return u.addn(Pdroite);
}

/**
 * return a new Vecteur which is the intersection of this plane with a line.
 * @param Pdroite
 * @param ndroite
 * @return null if plane // line
 */
public static Vecteur intersectionPlaneLine(Vecteur pPlane,Vecteur nPlane,Vecteur pLine,Vecteur nLine)
{
double tdroite;
Vecteur PdPp,u;
double r;
PdPp=pPlane.sub(pLine);
r=nLine.mul(nPlane);
if (r!=0) tdroite=PdPp.mul(nPlane)/r;  
else return null;//the line is parallel to the plane
u=nLine.scmul(tdroite);
return u.addn(pLine);
}

/**
 * get the line intersection of this plane with another
 * NOT FINISHED
 * @param p2
 * @return null if the planes are parallel
 */
public Line3D intersectionPlane(Plane p2)
{
Plane p1=this;
//the direction of the intersection line is the vector product of the 2 planes normals
Vecteur dir=p1.Z().vectmul(p2.Z());
//check if the planes are parallels:
if (dir.norme()==0) return null;

Vecteur pos=new Vecteur();

return new Line3D(pos,dir);

}


public Vecteur project(Vecteur p)
{
return intersectionLine(p,nPlane);
}

@Override
public Vecteur intersection(Vecteur pos, Vecteur dir)
{
//intersection point between the line and the surface
Vecteur i=Vecteur.intersectionPlaneLine(pPlane,nPlane,pos,dir);
return i;
}

/**
 * calc the  the plane passing through the origin closest to a cloud of points.
 *  Use the least square method solved analytically
 * @param points
 * @return
 */
public static Plane getClosestPlaneOLD(Vector<Vecteur> cloud)
{
//calc the center of the cloud
Vecteur c=new Vecteur();
for (Vecteur p:cloud) c._add(p);
c._scmul(1/cloud.size());
return new Plane(c,getClosestPlaneNormalOLD(cloud));
}

/**
 * doest'n work ..... to be debugged or reviewed.....
 * calc the  normal of the plane passing through the origin closest to a cloud of points.
 *  Use the least square method solved analytically
 * @param points
 * @return
 */
public static Vecteur getClosestPlaneNormalOLD(Vector<Vecteur> cloud)
{

//O origin (0,0,0) 
// the plane contains the origin
//n normal of the plane n=(nx,ny,nz)
// epsilon i = distance of point Pi to the plane= square root of scalar product: OPi.n
// S = sum (epsilon i)^2
// the partial derivative vector = (dS/dnx,dS/dny,dS/dnz) =0

//Matrix  A of the components of the partial derivative 
int n=cloud.size();
FullMatrix A=new FullMatrix(3,3);
Vecteur p;
for (int i=0;i<n;i++) 
	{
	p=cloud.elementAt(i);
	for (int l=0;l<3;l++)
		for (int c=0;c<=l;c++)
			A.affect(l, c, A.el(l, c)+p.coord(l)*p.coord(c));
	//complete the other half of this symetric matrix:
	for (int l=0;l<3;l++)
		for (int c=l+1;c<3;c++) A.affect(l, c, A.el(l, c));
	}
System.out.println(A);
//solve the system AtA.N=0 (N matrix of normals) with LU method:
FullMatrix B=new FullMatrix(3,1);
//B.affect(2, 0, 1);
int[] indx=new int[3];//for LU
FullMatrix.decompose_lu(A,indx);//A is destroyed!
FullMatrix.systeme_lu(A,indx,B);//now the solution is in nul!
//fill the normal data:
Vecteur normal=new Vecteur();
for (int ind=0;ind<3;ind++) normal.coordAffect(ind,B.get(ind,0));
//normal._normalise();
return normal;
}




/**
 * 
 * calc the  normal of the plane passing through the origin closest to a cloud of points.
 *  Use the least square method solved analytically
 * @param points
 * @return
 */
public static Plane getClosestPlane(Cloud cloud)
{
if( cloud.size()<3) 
	{
	System.out.println("cloud size <3 ("+cloud.size()+")");
	return null;
	}
/*
plane equation:  z=A*x+B*y+C
least square: minimize E(A,B,C)= sum [  (A*xi+B*yi+C-zi)^2  ]

see "cahier scanner3D.odt " page 6
*/

//center of gravity:
Vecteur p0=new Vecteur();
for (CloudPoint cp:cloud) 
	{
	//System.out.println("point:"+p);
	p0._add(cp.getP());
	}
p0._scmul(1.0/cloud.size());

//System.out.println("center of gravity:"+p0);

//moments
double sx2=0,sy2=0,sz2=0,sxy=0,sxz=0,syz=0;
double x,y,z;
for (CloudPoint cp:cloud) 
	{
	Vecteur p=cp.getP();
	x=p.x()-p0.x();
	y=p.y()-p0.y();
	z=p.z()-p0.z();
	sx2+=x*x;
	sy2+=y*y;
	sz2+=z*z;
	sxy+=x*y;
	sxz+=x*z;
	syz+=y*z;
	}
//System.out.println("sx2="+sx2);
//System.out.println("sy2="+sy2);
//System.out.println("sz2="+sz2);

boolean swapYZ=(sy2*5<sz2);
double r;
if (swapYZ)
	{
	//System.out.println("swapYZ");
	for (CloudPoint cp:cloud)  cp.getP().swapYZ();//swap Y and Z
	r=sy2;sy2=sz2;sz2=r;//swap
	r=sxy;sxy=sxz;sxz=r;//swap
	}

boolean swapXZ=(!swapYZ)&(sx2*5<sz2);
if (swapXZ)
	{
	System.out.println("swapXZ");
	for (CloudPoint cp:cloud)  cp.getP().swapXZ();//swap X and Z
	r=sx2;sx2=sz2;sz2=r;//swap
	r=sxy;sxy=syz;syz=r;//swap
	}


//fill the M1 matrix:
FullMatrix M1=new FullMatrix(3,3);
M1.affect(0,0,sx2);  M1.affect(0,1,sxy); 
M1.affect(1,0,sxy);  M1.affect(1,1,sy2);  
                                        M1.affect(2,2,1); 
//System.out.println("M1:"+M1);

//fill the M2 matrix:
FullMatrix M2=new FullMatrix(3,1);
M2.affect(0,0,sxz);
M2.affect(1,0,syz );
//System.out.println("M2:"+M2);

//solve the system by LU method:
int[] indx=new int[3];//for LU
FullMatrix.decompose_lu(M1,indx);//M1 is destroyed!
FullMatrix.systeme_lu(M1,indx,M2);//now the solution is in M2!

double A=M2.el(0, 0);
double B=M2.el(1, 0);
double C=M2.el(2, 0);

Vecteur Pplane=p0._add(0,0,C);
Vecteur normal=new Vecteur(A,B,-1).getNormalised();
//System.out.println("pplane:"+Pplane);
//System.out.println("normal:"+normal);

if (swapYZ)
	{
	for (CloudPoint cp:cloud)  cp.getP().swapYZ();//swap Y and Z
	normal.swapYZ();
	}
if (swapXZ)
	{
	for (CloudPoint cp:cloud)  cp.getP().swapXZ();//swap Y and Z
	normal.swapXZ();
	}

return new Plane(Pplane,normal);

}



public Vecteur getpPlane()
{
return pPlane;
}

public Vecteur getNormal()
{
return nPlane;
}


/**
 * return a frame with Z normal to the plane and X,Y such that the frame is direct and normalized
 * @return
 */
public Frame getLocalFrame()
{
if( (nPlane.x()==1)&&(nPlane.y()==0)&&(nPlane.y()==0)) 
	return new Frame(pPlane,new Vecteur(0,1,0),new Vecteur(0,0,1),new Vecteur(1,0,0)) ;
//get a vector perpendicular to Z from X
Vecteur X=new Vecteur(1,0,0);
//create X of local frame:
Vecteur localFrameX=X.sub(nPlane.scmul(nPlane.mul(X)));
localFrameX._normalise();
Vecteur localFrameY=nPlane.vectmul(localFrameX);
Frame localFrame=new Frame(pPlane,localFrameX,localFrameY,nPlane);
return localFrame;
}

public String toString()
{
String s="Plane\n";
s+="  pPlane="+pPlane+"\n";
s+="  nPlane="+nPlane;
return s;
}


@Override
public XMLElement toXML()
{
XMLElement el = new XMLElement();
el.setName(getClass().getSimpleName().toString().replace("class ", ""));

XMLElement nPlanexml=nPlane.toXML();
nPlanexml.setAttribute("name","nPlane");
System.out.println(nPlanexml);
el.addChild(nPlanexml);

XMLElement pPlanexml=pPlane.toXML();
pPlanexml.setAttribute("name","pPlane");
System.out.println(pPlanexml);
el.addChild(pPlanexml);

return el;
}

@Override
public void updateFromXML(XMLElement xml)
{
if (xml.getAttribute("ax")!=null)
	{
	double x=xml.getDoubleAttribute("x",0);
	double y=xml.getDoubleAttribute("y",0);
	double z=xml.getDoubleAttribute("z",0);
	double ax=xml.getDoubleAttribute("ax",0);
	double ay=xml.getDoubleAttribute("ay",0);
	pPlane.affect(x, y, z);
	Rotator rot=new Rotator(ax,ay,0);
	nPlane.affect(rot.turn(new Vecteur(0,0,1)));
	}
else
	{
	Enumeration<XMLElement> enumSons = xml.enumerateChildren();
	while (enumSons.hasMoreElements())
		{
		XMLElement son=enumSons.nextElement();
		if (son.getContent().compareTo("nPlane")==0) nPlane.updateFromXML(son);
		if (son.getContent().compareTo("pPlane")==0) pPlane.updateFromXML(son);
		}
	}
}


public Plane copy()
{
Plane p=new Plane();
p.nPlane=this.nPlane;
p.pPlane=this.pPlane;
return p;
}



}
