package com.photonlyx.toolbox.math.geometry;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import javax.swing.JFileChooser;

import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import com.photonlyx.toolbox.threeD.io.STLFileFilter;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;


public class TriangleMesh   implements Object3D, Iterable<Triangle3D>,TriangleMeshSource
{
	Vector<Triangle3D> vector=new Vector<Triangle3D>();//local coordinates
	
	
	public TriangleMesh()
	{
		super();
	}

	public TriangleMesh(String stl)
	{
		super();
		readSTL( stl);
	}

	public Triangle3DColor getTriangleSuchProjectionIcontainsPoint(double x, double y)
	{
		for (Object3D t:this) if (((Triangle3DColor)t).isInTriangle(x,y)) return ((Triangle3DColor)t);
		return null;
	}

	public void removeAllElements() {vector.removeAllElements();}
	
	/**
	 * add a triangle (local coordinates)
	 * @param t
	 */
	public void add(Triangle3D t) {vector.add(t);}
	
	public void add(TriangleMesh tm) {for (Triangle3D t:tm) add(t);}
	
	public int nbTriangles() {return vector.size();}
	
	/**
	 * get local triangle
	 * @param i
	 * @return
	 */
	public Triangle3D getTriangle(int i) {return vector.elementAt(i);}
	/**
	 * see http://fr.wikipedia.org/wiki/Fichier_de_st%C3%A9r%C3%A9olithographie
	 * @return
	 */
	public String saveToSTL()
	{
		StringBuffer sb=new StringBuffer();
		sb.append("solid Triangle3DSet\n");

		NumberFormat df6 = NumberFormat.getInstance(Locale.ENGLISH);
		df6.setMaximumFractionDigits(6);
		df6.setMinimumFractionDigits(6);
		df6.setGroupingUsed(false);

		//facet normal ni nj nk
		//outer loop
		//  vertex v1x v1y v1z
		//  vertex v2x v2y v2z
		//  vertex v3x v3y v3z
		//endloop
		//endfacet
		for (Triangle3D t:vector)
		{
			t.calcNormal();
			Vecteur n=t.getNormal();
			sb.append("facet normal "+n.x()+" "+n.y()+" "+n.z()+"\n");
			sb.append("outer loop\n");
			sb.append("\tvertex "+df6.format(t.p1().x())+" "+df6.format(t.p1().y())+" "+df6.format(t.p1().z())+"\n");
			sb.append("\tvertex "+df6.format(t.p2().x())+" "+df6.format(t.p2().y())+" "+df6.format(t.p2().z())+"\n");
			sb.append("\tvertex "+df6.format(t.p3().x())+" "+df6.format(t.p3().y())+" "+df6.format(t.p3().z())+"\n");
			sb.append("endloop\n");
			sb.append("endfacet\n");
		}
		sb.append("endsolid\n");

		return sb.toString();	
	}

	/**
	 * see https://en.wikipedia.org/wiki/Wavefront_.obj_file
	 * @return
	 */
	public String saveToOBJ()
	{
		StringBuffer sb=new StringBuffer();
		for (Object3D o:vector)
		{
			Triangle3D t=((Triangle3D)o);	
			sb.append("v "+t.p1().x()+" "+t.p1().y()+" "+t.p1().z()+"\n");
			sb.append("v "+t.p2().x()+" "+t.p2().y()+" "+t.p2().z()+"\n");
			sb.append("v "+t.p3().x()+" "+t.p3().y()+" "+t.p3().z()+"\n");
		}
		int c=1;
		for (Object3D o:this)
		{
			sb.append("f "+(c++)+" "+(c++)+" "+(c++)+"\n");
		}
		return sb.toString();	
	}

	public void saveToOBJ(String filename)
	{
	TextFiles.saveString(filename,saveToOBJ());
	}


	public String saveToPBRT()
	{
		StringBuffer sb=new StringBuffer();
		for (Object3D o:vector)
		{
			Triangle3DColor t=((Triangle3DColor)o);	
			//Vecteur n=t.getNormal();
			sb.append("Shape \"trianglemesh\" ");
			sb.append("\"integer indices\" [0 1 2 ]");
			sb.append("  \"point P\" [ ");
			sb.append(" "+t.p1().x()+" "+t.p1().y()+" "+t.p1().z()+" ");
			sb.append(" "+t.p2().x()+" "+t.p2().y()+" "+t.p2().z()+" ");
			sb.append(" "+t.p3().x()+" "+t.p3().y()+" "+t.p3().z()+" ");
			sb.append("  ] \n");
			//sb.append("\"normal N\" [0 1 2 ]");
			//sb.append(" "+n.x()+" "+n.y()+" "+n.z()+" ");
			//sb.append("\n");
		}
		return sb.toString();
	}


	public  void readSTLFile(String stlFullFileName)
	{
		//System.out.println(getClass()+" open "+stlFileName);
		String s=TextFiles.readFile(stlFullFileName).toString();
		readSTL(s);
	}

	public  void readSTL(String stl)
	{
		this.removeAllElements();
		//Triangle3DSet set=new Triangle3DSet();
		String[] lines=StringSource.readLines(stl);
		int lineNb=0;
		Definition def;
		if (new Definition(lines[lineNb]).word(0).compareTo("solid")!=0)
		{
			Messager.messErr(getClass()+" Can't find solid in first line");
			return ;
		}
		lineNb++;//go to 1rst facet
		while (new Definition(lines[lineNb]).word(0).compareTo("endsolid")!=0)
		{
			if (new Definition(lines[lineNb]).word(0).compareTo("facet")!=0)
			{
				Messager.messErr(getClass()+" Line "+lineNb+" Can't find facet in:"+lines[lineNb]);
				return ;
			}
			lineNb++;//go to outer loop
			lineNb++;//go to 1rst vertex
			Vecteur[] p=new Vecteur[3];
			int index=0;
			while ((def=new Definition(lines[lineNb])).word(0).compareTo("endloop")!=0)
			{
				double x=new Double(def.word(1)).doubleValue();
				double y=new Double(def.word(2)).doubleValue();
				double z=new Double(def.word(3)).doubleValue();
				p[index]=new Vecteur(x,y,z);
				index++;
				lineNb++;//go to next vertex or to end loop
			}
			lineNb++;//go to endfacet
			Triangle3DColor t=new Triangle3DColor(p[0],p[1],p[2]);
			this.add(t);
			lineNb++;//go to facet or endsolid
			//System.out.println(lineNb+" "+lines[lineNb]);
		}
	}




	/**
	 * convert an image of altitudes in a triangles set
	 * @param im
	 * @param set
	 * @param boxThickness if not zero create a box with upper surface sculpted with the image
	 */
	public static void convert(Signal2D1D im,TriangleMesh set,double boxThickness)
	{
		set.removeAllElements();
		double sx=((im.xmax()-im.xmin())/im.dimx());
		double sy=((im.ymax()-im.ymin())/im.dimy());
		for (int i=0;i<im.dimx()-1;i++)
			for (int j=0;j<im.dimy()-1;j++)
			{
				double x=im.xmin()+i*sx;
				double y=im.ymin()+j*sy;
				double z1=im.z(i, j);
				double z2=im.z(i+1, j);
				double z3=im.z(i+1, j+1);
				double z4=im.z(i, j+1);
				Vecteur p1=new Vecteur(x,y,z1);
				Vecteur p2=new Vecteur(x+sx,y,z2);
				Vecteur p3=new Vecteur(x+sx,y+sy,z3);
				Vecteur p4=new Vecteur(x,y+sy,z4);
				Triangle3DColor t1=new Triangle3DColor(p1,p2,p4,Color.black,true);
				Triangle3DColor t2=new Triangle3DColor(p2,p3,p4,Color.black,true);
				set.add(t1);
				set.add(t2);
			}
	
	if (boxThickness>0)
	{
	Signal2D1D imShapePostProcessed=im;
		
		//add the edges of a box having the shape as its upper face:
		double zBase=imShapePostProcessed.zmin()-boxThickness;
		for (int i=0;i<im.dimx()-1;i++)
			{
			double x=imShapePostProcessed.xmin()+i*sx;
			double y=imShapePostProcessed.ymin()+0*sy;
			double z1=imShapePostProcessed.z(i, 0);
			double z2=imShapePostProcessed.z(i+1, 0);
			Vecteur p1=new Vecteur(x,y,zBase);
			Vecteur p2=new Vecteur(x+sx,y,zBase);
			Vecteur p3=new Vecteur(x+sx,y,z2);
			Vecteur p4=new Vecteur(x,y,z1);
			set.add(new Triangle3D(p1,p2,p3));
			set.add(new Triangle3D(p1,p3,p4));
			}
		for (int i=0;i<im.dimx()-1;i++)
			{
			double x=imShapePostProcessed.xmin()+i*sx;
			double y=imShapePostProcessed.ymin()+(im.dimy()-1)*sy;
			double z1=imShapePostProcessed.z(i, im.dimy()-1);
			double z2=imShapePostProcessed.z(i+1, im.dimy()-1);
			Vecteur p1=new Vecteur(x,y,zBase);
			Vecteur p2=new Vecteur(x+sx,y,zBase);
			Vecteur p3=new Vecteur(x+sx,y,z2);
			Vecteur p4=new Vecteur(x,y,z1);
			set.add(new Triangle3D(p1,p2,p3));
			set.add(new Triangle3D(p1,p3,p4));
			}
		for (int j=0;j<im.dimy()-1;j++)
			{
			double x=imShapePostProcessed.xmin()+0*sx;
			double y=imShapePostProcessed.ymin()+j*sy;
			double z1=imShapePostProcessed.z(0, j);
			double z2=imShapePostProcessed.z(0, j+1);
			Vecteur p1=new Vecteur(x,y,zBase);
			Vecteur p2=new Vecteur(x,y+sy,zBase);
			Vecteur p3=new Vecteur(x,y+sy,z2);
			Vecteur p4=new Vecteur(x,y,z1);
			set.add(new Triangle3D(p1,p2,p3));
			set.add(new Triangle3D(p1,p3,p4));
			}
		for (int j=0;j<im.dimy()-1;j++)
			{
			double x=imShapePostProcessed.xmin()+(im.dimx()-1)*sx;
			double y=imShapePostProcessed.ymin()+j*sy;
			double z1=imShapePostProcessed.z(im.dimx()-1, j);
			double z2=imShapePostProcessed.z(im.dimx()-1, j+1);
			Vecteur p1=new Vecteur(x,y,zBase);
			Vecteur p2=new Vecteur(x,y+sy,zBase);
			Vecteur p3=new Vecteur(x,y+sy,z2);
			Vecteur p4=new Vecteur(x,y,z1);
			set.add(new Triangle3D(p1,p2,p3));
			set.add(new Triangle3D(p1,p3,p4));
			}
		//create face bellow
		double x1=imShapePostProcessed.xmin()+(0)*sx;
		double y1=imShapePostProcessed.ymin()+(0)*sy;
		double x2=imShapePostProcessed.xmin()+(im.dimx()-1)*sx;
		double y2=imShapePostProcessed.ymin()+(0)*sy;
		double x3=imShapePostProcessed.xmin()+(im.dimx()-1)*sx;
		double y3=imShapePostProcessed.ymin()+(im.dimy()-1)*sy;
		double x4=imShapePostProcessed.xmin()+(0)*sx;
		double y4=imShapePostProcessed.ymin()+(im.dimy()-1)*sy;
		Vecteur p1=new Vecteur(x1,y1,zBase);
		Vecteur p2=new Vecteur(x2,y2,zBase);
		Vecteur p3=new Vecteur(x3,y3,zBase);
		Vecteur p4=new Vecteur(x4,y4,zBase);
		set.add(new Triangle3D(p1,p2,p3));
		set.add(new Triangle3D(p1,p3,p4));
		}
	
	
	}
	
	public  void importSignal2D1D(Signal2D1D im)
	{
		TriangleMesh.convert(im,this,0);
	}
	
	public  void importSignal2D1D(Signal2D1D im,double boxThickness)
	{
		TriangleMesh.convert(im,this,boxThickness);
	}
	
	public void saveToSTL(String filename)
	{
	TextFiles.saveString(filename,saveToSTL());
	}
	
	
	
	/**
	 * save directly to a file, ask user where to save the file
	 */
	public void saveToSTLdialog()
	{
		//System.out.println(set.saveToSTL());
		JFileChooser df=new JFileChooser(Global.path);
		STLFileFilter ff=new STLFileFilter();
		df.addChoosableFileFilter(ff);
		df.setFileFilter(ff);

		NumberFormat df6 = NumberFormat.getInstance(Locale.ENGLISH);
		df6.setMaximumFractionDigits(6);
		df6.setMinimumFractionDigits(6);
		df6.setGroupingUsed(false);

		int returnVal = df.showSaveDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			Global.path=df.getSelectedFile().getParent()+File.separator;
			String filename=df.getSelectedFile().getName();
			if (!filename.endsWith(STLFileFilter.getExtension())) filename+="."+STLFileFilter.getExtension();
			try
			{
				FileOutputStream fo = new FileOutputStream(Global.path+filename);
				PrintWriter pw = new PrintWriter(fo);
				pw.print("solid Triangle3DSet\n");
				for (Object3D o:vector)
				{
					Triangle3DColor t=((Triangle3DColor)o);	
					t.calcNormal();
					Vecteur n=t.getNormal();
					pw.print("facet normal "+n.x()+" "+n.y()+" "+n.z()+"\n");
					pw.print("outer loop\n");
					pw.print("\tvertex "+df6.format(t.p1().x())+" "+df6.format(t.p1().y())+" "+df6.format(t.p1().z())+"\n");
					pw.print("\tvertex "+df6.format(t.p2().x())+" "+df6.format(t.p2().y())+" "+df6.format(t.p2().z())+"\n");
					pw.print("\tvertex "+df6.format(t.p3().x())+" "+df6.format(t.p3().y())+" "+df6.format(t.p3().z())+"\n");
					pw.print("endloop\n");
					pw.print("endfacet\n");
				}
				pw.print("endsolid\n");
				pw.flush();
				pw.close();
				fo.close();
			}
			catch (Exception e) 
			{
				Messager.messErr("TextFiles "+Messager.getString("Probleme_ecriture_fichier")+" "+filename);;
			}
			System.out.println("TextFiles "+filename+" saved");
		}	
	}






	/**
	 * 
	 * @param fullFilename
	 * @return
	 */
	public boolean updateFromBinarySTLfile(String fullFilename)
	{
		vector.removeAllElements();
		byte[] b0=new byte[80];
		//byte[] b1=new byte[4];
		BufferedInputStream br;
		try
		{
			br=new BufferedInputStream(new FileInputStream(fullFilename));
			//read header:
			br.read(b0,0,b0.length);
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading raw float image ");
			e.printStackTrace();
			return false;
		}


		byte[] bytes=new byte[4];
		int nbTriangles;

		//read the number of triangles:
		try
		{
			br.read(bytes,0,4);nbTriangles=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getInt();
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading raw float image ");
			e.printStackTrace();
			//br.close();
			return false;
		}

		System.out.println(getClass()+" Import STL binary: "+nbTriangles+" triangles");

		float x,y,z;
		//read data:
		try
		{
			for (int i=0;i<nbTriangles;i++)
			{
				br.read(bytes,0,4);x=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				br.read(bytes,0,4);y=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				br.read(bytes,0,4);z=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				Vecteur n=new Vecteur(x,y,z);
				br.read(bytes,0,4);x=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				br.read(bytes,0,4);y=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				br.read(bytes,0,4);z=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				Vecteur p1=new Vecteur(x,y,z);
				br.read(bytes,0,4);x=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				br.read(bytes,0,4);y=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				br.read(bytes,0,4);z=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				Vecteur p2=new Vecteur(x,y,z);
				br.read(bytes,0,4);x=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				br.read(bytes,0,4);y=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				br.read(bytes,0,4);z=ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN ).getFloat();
				Vecteur p3=new Vecteur(x,y,z);
				//pass the "attribute byte count":
				br.read();
				br.read();
				Triangle3DColor t=new Triangle3DColor(p1,p2,p3,Color.black);
				vector.add(t);
			}
			br.close();
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading STL binary file ");
			e.printStackTrace();
			return false;
		}
		System.out.println("STL binary file "+fullFilename+" loaded");

		return true;
	}


	private Vecteur getVecteur(byte[] b,int index)
	{
		double x=getFloat(b,index);
		double y=getFloat(b,index+4);
		double z=getFloat(b,index+8);
		return new Vecteur(x,y,z);
	}

	private double getFloat(byte[] b,int index)
	{
		return b[0+index]*Math.pow(256,3)+b[1+index]*Math.pow(256,2)+b[2+index]*256+b[3+index];
		//return b[3+index]*Math.pow(256,3)+b[2+index]*Math.pow(256,2)+b[1+index]*256+b[0+index];
	}



	public Vector<Segment3D> getIntersectedSegments(Plane plane)
	{
		Vector<Segment3D> list=new	Vector<Segment3D>();
		for (Object3D o:vector)
		{
			Triangle3DColor t=((Triangle3DColor)o);
			Segment3D seg=t.getIntersectedSegment(plane);
			if (seg!=null) list.add(seg);
		}
		return list;
	}





	/**
	 * add objects from the OBJ format
	 * https://en.wikipedia.org/wiki/Wavefront_.obj_file
	 * @param s
	 */
	public void readOBJ(String s)
	{
		vector.removeAllElements();
		// see spec in http://www.martinreddy.net/gfx/3d/OBJ.spec
		String[] li=StringSource.readLines(s,false);
		Vector<Vecteur> vertices=new Vector<Vecteur> ();
		for (String line:li)
		{
			if (line.length()<=1) continue;
			if (line.charAt(0)=='#') continue;
			Definition def=new Definition(line);
			if (line.startsWith("vt")) continue;
			if (line.startsWith("vn")) continue;
			if (line.charAt(0)=='v')
			{
				Vecteur v=new Vecteur(def.getValue(1,0),def.getValue(2,0),def.getValue(3,0));
				vertices.add(v);
			}
			if (line.charAt(0)=='f')
			{
				if (def.dim()==4)//triangle
				{
					int i1=new Integer(removeNormal(def.word(1)));
					int i2=new Integer(removeNormal(def.word(2)));
					int i3=new Integer(removeNormal(def.word(3)));
					Triangle3D t=new Triangle3D(vertices.elementAt(i1-1),
							vertices.elementAt(i2-1),vertices.elementAt(i3-1));
					vector.add(t);
				}
				if (def.dim()==5)//quad
				{
					int i1=new Integer(removeNormal(def.word(1)));
					int i2=new Integer(removeNormal(def.word(2)));
					int i3=new Integer(removeNormal(def.word(3)));
					int i4=new Integer(removeNormal(def.word(4)));
					Triangle3D t1=new Triangle3D(vertices.elementAt(i1-1),vertices.elementAt(i2-1),vertices.elementAt(i3-1));
					Triangle3D t2=new Triangle3D(vertices.elementAt(i1-1),vertices.elementAt(i3-1),vertices.elementAt(i4-1));
					vector.add(t1);
					vector.add(t2);
				}
			}
		}
		//System.out.println(this.getClass()+" load "+vector.size()+" triangles");
	}


	private static String removeNormal(String s)
	{
		int i=s.indexOf("/");
		if (i!=-1)s=s.substring(0,i);
		return s;
	}




	/**
	 * get the min cube containing the objects
	 * @return array of 2 Vecteur (xmin,ymin,zmin) and (xmax,ymax,zmax)
	 */
	public Vecteur[] getOccupyingCube()
	{
		Vecteur cornermin=new Vecteur(1000000000,1000000000,1000000000);
		Vecteur cornermax=new Vecteur(-1000000000,-1000000000,-1000000000);
		for (Object3D o:vector) o.checkOccupyingCube(cornermin,cornermax);
		Vecteur[] cube=new Vecteur[2];
		cube[0]=cornermin;
		cube[1]=cornermax;
		return cube;
	}





	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (Triangle3D t:vector) t.checkOccupyingCube(cornerMin, cornerMax);
	}


	@Override
	public void draw(Graphics g, Projector proj) 
	{
		for (Triangle3D t:vector) t.draw(g, proj); 
	}


	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		return 0;
	}


	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		for (Triangle3D t:vector) 
			{
			if (t.isOn(p, proj)) return true; 
			}
		return false;
	}

	@Override
	public Iterator<Triangle3D> iterator() 
	{
			return vector.iterator();
	}

	@Override
	public TriangleMesh getTriangles() {
		return this;
	}

	


}
