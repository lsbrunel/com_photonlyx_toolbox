package com.photonlyx.toolbox.math.geometry;

import java.awt.Graphics;
import java.awt.Point;

public class Quadrilatere3DFrame   implements Object3DFrame,TriangleMeshSource
{
	private Frame frame=new Frame();//local frame of the cylinder
	private Quadrilatere3D local;
	private Quadrilatere3D global;
	private TriangleMesh tris=new TriangleMesh();
	
	
	public  Quadrilatere3DFrame()
	{
		this.local=new Quadrilatere3D(new Vecteur(),new Vecteur(1,0,0),new Vecteur(1,1,0),new Vecteur(0,1,0));
		allocateGlobal();
		allocateTriangles();
	}
	
	public  Quadrilatere3DFrame(Quadrilatere3D q)
	{
		this.local=q;
		allocateGlobal();
		allocateTriangles();
	}
	
	
	private void allocateGlobal()
	{
		global=new Quadrilatere3D();
		updateGlobal() ;
	}
	
	private void allocateTriangles()
	{
		tris.removeAllElements();
		tris.add(new Triangle3D(global.p1(),global.p2(),global.p3()));
		tris.add(new Triangle3D(global.p1(),global.p3(),global.p4()));
		updateGlobal() ;
	}
	
	
	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		global.checkOccupyingCube(cornerMin, cornerMax);
	}
	
	@Override
	public void draw(Graphics g, Projector proj) 
	{
		global.draw(g, proj);
	}
	
	@Override
	public double getDistanceToScreen(Projector proj) 
	{
		return global.getDistanceToScreen(proj);
	}
	
	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		return global.isOn(p, proj);
	}
	
	@Override
	public TriangleMesh getTriangles() 
	{
		return tris;
	}
	
	@Override
	public Frame getFrame() 
	{
		return frame;
	}
	
	@Override
	public void updateGlobal() 
	{
		for (int i=0;i<4;i++)
		{
			frame.global(global.p(i), local.p(i));
		}
		
		
	}

}
