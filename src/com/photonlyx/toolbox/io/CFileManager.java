

package com.photonlyx.toolbox.io;


/**
interface for  objects that manipulate a file
*/
public interface CFileManager
{
/**
save the file.
@return false if no filename available
*/
public boolean save();

/**
save the file with a new filename
@param path the path of the file directory
@param filename the file name
*/
public boolean save(String path,String filename);

/**
load the file
@return true if could be loaded
*/
public boolean load();

/**
open a new file
@param path the path of the file directory
@param filename the file name
@return the filename string para
*/
public boolean open(String path,String filename);

/**
create a new file
@param newFileName the name of the new file
*/
public void newFile(String newFileName);


/**
tells if the file is updated or not
@return if true the file is up to date, if false the objects must be saved
*/
public boolean isSaved();


/**
set the objects with the status "saved"
*/
public void setSaved();

/**
tells if the file is new or has been already saved
*/
public boolean isNewFile();

public String getFileName();
public String getPath();


}
