

package com.photonlyx.toolbox.io;


import java.io.File;
import java.text.DecimalFormat;
import javax.swing.*;

import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.util.Global;



/**
ask the JoloFileManager son to create a new file
*/
public class OpenFileAction 
{
//links
private CFileManager jfm;
private CFileFilter jff;
private CJFrame fatherWindow;




public OpenFileAction(CFileManager jfm, CFileFilter jff,CJFrame fatherWindow)
{
	this.jfm = jfm;
	this.jff = jff;
	this.fatherWindow=fatherWindow;
}




/**
open a file chooser 
*/
public void work()
{
String ch=fatherWindow.getApp().getPath();
if (ch==null) ch=".";
JFileChooser df=new JFileChooser(ch);
df.addChoosableFileFilter(jff);
df.setFileFilter(jff);
int returnVal = df.showOpenDialog(null);
if(returnVal == JFileChooser.APPROVE_OPTION)
	{
	String path=df.getSelectedFile().getParent()+File.separator;
	fatherWindow.getApp().setPath(path);
	System.out.println(getClass()+" path="+path);
	String fileName=df.getSelectedFile().getName();
	jfm.open(path,fileName);
	if (fatherWindow!=null) fatherWindow.setTitle(/*path+*/fileName);
	}
else 
	{
	return;
	}
}






}