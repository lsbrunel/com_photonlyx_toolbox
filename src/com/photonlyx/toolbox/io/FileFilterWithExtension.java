package com.photonlyx.toolbox.io;

import java.io.FileFilter;

public interface FileFilterWithExtension extends FileFilter
{
	public String getFileExtension();
	
	public String getDescription();
}
