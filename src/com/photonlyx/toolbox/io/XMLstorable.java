package com.photonlyx.toolbox.io;

import nanoxml.XMLElement;

public interface XMLstorable 
{

public XMLElement toXML();
public void updateFromXML(XMLElement xml);


}
