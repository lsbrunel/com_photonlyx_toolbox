package com.photonlyx.toolbox.param;


import java.lang.reflect.Field;
import java.lang.reflect.Method;

import nanoxml.XMLElement;


public class ParInt extends Param 
{




public ParInt(Object paramOwner, String paramName) 
{
super(paramOwner, paramName);
}



public  String stringVal(){return ""+val();}


public int val()
{
Field field=null;
try
	{
	field=getParamOwner().getClass().getField(getParamName());
	if (field!=null) 
		{
		return field.getInt(getParamOwner());
		}
	}
catch (Exception e){};

Method m=getMethodGet();
if (m==null) return 0;
Integer dd=null;
try {dd=(Integer)m.invoke(getParamOwner(), (Object[])null);}
catch(Exception e){e.printStackTrace();}
int val;
if (dd!=null) val=dd.intValue();else val=0;
return val;
}



public boolean setVal(String string) 
{
int previous=val();
try
	{
	int val=new Integer(string).intValue();
	if (val!=previous) setChanged(true);
	setVal(val);
	return true;
	}
catch (NumberFormatException e)
	{
	return false;
	}
}

public void setVal(int d) 
{
Field field=null;
try
	{
	field=getParamOwner().getClass().getField(getParamName());
	if (field!=null) 
		{
		field.set(getParamOwner(), d);
		return;
		}
	}
catch (Exception e){};

Method m=getMethodSet();
if (m==null) return;
Integer dd=new Integer(d);
try {m.invoke(getParamOwner(), dd);}
catch(Exception e){e.printStackTrace();}
}


public void updateAttributes(XMLElement xml)
{
super.updateAttributes(xml);
setVal(xml.getIntAttribute("VAL"));
}


}
