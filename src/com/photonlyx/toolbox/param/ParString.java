package com.photonlyx.toolbox.param;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import nanoxml.XMLElement;


public class ParString extends Param
{
//public String val;





public ParString(Object paramOwner, String paramName) 
{
super(paramOwner, paramName);
}


//public String val()
//{
//Method[] methods=getParamOwner().getClass().getMethods();
//String ss=null;
//for (Method m:methods) 
//	{
//	String name=m.getName();
//	//System.out.println(getClass()+" "+name);
//	if (  (name.compareTo("get"+this.getParamName())==0) )
//		{
//		try {ss=(String)m.invoke(getParamOwner(), (Object[])null);}
//		catch(Exception e){e.printStackTrace();}
//		break;
//		}
//	}	
//return ss;
//}

public String val()
{
Field field=null;
try
	{
	field=getParamOwner().getClass().getField(getParamName());
	if (field!=null) 
		{
		return (String)field.get(getParamOwner());
		}
	}
catch (Exception e){};

Method m=getMethodGet();
if (m==null) return null;
String dd=null;
try {dd=(String)m.invoke(getParamOwner(), (Object[])null);}
catch(Exception e){e.printStackTrace();}
return dd;
}



public boolean setVal(String dd) 
{
//System.out.println(getClass()+" setting val "+this.getParamName()+" to :"+dd);
Field field=null;
try
	{
	field=getParamOwner().getClass().getField(getParamName());
	if (field!=null) 
		{
		field.set(getParamOwner(), dd);
		return true;
		}
	}
catch (Exception e){};

if (dd==null) dd="null";
String previous=val();
//System.out.println(previous);

if (previous!=null) 
	if (dd.compareTo(previous)!=0) setChanged(true);

Method m=getMethodSet();
if (m==null) return false;
try {m.invoke(getParamOwner(), dd);}
catch(Exception e){e.printStackTrace();}
return true;
}




public  String stringVal(){return val();}



public void updateAttributes(XMLElement xml)
{
//super.updateAttributes(xml);
setVal(xml.getStringAttribute("VAL"));
}


}
