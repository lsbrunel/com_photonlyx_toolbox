package com.photonlyx.toolbox.param;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import nanoxml.XMLElement;


public class ParDouble extends Param
{



//public double val;
public String format="0.000000";
private static DecimalFormatSymbols dfs=new DecimalFormatSymbols(Locale.ENGLISH);



public ParDouble(Object paramOwner, String paramName) 
{
super(paramOwner, paramName);
}


public String getFormat() {
	return format;
}


public void setFormat(String format) {
	this.format = format;
}


//public double valOLD()
//{
//Method[] methods=getParamOwner().getClass().getMethods();
//Double dd=null;
//for (Method m:methods) 
//	{
//	String name=m.getName();
//	//System.out.println(name);
//	if (  (name.compareTo("get"+this.getParamName())==0) )
//		{
//		try {dd=(Double)m.invoke(getParamOwner(), (Object[])null);}
//		catch(Exception e){e.printStackTrace();}
//		break;
//		}
//	}	
//double val;
//if (dd!=null) val=dd.doubleValue();else val=0;
//return val;
//}

public double val()
{
Field field=null;
try
	{
	field=getParamOwner().getClass().getField(getParamName());
	if (field!=null) 
		{
		return field.getDouble(getParamOwner());
		}
	}
catch (Exception e){};

Method m=getMethodGet();
if (m==null) return 0;
Double dd=null;
try {dd=(Double)m.invoke(getParamOwner(), (Object[])null);}
catch(Exception e){e.printStackTrace();}
double val;
if (dd!=null) val=dd.doubleValue();else val=0;
return val;
}




public String stringVal()
{
String s;
//if (format!=null) 
	{
	NumberFormat nf=new DecimalFormat(format,dfs);
	s=nf.format(val());
	}
//else s=""+val();
return s;
}
	
public boolean setVal(String s) 
{
double previous=val();
try
	{
	double val=new Double(s.replace(',','.')).doubleValue();
	if (val!=previous) setChanged(true);
	setVal(val);
	return true;
	}
catch (NumberFormatException e)
	{
	return false;
	}
}

//public void setValOLD(double d) 
//{
////System.out.println(getClass()+" setVal "+d);
//Method[] methods=getParamOwner().getClass().getMethods();
//Double dd=new Double(d);
//for (Method m:methods) 
//	{
//	String name=m.getName();
//	//System.out.println("Method:"+name);
//	if (  (name.compareTo("set"+this.getParamName())==0) )
//		{
//		try {m.invoke(getParamOwner(), dd);}
//		catch(Exception e){e.printStackTrace();}
//		break;
//		}
//	}	
//}

public void setVal(double d) 
{
Field field=null;
try
	{
	field=getParamOwner().getClass().getField(getParamName());
	if (field!=null) 
		{
		field.set(getParamOwner(), d);
		return;
		}
	}
catch (Exception e){};

Method m=getMethodSet();
if (m==null) return;
Double dd=new Double(d);
try {m.invoke(getParamOwner(), dd);}
catch(Exception e){e.printStackTrace();}
}

public void updateAttributes(XMLElement xml)
{
super.updateAttributes(xml);
setVal(xml.getDoubleAttribute("VAL"));
}

}
