package com.photonlyx.toolbox.chartApp;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.chart.Chart;
import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ParDoubleBox;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.imageGui.ImageWindow;
import com.photonlyx.toolbox.imageGui.PanelImage;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.TriangleMesh;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Quadrilatere3DColor;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;

public class ChartApp  extends WindowApp implements XMLstorable
{	
	private ChartPanel chartPanel=new ChartPanel();
	private PanelImage pi;
	private JTextArea jta;
	private Graph3DPanel  graph3DPanel =new Graph3DPanel();
	private Object3DColorSet shape3D=new Object3DColorSet();
	public double finalMinThickness=2;
	public boolean invertZ;
	public double tiltx,tilty;//post processing, tilt the shape after calculation, in mrad
	public boolean createBox=false;//post processing, if true put the shape as the upper face a box
	public double mulZ=1;//post processing, multiply z by a factor

	public ChartApp()
	{
		//super("com_cionin_chart_chartapp");
		super("com_photonlyx_chartapp",false,true,true);
		chartPanel.setApp(this);
		this.putMenuFile(new XMLFileStorage(this),new CFileFilter("cchart xml","cionin XML chart data"));

		chartPanel.hidePanelSide();
		chartPanel.getSplitPane().setDividerLocation(700);

		//panel for the chart
		JPanel jpmain=new JPanel();
		jpmain.setLayout(new BorderLayout());
		jta=new JTextArea();
		jta.setBackground(Global.background);

		//split pane for graph and legend:
		JSplitPane sp = new JSplitPane(JSplitPane.VERTICAL_SPLIT, chartPanel ,jta);
		sp.setDividerLocation(500);
		sp.setOneTouchExpandable(true); 
		sp.setContinuousLayout(true); 
		sp.setResizeWeight(1);
		jpmain.add(sp);

		this.add(jpmain, "Chart");

		AnalysePanel analysePanel=new AnalysePanel(chartPanel.getChart());
		analysePanel.addXCalculation("index");
		analysePanel.addXCalculation("time");
		analysePanel.addXCalculation("lambda");
		analysePanel.addXCalculation("user defined");

		analysePanel.addYCalculation("yValue");
		analysePanel.addYCalculation("integralProfile");
		analysePanel.addYCalculation("spotSize");
		analysePanel.addYCalculation("mean");
		analysePanel.addYCalculation("user defined");

		this.add(analysePanel,"Analyse");

		//set the file data and trigger
		TriggerList fileMenuTriggerList=new TriggerList();
		fileMenuTriggerList.add(new Trigger(chartPanel,"update"));
		this.getxMLFileStorage().addThingToStore(chartPanel.getChart());
		this.getxMLFileStorage().addThingToStore(this);
		this.getFileMenuTriggerList().add(fileMenuTriggerList);

		CImageProcessing cimp=new CImageProcessing();
		cimp.loadResource("com/photonlyx/chartApp/Penelope-Cruz.jpg");
		pi=new PanelImage(cimp);
		this.add(pi,"Image");


		this.add(graph3DPanel.getJPanel(), "3D");
		graph3DPanel.setRender(false);
		graph3DPanel.setWireFrame(true);
		//button to create 3D shape:
		{
			TriggerList tl_button=new TriggerList();
			tl_button.add(new Trigger(this,"updateGraph3DPanelShape"));
			tl_button.add(new Trigger(graph3DPanel,"update"));
			CButton cButton=new CButton(tl_button);
			cButton.setText("create3D");
			graph3DPanel.getPanelSide().add(cButton);
		}

		{
			String[] names1={"invertZ","tiltx","tilty","mulZ","createBox","finalMinThickness"};
			String[] units1={"","mrad","mrad","","","mm"};
			ParamsBox box1=new ParamsBox(this,null,"Post processing",names1,units1);
			graph3DPanel.getPanelSide().add(box1.getComponent());
			//change mouse wheel effect:
			ParDoubleBox pdb1=(ParDoubleBox)box1.getParamBoxOfParam("tiltx");
			pdb1.setWheelAdd(true);
			pdb1.setWheelCoef(10);
			ParDoubleBox pdb2=(ParDoubleBox)box1.getParamBoxOfParam("tilty");
			pdb2.setWheelAdd(true);
			pdb2.setWheelCoef(10);
		}

		{
			//button
			TriggerList tl_button5=new TriggerList();
			tl_button5.add(new Trigger(this,"saveShapeAsSTL"));
			//tl_button5.add(new Trigger(this,"saveShapeAsBinarySTL"));
			CButton cButton5=new CButton(tl_button5);
			cButton5.setText("Save as STL");
			graph3DPanel.getPanelSide().add(cButton5);
		}

	}


	//
	//
	//public void openFile(String path,String filename)
	//{
	//this.getxMLFileStorage().open(path, filename);
	//}

	public void updateGraph3DPanelShape()
	{
		//System.out.print("update 3D view ...");
		//graph3DPanel.removeAll3DObjects();
		//graph3DPanel.add(chartPanel.getChart().getSignal2D1D());
		//graph3DPanel.initialiseProjectorAuto();
		//graph3DPanel.repaint();
		//System.out.println("... done");


		graph3DPanel.addColorSet(shape3D);
		shape3D.removeAll();
		//calculateFinalShape();
		shape3D.addImageOfAltitudesAsQuadrilaters(chartPanel.getChart().getSignal2D1D());//add the image to the graph 3D
		showFinalShape();

		graph3DPanel.initialiseProjectorAuto();

	}



	/**
	 * create a closed box with normal going exterior
	 */
	public void showFinalShape()
	{
		Signal2D1D imShape0=chartPanel.getChart().getSignal2D1D();
		//post processing
		Signal2D1D imShape=imShape0.copy();
		if (mulZ!=1) imShape._multiply(mulZ);
		if (invertZ) imShape._multiply(-1);
		if (tiltx!=0) imShape._tiltx(tiltx*.001);	
		if (tilty!=0) imShape._tilty(tilty*.001);	

		graph3DPanel.removeAll3DObjects();
		graph3DPanel.addColorSet(shape3D);
		shape3D.removeAll();
		//calculateFinalShape();
		shape3D.addImageOfAltitudesAsQuadrilaters(imShape);//add the image to the graph 3D
		//Triangle3DSet.convert(imShapePostProcessed,trianglesShape);
		//graph3DPanel.add(trianglesShape);
		int w=imShape.dimx();
		int h=imShape.dimy();
		if (createBox) 
		{
			//add the edges of a box having the shape as its upper face:
			Quadrilatere3DColor q;
			double sx=((imShape.xmax()-imShape.xmin())/imShape.dimx());
			double sy=((imShape.ymax()-imShape.ymin())/imShape.dimy());
			double zBase=imShape.zmin()-finalMinThickness;
			for (int i=0;i<w-1;i++)
			{
				double x=imShape.xmin()+i*sx;
				double y=imShape.ymin()+0*sy;
				double z1=imShape.z(i, 0);
				double z2=imShape.z(i+1, 0);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x+sx,y,zBase);
				Vecteur p3=new Vecteur(x+sx,y,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				q=new Quadrilatere3DColor(p1,p2,p3,p4);
				shape3D.add(q);
			}
			for (int i=0;i<w-1;i++)
			{
				double x=imShape.xmin()+i*sx;
				double y=imShape.ymin()+(h-1)*sy;
				double z1=imShape.z(i, h-1);
				double z2=imShape.z(i+1, h-1);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x+sx,y,zBase);
				Vecteur p3=new Vecteur(x+sx,y,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				q=new Quadrilatere3DColor(p1,p4,p3,p2);
				shape3D.add(q);
			}
			for (int j=0;j<h-1;j++)
			{
				double x=imShape.xmin()+0*sx;
				double y=imShape.ymin()+j*sy;
				double z1=imShape.z(0, j);
				double z2=imShape.z(0, j+1);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x,y+sy,zBase);
				Vecteur p3=new Vecteur(x,y+sy,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				q=new Quadrilatere3DColor(p1,p4,p3,p2);
				shape3D.add(q);
			}
			for (int j=0;j<h-1;j++)
			{
				double x=imShape.xmin()+(w-1)*sx;
				double y=imShape.ymin()+j*sy;
				double z1=imShape.z(w-1, j);
				double z2=imShape.z(w-1, j+1);
				Vecteur p1=new Vecteur(x,y,zBase);
				Vecteur p2=new Vecteur(x,y+sy,zBase);
				Vecteur p3=new Vecteur(x,y+sy,z2);
				Vecteur p4=new Vecteur(x,y,z1);
				q=new Quadrilatere3DColor(p1,p2,p3,p4);
				shape3D.add(q);
			}
			//create face bellow
			double x1=imShape.xmin()+(0)*sx;
			double y1=imShape.ymin()+(0)*sy;
			double x2=imShape.xmin()+(w-1)*sx;
			double y2=imShape.ymin()+(0)*sy;
			double x3=imShape.xmin()+(w-1)*sx;
			double y3=imShape.ymin()+(h-1)*sy;
			double x4=imShape.xmin()+(0)*sx;
			double y4=imShape.ymin()+(h-1)*sy;
			Vecteur p1=new Vecteur(x1,y1,zBase);
			Vecteur p2=new Vecteur(x2,y2,zBase);
			Vecteur p3=new Vecteur(x3,y3,zBase);
			Vecteur p4=new Vecteur(x4,y4,zBase);
			q=new Quadrilatere3DColor(p1,p4,p3,p2);
			shape3D.add(q);
		}
	}


	public void saveShapeAsSTL()
	{
		TriangleMesh triangles=shape3D.transformToTriangles();
		triangles.saveToSTLdialog();
	}

	public static void main(String[] args) 
	{
		ChartApp chartApp;
		//Global.background=new Color(255,240,240);
		//Global.background=new Color(240,240,240);
		

		System.out.println(args.length+" argument(s)");
		for (String s:args) System.out.println(s+" ");
		//System.out.println();
		//Messager.mess("path: "+new File(".").getAbsolutePath());

		//if (args.length>2) if ((args[args.length-1]).endsWith(".cchart"))
		//	{
		//	String fileName="";
		//	for (String s:args) fileName+=s+" ";
		//	System.out.println("Open file:"+Global.path+fileName);
		//	chartApp=new ChartApp2();
		//	Global.path="";
		//	chartApp.getxMLFileStorage().open(Global.path,fileName);
		//	chartApp.getCJframe().setTitle(fileName);
		//	return;
		//	}

		if (args.length==1)
		{
			Global.path="./";
			//System.out.println("Global.path======"+Global.path);
			String fileName=args[0];
			//System.out.println(variableValue("Filename: "+args[0]));
			if (args[0].compareTo("-h")==0)
			{
				System.out.println(" options:");
				System.out.println(" file.csv  -> show graph encode in a csv file");
				System.out.println(" file.cchart  -> show cchart");
				System.out.println(" image.raw  -> show image encoded in floats (big endian)");
				System.out.println(" image.raw  -littleEndian -> show image encoded in floats (little endian)");
			}
			else if (fileName.endsWith(".csv"))
			{
				PlotSet ps=new PlotSet();
				chartApp=new ChartApp();
				boolean namesInFirstLine=false;
				ps.putStringInPlots(TextFiles.readFile(fileName).toString(), namesInFirstLine);
				chartApp.getChartPanel().getChart().add(ps);
				chartApp.getChartPanel().update();
			}
			else if (fileName.endsWith(".cchart"))
			{
				System.out.println("Open file:"+Global.path+fileName);
				chartApp=new ChartApp();
				chartApp.path="./";
				chartApp.getxMLFileStorage().open(Global.path,fileName);
				chartApp.getCJFrame().setTitle(fileName);
			}
			else if (fileName.endsWith(".raw"))
			{
				chartApp=new ChartApp();
				chartApp.path="./";
				Signal2D1D im=new Signal2D1D();
				im.readRawFloatImage(args[0],false);//bigEndian by default
				Chart c=chartApp.getChartPanel().getChart();
				c.setSignal2D1D(im);
				c.setGridLinex(false);
				c.setGridLiney(false);
				c.setKeepXYratio(true);
				chartApp.getChartPanel().hideLegend();
				chartApp.getChartPanel().showPalette();
				chartApp.getChartPanel().getChart().update();
			}
			else if (fileName.endsWith(".jpg"))
			{
				chartApp=new ChartApp();
				
				//load as Image
				CImageProcessing cimp=new CImageProcessing();
				chartApp.path="./";
				cimp.loadFile(chartApp.path, fileName, false);
				chartApp.getPanelImage().getCImage().loadFile(chartApp.path, fileName, false);
				//chartApp.selectPane(chartApp.getPanelImage());
				
				//load as matrix
				Signal2D1D im=new Signal2D1D();
				im.fillWithImage(chartApp.getPanelImage().getCImage().getImage(), "R", 1);
				Chart c=chartApp.getChartPanel().getChart();
				c.setSignal2D1D(im);
				c.setGridLinex(false);
				c.setGridLiney(false);
				c.setKeepXYratio(true);
				c.setFormatTickx("0");
				c.setFormatTicky("0");
				
				im._multiply(1/255.0);
				
				chartApp.getChartPanel().getChart().update();
				//chartApp.selectPane(chartApp.getChartPanel());
			}
			else if (fileName.endsWith(".obj"))
			{	
				chartApp=new ChartApp();
				Global.path="./";
				StringBuffer sb=TextFiles.readFile(Global.path+fileName);
				Object3DColorSet set=new Object3DColorSet();
				set.addObjObjects(sb.toString(),Color.BLUE);
				chartApp.getGraph3DPanel().addColorSet(set);
				chartApp.getGraph3DPanel().initialiseProjectorAuto();
				chartApp.selectPane(chartApp.getGraph3DPanel().getJPanel());
			}
		}
		//else if (args.length==2)
		//		{
		//		if ( args[0].endsWith(".raw") && args[1].compareTo("-littleEndian")==0 )
		//			{
		//			System.out.println("Filename: "+args[0]+" "+args[1]);
		//			chartApp=new ChartApp2();
		//			Global.path="./";
		//			Signal2D1D im=new Signal2D1D();
		//			im.readRawFloatImage(args[0],true);
		//			im._flipX();
		//			Chart c=chartApp.getChartPanel().getChart();
		//			c.setSignal2D1D(im);
		//			c.setGridLinex(false);
		//			c.setGridLiney(false);
		//			c.setKeepXYratio(true);
		//			chartApp.getChartPanel().showPalette();
		//			chartApp.getChartPanel().getChart().update();
		//				
		//			}
		//		}
		else if ((args.length==3)
				&&(args[0].endsWith(".raw"))&&(args[1].endsWith(".raw"))&&(args[2].endsWith(".raw")))
		{
			//compose the R G B channels
			Signal2D1D imR=new Signal2D1D();
			imR.readRawFloatImage(args[0],true);
			//imR._flipX();
			Signal2D1D imG=new Signal2D1D();
			imG.readRawFloatImage(args[1],true);
			//imG._flipX();
			Signal2D1D imB=new Signal2D1D();
			imB.readRawFloatImage(args[2],true);
			//imB._flipX();

			double max=Math.max(imR.maxValue(), imG.maxValue());
			max=Math.max(max, imB.maxValue());

			System.out.println("max="+max);


			int[] pix=new int[imR.dimx()*imR.dimy()];
			int w=imR.dimx();
			int h=imR.dimy();
			int r,g,b;
			for (int i=0;i<w;i++) 
				for (int j=0;j<h;j++) 
				{
					r=(int)(imR.getValue(i, j)/max*255);
					g=(int)(imG.getValue(i, j)/max*255);
					b=(int)(imB.getValue(i, j)/max*255);
					if (r<0) r=0;	if (r>255) r=255;
					if (g<0) g=0;	if (g>255) g=255;
					if (b<0) b=0;	if (b>255) b=255;
					//System.out.println("rgb="+r+" "+g+" "+b);
					pix[i+j*w]=new Color(r,g,b).getRGB() ;
				}

			CImageProcessing cimp=new CImageProcessing(w,h);
			cimp.setPixelsDirect(pix, w, h);
			cimp.saveImage("./","imageRGB.png");
			ImageWindow iw=new ImageWindow(cimp);
		}
		else
		{
			chartApp=new ChartApp();
			boolean hasObj=false;
			Color[] listc= {Color.blue,Color.red,Color.cyan,Color.green,Color.magenta,Color.pink};
			int indexc=0;
			for (String word:args)
			{
				if ( args[0].endsWith(".raw") && args[1].compareTo("-littleEndian")==0 )
				{
					System.out.println("Filename: "+args[0]+" "+args[1]);
					chartApp.path="./";
					Signal2D1D im=new Signal2D1D();
					im.readRawFloatImage(args[0],true);
					im._flipX();
					Chart c=chartApp.getChartPanel().getChart();
					c.setSignal2D1D(im);
					c.setGridLinex(false);
					c.setGridLiney(false);
					c.setKeepXYratio(true);
					chartApp.getChartPanel().showPalette();
					chartApp.getChartPanel().getChart().update();

				}
				if (word.endsWith(".obj"))
				{
					System.out.println("Load: "+word);
					hasObj=true;
					chartApp.path="./";
					StringBuffer sb=TextFiles.readFile(chartApp.path+word);
					Object3DColorSet set=new Object3DColorSet();
					set.addObjObjects(sb.toString(),listc[(indexc++)%listc.length]);
					chartApp.getGraph3DPanel().addColorSet(set);	
				}
			}
			if (hasObj)
			{
				chartApp.getGraph3DPanel().initialiseProjectorAuto();
				chartApp.selectPane(chartApp.getGraph3DPanel().getJPanel());
			}

			{
				Signal1D1DXY sig=new Signal1D1DXY();
				sig.addPoint(0, 1);
				sig.addPoint(1, 2);
				sig.addPoint(3, 1);
				Plot p=new Plot(sig);
				chartApp.getChartPanel().add(p);
			}
			{
				Signal1D1DXY sig=new Signal1D1DXY();
				sig.addPoint(0, 11);
				sig.addPoint(1, 12);
				sig.addPoint(3, 11);
				Signal1D1DXY error=new Signal1D1DXY();
				error.addPoint(0, 1);
				error.addPoint(1, 2);
				error.addPoint(3, 1);
				Plot p=new Plot(sig);
				p.setError(error);
				chartApp.getChartPanel().add(p);
			}




		}

	




	}


	public ChartPanel getChartPanel()
	{
		return chartPanel;
	}


	public PanelImage  getPanelImage()
	{
		return pi;
	}




	public Graph3DPanel getGraph3DPanel()
	{
		return graph3DPanel;
	}


	@Override
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		el.setAttribute("comments",jta.getText() );

		return el;
	}


	@Override
	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) jta.setText("");
		else 
		{
			String txt=xml.getStringAttribute("comments","");
			jta.setText(txt);
		}
	}


}
