package com.photonlyx.toolbox.imageGui.processor;

import com.photonlyx.toolbox.chart.ChartWindow;
import com.photonlyx.toolbox.imageGui.PanelImage;
import com.photonlyx.toolbox.math.signal.Signal2D1D;

public class FourierProcessor implements PanelImageProcessor
{
private PanelImage pim;
private ChartWindow fourierWindow=null;

public FourierProcessor()
{

}

public FourierProcessor(PanelImage pim)
{
this.pim=pim;
}

public void process()
{
Signal2D1D im=new Signal2D1D();
im.fillWithImage(pim.getPostProcessedImage().getImage(),"R", 1);
im.copy(im.fft2d());
if (fourierWindow==null) fourierWindow=new ChartWindow( im);
else 
	{
	fourierWindow.getCJFrame().setVisible(true);
	fourierWindow.getChart().setSignal2D1D(im);
	}
fourierWindow.getChart().setLogz(true);
fourierWindow.getChart().setGridLinex(false);
fourierWindow.getChart().setGridLiney(false);
fourierWindow.getChart().update();
}

@Override
public void setPanelImage(PanelImage pim)
{
this.pim=pim;

}



}
