package com.photonlyx.toolbox.imageGui;

import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.CImageProcessing;

public class PostProcessing
{
//private CImage cimage;
public int offset=0,amp=100; //to modify the contrast 
private double  turn=0;//rotation angle (to the right) in degrees
private boolean crop=false;//crop in the zone 1
private boolean mergeZones=false;//show an image composed by the 2 zones or ROI
private int x1,y1,w1,h1;//zone 1
private int x2,y2,w2,h2;//zone 2


public PostProcessing()
{

}

public CImageProcessing process(CImage cimage)
{
CImageProcessing imp2=new CImageProcessing(cimage);

imp2._mul(amp/100.0);
imp2._add(offset);

if (turn==90)
	{
	imp2._turnRight();
	}
else if (turn==-90)
	{
	imp2._turnLeft();
	}

if (crop)//zoom on the zone selection
	{
//	BufferedImage bim=imp2.crop(this.getXs(), this.getYs(), this.getWs(), this.getHs());
//	imp2.copy(bim);
	imp2._crop(x1,y1,w1,h1);
	}
if (!crop) if (this.mergeZones)
	{
	CImageProcessing zone1=imp2.crop(x1,y1, w1, h1);
	CImageProcessing zone2=imp2.crop(x2,y2, w2, h2);
	int w=Math.max(w1, w2);
	int h=h1+h2;
	CImageProcessing merge=new CImageProcessing(w,h);
	merge._paste(zone1,0,0);
	merge._paste(zone2,0,h1);
	imp2=merge;
	}

return imp2;
}

public int getOffset()
{
return offset;
}

public void setOffset(int offset)
{
this.offset = offset;
}

public int getAmp()
{
return amp;
}

public void setAmp(int amp)
{
this.amp = amp;
}

public double getTurn()
{
return turn;
}

public void setTurn(double turn)
{
this.turn = turn;
}

public boolean isCrop()
{
return crop;
}

public void setCrop(boolean crop)
{
this.crop = crop;
}

public boolean isMergeZones()
{
return mergeZones;
}

public void setMergeZones(boolean mergeZones)
{
this.mergeZones = mergeZones;
}

public int getX1()
{
return x1;
}

public void setX1(int x1)
{
this.x1 = x1;
}

public int getY1()
{
return y1;
}

public void setY1(int y1)
{
this.y1 = y1;
}

public int getW1()
{
return w1;
}

public void setW1(int w1)
{
this.w1 = w1;
}

public int getH1()
{
return h1;
}

public void setH1(int h1)
{
this.h1 = h1;
}

public int getX2()
{
return x2;
}

public void setX2(int x2)
{
this.x2 = x2;
}

public int getY2()
{
return y2;
}

public void setY2(int y2)
{
this.y2 = y2;
}

public int getW2()
{
return w2;
}

public void setW2(int w2)
{
this.w2 = w2;
}

public int getH2()
{
return h2;
}

public void setH2(int h2)
{
this.h2 = h2;
}

//public CImage getCimage()
//{
//return cimage;
//}
//
//public void setCimage(CImage cimage)
//{
//this.cimage = cimage;
//}
//
//




}
