
package com.photonlyx.toolbox.graph;

import java.util.*;

import nanoxml.XMLElement;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.mysql.MysqlDb;
import com.photonlyx.toolbox.mysql.MysqlFieldTypes;
import com.photonlyx.toolbox.mysql.MysqlTable;



/**
manage all the tasks . son of ActivityPanel
 */
public class Network  extends MysqlTable implements XMLstorable
{
	private boolean hasChanged=false;
	protected  String categories="cat1 cat2";

	protected double xminView=-30;
	protected double xmaxView=30;
	protected double yminView=-30;
	protected double ymaxView=30;
	protected String name; 

	public Network(MysqlDb mysqlDb,String name)
	{
		super(mysqlDb,name);
		Vector<MysqlFieldTypes> fieldsList=getFieldsList();
		fieldsList.add(new MysqlFieldTypes("dBnumber","MEDIUMINT(255)"));
		fieldsList.add(new MysqlFieldTypes("name","CHAR(255)"));
		fieldsList.add(new MysqlFieldTypes("xreal","FLOAT(24)"));
		fieldsList.add(new MysqlFieldTypes("yreal","FLOAT(24)"));
		fieldsList.add(new MysqlFieldTypes("radius","FLOAT(24)"));
		fieldsList.add(new MysqlFieldTypes("color","CHAR(32)"));
		fieldsList.add(new MysqlFieldTypes("label","VARCHAR(2048)"));
		fieldsList.add(new MysqlFieldTypes("status","MEDIUMINT(255)"));
		fieldsList.add(new MysqlFieldTypes("comment","CHAR(255)"));
		//fieldsList.add(new MysqlFieldTypes("categoryID",""));
		fieldsList.add(new MysqlFieldTypes("sonsNames","VARCHAR(1024)"));
	}


	public Network()
	{
		super(null,"");
		this.name = "activity";
	}



	public Network(String name)
	{
		super(null,name);
		this.name = name;
	}

	public Node getRowInstance()
	{
		return new Node(this);
	}


	public boolean isHasChanged()
	{
		return hasChanged;
	}


	//
	//
	//public boolean add(Node t)
	//{
	//this.addElement(t);
	//t.setNetwork(this);
	//return true;
	//}
	//
	//public boolean remove(Node t)
	//{
	//this.removeElement(t);
	//return true;
	//}



	public void addMainTask()
	{
		Node t=new Node(this,"main");
		t.setYreal(new Date().getTime());
		System.out.println(new Date().getTime());
		t.setXreal(0);
		t.setLabel("Main");
		this.addRow(t);
		hasChanged=true;
	}


	public void removeTask(Node t)
	{
		if (t.getFather()!=null) t.getFather().removeSon(t);
		Vector<Node> sons=t.getSonsVector();		
		for (Node son:sons) son.removeFather();
		this.removeRow(t);
		hasChanged=true;
	}





	/**
set the "mofified or not" status: 
@param b if true "the value has changed", if false "the value has not changed"
	 */
	public void setChanged(boolean b)
	{
		// System.out.println(getClass()+" "+name()+"  set hasChanged to:"+b);
		hasChanged=b;
	}

	/**
tell if the value of the param has changed
	 */
	public boolean hasChanged()
	{
		return hasChanged;
	}

	/**
	 * return a vector with the tasks of this day
	 * @param year
	 * @param month
	 * @param dayOfMonth
	 * @return
	 */
	public Vector<Node> lookForTaskOfThisDay(int year, int month,int dayOfMonth)
	{
		Vector<Node> v=new Vector<Node>();
		for (int i=0;i<size();i++) 
		{
			Node t=(Node)getRow(i);
			if ((t.year()==year)&(t.month()==month)&(t.day()==dayOfMonth)) v.add(t);
		}
		return v;
	}

	public Node getNodeOfName(String word)
	{
		for (int i=0;i<size();i++) 
		{
			Node t=(Node)getRow(i);
			if (t.getName().compareTo(word)==0) return t;
		}
		return null;
	}


	public double getXminView()
	{
		return xminView;
	}
	public void setXminView(double xminView)
	{
		this.xminView = xminView;
	}
	public double getXmaxView()
	{
		return xmaxView;
	}
	public void setXmaxView(double xmaxView)
	{
		this.xmaxView = xmaxView;
	}
	public double getyMinView()
	{
		return yminView;
	}
	public void setyMinView(double d)
	{
		this.yminView = d;
	}
	public double getyMaxView()
	{
		return ymaxView;
	}
	public void setyMaxView(double dateMaxView)
	{
		this.ymaxView = dateMaxView;
	}


	public void toXML(XMLElement el )
	{

	}

	@Override
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().getSimpleName().toString().replace("class ", ""));
//		el.setAttribute("name",name);
//		el.setAttribute("categories",categories);
//		el.setDoubleAttribute("xminView",xminView);
//		el.setDoubleAttribute("xmaxView",xmaxView);
//		el.setDoubleAttribute("yminView",yminView);
//		el.setDoubleAttribute("ymaxView",ymaxView);
		for (int i=0;i<size();i++) 
		{
			Node t=(Node)getRow(i);
			el.addChild(t.toXML());
		}
		return el;
	}

	public void updateAttributesFromXML(XMLElement xml)
	{
//		name=xml.getStringAttribute("name","activity");
//		categories=xml.getStringAttribute("categories");
//		xminView=xml.getDoubleAttribute("xminView",-30);
//		xmaxView=xml.getDoubleAttribute("xmaxView",30);
//		yminView=xml.getDoubleAttribute("yminView",-30.0);
//		ymaxView=xml.getDoubleAttribute("ymaxView",30.0);
	}

	@Override
	public void updateFromXML(XMLElement xml)
	{
		this.removeAllElements();
		if (xml==null) return;
		updateAttributesFromXML(xml);
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement son=enumSons.nextElement();
			Node t=new Node(this);
			this.addRow(t,false);
			t.updateFromXML(son);
		}
		//update the sons
		//updateSons();
	}



//	/**
//	 * update the sons from the list of names
//	 */
//	protected void updateSons()
//	{
//		for (int i=0;i<size();i++) 
//		{
//			Node n=(Node)getRow(i);
//			n.updateSons();
//		}
//	}
//


	public double getXminOfAllTasks()
	{
		double r=1e30;
		for (int i=0;i<size();i++) 
		{
			Node t=(Node)getRow(i);
			if (t.getXreal()<r) r=t.getXreal();
		}
		return r;
	}

	public double getXmaxOfAllTasks()
	{
		double r=-1e30;
		for (int i=0;i<size();i++) 
		{
			Node t=(Node)getRow(i);
			if (t.getXreal()>r) r=t.getXreal();
		}
		return r;
	}

	public double getDateMinOfAllTasks()
	{
		double r=1e30;
		for (int i=0;i<size();i++) 
		{
			Node t=(Node)getRow(i);
			if (t.getYreal()<r) r=t.getYreal();
		}
		return r;
	}

	public double getDateMaxOfAllTasks()
	{
		double r=-1e30;
		for (int i=0;i<size();i++) 
		{
			Node t=(Node)getRow(i);
			if (t.getYreal()>r) r=t.getYreal();
		}
		return r;
	}

	public void fitTasks2view()
	{
		double x1=getXminOfAllTasks();
		double x2=getXmaxOfAllTasks();
		double date1=getDateMinOfAllTasks();
		double date2=getDateMaxOfAllTasks();
		xminView=x1-(x2-x1)/2;
		xmaxView=x2+(x2-x1)/2;
		yminView=date1-(date2-date1)/2;
		ymaxView=date2+(date2-date1)/2;
	}





	public String getCategories()
	{
		return categories;
	}
	public void setCategories(String categories)
	{
		this.categories = categories;
	}


	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	//
	//
	//public Vector<Task> getTodoNotes()
	//{
	//Vector<Task> v=new Vector<Task>();
	//for (Task t:this)
	//	{
	//	//System.out.println(t.getLabel()+" status:"+t.getStatus());
	//	if (t.getStatus()!=Task.STATUS_DONE) v.add(t);
	//	}
	//return v;
	//}

	public Vector<Node> getTodosSorted()
	{
		Vector<Node> todos=new Vector<Node>();
		for (int i=0;i<size();i++) 
		{
			Node t=(Node)getRow(i);
			//System.out.println(t.getLabel()+" status:"+t.getStatus());
			if (t.getStatus()!=Node.STATUS_DONE) todos.add(t);
		}
		//todos._sortByDateEnd();
		Collections.sort(todos, DATE_END_ORDER);
		return todos;
	}

	static final Comparator<Node> DATE_ORDER = new Comparator<Node>() 
	{
		public int compare(Node e1, Node e2) 
		{
			double diff=(e1.getDate().getTime())-(e2.getDate().getTime());
			int i=0;
			if (diff<0) i= -1;
			if (diff==0) i= 0;
			if (diff>0) i= 1;
			return i;
		}
	};



	static final Comparator<Node> DATE_END_ORDER = new Comparator<Node>() 
	{
		public int compare(Node e1, Node e2) 
		{
			double diff=(e1.getDateEnd().getTime())-(e2.getDateEnd().getTime());
			int i=0;
			if (diff<0) i= -1;
			if (diff==0) i= 0;
			if (diff>0) i= 1;
			return i;
		}
	};

//
//	public void _sortByDate()
//	{
//		Collections.sort(this, DATE_ORDER);
//	}
//
//	public void _sortByDateEnd()
//	{
//		Collections.sort(this, DATE_END_ORDER);
//	}
//


	public void applyForces()
	{
		double dt=0.01;
		double[] dp;
		for (int i=0;i<size();i++) 
		{
			Node t=(Node)getRow(i);
			dp=t.calcDisplacement(dt);
			t.translateReal(dp);
		}

	}



	public void multilplyNodesSize(double d)
	{
		for (int i=0;i<size();i++) 
		{
			Node t=(Node)getRow(i);
			t.setRadius(t.getRadius() * d);
		}

	}

}