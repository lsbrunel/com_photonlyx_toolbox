package com.photonlyx.toolbox.graph.gui;

import java.awt.Dimension;

import javax.swing.JPanel;

import com.photonlyx.toolbox.graph.Network;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ConfigWindow;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;





public class GraphTestApp extends   WindowApp
{
private Network network=new Network();
//private DaysColors dc=new DaysColors();
private NetworkPanel networkPanel=new NetworkPanel(network,true);



public GraphTestApp(String appName)
{
super(appName,false,true,true);
super.getCJFrame().setSize(new Dimension(800,600));
super.getCJFrame().center();

this.putMenuFile(new XMLFileStorage(this), new CFileFilter("xml","network"));
this.getxMLFileStorage().addThingToStore(network);
//this.getxMLFileStorage().addThingToStore(dc);

//this.getFileMenuTriggerList().add(new Trigger(activityPanel,"repaint"));

this.add(networkPanel.getComponent(), "Network");




this.getCJFrame().validate();
}

/**
 * @param args
 */
public static void main(String[] args)
{
WindowApp wa=new GraphTestApp("com_cionin_graph_testapp");
}






}
