package com.photonlyx.toolbox.threeD.gui;




import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Segment3D;




public class Segment3DSet extends Object3DColorSet 
{
	

private Vector<Object3DColor> list=new Vector<Object3DColor>();



public Vector<Object3DColor> getObject3DList()
	{
		return list;
	}
	
	
public void add(Segment3DColor t)
	{
	list.add(t);
	}


public void clean()
{
list.removeAllElements();

}



}
