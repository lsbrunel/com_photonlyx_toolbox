package com.photonlyx.toolbox.threeD.gui;

import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelListener;
import java.util.Vector;

public interface Graph3DDrawingLayer extends MouseListener,MouseMotionListener,
												MouseWheelListener,KeyListener
{

public void draw(Graphics g);
public Vector<String>  getCommandsList();
//public void setGraph3DPanel( Graph3DPanel p3D);

}
