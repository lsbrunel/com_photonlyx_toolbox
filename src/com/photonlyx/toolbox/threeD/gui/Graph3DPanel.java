package com.photonlyx.toolbox.threeD.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Vector;
import javax.swing.*;

import com.photonlyx.toolbox.chart.ChartWindow;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.gui.InputDialog;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.math.function.Function1D1D;
import com.photonlyx.toolbox.math.function.usual.Line;
import com.photonlyx.toolbox.math.function.usual2D1D.Gaussian;
import com.photonlyx.toolbox.math.function.usual3D1D.SphericalGauss;
import com.photonlyx.toolbox.math.geometry.*;
import com.photonlyx.toolbox.math.geometry.Box;
import com.photonlyx.toolbox.math.geometry.Frame;
import com.photonlyx.toolbox.math.geometry.cloud.Cloud;
import com.photonlyx.toolbox.math.geometry.cloud.CloudPoint;
import com.photonlyx.toolbox.math.image.CImage;
import com.photonlyx.toolbox.math.image.DrawingLayer;
import com.photonlyx.toolbox.math.image.ImageSource;
import com.photonlyx.toolbox.math.image.PNG_JPG_FileFilter;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.math.signal.Signal3D1D;
import com.photonlyx.toolbox.threeD.gui.mesh.MeshRendering;
import com.photonlyx.toolbox.threeD.gui.mesh.Signal3D1Drendering;
import com.photonlyx.toolbox.threeD.io.STLFileFilter;
import com.photonlyx.toolbox.threeD.rendering.Sun;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;


public class Graph3DPanel  implements MouseListener,KeyListener,
MouseMotionListener,MouseWheelListener,
//Iterable<Object3DColor>,
ActionListener,DrawingLayer
{
	//links
	//private Object3DSet object3DSet;
	protected ProjectorCamera proj=new ProjectorCamera();
	//params
	private double stepDistance=0.01;
	private double stepAngle=0.01;
	public double axesLength=2;
	//private ParString projectionType;

	//internal

	//private double inc=0.05; //increment angles;
	//private boolean flagselect=false;
	private boolean flagmenu=false;
	//private Vector<Segment3D> vGlob=new Vector<Segment3D>();//list of the segments in global 3D coordinates to be printed
	//private Vector<Segment> vSegmentScreen=new Vector<Segment>();//list of the segments in screen 2D coordinates to be printed
	protected int[] pix1=new int[2];
	private int[] pixRectangleSelection2=new int[2];
	private int[] pixRectangleSelection1=new int[2];
	private int button=0;
	private int[] indices;//sorted indices of the object to draw from farther to closer
	private Vecteur cornermin=new Vecteur();
	private Vecteur cornermax=new Vecteur();
	private Quadrilatere3DColor planeXY,planeXZ,planeYZ;//planes of the limits box
	private Segment3DColor Ox,Oy,Oz;//planes of the limits box

	//private Vector<Object3DColor> listOfObject3DColor=new Vector<Object3DColor>();
	private Vector<Object3D> listOfObject3D=new Vector<Object3D>();
	private Vector<Object3DSet> listOfSets=new Vector<Object3DSet>();
	private Vector<Object3DColorSet> listOfColorSets=new Vector<Object3DColorSet>();
	private Vector<MeshRendering> listOfObject3DRendering=new Vector<MeshRendering>();

	private Vector<Object3DColor> axes=new Vector<Object3DColor>();
	private Vecteur target=new Vecteur(0,0,0);//point to turn around
	private boolean followTarget=true;
	private double distanceToTarget;
	private Vector<Graph3DDrawingLayer> graph3DDrawingLayers=new Vector<Graph3DDrawingLayer>();
	private Sun sun=new Sun();
	private boolean render=true;
	private boolean wireFrame=true;
	private boolean popUpMenu = true;
	private JPopupMenu popMenu,popMenuAddMesh,popMenuImport,popMenuExport;
	private Point mousePos;//mouse position
	private Point mouseStart;//mouse position when striking g r or s
	private String message="";

	private boolean selectionMode=false;
	private boolean translationMode=false;
	private boolean rotationMode=false;
	private boolean scaleMode=false;
	private boolean xMode=false;
	private boolean yMode=false;
	private boolean zMode=false;
	private Frame frameSaved=new Frame();

	private Vector<Object3D> selectedObjects=new Vector<Object3D>();

	private JPanel mainPanel;
	private JPanel jpDrawing;
	private JPanel panelSide;

	private DecimalFormat df3=new DecimalFormat("0.000");
	private String path=".";
	private WindowApp app;



	public  Graph3DPanel(WindowApp app)
	{
		this.path=app.path;
		init();
	}
	public  Graph3DPanel()
	{
		this.path=Global.path;		
		init();
	}
	

	private void init()
	{
		jpDrawing=new JPanel()
		{
			public void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				setAxes();
				g.setColor(Color.lightGray);
				g.drawLine(0,0,this.getWidth()-2, 0);
				g.drawLine(this.getWidth()-2, 0, this.getWidth()-2, this.getHeight()-2);
				g.drawLine(this.getWidth()-2, this.getHeight()-2,0, this.getHeight()-2);
				g.drawLine(0,this.getHeight()-2,0,0);
				draw(g,this,null);
			}
		};
		//jpDrawing.setBackground(Global.background);
		
		panelSide=new JPanel();
		panelSide.setBackground(Global.background);
		panelSide.setPreferredSize(new Dimension(100,0));

		mainPanel=new JPanel();
		mainPanel.setBackground(Global.background);
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(panelSide,BorderLayout.WEST);
		mainPanel.add(jpDrawing,BorderLayout.CENTER);

		jpDrawing.addMouseListener(this);
		jpDrawing.addMouseMotionListener(this);
		jpDrawing.addMouseWheelListener(this);
		jpDrawing.addKeyListener(this);
		//jp.setBackground(Color.LIGHT_GRAY);
		jpDrawing.setBackground(Global.background);

		//right click menu:
		popMenu=new JPopupMenu(Messager.getString("Action"));
		Vector<JMenuItem> popUpMenuItems1=new Vector<JMenuItem>();
		JMenuItem jmi;
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("add_mesh")));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("import")));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("export")));
		popUpMenuItems1.add(jmi=new JMenuItem(Messager.getString("erase_selected")));
		for (JMenuItem mi:popUpMenuItems1)
		{
			popMenu.add(mi);
			mi.addActionListener(this);	
			mi.setBackground(Global.background);
			mi.setFont(Global.font);
		}

		//add mesh sub-menu:
		popMenuAddMesh=new JPopupMenu(Messager.getString("add_mesh"));
		Vector<JMenuItem> popUpMenuItems2=new Vector<JMenuItem>();
		popUpMenuItems2.add(jmi=new JMenuItem(Messager.getString("add_plane")));
		popUpMenuItems2.add(jmi=new JMenuItem(Messager.getString("add_box")));
		popUpMenuItems2.add(jmi=new JMenuItem(Messager.getString("add_cylinder")));
		popUpMenuItems2.add(jmi=new JMenuItem(Messager.getString("add_cone")));
		popUpMenuItems2.add(jmi=new JMenuItem(Messager.getString("add_sphereICO")));
		popUpMenuItems2.add(jmi=new JMenuItem(Messager.getString("add_pyramid")));
		popUpMenuItems2.add(jmi=new JMenuItem(Messager.getString("add_spherical_cap")));
		popUpMenuItems2.add(jmi=new JMenuItem(Messager.getString("add_lens")));
		popUpMenuItems2.add(jmi=new JMenuItem(Messager.getString("add_sierpinski_pyramid")));
		for (JMenuItem mi:popUpMenuItems2)
		{
			popMenuAddMesh.add(mi);
			mi.addActionListener(this);	
			mi.setBackground(Global.background);
			mi.setFont(Global.font);
		}

		//import sub-menu:
		popMenuImport=new JPopupMenu(Messager.getString("import"));
		Vector<JMenuItem> popUpMenuItems3=new Vector<JMenuItem>();
		popUpMenuItems3.add(jmi=new JMenuItem(Messager.getString("import_stl")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/threeD/gui/importSTL.png"));
		popUpMenuItems3.add(jmi=new JMenuItem(Messager.getString("import_stl_binary")));
		jmi.setIcon(com.photonlyx.toolbox.util.Util.getIcon("com/cionin/threeD/gui/importSTL.png"));
		popUpMenuItems3.add(jmi=new JMenuItem(Messager.getString("import_points_cloud")));
		popUpMenuItems3.add(jmi=new JMenuItem(Messager.getString("import_obj")));
		popUpMenuItems3.add(jmi=new JMenuItem(Messager.getString("import_Signal3D1D")));
		popUpMenuItems3.add(jmi=new JMenuItem(Messager.getString("import_jpg_image")));
		for (JMenuItem mi:popUpMenuItems3)
		{
			popMenuImport.add(mi);
			mi.addActionListener(this);	
			mi.setBackground(Global.background);
			mi.setFont(Global.font);
		}
		

		//export sub-menu:
		popMenuExport=new JPopupMenu(Messager.getString("export"));
		Vector<JMenuItem> popUpMenuItems4=new Vector<JMenuItem>();
		popUpMenuItems4.add(jmi=new JMenuItem(Messager.getString("export_stl")));
		popUpMenuItems4.add(jmi=new JMenuItem(Messager.getString("export_obj")));
		popUpMenuItems4.add(jmi=new JMenuItem(Messager.getString("export_pbrt")));
		for (JMenuItem mi:popUpMenuItems4)
		{
			popMenuExport.add(mi);
			mi.addActionListener(this);	
			mi.setBackground(Global.background);
			mi.setFont(Global.font);
		}


		setAxes();
		sun.setAngle(35);
		sun.setAngleHor(0);
		initialiseProjector();
		repaint();
		this.initialiseInit();
		repaint();
	}



	public void setStepDistance(double stepDistance) 
	{
		this.stepDistance = stepDistance;
	}



	public void setStepAngle(double stepAngle) {
		this.stepAngle = stepAngle;
	}



	public double getAxesLength() {
		return axesLength;
	}



	public void setAxesLength(double axesLength) {
		this.axesLength = axesLength;
		setAxes();

	}

	//public void setBackground(String s)
	//{
	//jpDrawing.setBackground(ColorUtil.getColor(s));
	//jpDrawing.repaint();
	//}
	//
	//public String getBackground()
	//{
	//return ColorUtil.color2String(jpDrawing.getBackground());
	//}
	//



	public void addSet(Object3DSet object3DSet)
	{
		listOfSets.add(object3DSet);

		//updateOccupyingCube();

		//sort with respect to the  distances to camera -> draw further(hidden) objects before...
		//int n=list.size();
		//double[] distances=new double[n];
		//for (int i=0;i<n;i++) distances[i]=list.elementAt(i).getDistanceToScreen(p);
		//indices=fa.reuse.math.Sorter.sort(distances);
	}

	public void addColorSet(Object3DColorSet object3DColorSet)
	{
		listOfColorSets.add(object3DColorSet);
	}


	//	public void addObject3D(Object3DColor object3D)
	//	{
	//		listOfObject3DColor.add(object3D);
	//		//updateOccupyingCube();
	//	}

	public void addObject3D(Object3D object3D)
	{
		listOfObject3D.add(object3D);
		//updateOccupyingCube();
	}

	private  void add(TriangleMesh tris)
	{
		System.out.println("add TriangleMesh");
		Object3DColorSet set=new Object3DColorSet();
		this.addColorSet(set);
		for (Triangle3D t:tris.getTriangles()) 
		{
			Triangle3DColor tc=new Triangle3DColor(t,Color.WHITE,true);
			set.add(tc);
		}
	}

	public void add(Object3D object3D)
	{
		if (object3D instanceof TriangleMesh) add((TriangleMesh) object3D);
		else listOfObject3D.add(object3D);
		//updateOccupyingCube();
	}

	public void remove(Object3D object3D)
	{
		listOfObject3D.remove(object3D);
		//updateOccupyingCube();
	}

	//	public void add(Object3DColor object3D)
	//	{
	//		listOfObject3DColor.add(object3D);
	//		//updateOccupyingCube();
	//	}


	public void add(MeshRendering or)
	{
		listOfObject3DRendering.add(or);
	}


	public void removeAll3DObjects()
	{
		//empty list of object3D
		listOfObject3D.removeAllElements();
		//listOfObject3DColor.removeAllElements();
		listOfSets.removeAllElements();
		listOfColorSets.removeAllElements();
		listOfObject3DRendering.removeAllElements();
	}

	public void updateOccupyingCube()
	{
		cornermin=new Vecteur(1000000000,1000000000,1000000000);
		cornermax=new Vecteur(-1000000000,-1000000000,-1000000000);
		//for (Object3DColor o:listOfObject3DColor) o.checkOccupyingCube(cornermin,cornermax);
		for (Object3DSet set:listOfSets) 
			for (Object3D o:set) 
				o.checkOccupyingCube(cornermin,cornermax);
		for (Object3DColorSet set:listOfColorSets) 
			for (Object3D o:set) 
				o.checkOccupyingCube(cornermin,cornermax);
		for (Object3D o:listOfObject3D)o.checkOccupyingCube(cornermin,cornermax);
		//		System.out.println("corner min="+cornermin);
		//		System.out.println("corner max="+cornermax);
	}	



	private void setAxes()
	{
		//add the axes
		double dx=axesLength;
		double dy=axesLength;
		double dz=axesLength;
		Vecteur O=new Vecteur(0,0,0);
		Vecteur _Ox=new Vecteur(dx,0,0);
		Vecteur _Oy=new Vecteur(0,dy,0);
		Vecteur _Oz=new Vecteur(0,0,dz);
		Ox=new Segment3DColor(O,_Ox,Color.black.getRGB());
		Oy=new Segment3DColor(O,_Oy,Color.black.getRGB());
		Oz=new Segment3DColor(O,_Oz,Color.black.getRGB());
		axes.clear();
		axes.add(Ox);
		axes.add(Oy);
		axes.add(Oz);
		double letterSize=axesLength/20;
		//X symbol:
		axes.add(new Segment3DColor(_Ox.addn(new Vecteur(0,0,letterSize)),_Ox.addn(new Vecteur(letterSize,0,-letterSize))));
		axes.add(new Segment3DColor(_Ox.addn(new Vecteur(0,0,-letterSize)),_Ox.addn(new Vecteur(letterSize,0,letterSize))));
		//Y symbol:
		axes.add(new Segment3DColor(_Oy.addn(new Vecteur(0,letterSize,letterSize)),_Oy.addn(new Vecteur(0,0,-letterSize))));
		axes.add(new Segment3DColor(_Oy.addn(new Vecteur(0,0,letterSize)),_Oy.addn(new Vecteur(0,letterSize/2,0))));
		//Z symbol:
		axes.add(new Segment3DColor(_Oz.addn(new Vecteur(-letterSize,0,0)),_Oz.addn(new Vecteur(letterSize,0,0))));
		axes.add(new Segment3DColor(_Oz.addn(new Vecteur(-letterSize,0,0)),_Oz.addn(new Vecteur(letterSize,letterSize,0))));
		axes.add(new Segment3DColor(_Oz.addn(new Vecteur(-letterSize,letterSize,0)),_Oz.addn(new Vecteur(letterSize,letterSize,0))));
		updateOccupyingCube();
		//double encombrement=diagEncombrement.norme(); 
		//initialiseProjector();
		//jp.repaint();
	}

	public void initialiseProjectorAuto()
	{
		this.initialiseProjectorAuto(new Vecteur(3*Math.PI/4,0,-0.3));	
	}


	/**
	 * set the projector s to fit the objects in the view
	 */
	public void initialiseProjectorAuto(Vecteur angles)
	{
		//work();
		//set the projector
		//p.initialise(coinmin,coinmax,getComponent().getWidth(),getComponent().getHeight());
		updateOccupyingCube();
		target=new Vecteur((cornermin.x()+cornermax.x())/2,(cornermin.y()+cornermax.y())/2,(cornermin.z()+cornermax.z())/2);
		//System.out.println(getClass()+" target:"+target);
		double diag=cornermax.sub(cornermin).norme();
		//System.out.println(getClass()+" diag:"+diag);
		distanceToTarget=10*diag;
		double focal=distanceToTarget/diag*2;
		//System.out.println(getClass()+" focal:"+focal	);
		int screenSize=Math.min(jpDrawing.getWidth(),jpDrawing.getHeight());
		proj.initialise(angles,target,distanceToTarget,focal,screenSize,screenSize);
	}


	/**
	 * init the projector
	 */
	public void initialiseProjector()
	{
		target=new Vecteur(0,0,0);
		//System.out.println(getClass()+" target:"+target);
		double diag=5;
		//System.out.println(getClass()+" diag:"+diag);
		distanceToTarget=10*diag;
		double focal=distanceToTarget/diag;
		//System.out.println(getClass()+" focal:"+focal	);
		int screenSize=Math.min(getComponent().getWidth(),getComponent().getHeight());
		proj.initialise(target,distanceToTarget,focal,screenSize,screenSize);

	}

	public void initialiseInit()
	{
		target=new Vecteur(0,0,0);
		//System.out.println(getClass()+" target:"+target);
		double diag=5;
		//System.out.println(getClass()+" diag:"+diag);
		distanceToTarget=10*diag;
		double focal=distanceToTarget/diag;
		//System.out.println(getClass()+" focal:"+focal	);
		int screenSize=500;
		proj.initialise(target,distanceToTarget,focal,screenSize,screenSize);

	}

	public void keyPressed(KeyEvent e)
	{
		int key=e.getKeyCode();
		char keychar=e.getKeyChar();
		String s=KeyEvent.getKeyText(key);


		switch(key)
		{
		case KeyEvent.VK_DELETE:
		case KeyEvent.VK_BACK_SPACE:
			this.eraseSelected();
			break;
		case KeyEvent.VK_ESCAPE:
			if ((translationMode)||(rotationMode)||(scaleMode))
			{
				message="Modification aborted";
				//back to previous centre position:
				if (this.getSelectedObjects().size()==1)
				{
					Object3D ob=this.getSelectedObjects().elementAt(0);
					if (ob instanceof Object3DFrame)
					{
						Object3DFrame of=(Object3DFrame) ob;
						of.getFrame().copy(frameSaved);
						of.updateGlobal();
					}	
					translationMode=false;
					rotationMode=false;
					scaleMode=false;
					xMode=false;
					yMode=false;
					zMode=false;
				}
			}
			break;
		}

		switch (keychar)
		{
		case 't'   : proj.turn(0,0,stepAngle);break;  
		case 'T'   : proj.turn(0,0,-stepAngle);break;  
		//case 's'   : flagselect=!flagselect;break;  
		case 'm'   : flagmenu=!flagmenu;break;  
		case 'i'   : initialiseProjector();break;
		case 'I'   : initialiseProjectorAuto();break;
		//		case '1'   : initialiseProjectorAuto(new Vecteur(0,Math.PI/2,0));break;
		//		case '3'   : initialiseProjectorAuto(new Vecteur(Math.PI/2,0,0));break;
		//		case '5'   : initialiseProjectorAuto();break;
		case '7'   : initialiseProjectorAuto(new Vecteur(Math.PI,0,0));break;
		case 'f'  : followTarget=!followTarget;break;  
		case 'o'  ://more options
			CJFrame cjf=new CJFrame();
			cjf.setExitAppOnExit(false);
			cjf.setAutoCenter(true);
			cjf.setTitle("3D graph options");
			cjf.getContentPane().setLayout(new FlowLayout());
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"repaint"));
			String[] names1={"axesLength","render"};
			ParamsBox paramsBox1=new ParamsBox(this,tl,names1);
			cjf.getContentPane().add(paramsBox1.getComponent());
			String[] names2={"Angle","AngleHor"};
			ParamsBox paramsBox2=new ParamsBox(sun,tl,"Light source",names2);
			cjf.getContentPane().add(paramsBox2.getComponent());
			cjf.pack();
			cjf.setVisible(true);
			break;
		case 'l'  ://turn sun light
			double angleHor=sun.getAngleHor()+10;
			if (angleHor==360)angleHor=0;
			sun.setAngleHor(angleHor);
		case 'R'  ://rendered
			render=!render;
			break;
		case 'w'  :
			wireFrame=!wireFrame;
		case 'x'  :
			//if ((! translationMode)&&(! rotationMode)&&(! scaleMode)) this.eraseSelected();
			if ((translationMode)||(rotationMode)||(scaleMode)) xMode=true;
			break;
		case 'y'  :
			if ((translationMode)||(rotationMode)||(scaleMode)) yMode=true;
			break;
		case 'z'  :
			if ((translationMode)||(rotationMode)||(scaleMode)) zMode=true;
			break;
		case 'Z'  : proj.zoom();break;  
		case 'g'  :
			translationMode=true;
			mouseStart=mousePos;
			if (this.getSelectedObjects().size()>=1) 
			{
				Object3D ob=this.getSelectedObjects().elementAt(0);
				if (ob instanceof Object3DFrame)
				{
					frameSaved.copy( ((Object3DFrame)ob).getFrame() );
				}
			}
			break;
		case 's'  :
			scaleMode=true;
			mouseStart=mousePos;
			if (this.getSelectedObjects().size()>=1) 
			{
				Object3D ob=this.getSelectedObjects().elementAt(0);
				if (ob instanceof Object3DFrame)
				{
					frameSaved.copy( ((Object3DFrame)ob).getFrame() );
				}
			}
			break;
		case 'r'  :
			rotationMode=true;
			mouseStart=mousePos;
			if (this.getSelectedObjects().size()>=1) 
			{
				Object3D ob=this.getSelectedObjects().elementAt(0);
				if (ob instanceof Object3DFrame)
				{
					frameSaved.copy( ((Object3DFrame)ob).getFrame() );
				}
			}
			break;
		case 'c'  :
			Color c1=jpDrawing.getBackground().darker();
			jpDrawing.setBackground(c1);
			break;
		case 'C'  :
			jpDrawing.setBackground(jpDrawing.getBackground().brighter());
			break;
		}	
		switch(key)
		{
		case KeyEvent.VK_LEFT :
			proj.turn(0,stepAngle,0);
			if (followTarget) proj.translateToSeeTarget(target,distanceToTarget);
			break;  
		case KeyEvent.VK_RIGHT :
			proj.turn(0,-stepAngle,0);
			if (followTarget) proj.translateToSeeTarget(target,distanceToTarget);
			break;  
		case KeyEvent.VK_UP :
			if (followTarget) proj.translateToSeeTarget(target,distanceToTarget);
			proj.turn(-stepAngle,0,0);
			break;  
		case KeyEvent.VK_DOWN :
			if (followTarget) proj.translateToSeeTarget(target,distanceToTarget);
			proj.turn(stepAngle,0,0);
			break;  
		case KeyEvent.VK_B :
			selectionMode=!selectionMode;
			pixRectangleSelection1[0]=0;
			pixRectangleSelection1[1]=0;
			pixRectangleSelection2[0]=0;
			pixRectangleSelection2[1]=0;
			break;  
		}
		//	if ((s.compareTo("Down")==0)|| (s.compareTo("Bas")==0)) p.tourner(inc,0,0);
		//	if ((s.compareTo("Up")==0)||(s.compareTo("Haut")==0) ) p.tourner(-inc,0,0);
		//	if ((s.compareTo("Left")==0)||(s.compareTo("Gauche")==0)) p.tourner(0,inc,0);
		//	if ((s.compareTo("Right")==0)||(s.compareTo("Droite")==0)) p.tourner(0,-inc,0);
		//pass the event to the drawing layers
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) drawingLayer.keyPressed(e);
		jpDrawing.repaint();
		this.repaint();
	}
	public void keyTyped(KeyEvent e)
	{
	}
	public void keyReleased(KeyEvent e){}


	public void mouseClicked(MouseEvent e)
	{
		mousePos=e.getPoint();

		if ((e.getButton()==MouseEvent.BUTTON3)&& popUpMenu == true ) 
			popMenu.show(this.getJPanel(),e.getPoint().x,e.getPoint().y);

		if (e.getButton()==MouseEvent.BUTTON1) 
		{
			if ((translationMode)||(rotationMode)||(scaleMode))//leave modification mode
			{
				translationMode=false;
				rotationMode=false;
				scaleMode=false;
				xMode=false;
				yMode=false;
				zMode=false;
			}
			else
			{
				fillSelectedObjectsClick(e.getPoint());
				message=this.getSelectedObjects().size()+" objects selected";
			}
		}

		//pass the event to the drawing layers
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) 
		{
			drawingLayer.mouseClicked(e);
		}
		this.repaint();
	}

	public void mouseEntered(MouseEvent e)
	{
		jpDrawing.requestFocus();
		//pass the event to the drawing layers
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) drawingLayer.mouseEntered(e);
	}

	public void mouseExited(MouseEvent e)
	{
		//pass the event to the drawing layers
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) drawingLayer.mouseExited(e);
	}

	public void mousePressed(MouseEvent e)
	{
		if (e.getButton()==MouseEvent.BUTTON1)
		{
			//System.out.println("button 1");
			button=1;
			pix1[0]=e.getX();
			pix1[1]=e.getY();
			if (selectionMode)
			{
				pixRectangleSelection1[0]=e.getX();
				pixRectangleSelection1[1]=e.getY();
			}
		}
		if (e.getButton()==MouseEvent.BUTTON3)
		{
			button=3;
			pix1[0]=e.getX();
			pix1[1]=e.getY();
		}
		//pass the event to the drawing layers
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) drawingLayer.mousePressed(e);
	}


	public void mouseReleased(MouseEvent e)
	{
		if (e.getButton()==MouseEvent.BUTTON1) if (selectionMode) 
		{
			fillSelectedObjectsInBox(e.getPoint());
			selectionMode=false;
			repaint();
		}

		//pass the event to the drawing layers
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) drawingLayer.mouseReleased(e);
	}


	public void mouseDragged(MouseEvent e)
	{
		if ((selectionMode)&&(button==1))
		{
			pixRectangleSelection2[0]=e.getX();
			pixRectangleSelection2[1]=e.getY();
		}
		else if (button==3)
		{
			//System.out.println("button 1 drag");
			int xmouse=e.getX();
			int ymouse=e.getY();
			int dx=xmouse-pix1[0];
			int dy=ymouse-pix1[1];
			Vecteur translation=new Vecteur(stepDistance/2.0*dx,stepDistance/2.0*dy,0);
			proj.translater(translation);
			if (followTarget) target._add(new Vecteur(-stepDistance/2.0*dx,stepDistance/2.0*dy,0));
			pix1[0]=xmouse;
			pix1[1]=ymouse;
		}
		else if (button==1)
		{
			int xmouse=e.getX();
			int ymouse=e.getY();
			int dx=xmouse-pix1[0];
			int dy=ymouse-pix1[1];
			proj.turn(stepAngle*dy,-stepAngle*dx,0);
			if (followTarget) 
			{
				//System.out.println(getClass()+" distanceToTarget="+distanceToTarget);
				proj.translateToSeeTarget(target,distanceToTarget);
			}
			pix1[0]=xmouse;
			pix1[1]=ymouse;
		}
		jpDrawing.repaint();
		//pass the event to the drawing layers
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) drawingLayer.mouseDragged(e);
	}

	public void mouseMoved(MouseEvent e)
	{
		mousePos=e.getPoint();
		//manage mesh modifications:
		double dist=0;
		if (translationMode||rotationMode||scaleMode)
		{
			int dx=mousePos.x-mouseStart.x;
			//int dy=mousePos.y-mouseStart.y;
			//dist=Math.sqrt(dx*dx+dy*dy);
			dist=dx;
		}

		if (this.getSelectedObjects().size()>=1)
		{
			Object3D ob=this.getSelectedObjects().elementAt(0);
			if (ob instanceof Object3DFrame)
			{
				Object3DFrame of=(Object3DFrame) ob;
				if (translationMode)
				{
					double d=dist/30;
					//System.out.println(of.getClass().getName()+" d="+d);
					{
						if (xMode) message="Drag x "+df3.format(d);
						else if (yMode) message="Drag y "+df3.format(d);
						else if (zMode) message="Drag z "+df3.format(d);
						Vecteur o=frameSaved.getCentre();
						if (xMode) of.getFrame().setCentre(o.addn(new Vecteur(d,0,0)));
						if (yMode) of.getFrame().setCentre(o.addn(new Vecteur(0,d,0)));
						if (zMode) of.getFrame().setCentre(o.addn(new Vecteur(0,0,d)));
						of.updateGlobal();
					}
				}
				if (rotationMode)
				{
					double d=dist/300;
					if (xMode) message="Rotate x "+df3.format(d*180.0/Math.PI);
					else if (yMode) message="Rotate y "+df3.format(d*180.0/Math.PI);
					else if (zMode) message="Rotate z "+df3.format(d*180.0/Math.PI);
					Vecteur angles=null;
					if (xMode) angles=new Vecteur(d,0,0);
					if (yMode) angles=new Vecteur(0,d,0);
					if (zMode) angles=new Vecteur(0,0,d);
					if (angles!=null)
					{
						Frame f1=new Frame();
						f1.copy(frameSaved);
						f1._rotate(angles);
						of.getFrame().copy(f1);
						of.updateGlobal();
					}
				}
				if (scaleMode)
				{
					double d=1+dist/100;
					if (xMode) message="Scale x "+df3.format(d);
					else if (yMode) message="Scale y "+df3.format(d);
					else if (zMode) message="Scale z "+df3.format(d);
					else message="Scale"+df3.format(d);
					Vecteur scale=null;
					if (xMode) scale=new Vecteur(d,1,1);
					else if (yMode) scale=new Vecteur(1,d,1);
					else if (zMode) scale=new Vecteur(1,1,d);
					else scale=new Vecteur(d,d,d);
					if (scale!=null)
					{
						Frame f1=new Frame();
						f1.copy(frameSaved);
						f1._scale(scale);
						of.getFrame().copy(f1);
						of.updateGlobal();
					}
				}
			}
		}

		//pass the event to the drawing layers
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) drawingLayer.mouseMoved(e);

		this.repaint();

	}


	public void mouseWheelMoved(MouseWheelEvent e)
	{
		int nbClicks=e.getWheelRotation();
		if (nbClicks>=0) for (int i=0;i<nbClicks;i++) 
		{
			proj.unzoom();
			//	p.getCloser(stepDistance.val);
			//	distanceToTarget-=stepDistance.val;
		}
		if (nbClicks<=0) for (int i=0;i<-nbClicks;i++) 
		{
			proj.zoom();
			//	p.getFarther(stepDistance.val);
			//	distanceToTarget+=stepDistance.val;
		}
		//if (nbClicks>=0) for (int i=0;i<nbClicks;i++) p.zoom();
		//if (nbClicks<=0) for (int i=0;i<-nbClicks;i++) p.unzoom();
		jpDrawing.repaint();
		//pass the event to the drawing layers
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) drawingLayer.mouseClicked(e);
	}

	public JComponent getComponent(){return mainPanel;}
	public JPanel getMainPanel(){return this.mainPanel;}
	public JPanel getJPanel(){return this.jpDrawing;}
	public JPanel getPanelSide(){return panelSide;}
	//public java.awt.Point getInitLocation(){return jpDrawing.getLocation();}
	//public Dimension getInitSize(){return jpDrawing.getSize();}



	/**plot the menu*/
	void drawMenu(Graphics g,boolean flag)
	{
		int y=10;
		String s;
		g.setColor(Color.LIGHT_GRAY);

		//get the commands form other layers:
		Vector<String> v=new Vector<String>();
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) 
		{
			for (String s1:drawingLayer.getCommandsList()) v.add(s1);
		}

		if (flag)
		{
			s="MENU";g.drawString(s,10,y+=15);
			s="arrows to turn (f to follow the object when turning)";g.drawString(s,10,y+=15);
			s="c darker background";g.drawString(s,10,y+=15);
			s="C brighter background";g.drawString(s,10,y+=15);
			s="z Z to unzoom/zoom (change the focal)";g.drawString(s,10,y+=15);
			s="t T to turn around camera axis";g.drawString(s,10,y+=15);
			s="i reset view";g.drawString(s,10,y+=15);
			s="I auto view";g.drawString(s,10,y+=15);
			s="1 side view";g.drawString(s,10,y+=15);
			s="5 perspective view";g.drawString(s,10,y+=15);
			s="3 back view";g.drawString(s,10,y+=15);
			s="7 top view";g.drawString(s,10,y+=15);
			s="f to follow target";g.drawString(s,10,y+=15);
			s="mouse wheel to unzoom/zoom (change the focal)";g.drawString(s,10,y+=15);
			s="right click and drag mouse to translate";g.drawString(s,10,y+=15);
			s="left click and drag mouse to rotate ";g.drawString(s,10,y+=15);
			s="R render/no render ";g.drawString(s,10,y+=15);
			s="w wireframe/filled faces ";g.drawString(s,10,y+=15);
			s="b select (box) ";g.drawString(s,10,y+=15);
			s="g grab (follow by x y or z for direction) ";g.drawString(s,10,y+=15);
			s="r rotate (follow by x y or z for axis) ";g.drawString(s,10,y+=15);
			s="s scale (follow by x y or z for axis) ";g.drawString(s,10,y+=15);
			s="DEL or SUPPR erase selected ";g.drawString(s,10,y+=15);

			for (String s1:v) g.drawString(s1,10,y+=15);

			s="m for menu";g.drawString(s,10,y+=15);
			//if (followTarget) {s="(follow target)";g.drawString(s,10,col+=15);}
		}
		else 
		{
			//s="i to initialize view";g.drawString(s,10,col+=15);
			s="(m for menu)";g.drawString(s,10,y+=15);
			if (selectionMode) {g.setColor(Color.red);s="Selection box";g.drawString(s,10,y+=15);}
			//if (followTarget) {s="(follow target)"; g.drawString(s,10,col+=15);}
		}
	}

	void drawMessage(Graphics g,boolean flag)
	{
		g.setColor(Color.LIGHT_GRAY);
		String s;
		int y=jpDrawing.getHeight()-10;
		g.drawString(message,10,y);

	}


	public void setEnabled(boolean enabled){}

	public void repaint()
	{
		jpDrawing.repaint();
	}
	public void update()
	{
		jpDrawing.repaint();
	}


	/**
	 * add the image of altitudes (z) as triangles
	 * @param im
	 */
	public  void add(Signal2D1D im)
	{
		TriangleMesh set=new TriangleMesh();
		TriangleMesh.convert( im, set,0);
		Object3DColorSet setc=new Object3DColorSet();
		setc.add(set);		
		this.addColorSet(setc);
	}

	/**
	 * @deprecated
	 * @param s3D
	 * @param dotSize
	 * @param lut
	 * @param alpha
	 * @param rgb
	 */
	public void add(Signal3D1D s3D,int dotSize,Function1D1D lut,int alpha,int rgb)
	{
		this.addColorSet(Object3DColorSet.build3Dset(s3D,dotSize,lut,alpha,rgb));
		this.repaint();

	}



	/**
	 * add the image of altitudes (z) as quadrilaters
	 * @param im
	 */
	public void addImageOfAltitudesAsQuadrilaters(Signal2D1D im)
	{
		double sx=((im.xmax()-im.xmin())/im.dimx());
		double sy=((im.ymax()-im.ymin())/im.dimy());
		for (int i=0;i<im.dimx()-1;i++)
			for (int j=0;j<im.dimy()-1;j++)
			{
				double x=im.xmin()+i*sx;
				double y=im.ymin()+j*sy;
				double z1=im.z(i, j);
				double z2=im.z(i+1, j);
				double z3=im.z(i+1, j+1);
				double z4=im.z(i, j+1);
				Vecteur p1=new Vecteur(x,y,z1);
				Vecteur p2=new Vecteur(x+sx,y,z2);
				Vecteur p3=new Vecteur(x+sx,y+sy,z3);
				Vecteur p4=new Vecteur(x,y+sy,z4);
				Quadrilatere3DColor q=new Quadrilatere3DColor(p1,p2,p3,p4);
				this.addObject3D(q);
			}
	}

	//
	//	public Iterator<Object3DColor> iterator()
	//	{
	//		return listOfObject3DColor.iterator();
	//	}



	public Vector<Object3DColor> getAxes()
	{
		return axes;
	}


	public Sun getSun(){return sun;}

	public void add(Graph3DDrawingLayer dl)
	{
		graph3DDrawingLayers.add(dl);
		//dl.setGraph3DPanel(this);
	}


	public void add(Vector<Quadrilatere3DColor> quadrilatere3Ds)
	{
		for (Quadrilatere3DColor q:quadrilatere3Ds) add(q);
	}



	@Override
	public void actionPerformed(ActionEvent e)
	{
		Object source = e.getSource();

		if (source instanceof JMenuItem)//import an STL ASCII file
		{
			JMenuItem mi=(JMenuItem)source;
			String label=mi.getText();
			if (label.compareTo(Messager.getString("add_mesh"))==0)//get add mesh menu
			{
				popMenuAddMesh.show(this.getJPanel(),mousePos.x,mousePos.y);
			}
			if (label.compareTo(Messager.getString("import"))==0)//get import  menu
			{
				popMenuImport.show(this.getJPanel(),mousePos.x,mousePos.y);
			}
			if (label.compareTo(Messager.getString("export"))==0)//get export  menu
			{
				popMenuExport.show(this.getJPanel(),mousePos.x,mousePos.y);
			}

			if (label.compareTo(Messager.getString("import_stl"))==0)//import stl file
			{
				//if (app!=null) Global.path=app.path;
				JFileChooser df=new JFileChooser(path);
				STLFileFilter ff=new STLFileFilter();
				df.addChoosableFileFilter(ff);
				df.setFileFilter(ff);
				int returnVal = df.showOpenDialog(null);
				String filename="";
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					path=df.getSelectedFile().getParent()+File.separator;
					filename=df.getSelectedFile().getName();
					String s=TextFiles.readFile(path+filename).toString();
					TriangleMeshFrame mesh=new TriangleMeshFrame();
					mesh.readSTL(s);
					this.add(mesh.getTriangles());
					this.repaint();
				}	
				//if (app!=null) app.path=Global.path;
			}

			if (label.compareTo(Messager.getString("import_stl_binary"))==0)//import stl file
			{
				JFileChooser df=new JFileChooser(path);
				STLFileFilter ff=new STLFileFilter();
				df.addChoosableFileFilter(ff);
				df.setFileFilter(ff);
				int returnVal = df.showOpenDialog(null);
				String filename="";
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					path=df.getSelectedFile().getParent()+File.separator;
					filename=df.getSelectedFile().getName();
					TriangleMeshFrame set=new TriangleMeshFrame();
					set.readSTLinBinaryFile(path,filename);
					this.add(set.getTriangles());
					this.repaint();
				}	
			}


			if (label.compareTo(Messager.getString("export_stl"))==0)//export as stl file
			{
				//saveTrianglesAndQuadrilatersAsSTL();
				String s=null;
				if (this.getSelectedObjects().size()==1)
				{
					Object3D o=this.getSelectedObjects().elementAt(0);
					if (o instanceof TriangleMeshSource)
					{
						TriangleMeshSource ts=(TriangleMeshSource)o;
						s=ts.getTriangles().saveToSTL();
					}
					//					if (o instanceof TriangleMeshFrame)
					//					{
					//						TriangleMeshFrame tm=(TriangleMeshFrame)o;
					//						s=tm.saveToSTL();
					//					}

				}

				if (s!=null)
				{
					JFileChooser df=new JFileChooser(path);
					STLFileFilter ff=new STLFileFilter();
					df.addChoosableFileFilter(ff);
					df.setFileFilter(ff);
					int returnVal = df.showSaveDialog(null);
					if(returnVal == JFileChooser.APPROVE_OPTION)
					{
						path=df.getSelectedFile().getParent()+File.separator;
						String imageFileName=df.getSelectedFile().getName();
						if (!imageFileName.endsWith(ff.getExtension())) imageFileName+="."+ff.getExtension();
						TextFiles.saveString(path+imageFileName, s);
					}
				}

			}



			if (label.compareTo(Messager.getString("export_obj"))==0)//export as obj  file
			{
				String s=null;
				if (this.getSelectedObjects().size()==1)
				{
					Object3D o=this.getSelectedObjects().elementAt(0);
					if (o instanceof TriangleMeshSource)
					{
						TriangleMeshSource ts=(TriangleMeshSource)o;
						s=ts.getTriangles().saveToOBJ();
					}

				}

				if (s!=null)
				{
					JFileChooser df=new JFileChooser(path);
					String[] ext= {"obj"};
					CFileFilter ff=new CFileFilter(ext,"obj file");
					df.addChoosableFileFilter(ff);
					df.setFileFilter(ff);
					int returnVal = df.showSaveDialog(null);
					if(returnVal == JFileChooser.APPROVE_OPTION)
					{
						path=df.getSelectedFile().getParent()+File.separator;
						String imageFileName=df.getSelectedFile().getName();
						if (!imageFileName.endsWith(ff.getExtension())) imageFileName+="."+ff.getExtension();
						TextFiles.saveString(path+imageFileName, s);
					}
				}

			}




			if (label.compareTo(Messager.getString("export_pbrt"))==0)//export for pbrt
			{
				Object3DSet merge=new Object3DSet();
				for (Object3DSet set:listOfSets) merge.add(set);
				//for (Object3DColor o:listOfObject3DColor) merge.add(o);
				TriangleMesh ts=merge.transformToTriangles();
				String pbrt=ts.saveToPBRT();
				JFileChooser df=new JFileChooser(path);
				CFileFilter cff=new CFileFilter("pbrt","PBRT geometry file");
				df.addChoosableFileFilter(cff);
				df.setFileFilter(cff);
				int returnVal = df.showSaveDialog(null);
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					path=df.getSelectedFile().getParent()+File.separator;
					String imageFileName=df.getSelectedFile().getName();
					if (!imageFileName.endsWith(cff.getExtension())) imageFileName+="."+cff.getExtension();
					TextFiles.saveString(path+imageFileName, pbrt);
				}	
			}
			if (label.compareTo(Messager.getString("erase_selected"))==0)//erase selected objects
			{
				this.eraseAll();
			}
			if (label.compareTo(Messager.getString("import_points_cloud"))==0)//import a cloud of points
			{
				JFileChooser df=new JFileChooser(path);
				CFileFilter ff=new CFileFilter("xyz","cloud of points");
				df.addChoosableFileFilter(ff);
				df.setFileFilter(ff);
				int returnVal = df.showOpenDialog(null);
				String filename="";
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					path=df.getSelectedFile().getParent()+File.separator;
					filename=df.getSelectedFile().getName();

					StringBuffer sb=TextFiles.readFile(path+filename);
					Cloud cloud=new Cloud();
					cloud.readXYZFormat(sb);
					Object3DColorSet set=new Object3DColorSet();
					for (CloudPoint cp:cloud)
					{
						set.add(new Point3DColor(cp.getP(),Color.RED,1));
					}

					this.addColorSet(set);
					this.repaint();
				}	
			}
			if (label.compareTo(Messager.getString("import_obj"))==0)//import an obj file
			{
				JFileChooser df=new JFileChooser(path);
				String[] ext= {"obj"};
				CFileFilter ff=new CFileFilter(ext,"wavefront obj file");
				df.addChoosableFileFilter(ff);
				df.setFileFilter(ff);
				int returnVal = df.showOpenDialog(null);
				String filename="";
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					path=df.getSelectedFile().getParent()+File.separator;
					filename=df.getSelectedFile().getName();

					StringBuffer sb=TextFiles.readFile(path+filename);

					Object3DColorSet set=new Object3DColorSet();
					set.addObjObjects(sb.toString());

					this.addColorSet(set);
					this.repaint();
				}	
			}

			if (label.compareTo(Messager.getString("import_Signal3D1D"))==0)//import 3D->1D signal 
			{
				JFileChooser df=new JFileChooser(path);
				String[] ext= {"raw"};
				CFileFilter ff=new CFileFilter(ext,"Signal3D1D file");
				df.addChoosableFileFilter(ff);
				df.setFileFilter(ff);
				int returnVal = df.showOpenDialog(null);
				String filename="";
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					path=df.getSelectedFile().getParent()+File.separator;
					filename=df.getSelectedFile().getName();
					Signal3D1D sig=new Signal3D1D();
					sig.loadRawFloat(path, filename, false);
					Function1D1D lut=new Line(1,0);
					Object3DColorSet o3Dset=Object3DColorSet.build3Dset(sig,5,lut,0,Color.BLACK.getRGB());
					this.addColorSet(o3Dset);
					this.repaint();
				}	

			}
			if (label.compareTo(Messager.getString("import_jpg_image"))==0)//import jpg image
			{
				JFileChooser df=new JFileChooser(path);
				PNG_JPG_FileFilter ff=new PNG_JPG_FileFilter();
				df.addChoosableFileFilter(ff);
				df.setFileFilter(ff);
				int returnVal = df.showOpenDialog(null);
				String filename="";
				if(returnVal == JFileChooser.APPROVE_OPTION)
				{
					this.path=df.getSelectedFile().getParent()+File.separator;
					filename=df.getSelectedFile().getName();
					Signal2D1D shape=new Signal2D1D();

					//ask for the scale for XY 
					InputDialog inputDialog=new InputDialog();
					inputDialog.setFieldLabel("Image width (mm) ? ");
					inputDialog.setInitialFieldValue("50");
					inputDialog.show();
					//double scaleXY=new Double(inputDialog.getStringAnswer()); //mm/pix
					double w=Double.parseDouble(inputDialog.getStringAnswer());


					//ask for the scale for  Z
					InputDialog inputDialog2=new InputDialog();
					inputDialog2.setFieldLabel("Depth in z (mm)? ");
					inputDialog2.setInitialFieldValue("3");
					inputDialog2.show();
					//double scaleZ=new Double(inputDialog2.getStringAnswer());
					double dz=Double.parseDouble(inputDialog2.getStringAnswer());

					shape.loadFromImage(new CImage(this.path,filename,false));
					double scaleXY=w/shape.dimx();
					shape.setxmin(0);
					shape.setxmax(shape.dimx()*scaleXY);
					shape.setymin(0);
					shape.setymax(shape.dimy()*scaleXY);
					shape._multiply(dz/(shape.zmax()-shape.zmin()));
					shape._flipAroundX();
					//ChartWindow cw=new ChartWindow(shape);
					
					TriangleMeshFrame mesh=new TriangleMeshFrame();
					mesh.readSignal2D1D(shape);
					this.add(mesh);
					this.repaint();
				}
			}

			if (label.compareTo(Messager.getString("add_box"))==0)//add a box
			{
				Box b=new Box();
				listOfObject3D.add(b);
				this.repaint();
			}

			if (label.compareTo(Messager.getString("add_plane"))==0)//add a square
			{
				Quadrilatere3DFrame b=new Quadrilatere3DFrame();
				listOfObject3D.add(b);
				this.repaint();
			}

			if (label.compareTo(Messager.getString("add_cylinder"))==0)//add a cylinder
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("diameter h dim");
				inputDialog.setInitialFieldValue("2 1 20");
				inputDialog.show();
				Definition def=new Definition(inputDialog.getStringAnswer());
				double  d=new Double(def.word(0));
				double  h=new Double(def.word(1));
				int  dim=new Integer(def.word(2));
				listOfObject3D.add(new CylinderMesh(new Vecteur(),d,h,dim));
				this.repaint();
			}

			if (label.compareTo(Messager.getString("add_cone"))==0)//add a cone
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("diameter h dim");
				inputDialog.setInitialFieldValue("2 1 20");
				inputDialog.show();
				Definition def=new Definition(inputDialog.getStringAnswer());
				double  d=new Double(def.word(0));
				double  h=new Double(def.word(1));
				int  dim=new Integer(def.word(2));
				listOfObject3D.add(new ConeMesh(new Vecteur(),d,h,dim));
				this.repaint();
			}
			
			if (label.compareTo(Messager.getString("add_sphereICO"))==0)//add a sphere ICO
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("radius precision(0 to 4)");
				inputDialog.setInitialFieldValue("1 2");
				inputDialog.show();
				Definition def=new Definition(inputDialog.getStringAnswer());
				double  r= Double.parseDouble(def.word(0));
				int  p=Integer.parseInt(def.word(1));
				SphereICO s=new SphereICO(r,p);
				listOfObject3D.add(s);
				this.repaint();
			}

			if (label.compareTo(Messager.getString("add_spherical_cap"))==0)//add a spherical cap
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("sphereRadius capDiameter dim");
				inputDialog.setInitialFieldValue("1 1 20");
				inputDialog.show();
				Definition def=new Definition(inputDialog.getStringAnswer());
				double  r=new Double(def.word(0));
				double  dCap=new Double(def.word(1));
				int  dim=new Integer(def.word(2));
				SphericalCap sc=new SphericalCap(new Vecteur(),r,dCap,dim,false);
				listOfObject3D.add(sc);
				this.repaint();
			}

			if (label.compareTo(Messager.getString("add_lens"))==0)//add a lens
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("radius1 radius2 diameter thickness dim");
				inputDialog.setInitialFieldValue("-50 50 25 5 50");
				inputDialog.show();
				Definition def=new Definition(inputDialog.getStringAnswer());
				double  r1=new Double(def.word(0));
				double  r2=new Double(def.word(1));
				double  d=new Double(def.word(2));
				double  tc=new Double(def.word(3));
				int  dim=new Integer(def.word(4));
				Lens sc=new Lens(r1,r2,d,tc,dim);
				listOfObject3D.add(sc);
				this.repaint();
			}
			if (label.compareTo(Messager.getString("add_pyramid"))==0)//add a pyramid
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("a(side) h(height)");
				inputDialog.setInitialFieldValue("1 1");
				inputDialog.show();
				Definition def=new Definition(inputDialog.getStringAnswer());
				double  a= Double.parseDouble(def.word(0));
				double  h= Double.parseDouble(def.word(1));
				Pyramid sc=new Pyramid(a,h);
				listOfObject3D.add(sc);
				this.repaint();
			}
			if (label.compareTo(Messager.getString("add_sierpinski_pyramid"))==0)//add a fractal Sierpinski pyramid
			{
				InputDialog inputDialog=new InputDialog();
				inputDialog.setFieldLabel("side height order");
				inputDialog.setInitialFieldValue("1 1 3");
				inputDialog.show();
				Definition def=new Definition(inputDialog.getStringAnswer());
				double  a= Double.parseDouble(def.word(0));
				double  h= Double.parseDouble(def.word(1));
				int  p=Integer.parseInt(def.word(1));
				SierpinskiPyramid sc=new SierpinskiPyramid(a,h,p);
				listOfObject3D.add(sc);
				this.repaint();
			}

		}
	}


	public void fillSelectedObjectsClick(Point mousePos)
	{
		selectedObjects.removeAllElements();

		for (Object3D o:listOfObject3D) 
		{
			if (o.isOn(mousePos, proj)) selectedObjects.add(o);	
		}

		for (Object3DSet set:listOfSets) 
		{
			for (Object3D o:set) 
			{
				if (o.isOn(mousePos, proj)) selectedObjects.add(o);
			}
		}

		//TODO marche pas
		for (MeshRendering mr:listOfObject3DRendering) 
		{
			//System.out.println(mr.getClass());
			if (mr.isOn(mousePos, proj)) selectedObjects.add((Object3D)mr.getMeshSource());	
		}


		Global.setMessage(selectedObjects.size()+" selectedObjects");
		//for (Object3D o:selectedObjects) System.out.println(o.getClass());
	}


	public void fillSelectedObjectsInBox(Point mousePos)
	{
		selectedObjects.removeAllElements();
		for (Object3DSet set:listOfSets) 
		{
			for (Object3D o:set) 
			{
				if (o instanceof Point3DColor)	if (isSelected((Point3DColor)o)) selectedObjects.add(o);
				if (o instanceof CloudPoint)	if (isSelected((CloudPoint)o)) selectedObjects.add(o);
			}
		}

		//		for (Object3DColor o:listOfObject3DColor) 
		//		{
		//			if (o instanceof Point3DColor)	if (isSelected((Point3DColor)o)) selectedObjects.add(o);
		//			if (o instanceof CloudPoint)	if (isSelected((CloudPoint)o)) selectedObjects.add(o);
		//		}
		for (Object3D o:listOfObject3D) 
		{
			if (o.isOn(mousePos, proj)) selectedObjects.add(o);	
			if (o instanceof Point3DColor)	if (isSelected((Point3DColor)o)) selectedObjects.add(o);	
			if (o instanceof CloudPoint) if (isSelected((CloudPoint)o)) selectedObjects.add(o);
			if (o instanceof Cloud) 
			{
				Cloud cloud=(Cloud)o;
				for (CloudPoint cp:cloud) if (isSelected(cp)) selectedObjects.add(cp);
			}
		}
		Global.setMessage(selectedObjects.size()+" selectedObjects");
	}


	public Vector<Object3D> getSelectedObjects(){return selectedObjects;}


	private boolean isSelected(Point3DColor p)
	{
		double[] coord=p.getCooScreen(proj);
		return (
				(coord[0]>=pixRectangleSelection1[0])&&(coord[0]<=pixRectangleSelection2[0])
				&&
				(coord[1]>=pixRectangleSelection1[1])&&(coord[1]<=pixRectangleSelection2[1])
				);
	}

	private boolean isSelected(CloudPoint cp)
	{
		double[] coord=proj.calcCoorEcran(cp.getP());
		//System.out.println(coord[0]+" "+coord[1]);
		return (
				(coord[0]>=pixRectangleSelection1[0])&&(coord[0]<=pixRectangleSelection2[0])
				&&
				(coord[1]>=pixRectangleSelection1[1])&&(coord[1]<=pixRectangleSelection2[1])
				);
	}



	public void unselectAll()
	{
		selectedObjects.removeAllElements();
		this.repaint();
	}

	public void selectAll()
	{
		selectedObjects.removeAllElements();
		for (Object3DSet set:listOfSets) 
		{
			for (Object3D o:set) 
			{
				if (o instanceof Point3DColor)	 selectedObjects.add(o);
				if (o instanceof CloudPoint)	 selectedObjects.add(o);
			}
		}

		//		for (Object3DColor o:listOfObject3DColor) 
		//		{
		//			if (o instanceof Point3DColor)	 selectedObjects.add(o);
		//			if (o instanceof CloudPoint)	 selectedObjects.add(o);
		//
		//		}
		for (Object3D o:listOfObject3D) 
		{
			if (o instanceof Point3DColor)	 selectedObjects.add(o);	
			if (o instanceof CloudPoint)  selectedObjects.add(o);
			if (o instanceof Cloud) 
			{
				Cloud cloud=(Cloud)o;
				for (CloudPoint cp:cloud)  selectedObjects.add(cp);
			}
		}
		Global.setMessage(selectedObjects.size()+" selectedObjects");

		this.repaint();
	}


	@Override //(DrawingLayer)
	public void draw(Graphics g, JPanel jp, ImageSource is)
	{
		if (planeXY!=null) planeXY.draw(g, proj);
		if (planeXZ!=null) planeXZ.draw(g, proj);
		if (planeYZ!=null) planeYZ.draw(g, proj);
		if (Ox!=null) Ox.draw(g, proj);
		if (Oy!=null) Oy.draw(g, proj);
		if (Oz!=null) Oz.draw(g, proj);
		for (int i=0;i<axes.size();i++)
		{
			axes.elementAt(i).draw(g, proj);
		}
		for (Object3DColorSet set:listOfColorSets) 
		{
			if (render) set.draw(g, proj,sun);
			else set.draw(g, proj);
//			for (Object3DColor o:set) 
//			{		
//				if (o instanceof Facet) 
//				{
//					if (render)((Facet)o).draw(g, proj,sun);
//					else ((Facet)o).draw(g, proj,null);
//				}
//				else o.draw(g, proj);
//			}
		}

		for (Object3DSet set:listOfSets) 
		{
			g.setColor(Color.BLACK);
			for (Object3D o:set) 
			{		
				if (o instanceof Facet) 
				{
					if (render)((Facet)o).draw(g, proj,sun);
					else ((Facet)o).draw(g, proj,null);
				}
				else o.draw(g, proj);
			}
		}

		//		for (Object3DColor o:listOfObject3DColor) 
		//		{
		//			if (o instanceof Facet) 
		//			{
		//				((Facet) o).setFilled(wireFrame);
		//				if (render)((Facet)o).draw(g, proj,sun);
		//				else ((Facet)o).draw(g, proj,null);
		//			}
		//			else o.draw(g, proj);
		//		}

		for (Object3D o:listOfObject3D) 
		{
			g.setColor(Color.BLACK);
			o.draw(g, proj);
		}


		for (MeshRendering or:listOfObject3DRendering) 
		{
			or.draw(g, proj);
		}


		drawMenu(g,flagmenu);
		drawMessage(g,flagmenu);

		//draw the zooming rectangle
		if (selectionMode)
		{
			g.setColor(Color.black);
			int c0=(int)Math.min(pixRectangleSelection1[0],pixRectangleSelection2[0]);
			int c1=(int)Math.min(pixRectangleSelection1[1],pixRectangleSelection2[1]);
			int w=(int)Math.abs(pixRectangleSelection1[0]-pixRectangleSelection2[0]);
			int h=(int)Math.abs(pixRectangleSelection1[1]-pixRectangleSelection2[1]);
			g.drawRect(c0,c1,w,h);
		}

		//mark selected objects
		{
			g.setColor(Color.red);
			for (Object3D o:selectedObjects)
			{
				o.draw(g, proj);
			}
		}

		//additional drawing layers:
		for (Graph3DDrawingLayer drawingLayer:graph3DDrawingLayers) 
		{
			drawingLayer.draw(g);
		}

		//mouse cursor info:
		//g.setColor(Color.RED);
		//g.drawString(mouseMessage,mousePos.x,mousePos.y);
	}




	//
	//
	//public void add(Cloud cloud, Color color,int pointSize)
	//{
	//for (CloudPoint cp:cloud)
	//	{
	//	add(new Point3DColor(cp.getP(),color,pointSize));
	//	}
	//}
	//
	//
	//


	public void eraseAll()
	{
		this.removeAll3DObjects();
		initialiseProjector();
		this.update();

	}
	public void eraseSelected()
	{
		for (Object3D o:selectedObjects)
		{
			listOfObject3D.remove(o);
		}
		//initialiseProjector();
		this.update();
	}

	public void hidePanelSide()
	{
		mainPanel.remove(panelSide);
		mainPanel.validate();
	}



	public boolean isRender() {
		return render;
	}



	public void setRender(boolean render) {
		this.render = render;
	}



	public boolean isWireFrame() {
		return wireFrame;
	}



	public void setWireFrame(boolean wireFrame) {
		this.wireFrame = wireFrame;
	}



	public void delete(Object3D ob) 
	{
		listOfObject3D.remove(ob);
	}



	public ProjectorCamera getProjector() 
	{
		return proj;	
	}



	public Vector<Object3D> getListOfObject3D() {
		return listOfObject3D;
	}

	
	
	
	
	
	
	
	
	/**
	 * test prg
	 * @param args
	 */
	public static void main(String[] args)
	{
		//test prg:
		Graph3DPanel graph3DPanel=new Graph3DPanel();
		graph3DPanel.getPanelSide().setBackground(Color.DARK_GRAY);
		graph3DPanel.getJPanel().setBackground(Color.LIGHT_GRAY);
		Vecteur v1=new Vecteur(0,0,2);
		Vecteur v2=new Vecteur(1,0,2);
		Vecteur v3=new Vecteur(1,1,3);
		Vecteur v4=new Vecteur(0,1,2);
		//Quadrilatere3D q=new Quadrilatere3D(v1,v2,v3,v4,Color.blue,true);
		Triangle3DColor q=new Triangle3DColor(v1,v2,v3,Color.blue,true);
		//graph3DPanel.addObject3D(q);

		Vecteur v21=new Vecteur(0,0,3);
		Vecteur v22=new Vecteur(1,0,3);
		Vecteur v23=new Vecteur(1,1,3);
		Vecteur v24=new Vecteur(0,1,3);
		//Quadrilatere3D q2=new Quadrilatere3D(v21,v22,v23,v24,Color.red,true);
		//Triangle3DColor q2=new Triangle3DColor(v21,v22,v23,Color.red,true);

		//TriangleMesh set=new TriangleMesh();

		//set.add(q);
		//set.add(q2);

		//graph3DPanel.add(set);

		//graph3DPanel.add(new Box());
		//graph3DPanel.add(new CylinderMesh());

		//System.out.println("centre "+q.getCentre());
		//System.out.println("normal "+q.getNormal());
		//graph3DPanel.addObject3D(new Segment3DColor(q.getCentre(),q.getCentre().addn(q.getNormal())));

		//graph3DPanel.listOfObject3D.add(new Box());
		//graph3DPanel.add(new Box());

		//		System.out.println(set.saveToSTL());
		//		TextFiles.saveString("test.stl", set.saveToSTL());

		//		TriangleMeshFrame mesh=new TriangleMeshFrame();
		//		String s=TextFiles.readFile("/home/laurent/cap.stl").toString();
		//		mesh.readSTL(s);
		//		graph3DPanel.add(mesh);

		//	graph3DPanel.add(new SphericalCap(50,25,50));

		//	graph3DPanel.add(new Lens(0,50,25,5,50));


		//MeshRendering mr=new MeshRendering(new Mesh(new Box()),Color.BLUE);
		//MeshRendering mr=new MeshRendering(new Mesh(new SphericalCap()),Color.BLUE);

		//		Cloud c=new Cloud();
		//		for (int i=0;i<100;i++) c.add(new CloudPoint(new Vecteur(Math.random(),Math.random(),Math.random())));
		//		MeshRendering mr=new MeshRendering(new Mesh(c),Color.BLUE);
		//		graph3DPanel.add(mr);



		//		Signal3D1D sig=new Signal3D1D(-2,2,-2,2,-2,2,10,10,10);
		//		SphericalGauss sg=new SphericalGauss();
		//		sig.fillWithFunction(sg);
		//		Signal3D1Drendering sr=new Signal3D1Drendering(sig);
		//		graph3DPanel.add(sr);
		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(graph3DPanel,"update"));
		//			String[] names={"dotSize","autoLUT","transparent","log","cutX","cutY","cutZ"};
		//			ParamsBox paramsBox1=new ParamsBox(sr,tl,null,names,null,null,ParamsBox.VERTICAL,140,30);
		//			//((ParInt)paramsBox1.getParamBoxOfParam("cutX").getParam()).set;
		//			graph3DPanel.getPanelSide().add(paramsBox1.getComponent());
		//		}



		//		Signal2D1D sig2=new Signal2D1D(-2,2,-2,2,50,50);
		//		sig2.fillWithFonction(new Gaussian()); 
		//		graph3DPanel.add(sig2);

		//SphereICO sp=new SphereICO();
		//CylinderMesh cm=new CylinderMesh();
		//graph3DPanel.add(cm);
		Object3DColorSet setc=new Object3DColorSet();
		setc.add(new SphericalCap().getTriangles());		
		graph3DPanel.addColorSet(setc);


		//graph3DPanel.getJPanel().setBackground(Color.gray);

		//graph3DPanel.updateOccupyingCube();
		graph3DPanel.initialiseProjector();
		graph3DPanel.update();



		//create frame and add the panel 3D
		CJFrame cjf=new CJFrame();
		cjf.getContentPane().setLayout(new BorderLayout());
		cjf.setSize(600, 400);
		cjf.setVisible(true);
		cjf.add(graph3DPanel.getComponent());

	}


	
	
	
	
	
	
	

}