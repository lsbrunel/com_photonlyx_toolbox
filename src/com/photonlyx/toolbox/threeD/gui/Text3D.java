package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Vecteur;

public class Text3D implements Object3D
{
private Vecteur p;//position of the text
private String text="";
private Color color=Color.black;

public Text3D(String text,Vecteur p)
{
this.text=text;
this.p=p;
}

public Text3D(String text,Vecteur p,Color c)
{
this.text=text;
this.p=p;
color=c;
}

@Override
/**
 * draw the voxels sons if any
 */
public void draw(Graphics g, Projector proj)
{
double[] cooScreen1;
g.setColor(color);
cooScreen1=proj.calcCoorEcran(p);
g.drawString(text,(int)cooScreen1[0],(int)cooScreen1[1]);
}

@Override
public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
{
if (p==null) return;
for (int k=0;k<3;k++) 
	{
	double r=p.coord(k);
	if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
	if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
	}	
}

@Override
public double getDistanceToScreen(Projector proj)
{
return 	proj.zCoord(p);
}

public Color getColor()
{
return color;
}

public void setColor(Color color)
{
this.color = color;
}

@Override
public boolean isOn(Point p, Projector proj)
{
// TODO Auto-generated method stub
return false;
}







}
