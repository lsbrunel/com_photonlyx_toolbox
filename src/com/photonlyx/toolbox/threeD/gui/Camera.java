package com.photonlyx.toolbox.threeD.gui;
import java.awt.Color;
import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Plane;
import com.photonlyx.toolbox.math.geometry.PlaneFramed;
import com.photonlyx.toolbox.math.geometry.Rotator;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;





public class Camera extends Object3DColorSet
{
private double axInit=0,ayInit=0,azInit=0;//initial position
private double ax=0,ay=0,az=0;//fine tuning angle
private double lensx=0,lensy=0,lensz=0;//fine tuning position
private double focal=20;//ditance lens-sensor in mm
//private Vecteur angles=new Vecteur(ax,ay,az);//angular position 
//private Vecteur lens=new Vecteur(0,0,focal);
private Vecteur ccd0=new Vecteur(0,0,0);//ccd center
private Vecteur ccdx=new Vecteur(1,0,0),ccdy=new Vecteur(0,1,0),ccdz=new Vecteur(0,0,1);//local frame of the ccd
private double pixelX=0.0067,pixelY=0.0067;//pixel size in mm
private int nbPixelsX=1280,nbPixelsY=1024;


public static void main(String[] args)
{
//test prg:
Vecteur lens=new Vecteur(0,100*Math.sin(-20*Math.PI/180.0),100*Math.cos(-20*Math.PI/180.0));
double focal=20;
Camera sc=new Camera(focal,-20*Math.PI/180.0,0,0,lens.x(),lens.y(),lens.z(),0.0067,0.0067,1280,1024);
Vecteur P=new Vecteur(0,0,0);
int[] xy =new int[2];
sc.cameraPointMes(P,xy);
System.out.println(xy[0]+" "+xy[1]);
}


public Camera()
{
updateCCDLocalFrame();	
}


/**
 * 
 * @param focal
 * @param angles rotation of camera in rad (first az then ay then ax)
 * @param lens  lens position
 * @param pixelX  pixel size in x direction
 * @param pixelY pixel size in y direction
 * @param nbPixelsX 
 * @param nbPixelsY
 */
public Camera(double focal, double ax,double ay,double az, 
		double lensx,double lensy,double lensz,  double pixelX,
		double pixelY, int nbPixelsX, int nbPixelsY) {
	super();
	this.focal = focal;
	this.ax=ax;
	this.ay=ay;
	this.az=az;
	this.lensx = lensx;
	this.lensy = lensy;
	this.lensz = lensz;
	this.pixelX = pixelX;
	this.pixelY = pixelY;
	this.nbPixelsX = nbPixelsX;
	this.nbPixelsY = nbPixelsY;
	updateCCDLocalFrame();	
}

/**
 * set the angle to fix the frame from which the angles ax,ay,and az are refered
 * @param ax
 * @param ay
 * @param az
 */
public void setInitAngles(double axi,double ayi,double azi)
{
this.axInit=axi;	
this.ayInit=ayi;	
this.azInit=azi;	
updateCCDLocalFrame();	
}

public double getFocal() {
	return focal;
}


public double getAx() {
	return ax;
}


public void setAx(double ax) {
	this.ax = ax;
	updateCCDLocalFrame();	
}


public double getAy() {
	return ay;
}


public void setAy(double ay) {
	this.ay = ay;
	updateCCDLocalFrame();	
}


public double getAz() {
	return az;
}


public void setAz(double az) {
	this.az = az;
	updateCCDLocalFrame();	
}


public void setFocal(double focal) {
	this.focal = focal;
}

public double getPixelX() {
	return pixelX;
}


public double getLensx() {
	return lensx;
}


public void setLensx(double lensx) {
	this.lensx = lensx;
	updateCCDLocalFrame();
	}


public double getLensy() {
	return lensy;
}


public void setLensy(double lensy) {
	this.lensy = lensy;
	updateCCDLocalFrame();
	}


public double getLensz() {
	return lensz;
}


public void setLensz(double lensz) {
	this.lensz = lensz;
	updateCCDLocalFrame();
}


public void setPixelX(double pixelX) {
	this.pixelX = pixelX;
}


public double getPixelY() {
	return pixelY;
}


public void setPixelY(double pixelY) {
	this.pixelY = pixelY;
}


public int getNbPixelsX() {
	return nbPixelsX;
}


public void setNbPixelsX(int nbPixelsX) {
	this.nbPixelsX = nbPixelsX;
}


public int getNbPixelsY() {
	return nbPixelsY;
}


public void setNbPixelsY(int nbPixelsY) {
	this.nbPixelsY = nbPixelsY;
}


public Vecteur getCcd0() {
	return ccd0;
}


public Vecteur getCcdx() {
	return ccdx;
}


public Vecteur getCcdy() {
	return ccdy;
}


public Vecteur getCcdz() {
	return ccdz;
}


/**
 * place the ccd vectors and the lens according to angles and focal
 */
private  void updateCCDLocalFrame()
{
//ccd init angles
Rotator r=new Rotator(axInit,ayInit,azInit);
ccdx.affect(r.turn(new Vecteur(1,0,0)));
ccdy.affect(r.turn(new Vecteur(0,1,0)));
ccdz.affect(r.turn(new Vecteur(0,0,1)));
	
//ccd angle from init position
r=new Rotator(ax,ay,az);
Vecteur ccdx1=r.turn(ccdx);
Vecteur ccdy1=r.turn(ccdy);
Vecteur ccdz1=r.turn(ccdz);	
ccdx.affect(ccdx1);
ccdy.affect(ccdy1);
ccdz.affect(ccdz1);

//ccd position:
Vecteur lens=new Vecteur(lensx,lensy,lensz);
ccd0.affect(lens.addn(ccdz.scmul(-focal)));
//System.out.println(getClass()+"\n"+this);
}

/**
 * calc the image coord in pixels of a point source
 * @param P point source in space
 * @param xy coord of point image in sensor
 */
public void cameraPointMes(Vecteur P,int[] xy)
	{
	//calculate the director vector of the line source-lens
	Vecteur vectdir;
	//line formed by lens and source:
	Vecteur lens=new Vecteur(lensx,lensy,lensz);
	Vecteur o=lens;
	vectdir=P.sub(lens);
	//intersect the sensor surface:
	Vecteur	point=new Vecteur();
	point.affect(Vecteur.intersectionPlaneLine(ccd0,ccdz,o,vectdir));
	Vecteur	vectloc=point.sub(ccd0);
	xy[0]=nbPixelsX/2+(int)(ccdx.mul(vectloc)/pixelX);
	xy[1]=nbPixelsY/2+(int)(ccdy.mul(vectloc)/pixelY);
	//System.out.println("cameraPointMes"+" P "+P+" vectloc"+vectloc+" pix:"+xy[0]+" "+xy[1]);
	}


/**
 * gives a point of plane plane that gives a point image of pixel (i,j)
 * @param plane
 * @param i
 * @param j
 * @return
 */
public Vecteur unProjectOnAPlane(Plane plane,int i,int j)
{
//coordinates on the sensor:
double xi=(i-nbPixelsX/2)*pixelX;
double yi=(j-nbPixelsY/2)*pixelY;
//line lens pixel
Vecteur lens=new Vecteur(lensx,lensy,lensz);
Vecteur nline=ccdx.scmul(xi).addn(ccdy.scmul(yi)).addn(ccd0).sub(lens);
return plane.intersectionLine(lens, nline);
}



public double[] unProjectOnAPlaneFramed(PlaneFramed plane, int i, int j) 
{
//coordinates on the sensor:
double xi=(i-nbPixelsX/2)*pixelX;
double yi=(j-nbPixelsY/2)*pixelY;
//line lens pixel
Vecteur lens=new Vecteur(lensx,lensy,lensz);
Vecteur nline=ccdx.scmul(xi).addn(ccdy.scmul(yi)).addn(ccd0).sub(lens);
return plane.intersectionLineLocal(lens, nline);
}

	
	

public String toString()
{
Vecteur lens=new Vecteur(lensx,lensy,lensz);
StringBuffer sb=new StringBuffer();
sb.append("focal "+focal+"\n");
sb.append("pixel size "+pixelX+"x"+pixelY+"\n");
sb.append("image size "+nbPixelsX+"x"+nbPixelsY+"\n");
sb.append("lens "+lens+"\n");
sb.append("ccd0 "+ccd0+"\n");
sb.append("ccdx "+ccdx+"\n");
sb.append("ccdy "+ccdy+"\n");
return sb.toString();
}

public  Vector<Object3DColor> getObject3DList()
{
Vecteur lens=new Vecteur(lensx,lensy,lensz);

Vector<Object3DColor> v=new Vector<Object3DColor>();
Vecteur xx=ccdx.scmul(sensorW());
Vecteur xxm=ccdx.scmul(-sensorW());
Vecteur yy=ccdy.scmul(sensorH());
Vecteur yym=ccdy.scmul(-sensorH());
Vecteur P1=ccd0.addn(xx).addn(yy);
Vecteur P2=ccd0.addn(xxm).addn(yy);
Vecteur P3=ccd0.addn(xxm).addn(yym);
Vecteur P4=ccd0.addn(xx).addn(yym);
Quadrilatere3DColor q=new Quadrilatere3DColor(P1,P2,P3,P4,Color.black,false);
v.add(q);
Segment3DColor s=new Segment3DColor(ccd0,lens);
v.add(s);
v.add(new Segment3DColor(ccd0,ccd0.addn(xx)));
v.add(new Segment3DColor(ccd0,ccd0.addn(yy)));
return v;
}


public double sensorW() {
	return pixelX*nbPixelsX;
}


public double sensorH() {
	return pixelY*nbPixelsY;
}


public Vecteur getLens() {
	Vecteur lens=new Vecteur(lensx,lensy,lensz);
	return lens;
}


/**turn the camera with respect to its init frame*/
private void turn(double ax,double ay,double az)
{
//System.out.println(pX);
//System.out.println(pY);
//System.out.println(pZ);
Vecteur angles=new Vecteur(ax,ay,az);
Rotator r=new Rotator(angles);
//r.tourner(pX);
//make a turned frame from main frame
Vecteur pX1=r.turn(new Vecteur(1,0,0));
Vecteur pY1=r.turn(new Vecteur(0,1,0));
Vecteur pZ1=r.turn(new Vecteur(0,0,1));
//System.out.println(pX1);
//System.out.println(pY1);
//System.out.println(pZ1);

//Express the turned frame in the camera frame:
Vecteur pX2=pX1.changeCoordinates(ccdx,ccdy,ccdz);
Vecteur pY2=pY1.changeCoordinates(ccdx,ccdy,ccdz);
Vecteur pZ2=pZ1.changeCoordinates(ccdx,ccdy,ccdz);

//change the camera frame
ccdx.affect(pX2);
ccdy.affect(pY2);
ccdz.affect(pZ2);
//System.out.println(pX);
//System.out.println(pY);
//System.out.println(pZ);
//set the ccd center from the lens position:
Vecteur lens=new Vecteur(lensx,lensy,lensz);
ccd0.affect(lens.addn(ccdz.scmul(-focal)));
}


}
