package com.photonlyx.toolbox.threeD.gui;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Spliterator;
import java.util.function.Consumer;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.PolyLine3D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.rendering.Sun;

/**
 *  instance of Object3DColor interface
 *  */

public class Object3DColorInstance implements Object3DColor
{
	private Color color;
	private Object3D o;


	public Object3DColorInstance(Object3D o,Color c)
	{
		super();
		this.o=o;
		this.color=c;
	}

	public void setColor(Color c)
	{
		color=c;
	}

	public Object3D getObject3D() {
		return o;
	}

	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
	{
		o.checkOccupyingCube(cornerMin, cornerMax);
	}


	public void draw(Graphics g, Projector proj)
	{
		g.setColor(color);
		o.draw(g,proj);
	}

	public void draw(Graphics g, Projector proj, Sun sun)
	{
		this.draw(g,proj);
	}

	public double getDistanceToScreen(Projector proj)
	{
		return 	o.getDistanceToScreen(proj);
	}


	@Override
	public boolean isOn(Point p, Projector proj)
	{
		// TODO Auto-generated method stub
		return false;
	}




}
