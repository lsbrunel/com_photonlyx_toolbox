package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;

import com.photonlyx.toolbox.math.geometry.Vecteur;



public class Circle3DColor extends PolyLine3DColor
{
private double r=1;
private Vecteur centre=new Vecteur();
private Vecteur axis=new Vecteur(0,0,1);
private int fn=10;//nb of segments


public Circle3DColor()
{
super();
init();
}

/**
 * 
 * @param r radius
 * @param centre centre of the circle
 * @param axis normalized vector perpendicular to the circle plane
 * @param fn number of segments
 */
public Circle3DColor(double r, Vecteur centre, Vecteur axis,int fn,Color c)
{
super();
this.r = r;
this.centre = centre;
this.axis = axis;
this.fn=fn;
setColor(c);
init();
}

public Circle3DColor(double r, Vecteur centre, Vecteur axis,int fn,Color c,boolean seePoints)
{
super();
this.r = r;
this.centre = centre;
this.axis = axis;
this.fn=fn;
setColor(c);
this.setSeePoints(seePoints);
init();
}


private void init()
{
//find a vector perpendicular to axis
Vecteur v2=new Vecteur(1,0,0).vectmul(axis);
if (v2.norme()==0) v2=new Vecteur(0,1,0).vectmul(axis);
double da=Math.PI*2/fn;
v2.scmul(1/v2.norme());
Vecteur v1=v2.vectmul(axis);
for (int i=0;i<=fn;i++)
	{
	Vecteur p=v1.scmul(r*Math.cos(i*da)).addn(v2.scmul(r*Math.sin(i*da)));
	this.getPolyLine3D().addPoint(p.addn(centre));
	}	
}



}
