package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.rendering.Sun;

public class Triangle3DColor extends Triangle3D implements Facet,Object3DColor
{
	private Color color;
	private boolean filled=false; // if false paint only the frame

	public Triangle3DColor()
	{
		super();
		for (int i=0;i<3;i++) p[i]=new Vecteur();	
	}

	public Triangle3DColor(Vecteur _p1,Vecteur _p2,Vecteur _p3)
	{
		super(_p1,_p2,_p3);
		color=color.black;
	}

	public Triangle3DColor(Vecteur _p1,Vecteur _p2,Vecteur _p3,int colorRGB)
	{
		super(_p1,_p2,_p3);
		color=new Color(colorRGB);
	}

	public Triangle3DColor(Vecteur _p1,Vecteur _p2,Vecteur _p3,Color _color)
	{
		super(_p1,_p2,_p3);
		color=_color;
	}
	public Triangle3DColor(Vecteur _p1,Vecteur _p2,Vecteur _p3,Color _color,boolean _filled)
	{
		super(_p1,_p2,_p3);
		color=_color;
		filled=_filled;
	}

	public Triangle3DColor(Triangle3D t,Color _color,boolean _filled)
	{
		//p=new Vecteur[4];
		this.p[0]=t.p1();	
		this.p[1]=t.p2();	
		this.p[2]=t.p3();	
		color=_color;
		filled=_filled;
	}

	public Triangle3DColor(Triangle3D t,Color _color)
	{
		//p=new Vecteur[4];
		this.p[0]=t.p1();	
		this.p[1]=t.p2();	
		this.p[2]=t.p3();	
		color=_color;
	}


	public Vecteur p1(){return p[0];}
	public Vecteur p2(){return p[1];}
	public Vecteur p3(){return p[2];}



	/**if one of the is out of the cube, extends the cube*/
	public void checkOccupyingCube(Vecteur cornerMin,Vecteur cornerMax)
	{
		for (int j=0;j<3;j++)
		{
			for (int k=0;k<3;k++) 
			{
				double r=p[j].coord(k);
				if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
				if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
			}	
		}
	}



	public void draw(Graphics g,Projector proj)
	{
		g.setColor(color);
		super.draw(g,proj);
	}


	public void draw(Graphics g, Projector proj, Sun sun)
	{
		double[][] cooScreen=new double[3][];
		Polygon pol=new Polygon();
		//System.out.println("quad filled="+filled+" sun="+sun);
		for (int j=0;j<3;j++) 
		{
			cooScreen[j]=proj.calcCoorEcran(p[j]);
			pol.addPoint((int)cooScreen[j][0],(int)cooScreen[j][1]);
		}

		if ((sun!=null)&&(filled))
		{
			//Triangle3DColor t=new Triangle3DColor(p[0],p[1],p[2]);
			//	Vecteur v=sun.getIl();
			//	Vecteur n=t.getNormal();
			//cos of illumination vector:
			this.calcNormal();
			float cos_n_il=(float) this.getNormal().mul(sun.getIl().scmul(-1));
			if (cos_n_il<0) cos_n_il=0;
			//System.out.println(p[0]+"   "+n+" cos:"+cos_n_il);
			float[] cf=color.getComponents(null);
			Color c=new Color(cos_n_il*cf[0],cos_n_il*cf[1],cos_n_il*cf[2]);
			//	Color c=new Color((float)(cos_n_il*color.getRed()/255.0),(float)(cos_n_il*color.getGreen()/255.0),(float)(cos_n_il*color.getBlue()/255.0));
			//float[] hsb=Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
			//Color c=Color.getHSBColor(hsb[0],hsb[1],(float)(hsb[2]*cos_n_il));
			//Color c=new Color(cos_n_il,cos_n_il,cos_n_il);
			g.setColor(c);
			g.fillPolygon(pol);
		}
		else
		{
			//	if (filled) 
			//		{
			//		g.setColor(color);
			//		g.fillPolygon(pol);
			//		}
			//	else
			for (int j=0;j<3;j++) 
			{
				int k=j+1;
				if (k==3) k=0;
				{
					g.setColor(color);
					//g.setColor(Color.black);
					g.drawLine((int)cooScreen[j][0],(int) cooScreen[j][1], (int)cooScreen[k][0], (int)cooScreen[k][1]);
				}
				//g.drawPolygon(pol);
			}
		}
	}

	@Override
	public void setFilled(boolean b)
	{
		filled=b;

	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public boolean isFilled() {
		return filled;
	}


}
