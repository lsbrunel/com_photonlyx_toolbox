package com.photonlyx.toolbox.threeD.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.ProjectorCamera;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.rendering.Sun;


public class Camera3DView implements Object3DColor
{
private ProjectorCamera cam;

public Camera3DView(ProjectorCamera cam)
{
this.cam=cam;	
}

@Override
public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax)
{
cam.checkOccupyingCube(cornerMin, cornerMax);	
}

@Override
public void draw(Graphics g, Projector proj)
{
cam.draw(g, proj);	
}
public void draw(Graphics g, Projector proj, Sun sun)
{
	this.draw(g,proj);
}

@Override
public double getDistanceToScreen(Projector proj)
{
return cam.getDistanceToScreen(proj);
}

@Override
public boolean isOn(Point p, Projector proj)
{
// TODO Auto-generated method stub
return false;
}

@Override
public void setColor(Color c) {
	// TODO Auto-generated method stub
	
}



}
