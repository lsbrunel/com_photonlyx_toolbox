package com.photonlyx.toolbox.threeD.gui;


import java.awt.Color;
import java.awt.Graphics;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.threeD.rendering.Sun;



public interface Object3DColor extends Object3D
{

	public void setColor(Color c);
	public void draw(Graphics g, Projector proj, Sun sun);

}
