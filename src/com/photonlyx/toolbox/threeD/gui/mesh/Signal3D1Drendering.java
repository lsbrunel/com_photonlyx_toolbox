package com.photonlyx.toolbox.threeD.gui.mesh;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import com.photonlyx.toolbox.math.function.Function1D1D;
import com.photonlyx.toolbox.math.function.usual.Line;
import com.photonlyx.toolbox.math.function.usual.Ln;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal3D1D;
import com.photonlyx.toolbox.threeD.gui.Object3DColor;
import com.photonlyx.toolbox.threeD.gui.VoxelColor;
import com.photonlyx.toolbox.threeD.rendering.Sun;

public class Signal3D1Drendering  implements Object3DColor
{
	private Signal3D1D sig;
	private Function1D1D lut;
	private boolean autoLUT=true;
	private boolean log=false;
	private boolean transparent=true;
	private boolean voxels=false;
	private double vmin=0,vmax=1;
	private int dotSize=3;
	private int cutX=100,cutY=100,cutZ=100;
	private int alpha=0, rgb=0;
	private double xmin,xmax,ymin,ymax,zmin,zmax,sx,sy,sz,sigmin,sigmax;

	public	Signal3D1Drendering ()
	{
	}

	public	Signal3D1Drendering (Signal3D1D sig)
	{
		this.sig=sig;
		init();
	}

	private void init()
	{
		xmin=sig.xmin();
		ymin=sig.ymin();
		zmin=sig.zmin();
		xmax=sig.xmax();
		ymax=sig.ymax();
		zmax=sig.zmax();
		sx=((sig.xmax()-sig.xmin())/(sig.dimx()-1));
		sy=((sig.ymax()-sig.ymin())/(sig.dimy()-1));
		sz=((sig.zmax()-sig.zmin())/(sig.dimz()-1));
		sigmin=sig.minValue();
		sigmax=sig.maxValue();


	}



	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		if (xmin < cornerMin.coord(0)) cornerMin.coordAffect(0,xmin);
		if (xmax > cornerMax.coord(0)) cornerMax.coordAffect(0,xmax);
		if (ymin < cornerMin.coord(1)) cornerMin.coordAffect(1,ymin);
		if (ymax > cornerMax.coord(1)) cornerMax.coordAffect(1,ymax);
		if (zmin < cornerMin.coord(2)) cornerMin.coordAffect(2,zmin);
		if (zmax > cornerMax.coord(2)) cornerMax.coordAffect(2,zmax);
	}


	@Override
	public void draw(Graphics graphics, Projector proj) 
	{

		if (autoLUT)
		{
			vmin=sigmin;
			vmax=sigmax;
		}

		if (!log) 
		{
			double a=255/(vmax-vmin);
			double b=-vmin*a;
			lut=new Line(a,b);
		}
		else  
		{
			double l1=Math.log(vmin);
			double l2=Math.log(vmax);
			double a=255/(l2-l1);
			double b=-l1*a;
			lut=new Ln(a,b);
		}

		Color c=Color.red;
		Color c1=new Color(rgb);
		int r=c1.getRed();
		int g=c1.getGreen();
		int b=c1.getBlue();
		int v1;

		if (cutX>100)cutX=100;
		if (cutY>100)cutY=100;
		if (cutZ>100)cutZ=100;
		if (cutX<0)cutX=0;
		if (cutY<0)cutY=0;
		if (cutZ<0)cutZ=0;


		if (voxels)
		{
			//render as voxels
			for (int i=0;i<sig.dimx()-1;i++)
			{
				for (int j=0;j<sig.dimy()-1;j++)
				{
					for (int k=0;k<sig.dimz()-1;k++)
					{
						double v=sig.val(i,j,k);
						if (v==0) continue;
						v1=(int)lut.f(v);
						if (v1<0.00) v1=0;
						if (v1>=255) v1=255;
						//c=new Color(v1,v1,v1,alpha);
						//System.out.println(i+" "+j+" "+k+" v="+v+" v1="+v1);
						c=new Color(r,g,b,v1);

						Vecteur p1=new Vecteur(xmin+sx*i,ymin+sy*j,zmin+sz*k);
						Vecteur p2=new Vecteur(xmin+sx*(i+1),ymin+sy*(j+1),zmin+sz*(k+1));
						VoxelColor vc=new VoxelColor(null,p1,p2,c);
						vc.draw(graphics, proj);
					}
				}
			}
		}	
		else
		{
			//render as dots
			for (int i=0;i<sig.dimx()*cutX/100.0;i++)
			{
				for (int j=0;j<sig.dimy()*cutY/100.0;j++)
				{
					for (int k=0;k<sig.dimz()*cutZ/100.0;k++)
					{
						double v=sig.val(i,j,k);
						if (v==0) continue;
						v1=(int)lut.f(v);
						if (v1<0.00) v1=0;
						if (v1>=255) v1=255;
						//c=new Color(v1,v1,v1,alpha);
						//System.out.println(i+" "+j+" "+k+" v="+v+" v1="+v1);
						if (transparent) c=new Color(r,g,b,v1);
						else c=new Color(v1,v1,v1,255);

						Vecteur p=new Vecteur(xmin+sx*i,ymin+sy*j,zmin+sz*k);
						double[] cooScreen=proj.calcCoorEcran(p);
						graphics.setColor(c);
						graphics.fillRect((int)cooScreen[0]-dotSize/2, (int)cooScreen[1]-dotSize/2, dotSize, dotSize);
					}
				}
			}	
		}

		VoxelColor vc=new VoxelColor(null,
				new Vecteur(xmin,ymin,zmin),
				new Vecteur(xmax,ymax,zmax),
				Color.BLACK	);
		vc.draw(graphics, proj);


	}
	public void draw(Graphics g, Projector proj, Sun sun)
	{
		this.draw(g,proj);
	}

	@Override
	public double getDistanceToScreen(Projector proj) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		//		for (int i=0;i<sig.dimx();i++)
		//		{
		//			for (int j=0;j<sig.dimy();j++)
		//			{
		//				for (int k=0;k<sig.dimz();k++)
		//				{
		//					Vecteur v=new Vecteur(xmin+sx*i,ymin+sy*j,zmin+sz*k);
		//					double[] cooScreen=proj.calcCoorEcran(v);
		//					double d2=Math.pow(p.x-cooScreen[0],2)+Math.pow(p.y-cooScreen[1],2);
		//					if (d2<4) return true;		
		//				}
		//			}
		//		}
		return false;
	}

	@Override
	public void setColor(Color c) {
		// TODO Auto-generated method stub

	}

	public Signal3D1D getSig() {
		return sig;
	}

	public void setSig(Signal3D1D sig) {
		this.sig = sig;
		init();
	}

	public boolean isAutoLUT() {
		return autoLUT;
	}

	public void setAutoLUT(boolean autoLUT) {
		this.autoLUT = autoLUT;
	}

	public boolean isLog() {
		return log;
	}

	public void setLog(boolean log) {
		this.log = log;
	}

	public int getDotSize() {
		return dotSize;
	}

	public void setDotSize(int dotSize) {
		this.dotSize = dotSize;
	}

	public int getRgb() {
		return rgb;
	}

	public void setRgb(int rgb) {
		this.rgb = rgb;
	}

	public double getVmin() {
		return vmin;
	}

	public void setVmin(double vmin) {
		this.vmin = vmin;
	}

	public double getVmax() {
		return vmax;
	}

	public void setVmax(double vmax) {
		this.vmax = vmax;
	}



	public int getCutX() {
		return cutX;
	}

	public void setCutX(int cutX) {
		this.cutX = cutX;
	}

	public int getCutY() {
		return cutY;
	}

	public void setCutY(int cutY) {
		this.cutY = cutY;
	}

	public int getCutZ() {
		return cutZ;
	}

	public void setCutZ(int cutZ) {
		this.cutZ = cutZ;
	}

	public boolean isTransparent() {
		return transparent;
	}

	public void setTransparent(boolean transparent) {
		this.transparent = transparent;
	}




}
