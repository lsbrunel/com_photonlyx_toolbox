package com.photonlyx.toolbox.threeD.gui.mesh;

import java.awt.Graphics;
import java.awt.Point;
import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Projector;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.geometry.Vecteur2D;

public class Vertices extends Vector<Vecteur> implements Object3D
{
	private Vector<Vecteur2D>	screenCoord=new Vector<Vecteur2D>();
	private  int sizeOnScreen=2;

	public void allocateScreenCoords()
	{
		screenCoord.removeAllElements();
		for (Vecteur s:this) screenCoord.add(new Vecteur2D());
	}

	private void calcScreenCoord(Projector proj)
	{
		int c=0;
		for (Vecteur s:this) 
			{
			Vecteur2D segScreen=screenCoord.get(c++);
			proj.calcCoorScreen(segScreen, s);
			}	
	}
	
	@Override
	public void checkOccupyingCube(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (Vecteur s:this) 
				for (int k=0;k<3;k++)
				{
					double r=s.coord(k);
					if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
					if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
				}
	}

	@Override
	public void draw(Graphics g, Projector proj) 
	{
		if (screenCoord.size()==0) this.allocateScreenCoords();
		calcScreenCoord(proj);
		for (Vecteur2D s:screenCoord) 
			g.fillRect((int)(s.x()-sizeOnScreen/2),(int)( s.y()-sizeOnScreen/2), sizeOnScreen, sizeOnScreen);
	}

	@Override
	public double getDistanceToScreen(Projector proj) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isOn(Point p, Projector proj) 
	{
		for (Vecteur2D s:screenCoord)
		{
			double d2=Math.pow(p.x-s.x(),2)+Math.pow(p.y-s.y(),2);
			if (d2<4) return true;
		}
		return false;
	}

}
