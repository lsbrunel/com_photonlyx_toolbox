package com.photonlyx.toolbox.threeD.rendering;



import com.photonlyx.toolbox.math.analyse2D.Interpolation2D;
import com.photonlyx.toolbox.math.geometry.Rotator;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.signal.Signal2D1D;


public class View
{
private double specularHalfAngle=1;//degrees
private double specularReflectivity=0.004;
private Signal2D1D surfaceAsImage;//=new Signal2D1D(0,1,0,1,4,4);
private Interpolation2D surface;//=new Interpolation2D(surfaceAsImage);
private Signal2D1D imShadows=new Signal2D1D();//image of the shadows
private double xmin, xmax=1, ymin, ymax=1;
private int dimx=200,dimy=200;
private boolean specular=false;
private boolean subSurfScat=false;
private LightSource lightSource;



public View(Interpolation2D surface,LightSource lightSource,double xmin, double xmax, double ymin, double ymax, int dimx,int dimy)
{
super();
this.surface=surface;
this.xmin = xmin;
this.xmax = xmax;
this.ymin = ymin;
this.ymax = ymax;
this.dimx = dimx;
this.dimy = dimy;
this.lightSource = lightSource;
}

public View(Signal2D1D surfaceAsImage,LightSource lightSource,double xmin, double xmax, double ymin, double ymax, int dimx,int dimy)
{
super();
this.surfaceAsImage=surfaceAsImage;
this.xmin = xmin;
this.xmax = xmax;
this.ymin = ymin;
this.ymax = ymax;
this.dimx = dimx;
this.dimy = dimy;
this.lightSource = lightSource;
}

public View(Signal2D1D surfaceAsImage,LightSource lightSource)
{
super();
this.surfaceAsImage=surfaceAsImage;
this.xmin = surfaceAsImage.xmin();
this.xmax = surfaceAsImage.xmax();
this.ymin = surfaceAsImage.ymin();
this.ymax = surfaceAsImage.ymax();
this.dimx = surfaceAsImage.dimx();
this.dimy = surfaceAsImage.dimy();
this.lightSource = lightSource;
}

/**
 * create a view with a sun as ligth source
 * @param surfaceAsImage
 */
public View(Signal2D1D surfaceAsImage)
{
super();
this.surfaceAsImage=surfaceAsImage;
this.xmin = surfaceAsImage.xmin();
this.xmax = surfaceAsImage.xmax();
this.ymin = surfaceAsImage.ymin();
this.ymax = surfaceAsImage.ymax();
this.dimx = surfaceAsImage.dimx();
this.dimy = surfaceAsImage.dimy();
this.lightSource = new Sun(45,0);
}





/**
 * calc the shadowed view
 */
public void update()
{
//System.out.println("view update");
//simulate the shadows:
if (surfaceAsImage!=null)
	{
	surface=new Interpolation2D(surfaceAsImage);
	imShadows.init(surfaceAsImage.xmin(),surfaceAsImage.xmax(),surfaceAsImage.ymin(),surfaceAsImage.ymax(),surfaceAsImage.dimx(),surfaceAsImage.dimy());
	}

else imShadows.init(xmin,xmax,ymin,ymax,dimx,dimy);

//illumination direction:
//double norme=il.norme();

//viewing direction
Vecteur view=new Vecteur(0,0,1);
double cos_specularHalfAngle=Math.cos(specularHalfAngle*Math.PI/180.0);
//specular emission solid angle
double specular_sa=2*Math.PI*(1-cos_specularHalfAngle);

//il=new Vecteur(0,Math.cos(angleRad),Math.sin(angleRad));
Vecteur il;//illumination vector
double sx=(imShadows.xmax()-imShadows.xmin())/imShadows.dimx();
double sy=(imShadows.ymax()-imShadows.ymin())/imShadows.dimy();
for (int i=0;i<imShadows.dimx();i++)
	for (int j=0;j<imShadows.dimy();j++)
		{
		//System.out.println(i+" "+j);
		double[] p=new double[2];
		p[0]=imShadows.xmin()+i*sx;
		p[1]=imShadows.ymin()+j*sy;
		double der[]=surface.derivate(p);
		//local normal vector:
		Vecteur n=new Vecteur(-der[0],-der[1],1).getNormalised();
		
		//diffuse lambertian energy (independent of the viewing angle)
		il=lightSource.getIl(new Vecteur(p[0],p[1],surface.f(p)));
		double cos_n_il=n.mul(il.scmul(-1));
		if (cos_n_il<0) cos_n_il=0;
		//System.out.println(i+" "+j+" "+der[0]+" "+der[1]+" "+cos_n_il);
		double luminance=cos_n_il;
		
		//specular reflexion simulated by a cone :
		if (specular)
			{
			//calc the reflected ray:
			Vecteur reflectedRay=il.addn(n.scmul(2*cos_n_il));
			//if the reflected ray is angularly close to the view direction, add the specular luminance 
			if (reflectedRay.mul(view)>=cos_specularHalfAngle) 
				{
				luminance+=specularReflectivity/specular_sa*sx*sy;
				}
			}
		
		luminance*=lightSource.getSourcePower();
		//luminance=der[0];//debug
		imShadows.setValue(i, j, luminance);
		}

//if (subSurfScat)//convolve with the spot
//	{
//	DiffusionSpot ds=new DiffusionSpot(0.5,20);
//	for (int i=0;i<imShadows.dimx();i++)
//		for (int j=0;j<imShadows.dimy();j++)
//			{
//			
//			}

//	}





}


public double getSpecularHalfAngle()
{
return specularHalfAngle;
}



public void setSpecularHalfAngle(double specularHalfAngle)
{
this.specularHalfAngle = specularHalfAngle;
}



public double getSpecularReflectivity()
{
return specularReflectivity;
}



public void setSpecularReflectivity(double specularReflectivity)
{
this.specularReflectivity = specularReflectivity;
}

public Signal2D1D getViewImage()
{
return imShadows;
}



public double getXmin()
{
return xmin;
}



public void setXmin(double xmin)
{
this.xmin = xmin;
}



public double getXmax()
{
return xmax;
}



public void setXmax(double xmax)
{
this.xmax = xmax;
}



public double getYmin()
{
return ymin;
}



public void setYmin(double ymin)
{
this.ymin = ymin;
}



public double getYmax()
{
return ymax;
}



public void setYmax(double ymax)
{
this.ymax = ymax;
}



public int getDimx()
{
return dimx;
}



public void setDimx(int dimx)
{
this.dimx = dimx;
}



public int getDimy()
{
return dimy;
}



public void setDimy(int dimy)
{
this.dimy = dimy;
}



public boolean isSpecular()
{
return specular;
}



public void setSpecular(boolean specular)
{
this.specular = specular;
}



public boolean isSubSurfScat()
{
return subSurfScat;
}



public void setSubSurfScat(boolean subSurfScat)
{
this.subSurfScat = subSurfScat;
}



public Interpolation2D getSurface()
{
return surface;
}




public LightSource getLightSource()
{
return lightSource;
}




public void setLightSource(LightSource lightSource)
{
this.lightSource = lightSource;
}


}
