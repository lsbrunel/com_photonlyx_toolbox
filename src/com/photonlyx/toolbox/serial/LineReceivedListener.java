package com.photonlyx.toolbox.serial;

public interface LineReceivedListener 
{
	public  void actionWhenReceivingALine(String line);

}
