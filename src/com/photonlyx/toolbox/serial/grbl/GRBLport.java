package com.photonlyx.toolbox.serial.grbl;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;
import java.util.Locale;
import java.util.concurrent.BlockingQueue;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.photonlyx.toolbox.util.Global;

/**
 * @deprecated
 * @author laurent
 *
 */
public class GRBLport 
{
	private int bauds = 115200;
	public int[] baudsOptions = {9600,115200};
	private SerialPort port=null;
	private boolean connected=false;
	private GRBLlistener spl;
	private double[] pos=new double[3];
	private static DecimalFormatSymbols dfs=new DecimalFormatSymbols(Locale.ENGLISH);
	private static DecimalFormat df3=new DecimalFormat("0.000",dfs);
	
	
	
	public boolean connectPort()
	{
		SerialPort[] comPorts = SerialPort.getCommPorts();
		for (SerialPort p:comPorts)
		{
			System.out.println("see "+p.getSystemPortName());
			if ((p.getSystemPortName().contains("ttyACM")) 
					|| (p.getSystemPortName().contains("ttyUSB"))
					|| (p.getSystemPortName().contains("COM"))
			) port=p;
		}
		if (port!=null)
		{
			port.setBaudRate(bauds);
			//https://github.com/Fazecast/jSerialComm/wiki/Modes-of-Operation 
			//p.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 100, 0);
			port.openPort();

			spl=new GRBLlistener();
			port.addDataListener(spl);
			
			
			System.out.println("Connected to "+port.getSystemPortName());
			connected=true;
			return true;
		}
		return false;
	}


	public void closePort()
	{
		try
		{
			if (port!=null) port.closePort();
			port=null;
			Global.setMessage("Port closed");
		}
		catch(Exception e)
		{
			e.printStackTrace();	
		}
		connected=false;
	}


	public void setIncrementalMode()
	{
		send("G91\n");
	}

	public void setAbsoluteMode()
	{
		send("G90\n");
	}

	/**
	 * set the (0,0,0) position at this place
	 */
	public void setOriginHere()
	{
		send("G92\n");
	}

	public void send(String gcode)
	{
		//System.out.println("GRBLport send:"+gcode);
		try
		{
			sendLine(gcode);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();	
		}	

	}


	public SerialPort getSerialPort() {
		return port;
	}



	public boolean isConnected()
	{
		return connected;
	}



	public void setConnected(boolean connected)
	{
		this.connected = connected;
	}



	public Integer getBauds()
	{
		return bauds;
	}



	public void setBauds(Integer bauds)
	{
		this.bauds = bauds;
	}



/**
 * update position using ? command
 */
	public void updatePos()
	{
		this.send("?\r\n");
		try{ Thread.sleep(100);} catch(Exception e){;}
	}


	
/*
 * incremental
 */
	public void moveXYZ(double dx,double dy,double dz,double speed)
	{
		updatePos();
		System.out.println("pos: X"+df3.format(pos[0])+" Y"+df3.format(pos[1])+" Z"+df3.format(pos[2]));
		double newx=pos[0]+dx;
		double newy=pos[1]+dy;
		double newz=pos[2]+dz;
		String order="G1 "+"X"+df3.format(dx)+" Y"+df3.format(dy)+" Z"+df3.format(dz)+" F"+(int)speed+"\n";
		this.send(order+"\n");
		System.out.println("GRBLport moveXYZ goal: ("+newx+","+newy+","+newz+")");
		for(int i=0;i<200;i++)
		{
			try{ Thread.sleep(100);} catch(Exception e){;}
			updatePos();
			System.out.println("GRBLport moveXYZ   pos: X"+df3.format(pos[0])+" Y"+df3.format(pos[1])+" Z"+df3.format(pos[2]));
			double diff=Math.abs(newx-pos[0])+Math.abs(newy-pos[1])+Math.abs(newz-pos[2]);
			System.out.println("GRBLport moveXYZ   diff="+diff);
			{
				if (diff<0.001) 
					{
					System.out.println("GRBLport moveXYZ  Position reached!");
					break;
					}
			}
		}
		
	}
	
	
	
	
	/**
	 * 
	 * @return -1 error   1 is running    0 is stopped
	 */
	public int isMoving()
	{
		this.send("?\n");
		try{ Thread.sleep(100);} catch(Exception e){;}
		String s=spl.getGrbl_status();
		System.out.println("status: "+s);
		if (s!=null) 
		{
			if (s.charAt(1)=='R') return 1;else return 0;
		}
		else return -1;
	}

	/**
	 * update pos with x and y found in string
	 * @param s
	 * @param pos
	 */
	public static  void decodeGrblStatus(String s,double[] pos)
	{
		int index1=s.indexOf(':');
		int index2=s.indexOf(',',index1);
		pos[0]=Double.parseDouble(s.substring(index1+1,index2));
		int index3=s.indexOf(',',index2+1);
		//System.out.println("index1="+index1);
		//System.out.println("index2="+index2);
		//System.out.println("index3="+index3);
		pos[1]=Double.parseDouble(s.substring(index2+1,index3));
		int index4=s.indexOf(',',index3+1);
		pos[2]=Double.parseDouble(s.substring(index3+1,index4));
		System.out.println("******"+s+"   "+"("+pos[0]+","+pos[1]+","+pos[2]+")");
	}

	public void sendLine(String s) 
	{
		if (port==null) return;
		System.out.println("Send: " + s);
		try 
		{
			byte[] bytes=s.getBytes();
			port.writeBytes(bytes,bytes.length);
		} catch (Exception e) 
		{
			System.out.println("could not write to port");
		}
	}




	private  class GrblSerialSender {
		SerialPort port = null;
		Integer currentBufferSize = null;
		Integer maxBufferSize = null;

		BlockingQueue<Integer> completedCommands = null;
		LinkedList<Integer> activeCommandSizes;

		private GrblSerialSender(SerialPort sp, BlockingQueue<Integer> completedCommands, Integer activeBufferSize) {
			port = sp;
			maxBufferSize = activeBufferSize;
			currentBufferSize = 0;
			this.completedCommands = completedCommands;
			activeCommandSizes = new LinkedList<Integer>();
		}

		private void sendFile(File file) throws IOException {
			// Java 8
			//try(Stream<String> lines = Files.lines(Paths.get(file.getAbsolutePath()))){
			//    lines.forEach(s -> sendLine(s));
			//}

			//count the nb of lines
			int c=0;
			BufferedReader br2 = new BufferedReader(new FileReader(file));
			while (br2.readLine() != null) c++;
			br2.close();
			int nbLines=c;

			c=0;
			BufferedReader br = null;
			try {
				String sCurrentLine;
				br = new BufferedReader(new FileReader(file));
				while ((sCurrentLine = br.readLine()) != null) 
				{
					Global.setMessage("Send line "+(c)+"/"+nbLines+" ("+(int)((double)c/(double)nbLines*100)+"%)  "+sCurrentLine);
					sendLine(sCurrentLine);
					c++;
					//if (stopped) break;
				}
			} finally {
				if (br != null)br.close();
			}

			Global.setMessage("Done");   

		}
		


		private void sendLine(String s) {
			// Wait until there is room, if necessary.
			while (maxBufferSize < (currentBufferSize + s.length() + 1)) {
				try {
					//System.out.println("waiting for room.... active command count: " + this.activeCommandSizes.size());

					// Wait for a command to complete
					completedCommands.take();
					currentBufferSize -= this.activeCommandSizes.removeFirst();
				} catch (InterruptedException ex) {
					ex.printStackTrace();
					return;
				}
			}

			try {
				//System.out.println("Sending command: " + s.trim());
				//System.out.printf(".");
				byte[] buffer=(s.trim() + "\n").getBytes();
				port.writeBytes(buffer, buffer.length);
				int commandSize = s.length() + 1;
				activeCommandSizes.add(commandSize);
				currentBufferSize  += commandSize;
			} catch (Exception ex) {
				ex.printStackTrace();
				ex.printStackTrace();
				return;
			}
		}
	}








public String getGrbl_status() {
	return spl.getGrbl_status();
};


class	GRBLlistener implements SerialPortDataListener
{
	private String grbl_status;
	private StringBuilder inputBuffer=new StringBuilder();
	private BlockingQueue<Integer> completedCommands;
	
	
	public GRBLlistener()
	{
	}
	
	@Override
	public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_AVAILABLE; }
	@Override
	public void serialEvent(SerialPortEvent event)
	{
		if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)return;
		byte[] newData = new byte[port.bytesAvailable()];
		int numRead = port.readBytes(newData, newData.length);
		
		System.out.println("Read " + numRead + " bytes.");
		String s=new String(newData);System.out.println("GRBLlistener "+s);
		System.out.println(getClass()+" Received &&&: " + s);

		for (byte b: newData) 
		{
			if (b == '\n') 
			{
				String s1= inputBuffer.toString();
				//System.out.println(getClass()+" Received &&&: " + s1);
				//if (s1.startsWith("ok") ||s1.startsWith("done") ||  s1.startsWith("error")) completedCommands.add(1);
				if (s1.charAt(0)=='<') 
				{
					grbl_status=s1;
					GRBLport.decodeGrblStatus(grbl_status,pos);
					//Global.setMessage("x="+pos[0]+" y="+pos[1]);
				}
				inputBuffer.setLength(0);
			} 
			else 
			{
				inputBuffer.append((char)b);
				//System.out.println("inputBuffer="+inputBuffer);
			}
		}                

	}
	public String getGrbl_status()
	{
		return grbl_status;
	}

}




public double[] getPos() 
{
	return pos;
}



}
