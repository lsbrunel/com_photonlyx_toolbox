package com.photonlyx.toolbox.serial;

import java.util.Vector;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.photonlyx.toolbox.util.Global;

public  class Device 
{
	private int bauds = 115200;
	public int[] baudsOptions = {9600,115200};
	private SerialPort port=null;
	private boolean connected=false;
	private DeviceListener spl=new DeviceListener();
	private String identity;
	private Vector<LineReceivedListener> lineReceivedListeners= new Vector<LineReceivedListener>();

	/**
	 * connect the present port
	 */
	public boolean connectOnePort()
	{
		connected=false;
		SerialPort[] comPorts = SerialPort.getCommPorts();
		for (SerialPort p:comPorts)
		{
			System.out.println("Check port "+p.getSystemPortName());
		}
		//if (comPorts.length>0)	
		port=null;
		for (SerialPort port1:comPorts)
		{
			//port=comPorts[0];
			if (port1.getSystemPortName().contains("ttyS0")) continue;
			port1.setBaudRate(bauds);
			port1.openPort();
			port1.addDataListener(spl);
			port=port1;
			connected=true;
		}
		//if (port==null) return false;
		if (port!=null) 
		{
			if (port.isOpen()) 
			{
				System.out.println("Connected to port "+port.getDescriptivePortName()+" "+port.getSystemPortName());
				Global.setMessage("Connected to port "+port.getDescriptivePortName()+" "+port.getSystemPortName());
				return true;
			}
			else 
			{
				System.out.println("Port "+port.getSystemPortName()+" already used");
				Global.setMessage("Port "+port.getSystemPortName()+" already used");
				return false;
			}
		}
		else 
		{
			System.out.println("Can't see any serial port");
			Global.setMessage("Can't see any serial port");
			return false;
		}
	}

	/**
	 * connect the present port
	 */
	public boolean connectOnePort(String portName)
	{
		connected=false;
		SerialPort[] comPorts = SerialPort.getCommPorts();
		SerialPort port1=null;
		for (SerialPort p:comPorts)
		{
			System.out.println("Check port "+p.getSystemPortName());
			if (p.getSystemPortName().contains(portName)) port1=p;
		}
		port=null;

		if (port1!=null) 
		{
			port1.setBaudRate(bauds);
			port1.openPort();
			port1.addDataListener(spl);
			port=port1;
			connected=true;
			if (port.isOpen()) 
			{
				System.out.println("Connected to port "+port.getDescriptivePortName()+" "+port.getSystemPortName());
				Global.setMessage("Connected to port "+port.getDescriptivePortName()+" "+port.getSystemPortName());
				return true;
			}
			else 
			{
				System.out.println("Port "+port.getSystemPortName()+" already used");
				Global.setMessage("Port "+port.getSystemPortName()+" already used");
				return false;
			}
		}
		else 
		{
			System.out.println("Can't see any serial port with name containing "+portName);
			Global.setMessage("Can't see any serial port with name containing "+portName);
			return false;
		}
	}
	public void connectPort(String identity_)
	{
		SerialPort[] comPorts = SerialPort.getCommPorts();
		for (SerialPort p:comPorts)
		{
			p.setBaudRate(bauds);
			//https://github.com/Fazecast/jSerialComm/wiki/Modes-of-Operation 
			//p.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 100, 0);
			p.openPort();
			p.addDataListener(spl);
			port=p;
			connected=true;
			try {Thread.sleep(2000);} catch (Exception e) {};

			//check if it is the expected instrument:
			System.out.print("Ask identity of  "+p.getSystemPortName()+" ... ");
			Global.setMessage("Ask identity of  "+p.getSystemPortName()+" ... ");
			send("w\n");
			try {Thread.sleep(200);} catch (Exception e) {};
			//System.out.println(p.getSystemPortName()+" received : \""+spl.getLastReceivedLine()+"\"");
			String received=spl.getLastReceivedLine();
			System.out.println(" received: *"+received+"*");
			Global.setMessage(" received: "+received);
			if (received.contains(identity_)) 
			{
				//this is the correct port, leave
				this.identity=received.replaceAll("\n","");
				break;
			}
			else 
			{
				//this is not the correct port, close and try other ports
				connected=false;
				p.closePort();
				port=null;
			}			
		}
		if (port!=null) 
			{
			System.out.println("Device "+identity+" is connected to port "+port.getSystemPortName());
			Global.setMessage("Device "+identity+" is connected to port "+port.getSystemPortName());
			}
		else 
			{
			System.out.println("Can't see any serial port with identity "+identity_);
			Global.setMessage("Can't see any serial port with identity "+identity_);
			}
	}

	public void addLineReceivedListener(LineReceivedListener lrl)
	{
		lineReceivedListeners.add(lrl);
	}

	public void writeData(String s) 
	{
		send(s);
	}

	public void send(String s) 
	{
		if (port==null) return;
		//System.out.println("Send: " + s);
		try 
		{
			byte[] bytes=s.getBytes();
			port.writeBytes(bytes,bytes.length);
		} catch (Exception e) 
		{
			System.out.println("could not write to port");
		}
	}



	public void writeDataAndWaitForAknowledgment(String message) 
	{
		sendAndWaitForAknowledgment(message) ;
	}


	public void sendAndWaitForAknowledgment(String message) 
	{
		this.sendAndWaitForAknowledgment(message, "done");
	}

	public void sendAndWaitForAknowledgment(String message,String aknowledgment) 
	{
		this.getDeviceListener().eraseLastReceivedLine();
		try 
		{
			//System.out.println("SendAndWaitDone ->"+message);
			send(message);		
			//try{ Thread.sleep(100);} catch(Exception e){;}
			for (;;)
			{
				String l=this.getDeviceListener().getLastReceivedLine();
				//System.out.println("SendAndWaitDone ->lastReceivedLine="+l);
				if (l.startsWith(aknowledgment)) break;
				try{ Thread.sleep(100);} catch(Exception e){;}
			}

		} 
		catch (Exception e) 
		{
			System.out.println(identity+": could not write to port");
		}
	}



	public boolean isConnected()
	{
		return connected;
	}

	public boolean isAvailable()
	{
		return connected;
	}

	public DeviceListener getDeviceListener()
	{
		return spl;
	}



	public String getIdentity() {
		return identity;
	}



	class	DeviceListener implements SerialPortDataListener
	{
		private String lastReceivedLine="";

		private StringBuffer line=new StringBuffer();

		@Override
		public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_AVAILABLE; }

		public void eraseLastReceivedLine() 
		{
			lastReceivedLine="";
		}

		@Override
		public void serialEvent(SerialPortEvent event)
		{
			if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)return;
			byte[] newData = new byte[port.bytesAvailable()];
			int numRead = port.readBytes(newData, newData.length);
			//			System.out.println("Read " + numRead + " bytes.");
			//			String s=new String(newData);
			//			System.out.println(new String(newData));

			for (byte b: newData) 
			{
				if (b == '\n') 
				{
					lastReceivedLine= line.toString();
					//System.out.println(getClass()+" Received &&&: " + line);
					//if (s1.startsWith("ok") ||s1.startsWith("done") ||  s1.startsWith("error")) completedCommands.add(1);
					line.setLength(0);
					for (LineReceivedListener lrl:lineReceivedListeners) lrl.actionWhenReceivingALine(lastReceivedLine);
				} 
				else 
				{
					line.append((char)b);
					//System.out.println("inputBuffer="+inputBuffer);
				}
			}      

		}
		public String getLastReceivedLine()
		{
			return lastReceivedLine.toString();
		}

	}



	public void closePort() 
	{
		String s=port.getDescriptivePortName()+" "+port.getSystemPortName();
		port.closePort();
		connected=false;
		System.out.println("Port "+s+" closed");
	}



	public static void main(String[] args)
	{
		Device d=new Device();

		//		d.connectOnePort();
		//		try {Thread.sleep(2000);} catch (Exception e) {};
		//		d.send("w\n");
		//		try {Thread.sleep(5000);} catch (Exception e) {};
		//		d.closePort();

		//d.connectPort("lyxS_spheres");
		d.connectOnePort();
	}

}
