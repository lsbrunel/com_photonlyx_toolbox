package com.photonlyx.toolbox.serial.gui;

import java.util.Date;

import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.gui.ColorList;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.serial.Device;
import com.photonlyx.toolbox.serial.LineReceivedListener;
import com.photonlyx.toolbox.txt.Definition;

public class SerialPlotterApp extends WindowApp implements LineReceivedListener
{
	private Device d;
	private ChartPanel cp=new ChartPanel();
	private Date date0=new Date();
	private int previousn=-1;
	private Plot[] plots;

	public 	SerialPlotterApp()
	{
		super("com.photonlyx.toolbox.serial.gui.SerialPlotterApp");
		d=new Device();
		d.addLineReceivedListener(this);
		d.connectOnePort();

		cp.getChart().setXlabel("Time");
		cp.getChart().setYlabel("");
		cp.getChart().setXunit("s");
		cp.getChart().setFormatTickx("0");
		cp.getChart().setFormatTicky("0.000");
		cp.getSplitPane().setDividerLocation(800);
		cp.hidePanelSide();
		this.add(cp, "Chart");

	}

	public Device getDevice()
	{
		return d;
	}

	public void actionWhenReceivingALine(String line) 
	{
		//System.out.println(line);
		Definition def=new Definition(line);
		int n=def.dim();
		if (n!=previousn)
		{
			cp.getChart().removeAllPlots();
			plots=new Plot[n];
			for (int i=0;i<n;i++) plots[i]=new Plot();
			ColorList cl=new ColorList();
			for (Plot p:plots)
			{
				Signal1D1DXY sig=new Signal1D1DXY();
				p.setSignal(sig);
				p.setColor(cl.getNewColour());
				p.setDotSize(0);
				p.setLineThickness(3);
				cp.getChart().add(p);
			}
			previousn=n;
			cp.getChart().update();
		}
		int c=0;
		for (String s:def)
		{
			try
			{
				double v0= Double.parseDouble(s);
				double time=(new Date().getTime()-date0.getTime())/1000;
				Plot p=plots[c];
				Signal1D1DXY sig=(Signal1D1DXY)p.getSignal();
				sig.addPoint(time,v0);
			}
			catch(Exception e)
			{
				//System.out.println(s+" is not a number");
				//e.printStackTrace();
			}
			c++;
		}
		cp.getChart().update(false);

	}

	public static void main(String[] args) //throws Exception 
	{
		SerialPlotterApp app=new SerialPlotterApp();
	}

}
