package com.photonlyx.toolbox.triggering;

import java.lang.reflect.Method;

import com.photonlyx.toolbox.util.Messager;

public class Trigger 
{
private Object o;
private String methodName;



public Trigger(Object o, String methodName) {
	super();
	this.o = o;
	this.methodName = methodName;
}



public void trig()
{
if (o==null) return;
//System.out.println(getClass()+" trig :"+o.getClass()+" "+methodName);
Method[] methods=o.getClass().getMethods();
boolean done=false;
for (Method m:methods) 
	{
	String name=m.getName();
//		if (name.compareTo(methodName)==0)
//			{
//			System.out.println("Method "+name);
//			Class[] cls=m.getParameterTypes();
//			System.out.println("     Method args:");
//			for (Class cl:cls) System.out.print("      "+cl.getSimpleName()+" ");
//			System.out.println();
//			}
	if (
		(name.compareTo(methodName)==0)
		&
		(m.getParameterTypes().length==0)
		)
		{
		//System.out.println(getClass()+" Invoke Method "+name+" of "+o.getClass().getSimpleName());
		
		try 
			{
			m.invoke(o, (Object[])null);
			done=true;
			}
		catch(Exception e){e.printStackTrace();}
		}
	}
if (!done) Messager.messErr("Could not find method: "+methodName+" of "+o.getClass());
}

}
